(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-SG": "./node_modules/moment/locale/en-SG.js",
	"./en-SG.js": "./node_modules/moment/locale/en-SG.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.admin-auth-guard.service.ts":
/*!*************************************************!*\
  !*** ./src/app/app.admin-auth-guard.service.ts ***!
  \*************************************************/
/*! exports provided: AdminAuthGuardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminAuthGuardService", function() { return AdminAuthGuardService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_session_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.session-service */ "./src/app/app.session-service.ts");
/* harmony import */ var _model_EnumType__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./model/EnumType */ "./src/app/model/EnumType.ts");
/* harmony import */ var _shared_shared_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shared/shared.service */ "./src/app/shared/shared.service.ts");






var AdminAuthGuardService = /** @class */ (function () {
    function AdminAuthGuardService(router, _SessionService, sharedService) {
        this.router = router;
        this._SessionService = _SessionService;
        this.sharedService = sharedService;
    }
    AdminAuthGuardService.prototype.canActivate = function (route, state) {
        this.sharedService.isFullScreen = false;
        ;
        var url = state.url;
        var roles = (this._SessionService.getCookie("ROLES") == ""
            || this._SessionService.getCookie("ROLES") == null) ? [] : JSON.parse(JSON.stringify(this._SessionService.getCookie("ROLES")));
        if (roles.filter(function (n) { return n.role === _model_EnumType__WEBPACK_IMPORTED_MODULE_4__["EnumType"].SUPER_ADMIN; }).length > 0) {
            return true;
        }
        this.router.navigate(['']);
        return false;
    };
    AdminAuthGuardService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _app_session_service__WEBPACK_IMPORTED_MODULE_3__["SessionService"], _shared_shared_service__WEBPACK_IMPORTED_MODULE_5__["SharedService"]])
    ], AdminAuthGuardService);
    return AdminAuthGuardService;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div class=\"wrapper\" *ngIf=\"sessionData == null\">\r\n  Loading...\r\n</div> -->\r\n\r\n<div class=\"wrapper\">\r\n  <header class=\"main-header\" *ngIf=\"!sharedService.isFullScreen\">\r\n\r\n    <a href=\"#\" class=\"logo\">\r\n      <!-- mini logo for sidebar mini 50x50 pixels -->\r\n      <span class=\"logo-mini\">\r\n        <svg class=\"icon_logo\" shape-rendering=\"geometricPrecision\" class=\"user-image\" width=\"30\" height=\"30\"\r\n          style=\"fill: white; margin: 9px;\">\r\n          <use xlink:href=\"./assets/dist/img/icons.svg#icon_logo\"></use>\r\n        </svg>\r\n      </span>\r\n      <!-- logo for regular state and mobile devices -->\r\n      <span class=\"logo-lg\">\r\n        <svg class=\"icon_logo\" shape-rendering=\"geometricPrecision\" class=\"user-image\" width=\"50\" height=\"50\"\r\n          style=\"fill: white\">\r\n          <use xlink:href=\"./assets/dist/img/icons.svg#icon_logo\"></use>\r\n        </svg> <span style=\"position: absolute;margin: 0px 5px;font-weight: bold;\">DATA SYNC</span>\r\n      </span>\r\n    </a>\r\n\r\n    <!-- Header Navbar: style can be found in header.less -->\r\n    <nav class=\"navbar navbar-static-top\">\r\n      <!-- Sidebar toggle button-->\r\n      <a href=\"#\" class=\"sidebar-toggle\" data-toggle=\"push-menu\" role=\"button\" style=\"padding: 20px\">\r\n        <svg class=\"icon_update\" shape-rendering=\"geometricPrecision\" width=\"25\" height=\"25\" style=\"fill: #FFFFFF\">\r\n          <use xlink:href=\"./assets/dist/img/icons.svg#icon_menu\"></use>\r\n        </svg>\r\n      </a>\r\n      <div class=\"navbar-custom-menu\">\r\n        <!-- <ul class=\"nav navbar-nav\">\r\n          <li class=\"dropdown user user-menu\" *ngIf=\"LA_SessionData != ''\">\r\n            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">\r\n              <svg class=\"icon_user_profile\" shape-rendering=\"geometricPrecision\" class=\"user-image\" width=\"20\"\r\n                height=\"20\">\r\n                <use xlink:href=\"./assets/dist/img/icons.svg#icon_user_profile\"></use>\r\n              </svg>\r\n              <ngx-avatar name=\"Karnesh Kumar\"\r\n                [src]=\"'http://cedprod.corp.netapp.com/photos/' + LA_SessionData.uid + '.jpg'\" size=\"50\"\r\n                cornerRadius=\"5\" style=\"position: relative;top: -7px;right: 5px;float: left\">\r\n              </ngx-avatar>\r\n              <span class=\"label label-success\">Login As</span>\r\n              <span class=\"hidden-xs\">{{LA_SessionData.firstName + ' ' + LA_SessionData.lastName}}</span>\r\n            </a>\r\n            <ul class=\"dropdown-menu\">\r\n              <li class=\"user-body\">\r\n                <a (click)=\"LALogout()\">\r\n                  <svg class=\"icon_logout\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n                    <use xlink:href=\"./assets/dist/img/icons.svg#icon_logout\"></use>\r\n                  </svg>\r\n                  Log out\r\n                </a>\r\n              </li>\r\n            </ul>\r\n          </li>\r\n          <li class=\"dropdown user user-menu\" *ngIf=\"LA_SessionData == ''\">\r\n           \r\n            <ul class=\"dropdown-menu\">\r\n              <li class=\"user-body\">\r\n                <a (click)=\"Logout()\">\r\n                  <svg class=\"icon_logout\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n                    <use xlink:href=\"./assets/dist/img/icons.svg#icon_logout\"></use>\r\n                  </svg>\r\n                  Log out\r\n                </a>\r\n              </li>\r\n            </ul>\r\n          </li>\r\n          <li class=\"messages-menu\">\r\n            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" style=\"padding: 20px\">\r\n              <svg class=\"icon_update\" shape-rendering=\"geometricPrecision\" width=\"25\" height=\"25\" style=\"fill: #FFFFFF\">\r\n                <use xlink:href=\"./assets/dist/img/icons.svg#icon_settings\"></use>\r\n              </svg>\r\n            </a>\r\n          </li>\r\n        </ul> -->\r\n        <ul class=\"nav navbar-nav\">\r\n          <li class=\"dropdown user user-menu\" *ngIf=\"sharedService.laSession === ''\">\r\n            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">\r\n              <ngx-avatar name=\"{{loginData.firstName + ' ' + loginData.lastName}}\"\r\n                [src]=\"'http://cedprod.corp.netapp.com/photos/' + loginData.userId + '.jpg'\" size=\"35\" cornerRadius=\"5\"\r\n                class=\"profile-icon\">\r\n              </ngx-avatar>\r\n              <span class=\"hidden-xs\">{{loginData.firstName + ' ' + loginData.lastName}}</span>\r\n            </a>\r\n            <ul class=\"dropdown-menu\">\r\n              <li class=\"user-body\">\r\n                <a (click)=\"Logout()\">\r\n                  <svg class=\"icon_logout\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n                    <use xlink:href=\"./assets/dist/img/icons.svg#icon_logout\"></use>\r\n                  </svg>\r\n                  Log out\r\n                </a>\r\n              </li>\r\n            </ul>\r\n          </li>\r\n          <li class=\"dropdown user user-menu\" *ngIf=\"sharedService.laSession !== ''\">\r\n            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">\r\n              <small class=\"label bg-green\">LOGIN AS</small>\r\n              <ngx-avatar name=\"{{sharedService.laSession.firstName + ' ' + sharedService.laSession.lastName}}\"\r\n                [src]=\"'http://cedprod.corp.netapp.com/photos/' + sharedService.laSession.userId + '.jpg'\" size=\"35\"\r\n                cornerRadius=\"5\" class=\"profile-icon\">\r\n              </ngx-avatar>\r\n              <span\r\n                class=\"hidden-xs\">{{sharedService.laSession.firstName + ' ' + sharedService.laSession.lastName}}</span>\r\n            </a>\r\n            <ul class=\"dropdown-menu\">\r\n              <li class=\"user-body\">\r\n                <a (click)=\"laLogout()\">\r\n                  <svg class=\"icon_logout\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n                    <use xlink:href=\"./assets/dist/img/icons.svg#icon_logout\"></use>\r\n                  </svg>\r\n                  Log out\r\n                </a>\r\n              </li>\r\n            </ul>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </nav>\r\n  </header>\r\n  <!-- Left side column. contains the logo and sidebar -->\r\n  <aside class=\"main-sidebar\" *ngIf=\"!sharedService.isFullScreen\">\r\n    <!-- sidebar: style can be found in sidebar.less -->\r\n    <section class=\"sidebar\">\r\n      <!-- sidebar menu: : style can be found in sidebar.less -->\r\n      <ul class=\"sidebar-menu\" data-widget=\"tree\">\r\n\r\n\r\n        <li [routerLinkActive]=\"['active']\" *ngFor=\"let item of appMenu\" class=\"treeview\">\r\n          <a [routerLink]=\"[item.url]\" *ngIf=\"item.submenu.length == 0\">\r\n            <svg [attr.class]=\"item.icon\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n              <use [attr.xlink:href]=\"'./assets/dist/img/icons.svg#' + item.icon\"></use>\r\n            </svg> <span>&nbsp;{{item.name}}</span>\r\n\r\n          </a>\r\n          <a href=\"#\" *ngIf=\"item.submenu.length > 0\">\r\n            <svg [attr.class]=\"item.icon\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n              <use [attr.xlink:href]=\"'./assets/dist/img/icons.svg#' + item.icon\"></use>\r\n            </svg> <span>&nbsp;{{item.name}}</span>\r\n            <span class=\"pull-right-container\">\r\n              <i class=\"fa fa-angle-left pull-right\"></i>\r\n            </span>\r\n          </a>\r\n          <ul class=\"treeview-menu\" *ngIf=\"item.submenu.length > 0\">\r\n            <li *ngFor=\"let menuitem of item.submenu\" class=\"treeview\">\r\n              <a [routerLink]=\"[menuitem.url]\">\r\n                <svg [attr.class]=\"menuitem.icon\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n                  <use [attr.xlink:href]=\"'./assets/dist/img/icons.svg#' + menuitem.icon\"></use>\r\n                </svg> <span>&nbsp;{{menuitem.name}}</span>\r\n              </a>\r\n            </li>\r\n          </ul>\r\n        </li>\r\n        <li class=\"treeview\" style=\"display:none\">\r\n          <a href=\"#\">\r\n            <svg shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n              <use href=\"./assets/dist/img/icons.svg#icon_rules\"></use>\r\n            </svg> <span>Account Ownership</span> <span class=\"pull-right-container\">\r\n              <i class=\"fa fa-angle-left pull-right\"></i>\r\n            </span>\r\n          </a>\r\n        </li>\r\n      </ul>\r\n      <div class=\"sidebar-footer hidden-small\">\r\n        <a (click)=\"OpenSettings()\" *ngIf=\"sharedService.settingPermission\">\r\n          <span class=\"glyphicon glyphicon-cog\" aria-hidden=\"true\" style=\"color:#0067C5\"></span> Settings\r\n        </a>\r\n      </div>\r\n    </section>\r\n    <!-- /.sidebar -->\r\n  </aside>\r\n  <!-- Content Wrapper. Contains page content -->\r\n  <div [ngClass]=\"{'fullscreen': sharedService.isFullScreen}\" class=\"content-wrapper\" style=\"min-height: 617px;\">\r\n    <router-outlet></router-outlet>\r\n    <app-tour-guide></app-tour-guide>\r\n    <!-- /.content -->\r\n  </div>\r\n  <!-- /.content-wrapper -->\r\n  <!-- <footer class=\"main-footer\">\r\n    <strong>Copyright &copy; 2018&nbsp;<a href=\"https://netapp.com\" class=\"text-orange\">NetApp Auth</a>.</strong>\r\n    All rights reserved.\r\n  </footer> -->]\r\n</div>"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sidebar-footer {\n  bottom: 0;\n  clear: both;\n  display: block;\n  padding: 0px;\n  position: fixed;\n  width: 229px;\n  background: #2a3f54;\n  z-index: 999; }\n\n.sidebar-footer a {\n  padding: 12px 7px 7px 15px;\n  width: 100%;\n  font-size: 15px;\n  display: block;\n  float: left;\n  background: #f7f7f7;\n  cursor: pointer;\n  color: #333; }\n\n.sidebar-footer a:hover {\n  color: #0067c5; }\n\n.sidebar-footer a svg {\n  vertical-align: top; }\n\n.icon_home {\n  fill: #dc3c00; }\n\n.icon_leads {\n  fill: #f9b20a; }\n\n.icon_lead_count {\n  fill: #e95f20; }\n\n.icon_incumbent_partner {\n  fill: #0074c7; }\n\n.icon_eligible_partner {\n  fill: #dc4b3d; }\n\n.icon_unknown {\n  fill: #dc4b3d; }\n\n.icon_lead_catcher {\n  fill: #f4a529; }\n\n.icon_rules {\n  fill: #dc4b3d; }\n\n.icon_managing_rules {\n  fill: #507e19; }\n\n.icon_account_owner {\n  fill: #0067c5; }\n\n.fullscreen {\n  margin-left: 0px;\n  padding-top: 0px !important; }\n\n.profile-icon {\n  top: -7px;\n  right: 5px;\n  float: left;\n  position: relative; }\n\n.event-message {\n  width: 450px;\n  font-size: 15px;\n  border-radius: 5px;\n  margin: 5px 0px;\n  padding: 2px 5px;\n  text-align: justify;\n  margin-right: 500px;\n  border: solid 1px #0067c5;\n  color: white;\n  cursor: pointer; }\n\n#loading-wrapper {\n  background-color: rgba(215, 215, 215, 0.3);\n  height: 100%;\n  position: fixed;\n  width: 100%;\n  z-index: 19999;\n  /* PULSE BUBBLES */ }\n\n#loading-wrapper #loading-message {\n    color: #555;\n    font-weight: bold;\n    margin-top: 5px;\n    text-align: center; }\n\n@-webkit-keyframes pulse {\n  from {\n    opacity: 1;\n    -webkit-transform: scale(1);\n            transform: scale(1); }\n  to {\n    opacity: 0.25;\n    -webkit-transform: scale(0.75);\n            transform: scale(0.75); } }\n\n@keyframes pulse {\n  from {\n    opacity: 1;\n    -webkit-transform: scale(1);\n            transform: scale(1); }\n  to {\n    opacity: 0.25;\n    -webkit-transform: scale(0.75);\n            transform: scale(0.75); } }\n\n#loading-wrapper #loading.spinner-box {\n    margin-top: 22%;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    background-color: transparent; }\n\n#loading-wrapper .pulse-container {\n    width: 60px;\n    display: flex;\n    justify-content: space-between;\n    align-items: center; }\n\n#loading-wrapper .pulse-bubble {\n    width: 15px;\n    height: 15px;\n    border-radius: 50%;\n    background-color: #0070b1; }\n\n#loading-wrapper .pulse-bubble-1 {\n    -webkit-animation: pulse 0.4s ease 0s infinite alternate;\n            animation: pulse 0.4s ease 0s infinite alternate; }\n\n#loading-wrapper .pulse-bubble-2 {\n    -webkit-animation: pulse 0.4s ease 0.2s infinite alternate;\n            animation: pulse 0.4s ease 0.2s infinite alternate; }\n\n#loading-wrapper .pulse-bubble-3 {\n    -webkit-animation: pulse 0.4s ease 0.4s infinite alternate;\n            animation: pulse 0.4s ease 0.4s infinite alternate; }\n\n.app-name {\n  font-size: 16px;\n  margin: 0;\n  float: left;\n  padding: 16px 0px;\n  color: white; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9FOlxcVUktUHJvamVjdFxcRVRMXFxzcmNcXHNyYy9hcHBcXGFwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFNBQVM7RUFDVCxXQUFXO0VBQ1gsY0FBYztFQUNkLFlBQVk7RUFDWixlQUFlO0VBQ2YsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7O0FBRWQ7RUFDRSwwQkFBMEI7RUFDMUIsV0FBVztFQUNYLGVBQWU7RUFDZixjQUFjO0VBQ2QsV0FBVztFQUNYLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YsV0FBVyxFQUFBOztBQUViO0VBQ0UsY0FBYyxFQUFBOztBQUVoQjtFQUNFLG1CQUFtQixFQUFBOztBQUVyQjtFQUNFLGFBQWEsRUFBQTs7QUFFZjtFQUNFLGFBQWEsRUFBQTs7QUFFZjtFQUNFLGFBQWEsRUFBQTs7QUFFZjtFQUNFLGFBQWEsRUFBQTs7QUFFZjtFQUNFLGFBQWEsRUFBQTs7QUFFZjtFQUNFLGFBQWEsRUFBQTs7QUFFZjtFQUNFLGFBQWEsRUFBQTs7QUFFZjtFQUNFLGFBQWEsRUFBQTs7QUFFZjtFQUNFLGFBQWEsRUFBQTs7QUFFZjtFQUNFLGFBQWEsRUFBQTs7QUFFZjtFQUNFLGdCQUFnQjtFQUNoQiwyQkFBMkIsRUFBQTs7QUFFN0I7RUFDRSxTQUFTO0VBQ1QsVUFBVTtFQUNWLFdBQVc7RUFDWCxrQkFBa0IsRUFBQTs7QUFFcEI7RUFDRSxZQUFZO0VBQ1osZUFBZTtFQUVmLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIseUJBQXlCO0VBQ3pCLFlBQVk7RUFDWixlQUFlLEVBQUE7O0FBRWpCO0VBQ0UsMENBQTBDO0VBQzFDLFlBQVk7RUFDWixlQUFlO0VBQ2YsV0FBVztFQUNYLGNBQWM7RUEwQmQsa0JBQUEsRUFBbUI7O0FBL0JyQjtJQU9JLFdBQVc7SUFDWCxpQkFBaUI7SUFDakIsZUFBZTtJQUNmLGtCQUFrQixFQUFBOztBQUVwQjtFQUNFO0lBQ0UsVUFBVTtJQUNWLDJCQUFtQjtZQUFuQixtQkFBbUIsRUFBQTtFQUVyQjtJQUNFLGFBQWE7SUFDYiw4QkFBc0I7WUFBdEIsc0JBQXNCLEVBQUEsRUFBQTs7QUFQMUI7RUFDRTtJQUNFLFVBQVU7SUFDViwyQkFBbUI7WUFBbkIsbUJBQW1CLEVBQUE7RUFFckI7SUFDRSxhQUFhO0lBQ2IsOEJBQXNCO1lBQXRCLHNCQUFzQixFQUFBLEVBQUE7O0FBbkI1QjtJQXVCSSxlQUFlO0lBRWYsYUFBYTtJQUNiLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsNkJBQTZCLEVBQUE7O0FBNUJqQztJQWtDSSxXQUFXO0lBQ1gsYUFBYTtJQUNiLDhCQUE4QjtJQUM5QixtQkFBbUIsRUFBQTs7QUFyQ3ZCO0lBeUNJLFdBQVc7SUFDWCxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLHlCQUF5QixFQUFBOztBQTVDN0I7SUFnREksd0RBQWdEO1lBQWhELGdEQUFnRCxFQUFBOztBQWhEcEQ7SUFtREksMERBQWtEO1lBQWxELGtEQUFrRCxFQUFBOztBQW5EdEQ7SUFzREksMERBQWtEO1lBQWxELGtEQUFrRCxFQUFBOztBQUd0RDtFQUNFLGVBQWU7RUFDZixTQUFTO0VBQ1QsV0FBVztFQUNYLGlCQUFpQjtFQUNqQixZQUFZLEVBQUEiLCJmaWxlIjoiYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zaWRlYmFyLWZvb3RlciB7XHJcbiAgYm90dG9tOiAwO1xyXG4gIGNsZWFyOiBib3RoO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHBhZGRpbmc6IDBweDtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgd2lkdGg6IDIyOXB4O1xyXG4gIGJhY2tncm91bmQ6ICMyYTNmNTQ7XHJcbiAgei1pbmRleDogOTk5O1xyXG59XHJcbi5zaWRlYmFyLWZvb3RlciBhIHtcclxuICBwYWRkaW5nOiAxMnB4IDdweCA3cHggMTVweDtcclxuICB3aWR0aDogMTAwJTtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgYmFja2dyb3VuZDogI2Y3ZjdmNztcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgY29sb3I6ICMzMzM7XHJcbn1cclxuLnNpZGViYXItZm9vdGVyIGE6aG92ZXIge1xyXG4gIGNvbG9yOiAjMDA2N2M1O1xyXG59XHJcbi5zaWRlYmFyLWZvb3RlciBhIHN2ZyB7XHJcbiAgdmVydGljYWwtYWxpZ246IHRvcDtcclxufVxyXG4uaWNvbl9ob21lIHtcclxuICBmaWxsOiAjZGMzYzAwO1xyXG59XHJcbi5pY29uX2xlYWRzIHtcclxuICBmaWxsOiAjZjliMjBhO1xyXG59XHJcbi5pY29uX2xlYWRfY291bnQge1xyXG4gIGZpbGw6ICNlOTVmMjA7XHJcbn1cclxuLmljb25faW5jdW1iZW50X3BhcnRuZXIge1xyXG4gIGZpbGw6ICMwMDc0Yzc7XHJcbn1cclxuLmljb25fZWxpZ2libGVfcGFydG5lciB7XHJcbiAgZmlsbDogI2RjNGIzZDtcclxufVxyXG4uaWNvbl91bmtub3duIHtcclxuICBmaWxsOiAjZGM0YjNkO1xyXG59XHJcbi5pY29uX2xlYWRfY2F0Y2hlciB7XHJcbiAgZmlsbDogI2Y0YTUyOTtcclxufVxyXG4uaWNvbl9ydWxlcyB7XHJcbiAgZmlsbDogI2RjNGIzZDtcclxufVxyXG4uaWNvbl9tYW5hZ2luZ19ydWxlcyB7XHJcbiAgZmlsbDogIzUwN2UxOTtcclxufVxyXG4uaWNvbl9hY2NvdW50X293bmVyIHtcclxuICBmaWxsOiAjMDA2N2M1O1xyXG59XHJcbi5mdWxsc2NyZWVuIHtcclxuICBtYXJnaW4tbGVmdDogMHB4O1xyXG4gIHBhZGRpbmctdG9wOiAwcHggIWltcG9ydGFudDtcclxufVxyXG4ucHJvZmlsZS1pY29uIHtcclxuICB0b3A6IC03cHg7XHJcbiAgcmlnaHQ6IDVweDtcclxuICBmbG9hdDogbGVmdDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuLmV2ZW50LW1lc3NhZ2Uge1xyXG4gIHdpZHRoOiA0NTBweDtcclxuICBmb250LXNpemU6IDE1cHg7XHJcblxyXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICBtYXJnaW46IDVweCAwcHg7XHJcbiAgcGFkZGluZzogMnB4IDVweDtcclxuICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xyXG4gIG1hcmdpbi1yaWdodDogNTAwcHg7XHJcbiAgYm9yZGVyOiBzb2xpZCAxcHggIzAwNjdjNTtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcbiNsb2FkaW5nLXdyYXBwZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjE1LCAyMTUsIDIxNSwgMC4zKTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIHotaW5kZXg6IDE5OTk5O1xyXG4gICNsb2FkaW5nLW1lc3NhZ2Uge1xyXG4gICAgY29sb3I6ICM1NTU7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIG1hcmdpbi10b3A6IDVweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcbiAgQGtleWZyYW1lcyBwdWxzZSB7XHJcbiAgICBmcm9tIHtcclxuICAgICAgb3BhY2l0eTogMTtcclxuICAgICAgdHJhbnNmb3JtOiBzY2FsZSgxKTtcclxuICAgIH1cclxuICAgIHRvIHtcclxuICAgICAgb3BhY2l0eTogMC4yNTtcclxuICAgICAgdHJhbnNmb3JtOiBzY2FsZSgwLjc1KTtcclxuICAgIH1cclxuICB9XHJcbiAgI2xvYWRpbmcuc3Bpbm5lci1ib3gge1xyXG4gICAgbWFyZ2luLXRvcDogMjIlO1xyXG4gICAgLy8gbWFyZ2luLWxlZnQ6IC0yMCU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgfVxyXG5cclxuICAvKiBQVUxTRSBCVUJCTEVTICovXHJcblxyXG4gIC5wdWxzZS1jb250YWluZXIge1xyXG4gICAgd2lkdGg6IDYwcHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICB9XHJcblxyXG4gIC5wdWxzZS1idWJibGUge1xyXG4gICAgd2lkdGg6IDE1cHg7XHJcbiAgICBoZWlnaHQ6IDE1cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDA3MGIxO1xyXG4gIH1cclxuXHJcbiAgLnB1bHNlLWJ1YmJsZS0xIHtcclxuICAgIGFuaW1hdGlvbjogcHVsc2UgMC40cyBlYXNlIDBzIGluZmluaXRlIGFsdGVybmF0ZTtcclxuICB9XHJcbiAgLnB1bHNlLWJ1YmJsZS0yIHtcclxuICAgIGFuaW1hdGlvbjogcHVsc2UgMC40cyBlYXNlIDAuMnMgaW5maW5pdGUgYWx0ZXJuYXRlO1xyXG4gIH1cclxuICAucHVsc2UtYnViYmxlLTMge1xyXG4gICAgYW5pbWF0aW9uOiBwdWxzZSAwLjRzIGVhc2UgMC40cyBpbmZpbml0ZSBhbHRlcm5hdGU7XHJcbiAgfVxyXG59XHJcbi5hcHAtbmFtZSB7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIG1hcmdpbjogMDtcclxuICBmbG9hdDogbGVmdDtcclxuICBwYWRkaW5nOiAxNnB4IDBweDtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./shared/data.service */ "./src/app/shared/data.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _app_session_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.session-service */ "./src/app/app.session-service.ts");
/* harmony import */ var _model_user__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./model/user */ "./src/app/model/user.ts");
/* harmony import */ var ngx_order_pipe__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-order-pipe */ "./node_modules/ngx-order-pipe/ngx-order-pipe.es5.js");
/* harmony import */ var _shared_activity_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./shared/activity.service */ "./src/app/shared/activity.service.ts");
/* harmony import */ var _shared_shared_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./shared/shared.service */ "./src/app/shared/shared.service.ts");
/* harmony import */ var _settings_users_users_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./settings/users/users.service */ "./src/app/settings/users/users.service.ts");
/* harmony import */ var _environments_config__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../environments/config */ "./src/environments/config.ts");
// import { Component } from '@angular/core';
// import { DataService } from './shared/data.service';
// import { Router } from '@angular/router';
// import { SessionService } from './app.session-service';

// import { User } from './model/user';
// import { Role } from './model/role';
// import { EnumType } from './model/EnumType';
// import { OrderPipe } from 'ngx-order-pipe';
// import { UIPermission } from './model/menu';
// import { ActivityService } from './shared/activity.service';
// import { FeatureService } from './features/features.service';
// import { environment } from '../environments/environment';
// import { UserService } from './settings/users/users.service';
// import { SharedService } from './shared/shared.service';
// @Component({
//   selector: 'app-root',
//   templateUrl: './app.component.html',
//   styleUrls: ['./app.component.scss'],
//   providers: [
//     DataService
//   ]
// })
// export class AppComponent {
//   title = 'PRM';
//   url = '';
//   absPath = '';
//   code = '';
//   sessionData: any;
//   selectedItem: string;
//   LA_SessionData = '';
//   roles: Array<Role> = new Array<Role>();
//   settingPermission = false;
//   newUser: User = new User();
//   UIPermissionEnabled = false;
//   access_token = '';
//   response: any;
//   appMenu: Array<UIPermission> = new Array<UIPermission>();
//   fullscreen = false;
//   getSOS_URL(redirectUrl) {
//     return environment.oAuth_SSO_Url + environment.oAuth_SSO_Auth_Url + `${redirectUrl}&scope=leadapp.me&state=foobar`;
//   }
//   getSOS_Logout_URL() {
//     return environment.oAuth_SSO_Logout + this.url;
//   }
//   constructor(
//     private route: Router,
//     private activityService: ActivityService,
//     private sessionService: SessionService,
//     private userService: UserService,
//     private featureService: FeatureService,
//     private orderPipe: OrderPipe,
//     private sharedService: SharedService,
//     private dataService: DataService) {
//     this.url = window.location.origin + '/';
//     this.absPath = window.location.href;
//     this.fullscreen = this.IsAuthAllowed(window.location.pathname);
//     this.code = window.location.search == '' ? '' : window.location.search.match('code=([^&]*)') == null ? '' : window.location.search.match('code=([^&]*)')[1];
//   }
//   ngOnInit() {
//     this.getAllAuthMenu();
//   }
//   getAllAuthMenu() {
//     const data = '{"requestId":"7cf77166-bfd1-4b5b-b280-0463262792c3","success":true,"message":"","results":[[{"id":100,"name":"RuleEngine","url":" ","sequenceOrder":1,"createdBy":"KARNESH","updatedBy":"KARNESH"}],[{"id":101,"featureId":101,"name":"Dashboard","url":"dashboard","sequenceOrder":1,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_dashboard"},{"id":102,"featureId":102,"name":"Connections","url":"connections","sequenceOrder":2,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_plug"},{"id":103,"featureId":103,"name":"Jobs","url":"jobs","sequenceOrder":3,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_job"}, {"id":103,"featureId":103,"name":"Reports","url":"reports","sequenceOrder":3,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_report"}]]}';
//     if (JSON.parse(data).results != null &&
//       JSON.parse(data).results.length > 0) {
//       this.response = JSON.parse(data).results;
//       localStorage.setItem('UILIST', JSON.stringify(this.response));
//       const levelZero = this.orderPipe.transform(this.response[1], 'sequenceOrder');
//       if (levelZero != null) {
//         levelZero.forEach(appElement => { //
//           const appMenuList = [];
//           const levelOne = this.orderPipe.transform(this.response[2], 'sequenceOrder');
//           if (levelOne != null) {
//             levelOne.filter(n => n.parentId === appElement.id).forEach(appMenuElement => {
//               const appSubMenuList = [];
//               appMenuList.push({
//                 id: appMenuElement.id, name: appMenuElement.name, url: appMenuElement.url,
//                 featureId: appMenuElement.featureId, createdBY: appMenuElement.createdBY, icon: appMenuElement.icon,
//                 sequenceOrder: appMenuElement.sequenceOrder, updatedBY: appMenuElement.updatedBY, parentId: appMenuElement.parentId,
//                 submenu: appSubMenuList
//               });
//             });
//           }
//           this.appMenu.push({
//             id: appElement.id, name: appElement.name, url: appElement.url,
//             featureId: appElement.featureId, createdBY: appElement.createdBY, icon: appElement.icon,
//             sequenceOrder: appElement.sequenceOrder, updatedBY: appElement.updatedBY, parentId: appElement.parentId,
//             submenu: appMenuList
//           });
//         });
//       }
//     }
//     // this.featureService.GetAllMenu('RuleEngine')
//     //   .subscribe(data => {
//     //     data = '{"requestId":"7cf77166-bfd1-4b5b-b280-0463262792c3","success":true,"message":"","results":[[{"id":100,"name":"RuleEngine","url":" ","sequenceOrder":1,"createdBy":"KARNESH","updatedBy":"KARNESH"}],[{"id":101,"featureId":101,"name":"Dashboard","url":"dashboard","sequenceOrder":1,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_home"},{"id":102,"featureId":102,"name":"Connection","url":"connection","sequenceOrder":2,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_leads"},{"id":103,"featureId":103,"name":"Jobs","url":"jobs","sequenceOrder":3,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_leads"}, {"id":103,"featureId":103,"name":"Report","url":"jobs","sequenceOrder":3,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_report"}]]}';
//     //     if (JSON.parse(JSON.stringify(data)).results != null &&
//     //       JSON.parse(JSON.stringify(data)).results.length > 0) {
//     //       this.response = JSON.parse(JSON.stringify(data)).results;
//     //       localStorage.setItem('UILIST', JSON.stringify(this.response));
//     //       const levelZero = this.orderPipe.transform(this.response[1], 'sequenceOrder');
//     //       if (levelZero != null) {
//     //         levelZero.forEach(appElement => { //
//     //           const appMenuList = [];
//     //           const levelOne = this.orderPipe.transform(this.response[2], 'sequenceOrder');
//     //           if (levelOne != null) {
//     //             levelOne.filter(n => n.parentId === appElement.id).forEach(appMenuElement => {
//     //               const appSubMenuList = [];
//     //               appMenuList.push({
//     //                 id: appMenuElement.id, name: appMenuElement.name, url: appMenuElement.url,
//     //                 featureId: appMenuElement.featureId, createdBY: appMenuElement.createdBY, icon: appMenuElement.icon,
//     //                 sequenceOrder: appMenuElement.sequenceOrder, updatedBY: appMenuElement.updatedBY, parentId: appMenuElement.parentId,
//     //                 submenu: appSubMenuList
//     //               });
//     //             });
//     //           }
//     //           this.appMenu.push({
//     //             id: appElement.id, name: appElement.name, url: appElement.url,
//     //             featureId: appElement.featureId, createdBY: appElement.createdBY, icon: appElement.icon,
//     //             sequenceOrder: appElement.sequenceOrder, updatedBY: appElement.updatedBY, parentId: appElement.parentId,
//     //             submenu: appMenuList
//     //           });
//     //         });
//     //       }
//     //     }
//     //   }
//     // , err => {
//     // });
//   }
//   listClick(event, newValue) {
//     console.log(newValue);
//     this.selectedItem = newValue;  // don't forget to update the model here
//     // ... do other stuff here ...
//   }
//   getToken() {
//     this.sessionService.getToken(this.code, this.url.substr(0, this.url.length - 1))
//       .subscribe(data => {
//         if (data !== null && data !== '') {
//           this.sessionService.setCookie('LD_ACCESS_TOKEN', JSON.stringify(data));
//           const response = JSON.parse(this.sessionService.getCookie('LD_ACCESS_TOKEN'));
//           this.sessionService.getProfile(response.access_token)
//             .subscribe(profile => {
//               this.sessionService.setCookie('LD_USER_DETAIL', JSON.stringify(profile));
//               this.sessionData = JSON.parse(this.sessionService.getCookie('LD_USER_DETAIL'));
//               this.GetSignInUser(this.sessionData.uid);
//             }, error => {
//               console.log(error);
//             });
//         }
//         console.log(data);
//       }, error => {
//         console.log(error);
//       });
//   }
//   isActive() {
//     this.dataService.getCurrentUrl().subscribe(response => {
//       return response;
//     }, error => {
//       return error;
//     });
//   }
//   OpenSettings() {
//     this.url = '';
//     this.route.navigate(['settings']);
//   }
//   Logout() {
//     this.sessionService.deleteCookie('LD_ACCESS_TOKEN');
//     this.sessionService.deleteCookie('LD_USER_DETAIL');
//     window.location.href = this.getSOS_Logout_URL();
//   }
//   LALogout() {
//     this.sessionService.deleteCookie('LA_USER_DETAIL');
//     const domain = window.location.origin;
//     window.location.href = domain;
//   }
//   private GetSignInUser(userid: string) {
//     this.ProtralRedirection();
//     if (this.sessionService.getCookie('LA_USER_DETAIL') !== '') {
//       this.LA_SessionData = JSON.parse(this.sessionService.getCookie('LA_USER_DETAIL'));
//       userid = this.LA_SessionData['userId'];
//     }
//     this.userService.GetSignInUser(userid).subscribe(data => {
//       const response = JSON.parse(JSON.stringify(data));
//       if (response.success) {
//         this.GetUserRoles(userid);
//         if (this.sessionService.getCookie('LA_USER_DETAIL') !== '') {
//           this.userService.UserAudit(userid, 'Success').subscribe(userAudit => console.log(userAudit), error => console.log(error));
//         }
//       } else {
//         if (this.sessionService.getCookie('LA_USER_DETAIL') !== '') {
//           this.userService.UserAudit(userid, 'Failed').subscribe(userAudit => console.log(userAudit), error => console.log(error));
//         }
//         this.UIPermissionEnabled = false;
//         this.dataService.setData(this.UIPermissionEnabled);
//       }
//       this.UpdateProfile();
//     }, error => {
//     });
//   }
//   private UpdateProfile() {
//     this.newUser.firstName = this.sessionData.firstname;
//     this.newUser.lastName = this.sessionData.lastname;
//     this.newUser.emailId = this.sessionData.mail;
//     this.newUser.userId = this.sessionData.uid;
//     this.newUser.createdBy = 'System';
//     this.userService.AddUser(this.newUser).subscribe(data => {
//     }, err => { });
//   }
//   private GetUserRoles(userid) {
//     if (this.sessionService.getCookie('LA_USER_DETAIL') !== '') {
//       this.LA_SessionData = JSON.parse(this.sessionService.getCookie('LA_USER_DETAIL'));
//       userid = this.LA_SessionData['userId'];
//     }
//     this.userService.GetUserRoles(userid).subscribe(data => {
//       if (JSON.parse(JSON.stringify(data)).success === true && JSON.parse(JSON.stringify(data)).results[0].roleDetailsResponse.length > 0) {
//         this.UIPermissionEnabled = true;
//         this.getAllAuthMenu();
//         this.roles = JSON.parse(JSON.stringify(data)).results[0].roleDetailsResponse;
//         this.settingPermission = this.roles.filter(n => n.role === EnumType.SUPER_ADMIN).length > 0 ? true : false;
//         this.sessionService.setCookie('ROLES', JSON.stringify(this.roles));
//       }
//     }, err => {
//     });
//   }
//   private IsAuthAllowed(url): boolean {
//     let ret = false;
//     switch (true) {
//       case (url.indexOf('login') !== -1):
//         ret = true;
//         break;
//       case (url.indexOf('promotion/oppty/add/') !== -1):
//         ret = true;
//         break;
//       case (url.indexOf('solution') !== -1):
//         ret = true;
//         break;
//       default:
//         ret = false;
//         break;
//     }
//     return ret;
//   }
//   private ProtralRedirection(): void {
//     const redirectURI = this.sessionService.getCookie('redirect_uri');
//     this.sessionService.deleteCookie('redirect_uri');
//     if (redirectURI !== '' && !this.IsAuthAllowed(redirectURI)) {
//       this.route.navigate([redirectURI]);
//     } else if (redirectURI !== '') {
//       window.location.href = this.url.substr(0, this.url.length - 1) + '/' + redirectURI;
//     }
//   }
// }










var AppComponent = /** @class */ (function () {
    function AppComponent(route, activityService, activatedRoute, sessionService, userService, orderPipe, dataService, sharedService) {
        this.route = route;
        this.activityService = activityService;
        this.activatedRoute = activatedRoute;
        this.sessionService = sessionService;
        this.userService = userService;
        this.orderPipe = orderPipe;
        this.dataService = dataService;
        this.sharedService = sharedService;
        this.title = 'app';
        this.headertext = 'Welcome to Sales OnBoard Checklist';
        this.ret = false;
        this.roles = new Array();
        this.newUser = new _model_user__WEBPACK_IMPORTED_MODULE_5__["User"]();
        this.UIPermissionEnabled = true;
        this.appMenu = new Array();
        // byPassHost = ['localhost', 'core-ui-rxz-core-stg.npc-us-west-dc61.ncloud.netapp.com', '10.103.129.17','http://cst-etl-vm.eastus.cloudapp.azure.com'];
        this.byPassHost = ['localhost', 'core-ui-rxz-core-stg.npc-us-west-dc61.ncloud.netapp.com', 'cst-etl-vm.eastus.cloudapp.azure.com'];
        localStorage.setItem('filtersMV', null);
        localStorage.setItem('defaultFilter', null);
        this.laSessionData = '';
        this.settingPermission = false;
        this.fullscreen = false;
        this.url = window.location.origin + '/';
        this.absPath = window.location.href;
        this.fullscreen = this.IsAuthAllowed(window.location.pathname);
        this.code = window.location.search === '' ? '' : window.location.search.match('code=([^&]*)') === null ? '' : window.location.search.
            match('code=([^&]*)')[1];
    }
    AppComponent.prototype.getSOS_URL = function () {
        return "" + _environments_config__WEBPACK_IMPORTED_MODULE_10__["Config"].getEnvironmentVariable('oAuth_SSO_Url') + _environments_config__WEBPACK_IMPORTED_MODULE_10__["Config"].getEnvironmentVariable('oAuth_SSO_Auth_Url') + "\n    " + _environments_config__WEBPACK_IMPORTED_MODULE_10__["Config"].getEnvironmentVariable('redirectURI') + _environments_config__WEBPACK_IMPORTED_MODULE_10__["Config"].getEnvironmentVariable('oAuth_SSO_Scope_Url');
    };
    AppComponent.prototype.getSOS_Logout_URL = function () {
        return "" + _environments_config__WEBPACK_IMPORTED_MODULE_10__["Config"].getEnvironmentVariable('oAuth_SSO_Logout') + this.url;
    };
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loginData = JSON.parse(JSON.stringify(this.sessionService.getCookie('LOGIN').results[0]));
        this.getAllAuthMenu();
        if (this.byPassHost.filter(function (n) { return _this.url.indexOf(n) > -1; }).length > 0) {
            this.sessionService.setData();
            this.sharedService.GetUserRoles();
        }
        this.sessionData = this.sessionService.getCookie('LD_USER_DETAIL') === '' ? null : this.sessionService.getCookie('LD_USER_DETAIL');
        if (this.sessionData === null && this.code === '') { //  && !this.fullscreen
            window.location.href = this.getSOS_URL(); // REDIRECT TO SSO LOGIN PAGE
        }
        else {
            if (this.sessionData === null) {
                this.getToken();
            }
            else {
                this.sharedService.GetUserRoles();
            }
        }
    };
    AppComponent.prototype.loginAs = function () {
        this.loginData = JSON.parse(JSON.stringify(this.sessionService.getCookie('LOGIN').results[0]));
        // this.sessionService.setCookie('LD_USER_DETAIL', JSON.stringify(this.loginData));
        this.appMenu = [];
        this.getAllAuthMenu();
        this.sharedService.GetUserRoles();
        // this.sharedService.GetUserRoles();
    };
    // getAllAuthMenu() {
    //   this.featureService.GetAllMenu('ALL').subscribe(data => {
    //     if (JSON.parse(JSON.stringify(data)).results != null &&
    //       JSON.parse(JSON.stringify(data)).results.length > 0) {
    //       this.response = JSON.parse(JSON.stringify(data)).results;
    //       const levelZero = this.orderPipe.transform(this.response[0], 'sequenceOrder');
    //       levelZero.forEach(appElement => { //
    //         const appMenuList = [];
    //         const levelOne = this.orderPipe.transform(this.response[1], 'sequenceOrder');
    //         levelOne.filter(n => n.parentId === appElement.id).forEach(appMereportsnuElement => {
    //           const appSubMenuList = [];
    //           const levelTwo = this.orderPipe.transform(this.response[2], 'sequenceOrder');
    //           levelTwo.filter(n => n.parentId === appMenuElement.id).forEach(appSubMenuElement => {
    //             appSubMenuList.push({
    //               id: appSubMenuElement.id, name: appSubMenuElement.name, url: appSubMenuElement.url,
    //               featureId: appSubMenuElement.featureId, createdBY: appSubMenuElement.createdBY, icon: appSubMenuElement.icon,
    //               sequenceOrder: appSubMenuElement.sequenceOrder, updatedBY:
    // appSubMenuElement.updatedBY, parentId: appSubMenuElement.parentId
    //             });
    //           });
    //           appMenuList.push({
    //             id: appMenuElement.id, name: appMenuElement.name, url: appMenuElement.url,
    //             featureId: appMenuElement.featureId, createdBY: appMenuElement.createdBY, icon: appMenuElement.icon,
    //             sequenceOrder: appMenuElement.sequenceOrder, updatedBY: appMenuElement.updatedBY,
    //             parentId: appMenuElement.parentId, submenu: appSubMenuList
    //           });
    //         });
    //         this.appMenu.push({
    //           id: appElement.id, name: appElement.name, url: appElement.url,
    //           featureId: appElement.featureId, createdBY: appElement.createdBY, icon: appElement.icon,
    //           sequenceOrder: appElement.sequenceOrder, updatedBY: appElement.updatedBY, parentId: appElement.parentId, submenu: appMenuList
    //         });
    //       });
    //     }
    //   }, err => {
    //   });
    // }
    AppComponent.prototype.getAllAuthMenu = function () {
        var _this = this;
        var data = '{"requestId":"7cf77166-bfd1-4b5b-b280-0463262792c3","success":true,"message":"","results":[[{"id":100,"name":"RuleEngine","url":" ","sequenceOrder":1,"createdBy":"KARNESH","updatedBy":"KARNESH"}],[{"id":101,"featureId":101,"name":"Dashboard","url":"dashboard","sequenceOrder":1,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_dashboard"},{"id":102,"featureId":102,"name":"Connections","url":"connections","sequenceOrder":2,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_plug"},{"id":103,"featureId":103,"name":"Jobs","url":"jobs","sequenceOrder":3,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_job"}, {"id":103,"featureId":103,"name":"Reports","url":"reports","sequenceOrder":3,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_report"}]]}';
        if (JSON.parse(data).results != null &&
            JSON.parse(data).results.length > 0) {
            this.response = JSON.parse(data).results;
            localStorage.setItem('UILIST', JSON.stringify(this.response));
            var levelZero = this.orderPipe.transform(this.response[1], 'sequenceOrder');
            if (levelZero != null) {
                levelZero.forEach(function (appElement) {
                    var appMenuList = [];
                    var levelOne = _this.orderPipe.transform(_this.response[2], 'sequenceOrder');
                    if (levelOne != null) {
                        levelOne.filter(function (n) { return n.parentId === appElement.id; }).forEach(function (appMenuElement) {
                            var appSubMenuList = [];
                            appMenuList.push({
                                id: appMenuElement.id, name: appMenuElement.name, url: appMenuElement.url,
                                featureId: appMenuElement.featureId, createdBY: appMenuElement.createdBY, icon: appMenuElement.icon,
                                sequenceOrder: appMenuElement.sequenceOrder, updatedBY: appMenuElement.updatedBY, parentId: appMenuElement.parentId,
                                submenu: appSubMenuList
                            });
                        });
                    }
                    _this.appMenu.push({
                        id: appElement.id, name: appElement.name, url: appElement.url,
                        featureId: appElement.featureId, createdBY: appElement.createdBY, icon: appElement.icon,
                        sequenceOrder: appElement.sequenceOrder, updatedBY: appElement.updatedBY, parentId: appElement.parentId,
                        submenu: appMenuList
                    });
                });
            }
        }
    };
    AppComponent.prototype.getToken = function () {
        var _this = this;
        var codeString = this.code;
        console.log('code = ' + codeString);
        this.sessionService.getToken(codeString, _environments_config__WEBPACK_IMPORTED_MODULE_10__["Config"].getEnvironmentVariable('redirectURI'))
            .subscribe(function (data) {
            if (data != null && data !== '') {
                console.log('data = ' + data);
                _this.sessionService.setCookie('LD_ACCESS_TOKEN', JSON.stringify(data));
                _this.sessionService.getProfile(_this.sessionService.getCookie('LD_ACCESS_TOKEN').access_token).subscribe(function (res) {
                    _this.sessionService.setCookie('LD_USER_DETAIL', JSON.stringify(res));
                    // this.sharedService.updateUserProfile();
                    _this.sessionData = _this.sessionService.getCookie('LD_USER_DETAIL');
                    console.log('getToken() called');
                    _this.sharedService.GetUserRoles();
                }, function (error) {
                    console.log(error);
                });
            }
        }, function (error) {
            console.log(error);
        });
    };
    // isActive() {
    //   this.dataService.getCurrentUrl().subscribe(response => function () {
    //     return response;
    //   }, error => function () {
    //     return error;
    //   });
    // }
    AppComponent.prototype.OpenSettings = function () {
        this.url = '';
        this.route.navigate(['settings']);
    };
    AppComponent.prototype.Logout = function () {
        this.sessionService.deleteCookie('LD_ACCESS_TOKEN');
        this.sessionService.deleteCookie('LD_USER_DETAIL');
        this.sessionService.deleteCookie('LOGIN');
        this.sharedService.isFullScreen = true;
        this.route.navigate(['login']);
        //  window.location.href = this.getSOS_Logout_URL();
    };
    AppComponent.prototype.laLogout = function () {
        this.sessionService.deleteCookie('LA_USER_DETAIL');
        var domain = window.location.origin;
        window.location.href = domain;
    };
    AppComponent.prototype.UpdateProfile = function () {
        this.newUser.firstName = this.sessionData.firstname;
        this.newUser.lastName = this.sessionData.lastname;
        this.newUser.emailId = this.sessionData.mail;
        this.newUser.userId = this.sessionData.uid;
        this.newUser.createdBy = 'System';
        this.userService.AddUser(this.newUser).subscribe(function (data) {
        }, function (err) { });
    };
    AppComponent.prototype.IsAuthAllowed = function (url) {
        var ret = false;
        switch (true) {
            case (url.indexOf('login') !== -1):
                ret = true;
                break;
            case (url.indexOf('promotion/oppty/add/') !== -1):
                ret = true;
                break;
            case (url.indexOf('solution') !== -1):
                ret = true;
                break;
            default:
                ret = false;
                break;
        }
        return ret;
    };
    AppComponent.prototype.ProtralRedirection = function () {
        var redirectUri = this.sessionService.getCookie('redirect_uri');
        this.sessionService.deleteCookie('redirect_uri');
        if (redirectUri !== '' && !this.IsAuthAllowed(redirectUri)) {
            this.route.navigate([redirectUri]);
        }
        else if (redirectUri !== '') {
            window.location.href = this.url.substr(0, this.url.length - 1) + '/' + redirectUri;
        }
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            providers: [
                _shared_data_service__WEBPACK_IMPORTED_MODULE_2__["DataService"],
                _shared_data_service__WEBPACK_IMPORTED_MODULE_2__["DataService"]
            ],
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _shared_activity_service__WEBPACK_IMPORTED_MODULE_7__["ActivityService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _app_session_service__WEBPACK_IMPORTED_MODULE_4__["SessionService"],
            _settings_users_users_service__WEBPACK_IMPORTED_MODULE_9__["UserService"],
            ngx_order_pipe__WEBPACK_IMPORTED_MODULE_6__["OrderPipe"],
            _shared_data_service__WEBPACK_IMPORTED_MODULE_2__["DataService"],
            _shared_shared_service__WEBPACK_IMPORTED_MODULE_8__["SharedService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _app_routing__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.routing */ "./src/app/app.routing.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/index.js");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(ng2_charts__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_order_pipe__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-order-pipe */ "./node_modules/ngx-order-pipe/ngx-order-pipe.es5.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var ngx_modal__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-modal */ "./node_modules/ngx-modal/index.js");
/* harmony import */ var ngx_modal__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(ngx_modal__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _progress_kendo_angular_upload__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @progress/kendo-angular-upload */ "./node_modules/@progress/kendo-angular-upload/dist/fesm5/index.js");
/* harmony import */ var ngx_treeview__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ngx-treeview */ "./node_modules/ngx-treeview/src/index.js");
/* harmony import */ var _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @swimlane/ngx-charts */ "./node_modules/@swimlane/ngx-charts/release/esm.js");
/* harmony import */ var angular_progress_bar__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! angular-progress-bar */ "./node_modules/angular-progress-bar/fesm5/angular-progress-bar.js");
/* harmony import */ var _progress_kendo_angular_tooltip__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @progress/kendo-angular-tooltip */ "./node_modules/@progress/kendo-angular-tooltip/dist/fesm5/index.js");
/* harmony import */ var ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ngx-bootstrap/datepicker */ "./node_modules/ngx-bootstrap/datepicker/fesm5/ngx-bootstrap-datepicker.js");
/* harmony import */ var _progress_kendo_angular_layout__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @progress/kendo-angular-layout */ "./node_modules/@progress/kendo-angular-layout/dist/fesm5/index.js");
/* harmony import */ var _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @progress/kendo-angular-grid */ "./node_modules/@progress/kendo-angular-grid/dist/fesm5/index.js");
/* harmony import */ var _progress_kendo_angular_toolbar__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @progress/kendo-angular-toolbar */ "./node_modules/@progress/kendo-angular-toolbar/dist/fesm5/index.js");
/* harmony import */ var _progress_kendo_angular_dropdowns__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @progress/kendo-angular-dropdowns */ "./node_modules/@progress/kendo-angular-dropdowns/dist/fesm5/index.js");
/* harmony import */ var _progress_kendo_angular_buttons__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @progress/kendo-angular-buttons */ "./node_modules/@progress/kendo-angular-buttons/dist/fesm5/index.js");
/* harmony import */ var _progress_kendo_angular_ripple__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @progress/kendo-angular-ripple */ "./node_modules/@progress/kendo-angular-ripple/dist/es/index.js");
/* harmony import */ var _progress_kendo_angular_dialog__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @progress/kendo-angular-dialog */ "./node_modules/@progress/kendo-angular-dialog/dist/fesm5/index.js");
/* harmony import */ var _settings_setting_module__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./settings/setting-module */ "./src/app/settings/setting-module.ts");
/* harmony import */ var _settings_users_users_service__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./settings/users/users.service */ "./src/app/settings/users/users.service.ts");
/* harmony import */ var _settings_roles_roles_service__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./settings/roles/roles.service */ "./src/app/settings/roles/roles.service.ts");
/* harmony import */ var _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! @progress/kendo-angular-popup */ "./node_modules/@progress/kendo-angular-popup/dist/es/index.js");
/* harmony import */ var _progress_kendo_angular_dateinputs__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! @progress/kendo-angular-dateinputs */ "./node_modules/@progress/kendo-angular-dateinputs/dist/fesm5/index.js");
/* harmony import */ var ngx_avatar__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ngx-avatar */ "./node_modules/ngx-avatar/fesm5/ngx-avatar.js");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm5/drag-drop.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var ngx_device_detector__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ngx-device-detector */ "./node_modules/ngx-device-detector/ngx-device-detector.umd.js");
/* harmony import */ var ngx_device_detector__WEBPACK_IMPORTED_MODULE_34___default = /*#__PURE__*/__webpack_require__.n(ngx_device_detector__WEBPACK_IMPORTED_MODULE_34__);
/* harmony import */ var _progress_kendo_angular_inputs__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! @progress/kendo-angular-inputs */ "./node_modules/@progress/kendo-angular-inputs/dist/fesm5/index.js");
/* harmony import */ var cron_editor__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! cron-editor */ "./node_modules/cron-editor/fesm5/cron-editor.js");
/* harmony import */ var odata_v4_ng__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! odata-v4-ng */ "./node_modules/odata-v4-ng/fesm5/odata-v4-ng.js");
/* harmony import */ var ngx_daterangepicker_material__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ngx-daterangepicker-material */ "./node_modules/ngx-daterangepicker-material/fesm5/ngx-daterangepicker-material.js");
/* harmony import */ var _app_session_service__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./app.session-service */ "./src/app/app.session-service.ts");
/* harmony import */ var _features_features_service__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./features/features.service */ "./src/app/features/features.service.ts");
/* harmony import */ var _shared_export_service__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./shared/export.service */ "./src/app/shared/export.service.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _app_admin_auth_guard_service__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./app.admin-auth-guard.service */ "./src/app/app.admin-auth-guard.service.ts");
/* harmony import */ var _app_user_auth_guard_service__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./app.user-auth-guard.service */ "./src/app/app.user-auth-guard.service.ts");
/* harmony import */ var _core_component_comment_comment_service__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./core-component/comment/comment-service */ "./src/app/core-component/comment/comment-service.ts");
/* harmony import */ var _common_service__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ./common-service */ "./src/app/common-service.ts");
/* harmony import */ var _shared_activity_service__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ./shared/activity.service */ "./src/app/shared/activity.service.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var ckeditor4_angular__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ckeditor4-angular */ "./node_modules/ckeditor4-angular/fesm5/ckeditor4-angular.js");
/* harmony import */ var _core_component_comment_comment_component__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ./core-component/comment/comment.component */ "./src/app/core-component/comment/comment.component.ts");
/* harmony import */ var _jobs_jobs_component__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ./jobs/jobs.component */ "./src/app/jobs/jobs.component.ts");
/* harmony import */ var _connections_connections_component__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! ./connections/connections.component */ "./src/app/connections/connections.component.ts");
/* harmony import */ var _connections_add_connection_add_connection_component__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(/*! ./connections/add-connection/add-connection.component */ "./src/app/connections/add-connection/add-connection.component.ts");
/* harmony import */ var _connector_odata_connector_odata_connector_component__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(/*! ./connector/odata-connector/odata-connector.component */ "./src/app/connector/odata-connector/odata-connector.component.ts");
/* harmony import */ var _connector_rdbms_connector_rdbms_connector_component__WEBPACK_IMPORTED_MODULE_56__ = __webpack_require__(/*! ./connector/rdbms-connector/rdbms-connector.component */ "./src/app/connector/rdbms-connector/rdbms-connector.component.ts");
/* harmony import */ var _jobs_new_job_new_job_component__WEBPACK_IMPORTED_MODULE_57__ = __webpack_require__(/*! ./jobs/new-job/new-job.component */ "./src/app/jobs/new-job/new-job.component.ts");
/* harmony import */ var _jobs_log_log_component__WEBPACK_IMPORTED_MODULE_58__ = __webpack_require__(/*! ./jobs/log/log.component */ "./src/app/jobs/log/log.component.ts");
/* harmony import */ var _jobs_log_status_log_status_component__WEBPACK_IMPORTED_MODULE_59__ = __webpack_require__(/*! ./jobs/log-status/log-status.component */ "./src/app/jobs/log-status/log-status.component.ts");
/* harmony import */ var _bulk_load_bulk_load_component__WEBPACK_IMPORTED_MODULE_60__ = __webpack_require__(/*! ./bulk-load/bulk-load.component */ "./src/app/bulk-load/bulk-load.component.ts");
/* harmony import */ var _bulk_load_bulk_service__WEBPACK_IMPORTED_MODULE_61__ = __webpack_require__(/*! ./bulk-load/bulk.service */ "./src/app/bulk-load/bulk.service.ts");
/* harmony import */ var _shared_tour_guide_tour_guide_component__WEBPACK_IMPORTED_MODULE_62__ = __webpack_require__(/*! ./shared/tour-guide/tour-guide.component */ "./src/app/shared/tour-guide/tour-guide.component.ts");
/* harmony import */ var _pipe_search_pipe__WEBPACK_IMPORTED_MODULE_63__ = __webpack_require__(/*! ./pipe/search.pipe */ "./src/app/pipe/search.pipe.ts");
/* harmony import */ var _connector_api_connector_api_connector_component__WEBPACK_IMPORTED_MODULE_64__ = __webpack_require__(/*! ./connector/api-connector/api-connector.component */ "./src/app/connector/api-connector/api-connector.component.ts");
/* harmony import */ var angular_draggable_droppable__WEBPACK_IMPORTED_MODULE_65__ = __webpack_require__(/*! angular-draggable-droppable */ "./node_modules/angular-draggable-droppable/fesm5/angular-draggable-droppable.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_66__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _auth_guard_service__WEBPACK_IMPORTED_MODULE_67__ = __webpack_require__(/*! ./auth-guard.service */ "./src/app/auth-guard.service.ts");
/* harmony import */ var _shared_shared_service__WEBPACK_IMPORTED_MODULE_68__ = __webpack_require__(/*! ./shared/shared.service */ "./src/app/shared/shared.service.ts");






































































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_48__["AppComponent"],
                _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_49__["DashboardComponent"],
                _core_component_comment_comment_component__WEBPACK_IMPORTED_MODULE_51__["CommentComponent"],
                _jobs_jobs_component__WEBPACK_IMPORTED_MODULE_52__["JobsComponent"],
                _connections_connections_component__WEBPACK_IMPORTED_MODULE_53__["ConnectionsComponent"],
                _connections_add_connection_add_connection_component__WEBPACK_IMPORTED_MODULE_54__["AddConnectionComponent"],
                _connector_rdbms_connector_rdbms_connector_component__WEBPACK_IMPORTED_MODULE_56__["RdbmsConnectorComponent"],
                _connector_odata_connector_odata_connector_component__WEBPACK_IMPORTED_MODULE_55__["ODataConnectorComponent"],
                _jobs_new_job_new_job_component__WEBPACK_IMPORTED_MODULE_57__["NewJobComponent"],
                _jobs_log_log_component__WEBPACK_IMPORTED_MODULE_58__["LogComponent"],
                _jobs_log_status_log_status_component__WEBPACK_IMPORTED_MODULE_59__["LogStatusComponent"],
                _bulk_load_bulk_load_component__WEBPACK_IMPORTED_MODULE_60__["BulkLoadComponent"],
                _shared_tour_guide_tour_guide_component__WEBPACK_IMPORTED_MODULE_62__["TourGuideComponent"],
                _pipe_search_pipe__WEBPACK_IMPORTED_MODULE_63__["SearchPipe"],
                _connector_api_connector_api_connector_component__WEBPACK_IMPORTED_MODULE_64__["ApiConnectorComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_66__["LoginComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"],
                _settings_setting_module__WEBPACK_IMPORTED_MODULE_26__["SettingModule"],
                ngx_modal__WEBPACK_IMPORTED_MODULE_12__["ModalModule"],
                ngx_treeview__WEBPACK_IMPORTED_MODULE_14__["TreeviewModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                ngx_order_pipe__WEBPACK_IMPORTED_MODULE_10__["OrderModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
                _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_15__["NgxChartsModule"],
                ng2_charts__WEBPACK_IMPORTED_MODULE_7__["ChartsModule"],
                angular_progress_bar__WEBPACK_IMPORTED_MODULE_16__["ProgressBarModule"],
                ngx_device_detector__WEBPACK_IMPORTED_MODULE_34__["DeviceDetectorModule"].forRoot(),
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forRoot(_app_routing__WEBPACK_IMPORTED_MODULE_5__["routes"]),
                _progress_kendo_angular_tooltip__WEBPACK_IMPORTED_MODULE_17__["TooltipModule"],
                ngx_toastr__WEBPACK_IMPORTED_MODULE_11__["ToastrModule"].forRoot(),
                _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_20__["GridModule"],
                _progress_kendo_angular_toolbar__WEBPACK_IMPORTED_MODULE_21__["ToolBarModule"],
                _progress_kendo_angular_buttons__WEBPACK_IMPORTED_MODULE_23__["ButtonsModule"],
                _progress_kendo_angular_ripple__WEBPACK_IMPORTED_MODULE_24__["RippleModule"],
                ngx_bootstrap_datepicker__WEBPACK_IMPORTED_MODULE_18__["BsDatepickerModule"].forRoot(),
                _progress_kendo_angular_layout__WEBPACK_IMPORTED_MODULE_19__["LayoutModule"],
                _progress_kendo_angular_dropdowns__WEBPACK_IMPORTED_MODULE_22__["DropDownsModule"],
                _progress_kendo_angular_ripple__WEBPACK_IMPORTED_MODULE_24__["RippleModule"],
                _progress_kendo_angular_dialog__WEBPACK_IMPORTED_MODULE_25__["DialogsModule"],
                _progress_kendo_angular_dateinputs__WEBPACK_IMPORTED_MODULE_30__["DateInputsModule"],
                _progress_kendo_angular_popup__WEBPACK_IMPORTED_MODULE_29__["PopupModule"],
                ngx_avatar__WEBPACK_IMPORTED_MODULE_31__["AvatarModule"],
                _progress_kendo_angular_dialog__WEBPACK_IMPORTED_MODULE_25__["WindowModule"],
                _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_32__["DragDropModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_33__["MatCardModule"],
                _progress_kendo_angular_inputs__WEBPACK_IMPORTED_MODULE_35__["InputsModule"],
                ckeditor4_angular__WEBPACK_IMPORTED_MODULE_50__["CKEditorModule"],
                odata_v4_ng__WEBPACK_IMPORTED_MODULE_37__["ODataModule"],
                cron_editor__WEBPACK_IMPORTED_MODULE_36__["CronEditorModule"],
                ngx_daterangepicker_material__WEBPACK_IMPORTED_MODULE_38__["NgxDaterangepickerMd"].forRoot(),
                ngx_toastr__WEBPACK_IMPORTED_MODULE_11__["ToastrModule"].forRoot(),
                angular_draggable_droppable__WEBPACK_IMPORTED_MODULE_65__["DragAndDropModule"],
                _progress_kendo_angular_upload__WEBPACK_IMPORTED_MODULE_13__["UploadModule"]
            ],
            providers: [
                _app_admin_auth_guard_service__WEBPACK_IMPORTED_MODULE_43__["AdminAuthGuardService"],
                _app_user_auth_guard_service__WEBPACK_IMPORTED_MODULE_44__["UserAuthGuardService"],
                _app_session_service__WEBPACK_IMPORTED_MODULE_39__["SessionService"],
                _settings_users_users_service__WEBPACK_IMPORTED_MODULE_27__["UserService"],
                _settings_roles_roles_service__WEBPACK_IMPORTED_MODULE_28__["RolesService"],
                ngx_cookie_service__WEBPACK_IMPORTED_MODULE_42__["CookieService"],
                _shared_export_service__WEBPACK_IMPORTED_MODULE_41__["ExportService"],
                _angular_common__WEBPACK_IMPORTED_MODULE_8__["DatePipe"],
                _shared_shared_service__WEBPACK_IMPORTED_MODULE_68__["SharedService"],
                _common_service__WEBPACK_IMPORTED_MODULE_46__["CommonService"],
                _shared_activity_service__WEBPACK_IMPORTED_MODULE_47__["ActivityService"],
                _core_component_comment_comment_service__WEBPACK_IMPORTED_MODULE_45__["CommentService"],
                odata_v4_ng__WEBPACK_IMPORTED_MODULE_37__["ODataService"],
                _features_features_service__WEBPACK_IMPORTED_MODULE_40__["FeatureService"],
                _bulk_load_bulk_service__WEBPACK_IMPORTED_MODULE_61__["BulkService"],
                _shared_tour_guide_tour_guide_component__WEBPACK_IMPORTED_MODULE_62__["TourGuideComponent"],
                _auth_guard_service__WEBPACK_IMPORTED_MODULE_67__["AuthGuardService"]
            ],
            entryComponents: [_connections_add_connection_add_connection_component__WEBPACK_IMPORTED_MODULE_54__["AddConnectionComponent"], _connector_rdbms_connector_rdbms_connector_component__WEBPACK_IMPORTED_MODULE_56__["RdbmsConnectorComponent"], _connector_odata_connector_odata_connector_component__WEBPACK_IMPORTED_MODULE_55__["ODataConnectorComponent"]],
            exports: [
                _connections_add_connection_add_connection_component__WEBPACK_IMPORTED_MODULE_54__["AddConnectionComponent"], _connector_rdbms_connector_rdbms_connector_component__WEBPACK_IMPORTED_MODULE_56__["RdbmsConnectorComponent"], _connector_odata_connector_odata_connector_component__WEBPACK_IMPORTED_MODULE_55__["ODataConnectorComponent"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_48__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routing.ts":
/*!********************************!*\
  !*** ./src/app/app.routing.ts ***!
  \********************************/
/*! exports provided: routes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var _core_component_comment_comment_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./core-component/comment/comment.component */ "./src/app/core-component/comment/comment.component.ts");
/* harmony import */ var _jobs_jobs_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./jobs/jobs.component */ "./src/app/jobs/jobs.component.ts");
/* harmony import */ var _connections_connections_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./connections/connections.component */ "./src/app/connections/connections.component.ts");
/* harmony import */ var _jobs_new_job_new_job_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./jobs/new-job/new-job.component */ "./src/app/jobs/new-job/new-job.component.ts");
/* harmony import */ var _bulk_load_bulk_load_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./bulk-load/bulk-load.component */ "./src/app/bulk-load/bulk-load.component.ts");
/* harmony import */ var _auth_guard_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./auth-guard.service */ "./src/app/auth-guard.service.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");

// import { UserManagmentComponent } from './user-managment/user-managment.component';
// import { UsersComponent } from './users/users.component';
// import { RolesComponent } from './roles/roles.component';
// import { EditComponent } from './users/edit/edit.component';







var routes = [
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_7__["LoginComponent"] },
    { path: 'dashboard', component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_0__["DashboardComponent"], canActivate: [_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]] },
    { path: 'connections', component: _connections_connections_component__WEBPACK_IMPORTED_MODULE_3__["ConnectionsComponent"], canActivate: [_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]] },
    { path: 'jobs', component: _jobs_jobs_component__WEBPACK_IMPORTED_MODULE_2__["JobsComponent"], canActivate: [_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]] },
    { path: 'jobs/:jobId', component: _jobs_new_job_new_job_component__WEBPACK_IMPORTED_MODULE_4__["NewJobComponent"], canActivate: [_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuardService"]] },
    { path: 'jobs/new', component: _jobs_new_job_new_job_component__WEBPACK_IMPORTED_MODULE_4__["NewJobComponent"] },
    { path: 'bulk-load/new', component: _bulk_load_bulk_load_component__WEBPACK_IMPORTED_MODULE_5__["BulkLoadComponent"] },
    { path: 'comment', component: _core_component_comment_comment_component__WEBPACK_IMPORTED_MODULE_1__["CommentComponent"] },
    { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    { path: '**', redirectTo: 'not-found' }
];


/***/ }),

/***/ "./src/app/app.session-service.ts":
/*!****************************************!*\
  !*** ./src/app/app.session-service.ts ***!
  \****************************************/
/*! exports provided: SessionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SessionService", function() { return SessionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_config__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../environments/config */ "./src/environments/config.ts");





var SessionService = /** @class */ (function () {
    function SessionService(http, cookieService) {
        this.http = http;
        this.cookieService = cookieService;
        // oAuth_SSO_Url: string = environment.oAuth_SSO_Url;
        // getTokenEndpoint = environment.oAuthApiUrl + 'token/';
        // getProfileEndpoint = environment.oAuthApiUrl + 'userDetail/';
        this.baseUrl = _environments_config__WEBPACK_IMPORTED_MODULE_4__["Config"].getEnvironmentVariable('endPoint');
        this.getTokenEndpoint = this.baseUrl + 'oauth/token/';
        this.getProfileEndpoint = this.baseUrl + 'oauth/userDetail/';
    }
    SessionService.prototype.getToken = function (code, url) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json'
            })
        };
        var urls = this.getTokenEndpoint + (code + "?redirectionURL=" + url);
        return this.http.get(urls, httpOptions);
    };
    SessionService.prototype.getProfile = function (accessToken) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json'
            })
        };
        var url = this.getProfileEndpoint + ("" + accessToken);
        return this.http.get(url, httpOptions);
    };
    /**
     * Method to get session data
     */
    SessionService.prototype.getData = function () {
        return this.cookieService.get('LD_USER_DETAIL');
    };
    /**
     * Setting local session for development
     */
    SessionService.prototype.setData = function () {
        var data = {
            uid: 'karnesh', mail: 'Karnesh.Kumar@netapp.com', lastname: 'Kumar',
            commonname: 'Karnesh Kumar', firstname: 'Karnesh', uri: '/ms_oauth/resources/leadapp/me/karnesh'
        };
        this.cookieService.set('LD_USER_DETAIL', JSON.stringify(data));
    };
    /**
     * Method to set cookie
     * @param key Set Cookie Name
     * @param value Set Cookie Value
     */
    SessionService.prototype.setCookie = function (key, value) {
        this.cookieService.set(key, value);
    };
    SessionService.prototype.getCookie = function (key) {
        return this.cookieService.get(key) === '' ? '' : JSON.parse(this.cookieService.get(key));
    };
    SessionService.prototype.deleteCookie = function (key) {
        this.cookieService.delete(key);
    };
    /**
     * Session data method to get value from session data
     * @param Key Pass key to get session parameter value
     */
    SessionService.prototype.getSessionValue = function (Key) {
        return this.cookieService.get('LD_USER_DETAIL') === '' ? '' : JSON.parse(this.cookieService.get('LD_USER_DETAIL'))[Key];
    };
    SessionService.prototype.deleteSession = function () {
        this.cookieService.delete('LD_USER_DETAIL');
    };
    SessionService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], ngx_cookie_service__WEBPACK_IMPORTED_MODULE_2__["CookieService"]])
    ], SessionService);
    return SessionService;
}());



/***/ }),

/***/ "./src/app/app.user-auth-guard.service.ts":
/*!************************************************!*\
  !*** ./src/app/app.user-auth-guard.service.ts ***!
  \************************************************/
/*! exports provided: UserAuthGuardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserAuthGuardService", function() { return UserAuthGuardService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_session_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.session-service */ "./src/app/app.session-service.ts");
/* harmony import */ var _shared_shared_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./shared/shared.service */ "./src/app/shared/shared.service.ts");





var UserAuthGuardService = /** @class */ (function () {
    function UserAuthGuardService(router, _SessionService, sharedService) {
        this.router = router;
        this._SessionService = _SessionService;
        this.sharedService = sharedService;
    }
    UserAuthGuardService.prototype.canActivate = function (route, state) {
        this.sharedService.isFullScreen = false;
        var url = state.url;
        var roles = (this._SessionService.getCookie("ROLES") == ""
            || this._SessionService.getCookie("ROLES") == null) ? [] : JSON.parse(JSON.stringify(this._SessionService.getCookie("ROLES")));
        return true;
        //  if (roles.filter(n => n.role !== EnumType.SUPER_ADMIN).length > 0) {
        //     return true;
        // }
        this.router.navigate(['']);
        return false;
    };
    UserAuthGuardService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _app_session_service__WEBPACK_IMPORTED_MODULE_3__["SessionService"], _shared_shared_service__WEBPACK_IMPORTED_MODULE_4__["SharedService"]])
    ], UserAuthGuardService);
    return UserAuthGuardService;
}());



/***/ }),

/***/ "./src/app/auth-guard.service.ts":
/*!***************************************!*\
  !*** ./src/app/auth-guard.service.ts ***!
  \***************************************/
/*! exports provided: AuthGuardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuardService", function() { return AuthGuardService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _app_session_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.session-service */ "./src/app/app.session-service.ts");
/* harmony import */ var _shared_shared_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./shared/shared.service */ "./src/app/shared/shared.service.ts");





var AuthGuardService = /** @class */ (function () {
    function AuthGuardService(router, sessionService, sharedService) {
        this.router = router;
        this.sessionService = sessionService;
        this.sharedService = sharedService;
    }
    AuthGuardService.prototype.canActivate = function (route, state) {
        this.sessionData = this.sessionService.getCookie('LOGIN');
        if (this.sessionData === undefined || this.sessionData === '') {
            this.router.navigate(['dashboard']);
        }
        else if (window.location.pathname.indexOf('login') !== -1 || window.location.pathname.indexOf('/') !== -1) {
            this.sharedService.isFullScreen = false;
            return true;
        }
        else {
            this.router.navigate(['dashboard']);
            return false;
        }
        return true;
    };
    AuthGuardService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _app_session_service__WEBPACK_IMPORTED_MODULE_3__["SessionService"],
            _shared_shared_service__WEBPACK_IMPORTED_MODULE_4__["SharedService"]])
    ], AuthGuardService);
    return AuthGuardService;
}());



/***/ }),

/***/ "./src/app/bulk-load/bulk-load.component.html":
/*!****************************************************!*\
  !*** ./src/app/bulk-load/bulk-load.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"job_header\" class=\"content-header\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-6 padding-left-5\">\r\n      <input kendoInput type=\"text\" id=\"bulkJobName\" class=\"job_name\" placeholder=\"New Bulk Job\" [(ngModel)]=\"bulkJob.name\" />\r\n    </div>\r\n    <div kendoTooltip class=\"col-sm-6 padding-right-5\" >\r\n      <button kendoButton class=\"pull-right k-button-default\" id=\"saveBtn\" (click)=\"saveBulkJob()\" [disabled]=\"bulkJob.name===''\">\r\n        <svg title=\"Save Job\" class=\"icon_connection\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n          <use xlink:href=\"./assets/dist/img/icons.svg#icon_save\"></use>\r\n        </svg>\r\n      </button>\r\n    </div>\r\n  </div>\r\n</section>\r\n<section id=\"job_content\">\r\n  <div class=\"box box-warning box-solid\" *ngIf=\"isSyncAble\" style=\"box-shadow: 0px 5px 17px 1px rgba(0,0 ,0, 0.5);\r\n    position: absolute;z-index: 100;width: 400px;left: 45%;\">\r\n    <div class=\"box-body\" style=\"margin: 0;font-size: 12px;\">\r\n      <div *ngIf=\"isSyncAble\">\r\n        <p>Found few column in source table. Do you want to sync with target table metadata?\r\n        </p>\r\n        <div class=\"box-tools pull-right\">\r\n          <button kendoButton [primary]=\"true\" (click)=\"getUpdatedTableMetadata()\">Ok\r\n          </button>&nbsp;\r\n          <button type=\"button\" class=\"btn btn-default btn-box-tool\" (click)=\"isSyncAble = !isSyncAble\"\r\n            data-widget=\"collapse\">Cancel\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- /.box-body -->\r\n  </div>\r\n  <div class=\"col-sm-3 connection\">\r\n  \r\n    <div class=\"row connection_header\" id=\"sourceConnection\">\r\n      <div class=\"col-lg-12\" style=\"padding: 0 14px 2px 1px\">\r\n        <kendo-dropdownlist [data]=\"sourceConnections\" class=\"custom_dpd\" height=\"40\" [filterable]=\"true\"\r\n          [textField]=\"'name'\" [valueField]=\"'connectionId'\" (filterChange)=\"filterSourceConnection($event)\"\r\n          [(ngModel)]=\"sourceConnectionValue\" (selectionChange)=\"selectionChangeSourceConnection($event)\"\r\n          *ngIf=\"!isSourceConnectionConnected\">\r\n        </kendo-dropdownlist>\r\n      </div>\r\n    </div>\r\n   \r\n    <div cdkDropList #todoList=\"cdkDropList\" [cdkDropListData]=\"sourceCollection\" [cdkDropListConnectedTo]=\"[doneList]\"\r\n      class=\"collection-list\" (cdkDropListDropped)=\"drop($event)\">\r\n      <div class=\"collection-box\" *ngFor=\"let item of sourceCollection\" cdkDrag>{{item.text}}</div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-sm-3 connection\">\r\n    <div class=\"row connection_header\" style=\"height: 46px;\">\r\n      <div class=\"col-lg-12 padding-left-5\">\r\n        <h4 style=\"font-size: 16px; margin-top: 10px\">Selected Collections</h4>\r\n      </div>\r\n    </div>\r\n    <div cdkDropList #doneList=\"cdkDropList\" [cdkDropListData]=\"done\" [cdkDropListConnectedTo]=\"[todoList]\"\r\n      class=\"collection-list\" (cdkDropListDropped)=\"drop($event)\">\r\n      <div class=\"collection-box\" *ngFor=\"let item of done\" cdkDrag>{{item.text}}</div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-sm-6 connection\">\r\n    <div class=\"row connection_header\" id=\"destinationCollection\" style=\"height: 46px;\">\r\n      <div class=\"col-md-12 padding-left-5\">\r\n        <kendo-dropdownlist [data]=\"destinationConnections\" class=\"custom_dpd\" height=\"40\" [filterable]=\"true\"\r\n          [textField]=\"'name'\" [valueField]=\"'connectionId'\" (filterChange)=\"filterDestinationConnection($event)\"\r\n          [(ngModel)]=\"destinationConnectionValue\" (selectionChange)=\"selectionChangeDestinationConnection($event)\"\r\n          *ngIf=\"!isDestinationConnectionConnected\">\r\n        </kendo-dropdownlist>\r\n      </div>\r\n      <div class=\"col-md-4\" id=\"prefix\">\r\n        <input kendoInput type=\"text\" class=\"job_name\" placeholder=\"Prefix\"\r\n          style=\"border: 1px solid #d7d7d7;padding: 3px;\" [(ngModel)]=\"bulkJob.prefix\" />\r\n      </div>\r\n      <div class=\"col-md-4\" id=\"schedular\">\r\n        <input kendoInput type=\"text\" ngxDaterangepickerMd [isInvalidDate]=\"isInvalidDate\" [isCustomDate]=\"isCustomDate\"\r\n          [locale]=\"{format: 'MM/DD/YYYY HH:mm:ss', firstDay: 1, clearLabel: 'Clear'}\" (change)=\"getSubmittedDateTimeRange($event)\"\r\n          [(ngModel)]=\"selectedEffectiveDate\" [singleDatePicker]=\"true\" [autoApply]=\"true\" [timePicker]=\"true\" [timePicker24Hour]=\"true\" [timePickerSeconds]=\"true\"/>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"col-md-12\" style=\"margin-top: 5px\">\r\n        <!-- <cron-editor [(cron)]=\"cronExpression\" [disabled]=\"isCronDisabled\" [(options)]=\"cronOptions\"></cron-editor> -->\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n<div class=\"overlay\" *ngIf=\"isProcessing\">\r\n  <i class=\"fa fa-refresh fa-spin\"></i>\r\n</div>"

/***/ }),

/***/ "./src/app/bulk-load/bulk-load.component.scss":
/*!****************************************************!*\
  !*** ./src/app/bulk-load/bulk-load.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".content-wrapper {\n  padding-top: 0px !important; }\n\n#job_header {\n  margin: 10px 15px;\n  background: #fff;\n  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);\n  border-radius: 0.25rem;\n  padding: 5px 15px 4px 15px; }\n\n#job_header kendo-popup {\n    position: absolute;\n    width: 185px;\n    top: 39px;\n    left: 15px; }\n\n#job_header kendo-popup.scheduler-popup {\n    right: 5px;\n    width: 550px !important;\n    left: unset !important;\n    top: 46px !important;\n    position: absolute;\n    padding: 10px;\n    background: #fff;\n    box-shadow: 0px 3px 3px 0px #ccc;\n    right: 20%; }\n\n#job_header kendo-popup.scheduler-popup hr {\n      margin: 0px; }\n\n#job_header kendo-popup.scheduler-popup p {\n      margin: 0px;\n      padding: 10px 10px 0px 10px; }\n\n#job_header .job_name {\n    border: 0px;\n    border: solid 1px #e7e7e7;\n    font-size: 20px;\n    height: 40px;\n    outline: none;\n    padding: 0px 10px;\n    text-transform: capitalize;\n    width: 100%; }\n\n#job_header .job_name :visited {\n      background-color: #ffffff; }\n\n#job_header .job_name:hover {\n    background-color: #ffffff; }\n\n#job_header .job_name:hover ::-webkit-input-placeholder {\n      color: white; }\n\n#job_header .job_name::-webkit-input-placeholder {\n    font-size: 20px; }\n\n#job_header .job_name::-moz-placeholder {\n    font-size: 20px; }\n\n#job_header .job_name::-ms-input-placeholder {\n    font-size: 20px; }\n\n#job_header .job_name::placeholder {\n    font-size: 20px; }\n\n#job_header button {\n    margin: 5px 8px; }\n\n#job_header h3,\n  #job_header p {\n    margin: 0px 0px 0px -7px; }\n\n#job_header .content {\n    padding: 30px;\n    color: #787878;\n    background-color: #fcf7f8;\n    border: 1px solid rgba(0, 0, 0, 0.05); }\n\n#job_content {\n  margin: 10px 15px;\n  position: relative;\n  display: flex;\n  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);\n  border-radius: 0.25rem;\n  background: white; }\n\n#job_content .connection {\n    height: 79vh;\n    padding: 0px 10px; }\n\n#job_content .connection .connection_header {\n      padding: 0px 0px 0px 5px; }\n\n#job_content .connection .connection_header .col-lg-11 {\n        padding-left: 5px;\n        padding-right: 0px; }\n\n#job_content .connection .connection_header kendo-dropdownlist {\n        margin: 8px 5px;\n        width: 100%;\n        background: transparent; }\n\n#job_content .connection .connection_header button {\n        margin: 8px 0px 0px 0px;\n        background: transparent;\n        padding: 4px;\n        border-width: 1px; }\n\n#job_content .connection .connection_header button:hover {\n        background-color: #f1f1ff; }\n\n#job_content .connection .connection_message {\n      height: auto; }\n\n#job_content .connection .connection_message div.message1,\n      #job_content .connection .connection_message div.message2 {\n        margin-top: 40%;\n        padding: 0px 35px;\n        text-align: center; }\n\n#job_content .connection .connection_message div.message1 > svg.icon_add_item {\n        margin: 0px 5px;\n        position: absolute;\n        width: 12px; }\n\n#job_content .connection kendo-popup.source-connection-option {\n      right: 5px;\n      width: 156px !important;\n      left: unset !important;\n      top: 46px !important;\n      position: absolute; }\n\n#job_content .connection kendo-popup.source-connection-option > .k-popup {\n      width: 160px; }\n\n#job_content .connection kendo-popup.destination-connection-option {\n      right: 5px;\n      width: 156px !important;\n      left: unset !important;\n      top: 46px !important; }\n\n#job_content .collection {\n    margin-top: 10px;\n    height: 70vh;\n    overflow-y: auto;\n    overflow-x: hidden; }\n\n#job_content .collection::-webkit-scrollbar-track {\n      -webkit-box-shadow: inset 0 0 6px #ffffff;\n      background-color: #ffffff; }\n\n#job_content .collection::-webkit-scrollbar {\n      width: 6px;\n      background-color: #ffffff; }\n\n#job_content .collection::-webkit-scrollbar-thumb {\n      background-color: #9ea5ab; }\n\n#job_content .collection > .name {\n      background: #f0f2f3;\n      font-size: 13px;\n      padding: 10px;\n      word-break: break-all; }\n\n#job_content .collection > .name .option {\n        border-radius: 50%;\n        float: right;\n        margin-top: -1px;\n        width: 30px; }\n\n#job_content .collection > .name > input {\n        background-color: white;\n        margin-top: 0px;\n        width: 94%; }\n\n#job_content .collection > .name > button > svg {\n        height: 18px;\n        cursor: pointer; }\n\n#job_content .collection .parameters,\n    #job_content .collection .columns {\n      margin: 5px;\n      min-height: 80px;\n      border: solid 1px #e1e1e1;\n      border-radius: 5px;\n      padding: 10px; }\n\n#job_content .collection .parameters.parameters:hover,\n      #job_content .collection .columns.parameters:hover {\n        background-color: #f9f9f9;\n        cursor: move; }\n\n#job_content .collection .parameters .edit_icon,\n      #job_content .collection .columns .edit_icon {\n        border-radius: 50%;\n        float: right;\n        margin-top: -6px;\n        width: 30px; }\n\n#job_content .collection .parameters .name,\n      #job_content .collection .columns .name {\n        font-weight: bold;\n        font-size: 12px; }\n\n#job_content .collection .parameters .name button.okBtn,\n        #job_content .collection .columns .name button.okBtn {\n          float: right;\n          margin-right: 0px;\n          margin-top: 2px; }\n\n#job_content .collection .parameters .type,\n      #job_content .collection .columns .type {\n        font-size: 11.5px;\n        margin-top: 8px;\n        text-transform: uppercase; }\n\n#job_content .collection .parameters .type button > svg,\n        #job_content .collection .columns .type button > svg {\n          height: 12px;\n          cursor: pointer; }\n\n#job_content .collection .parameters .mapping > input,\n      #job_content .collection .columns .mapping > input {\n        width: 100%;\n        font-size: 12px; }\n\n#job_content .collection .parameters .option,\n      #job_content .collection .columns .option {\n        border-radius: 50%;\n        float: unset;\n        margin-left: 5px;\n        width: 20px;\n        padding: 1px;\n        height: 20px;\n        border: 0;\n        display: contents; }\n\n#job_content .collection .parameters input,\n      #job_content .collection .parameters kendo-dropdownlist,\n      #job_content .collection .columns input,\n      #job_content .collection .columns kendo-dropdownlist {\n        width: 95%; }\n\n#job_content .collection .parameters button > svg,\n      #job_content .collection .columns button > svg {\n        height: 12px;\n        cursor: pointer;\n        fill: #666; }\n\n#job_content .collection .parameters label.title,\n      #job_content .collection .columns label.title {\n        font-size: 10px;\n        margin-bottom: 0px;\n        width: 100%;\n        text-transform: uppercase; }\n\n#job_content .collection .parameters input.column_name,\n      #job_content .collection .parameters input.column_mapping,\n      #job_content .collection .columns input.column_name,\n      #job_content .collection .columns input.column_mapping {\n        border: solid 1px #cccccc;\n        border-radius: 3px;\n        background: #ffffff;\n        font-weight: normal;\n        margin-top: 0px !important;\n        margin-bottom: 6px !important;\n        outline: none;\n        padding: 3px;\n        width: 95%; }\n\n#job_content .collection .parameters .column_type,\n      #job_content .collection .columns .column_type {\n        width: 100%;\n        margin-bottom: 6px !important; }\n\n#job_content .collection .columns:hover .option {\n      display: contents; }\n\n#job_content .collection input {\n      border: solid 1px #cccccc;\n      border-radius: 3px;\n      background: #f1f1f1;\n      margin-top: 6px;\n      outline: none;\n      padding: 3px;\n      width: 100%; }\n\n#job_content .paramerter > .type {\n    font-size: 12px;\n    margin-top: 10px; }\n\n.k-grid {\n  font-size: 12px; }\n\n.nav > li > a {\n  padding-left: 30px;\n  font-size: 12px;\n  text-align: left; }\n\n.nav > li:hover,\n.nav > li:hover a,\n.nav > li:hover a > svg {\n  background: #0067c5;\n  color: white;\n  fill: white; }\n\n.nav > li > a > svg {\n  position: absolute;\n  left: 8px; }\n\n.overlay {\n  background: rgba(0, 0, 0, 0.2);\n  width: 100%;\n  height: 85vh;\n  z-index: 100000;\n  position: absolute; }\n\n.overlay > .fa {\n  position: absolute;\n  top: 50%;\n  left: 42%;\n  margin-left: -15px;\n  margin-top: -15px;\n  color: #000;\n  font-size: 30px; }\n\n.overlay > p {\n  position: absolute;\n  top: 65%;\n  left: 49%;\n  margin-left: -15px;\n  margin-top: -15px;\n  color: #000;\n  font-size: 12px; }\n\n.overlay > p.loading {\n  left: 49%; }\n\n.overlay > p.processing {\n  left: 49%; }\n\n.overlay > p.validating {\n  left: 49%; }\n\n.top-Margin {\n  margin-top: 10px;\n  margin-left: 403px; }\n\n.anchor {\n  display: block;\n  width: 120px;\n  margin: 200px auto; }\n\n.scrolling-wrapper-flexbox {\n  display: flex;\n  flex-wrap: nowrap;\n  overflow-x: auto; }\n\n.scrolling-wrapper-flexbox .card {\n    flex: 0 0 auto; }\n\ntable.d_filter {\n  font-size: 10px; }\n\ntable.d_filter > tbody > tr > td {\n  padding: 2px;\n  line-height: 2.4em; }\n\n.error {\n  background-color: #ffbebe !important; }\n\n.padding-left-5 {\n  padding-left: 5px; }\n\n.padding-right-5 {\n  padding-right: 5px; }\n\n.k-grid {\n  font-size: 12px; }\n\n.nav > li > a {\n  padding-left: 30px;\n  font-size: 12px;\n  text-align: left; }\n\n.nav > li:hover,\n.nav > li:hover a,\n.nav > li:hover a > svg {\n  background: #0067c5;\n  color: white;\n  fill: white; }\n\n.nav > li > a > svg {\n  position: absolute;\n  left: 8px; }\n\n.overlay {\n  background: rgba(0, 0, 0, 0.2);\n  width: 100%;\n  height: 85vh;\n  z-index: 100000;\n  position: absolute; }\n\n.overlay > .fa {\n  position: absolute;\n  top: 50%;\n  left: 42%;\n  margin-left: -15px;\n  margin-top: -15px;\n  color: #000;\n  font-size: 30px; }\n\n.overlay > p {\n  position: absolute;\n  top: 65%;\n  left: 49%;\n  margin-left: -15px;\n  margin-top: -15px;\n  color: #000;\n  font-size: 12px; }\n\n.overlay > p.loading {\n  left: 49%; }\n\n.overlay > p.processing {\n  left: 49%; }\n\n.overlay > p.validating {\n  left: 49%; }\n\n.top-Margin {\n  margin-top: 10px;\n  margin-left: 403px; }\n\n.anchor {\n  display: block;\n  width: 120px;\n  margin: 200px auto; }\n\n.scrolling-wrapper-flexbox {\n  display: flex;\n  flex-wrap: nowrap;\n  overflow-x: auto; }\n\n.scrolling-wrapper-flexbox .card {\n    flex: 0 0 auto; }\n\ntable.d_filter {\n  font-size: 10px; }\n\ntable.d_filter > tbody > tr > td {\n  padding: 2px;\n  line-height: 2.4em; }\n\n.error {\n  background-color: #ffbebe !important; }\n\n.example-container {\n  width: 400px;\n  max-width: 100%;\n  margin: 0 25px 25px 0;\n  display: inline-block;\n  vertical-align: top; }\n\n.collection-list {\n  border: solid 1px #ccc;\n  min-height: 60px;\n  background: white;\n  border-radius: 4px;\n  overflow: hidden;\n  display: block;\n  margin-left: -9px;\n  margin-right: -9px;\n  height: 91%;\n  box-shadow: 0px 0px 5px 1px inset #e8e3e3;\n  overflow-y: scroll; }\n\n.collection-box {\n  padding: 10px 10px;\n  border-bottom: solid 1px #ccc;\n  color: rgba(0, 0, 0, 0.87);\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  justify-content: space-between;\n  box-sizing: border-box;\n  cursor: move;\n  background: white;\n  font-size: 14px;\n  margin: 5px;\n  word-break: break-all; }\n\n.cdk-drag-preview {\n  box-sizing: border-box;\n  border-radius: 4px;\n  box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2), 0 8px 10px 1px rgba(0, 0, 0, 0.14), 0 3px 14px 2px rgba(0, 0, 0, 0.12); }\n\n.cdk-drag-placeholder {\n  opacity: 0; }\n\n.cdk-drag-animating {\n  transition: -webkit-transform 250ms cubic-bezier(0, 0, 0.2, 1);\n  transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\n  transition: transform 250ms cubic-bezier(0, 0, 0.2, 1), -webkit-transform 250ms cubic-bezier(0, 0, 0.2, 1); }\n\n.collection-box:last-child {\n  border: none; }\n\n.collection-list.cdk-drop-list-dragging .collection-box:not(.cdk-drag-placeholder) {\n  transition: -webkit-transform 250ms cubic-bezier(0, 0, 0.2, 1);\n  transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\n  transition: transform 250ms cubic-bezier(0, 0, 0.2, 1), -webkit-transform 250ms cubic-bezier(0, 0, 0.2, 1); }\n\n::-webkit-scrollbar {\n  width: 0.5em; }\n\n::-webkit-scrollbar-track {\n  -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3); }\n\n::-webkit-scrollbar-thumb {\n  background-color: darkgrey;\n  outline: 1px solid slategrey; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9idWxrLWxvYWQvRTpcXFVJLVByb2plY3RcXEVUTFxcc3JjXFxzcmMvYXBwXFxidWxrLWxvYWRcXGJ1bGstbG9hZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLDJCQUEyQixFQUFBOztBQUU3QjtFQUNFLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsd0NBQXdDO0VBQ3hDLHNCQUFzQjtFQUN0QiwwQkFBMEIsRUFBQTs7QUFMNUI7SUFPSSxrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLFNBQVM7SUFDVCxVQUFVLEVBQUE7O0FBVmQ7SUFhSSxVQUFVO0lBQ1YsdUJBQXVCO0lBQ3ZCLHNCQUFzQjtJQUN0QixvQkFBb0I7SUFDcEIsa0JBQWtCO0lBQ2xCLGFBQWE7SUFDYixnQkFBZ0I7SUFDaEIsZ0NBQWdDO0lBQ2hDLFVBQVUsRUFBQTs7QUFyQmQ7TUF1Qk0sV0FBVyxFQUFBOztBQXZCakI7TUEwQk0sV0FBVztNQUNYLDJCQUEyQixFQUFBOztBQTNCakM7SUErQkksV0FBVztJQUNYLHlCQUF5QjtJQUN6QixlQUFlO0lBQ2YsWUFBWTtJQUNaLGFBQWE7SUFDYixpQkFBaUI7SUFDakIsMEJBQTBCO0lBQzFCLFdBQVcsRUFBQTs7QUF0Q2Y7TUF3Q00seUJBQXlCLEVBQUE7O0FBeEMvQjtJQTZDSSx5QkFBeUIsRUFBQTs7QUE3QzdCO01BK0NNLFlBQVksRUFBQTs7QUEvQ2xCO0lBb0RJLGVBQWUsRUFBQTs7QUFwRG5CO0lBb0RJLGVBQWUsRUFBQTs7QUFwRG5CO0lBb0RJLGVBQWUsRUFBQTs7QUFwRG5CO0lBb0RJLGVBQWUsRUFBQTs7QUFwRG5CO0lBd0RJLGVBQWUsRUFBQTs7QUF4RG5COztJQTZESSx3QkFBd0IsRUFBQTs7QUE3RDVCO0lBZ0VJLGFBQWE7SUFDYixjQUFjO0lBQ2QseUJBQXlCO0lBQ3pCLHFDQUFxQyxFQUFBOztBQUd6QztFQUNFLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLHdDQUF3QztFQUN4QyxzQkFBc0I7RUFDdEIsaUJBQWlCLEVBQUE7O0FBTm5CO0lBU0ksWUFBWTtJQUNaLGlCQUFpQixFQUFBOztBQVZyQjtNQVlNLHdCQUF3QixFQUFBOztBQVo5QjtRQWdCUSxpQkFBaUI7UUFDakIsa0JBQWtCLEVBQUE7O0FBakIxQjtRQW9CUSxlQUFlO1FBQ2YsV0FBVztRQUNYLHVCQUF1QixFQUFBOztBQXRCL0I7UUF5QlEsdUJBQXVCO1FBQ3ZCLHVCQUF1QjtRQUN2QixZQUFZO1FBQ1osaUJBQWlCLEVBQUE7O0FBNUJ6QjtRQStCUSx5QkFBeUIsRUFBQTs7QUEvQmpDO01BbUNNLFlBQVksRUFBQTs7QUFuQ2xCOztRQXNDUSxlQUFlO1FBQ2YsaUJBQWlCO1FBQ2pCLGtCQUFrQixFQUFBOztBQXhDMUI7UUEyQ1EsZUFBZTtRQUNmLGtCQUFrQjtRQUNsQixXQUFXLEVBQUE7O0FBN0NuQjtNQWlETSxVQUFVO01BQ1YsdUJBQXVCO01BQ3ZCLHNCQUFzQjtNQUN0QixvQkFBb0I7TUFDcEIsa0JBQWtCLEVBQUE7O0FBckR4QjtNQXdETSxZQUFZLEVBQUE7O0FBeERsQjtNQTJETSxVQUFVO01BQ1YsdUJBQXVCO01BQ3ZCLHNCQUFzQjtNQUN0QixvQkFBb0IsRUFBQTs7QUE5RDFCO0lBa0VJLGdCQUFnQjtJQUNoQixZQUFZO0lBRVosZ0JBQWdCO0lBQ2hCLGtCQUFrQixFQUFBOztBQXRFdEI7TUF3RU0seUNBQXlDO01BQ3pDLHlCQUF5QixFQUFBOztBQXpFL0I7TUE2RU0sVUFBVTtNQUNWLHlCQUF5QixFQUFBOztBQTlFL0I7TUFrRk0seUJBQW9DLEVBQUE7O0FBbEYxQztNQXFGTSxtQkFBbUI7TUFDbkIsZUFBZTtNQUNmLGFBQWE7TUFDYixxQkFBcUIsRUFBQTs7QUF4RjNCO1FBMEZRLGtCQUFrQjtRQUNsQixZQUFZO1FBQ1osZ0JBQWdCO1FBQ2hCLFdBQVcsRUFBQTs7QUE3Rm5CO1FBZ0dRLHVCQUF1QjtRQUN2QixlQUFlO1FBQ2YsVUFBVSxFQUFBOztBQWxHbEI7UUFxR1EsWUFBWTtRQUNaLGVBQWUsRUFBQTs7QUF0R3ZCOztNQTJHTSxXQUFXO01BQ1gsZ0JBQWdCO01BQ2hCLHlCQUF5QjtNQUN6QixrQkFBa0I7TUFDbEIsYUFBYSxFQUFBOztBQS9HbkI7O1FBaUhRLHlCQUF5QjtRQUN6QixZQUFZLEVBQUE7O0FBbEhwQjs7UUFxSFEsa0JBQWtCO1FBQ2xCLFlBQVk7UUFDWixnQkFBZ0I7UUFDaEIsV0FBVyxFQUFBOztBQXhIbkI7O1FBMkhRLGlCQUFpQjtRQUNqQixlQUFlLEVBQUE7O0FBNUh2Qjs7VUErSFUsWUFBWTtVQUNaLGlCQUFpQjtVQUNqQixlQUFlLEVBQUE7O0FBakl6Qjs7UUFxSVEsaUJBQWlCO1FBQ2pCLGVBQWU7UUFDZix5QkFBeUIsRUFBQTs7QUF2SWpDOztVQTBJVSxZQUFZO1VBQ1osZUFBZSxFQUFBOztBQTNJekI7O1FBZ0pVLFdBQVc7UUFDWCxlQUFlLEVBQUE7O0FBakp6Qjs7UUFxSlEsa0JBQWtCO1FBQ2xCLFlBQVk7UUFDWixnQkFBZ0I7UUFDaEIsV0FBVztRQUNYLFlBQVk7UUFDWixZQUFZO1FBQ1osU0FBUztRQUNULGlCQUFpQixFQUFBOztBQTVKekI7Ozs7UUFnS1EsVUFBVSxFQUFBOztBQWhLbEI7O1FBbUtRLFlBQVk7UUFDWixlQUFlO1FBQ2YsVUFBVSxFQUFBOztBQXJLbEI7O1FBd0tRLGVBQWU7UUFDZixrQkFBa0I7UUFDbEIsV0FBVztRQUNYLHlCQUF5QixFQUFBOztBQTNLakM7Ozs7UUErS1EseUJBQXlCO1FBQ3pCLGtCQUFrQjtRQUNsQixtQkFBbUI7UUFDbkIsbUJBQW1CO1FBQ25CLDBCQUEwQjtRQUMxQiw2QkFBNkI7UUFDN0IsYUFBYTtRQUNiLFlBQVk7UUFDWixVQUFVLEVBQUE7O0FBdkxsQjs7UUEwTFEsV0FBVztRQUNYLDZCQUE2QixFQUFBOztBQTNMckM7TUErTE0saUJBQWlCLEVBQUE7O0FBL0x2QjtNQWtNTSx5QkFBeUI7TUFDekIsa0JBQWtCO01BQ2xCLG1CQUFtQjtNQUNuQixlQUFlO01BQ2YsYUFBYTtNQUNiLFlBQVk7TUFDWixXQUFXLEVBQUE7O0FBeE1qQjtJQTRNSSxlQUFlO0lBQ2YsZ0JBQWdCLEVBQUE7O0FBR3BCO0VBQ0UsZUFBZSxFQUFBOztBQUVqQjtFQUNFLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsZ0JBQWdCLEVBQUE7O0FBRWxCOzs7RUFHRSxtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLFdBQVcsRUFBQTs7QUFFYjtFQUNFLGtCQUFrQjtFQUNsQixTQUFTLEVBQUE7O0FBRVg7RUFDRSw4QkFBOEI7RUFDOUIsV0FBVztFQUNYLFlBQVk7RUFDWixlQUFlO0VBQ2Ysa0JBQWtCLEVBQUE7O0FBRXBCO0VBQ0Usa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixTQUFTO0VBQ1Qsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixXQUFXO0VBQ1gsZUFBZSxFQUFBOztBQUVqQjtFQUNFLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsU0FBUztFQUNULGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsV0FBVztFQUNYLGVBQWUsRUFBQTs7QUFFakI7RUFDRSxTQUFTLEVBQUE7O0FBR1g7RUFDRSxTQUFTLEVBQUE7O0FBR1g7RUFDRSxTQUFTLEVBQUE7O0FBRVg7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCLEVBQUE7O0FBRXBCO0VBQ0UsY0FBYztFQUNkLFlBQVk7RUFDWixrQkFBa0IsRUFBQTs7QUFFcEI7RUFDRSxhQUFhO0VBQ2IsaUJBQWlCO0VBQ2pCLGdCQUFnQixFQUFBOztBQUhsQjtJQU1JLGNBQWMsRUFBQTs7QUFHbEI7RUFDRSxlQUFlLEVBQUE7O0FBRWpCO0VBQ0UsWUFBWTtFQUNaLGtCQUFrQixFQUFBOztBQUVwQjtFQUNFLG9DQUErQyxFQUFBOztBQUVqRDtFQUNFLGlCQUFpQixFQUFBOztBQUVuQjtFQUNFLGtCQUFrQixFQUFBOztBQUdwQjtFQUNFLGVBQWUsRUFBQTs7QUFFakI7RUFDRSxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGdCQUFnQixFQUFBOztBQUVsQjs7O0VBR0UsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixXQUFXLEVBQUE7O0FBRWI7RUFDRSxrQkFBa0I7RUFDbEIsU0FBUyxFQUFBOztBQUVYO0VBQ0UsOEJBQThCO0VBQzlCLFdBQVc7RUFDWCxZQUFZO0VBQ1osZUFBZTtFQUNmLGtCQUFrQixFQUFBOztBQUVwQjtFQUNFLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsU0FBUztFQUNULGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsV0FBVztFQUNYLGVBQWUsRUFBQTs7QUFFakI7RUFDRSxrQkFBa0I7RUFDbEIsUUFBUTtFQUNSLFNBQVM7RUFDVCxrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLFdBQVc7RUFDWCxlQUFlLEVBQUE7O0FBRWpCO0VBQ0UsU0FBUyxFQUFBOztBQUdYO0VBQ0UsU0FBUyxFQUFBOztBQUdYO0VBQ0UsU0FBUyxFQUFBOztBQUVYO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQixFQUFBOztBQUVwQjtFQUNFLGNBQWM7RUFDZCxZQUFZO0VBQ1osa0JBQWtCLEVBQUE7O0FBRXBCO0VBQ0UsYUFBYTtFQUNiLGlCQUFpQjtFQUNqQixnQkFBZ0IsRUFBQTs7QUFIbEI7SUFNSSxjQUFjLEVBQUE7O0FBR2xCO0VBQ0UsZUFBZSxFQUFBOztBQUVqQjtFQUNFLFlBQVk7RUFDWixrQkFBa0IsRUFBQTs7QUFFcEI7RUFDRSxvQ0FBK0MsRUFBQTs7QUFFakQ7RUFDRSxZQUFZO0VBQ1osZUFBZTtFQUNmLHFCQUFxQjtFQUNyQixxQkFBcUI7RUFDckIsbUJBQW1CLEVBQUE7O0FBR3JCO0VBQ0Usc0JBQXNCO0VBQ3RCLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gseUNBQXlDO0VBQ3pDLGtCQUFrQixFQUFBOztBQUdwQjtFQUNFLGtCQUFrQjtFQUNsQiw2QkFBNkI7RUFDN0IsMEJBQTBCO0VBQzFCLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLDhCQUE4QjtFQUM5QixzQkFBc0I7RUFDdEIsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsV0FBVztFQUNYLHFCQUFxQixFQUFBOztBQUd2QjtFQUNFLHNCQUFzQjtFQUN0QixrQkFBa0I7RUFDbEIscUhBQXFILEVBQUE7O0FBR3ZIO0VBQ0UsVUFBVSxFQUFBOztBQUdaO0VBQ0UsOERBQXNEO0VBQXRELHNEQUFzRDtFQUF0RCwwR0FBc0QsRUFBQTs7QUFHeEQ7RUFDRSxZQUFZLEVBQUE7O0FBR2Q7RUFDRSw4REFBc0Q7RUFBdEQsc0RBQXNEO0VBQXRELDBHQUFzRCxFQUFBOztBQUV4RDtFQUNFLFlBQVksRUFBQTs7QUFFZDtFQUNFLG9EQUFpRCxFQUFBOztBQUVuRDtFQUNFLDBCQUEwQjtFQUMxQiw0QkFBNEIsRUFBQSIsImZpbGUiOiJhcHAvYnVsay1sb2FkL2J1bGstbG9hZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250ZW50LXdyYXBwZXIge1xyXG4gIHBhZGRpbmctdG9wOiAwcHggIWltcG9ydGFudDtcclxufVxyXG4jam9iX2hlYWRlciB7XHJcbiAgbWFyZ2luOiAxMHB4IDE1cHg7XHJcbiAgYmFja2dyb3VuZDogI2ZmZjtcclxuICBib3gtc2hhZG93OiAwIDJweCA1cHggcmdiYSgwLCAwLCAwLCAwLjEpO1xyXG4gIGJvcmRlci1yYWRpdXM6IDAuMjVyZW07XHJcbiAgcGFkZGluZzogNXB4IDE1cHggNHB4IDE1cHg7XHJcbiAga2VuZG8tcG9wdXAge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgd2lkdGg6IDE4NXB4O1xyXG4gICAgdG9wOiAzOXB4O1xyXG4gICAgbGVmdDogMTVweDtcclxuICB9XHJcbiAga2VuZG8tcG9wdXAuc2NoZWR1bGVyLXBvcHVwIHtcclxuICAgIHJpZ2h0OiA1cHg7XHJcbiAgICB3aWR0aDogNTUwcHggIWltcG9ydGFudDtcclxuICAgIGxlZnQ6IHVuc2V0ICFpbXBvcnRhbnQ7XHJcbiAgICB0b3A6IDQ2cHggIWltcG9ydGFudDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDNweCAzcHggMHB4ICNjY2M7XHJcbiAgICByaWdodDogMjAlO1xyXG4gICAgaHIge1xyXG4gICAgICBtYXJnaW46IDBweDtcclxuICAgIH1cclxuICAgIHAge1xyXG4gICAgICBtYXJnaW46IDBweDtcclxuICAgICAgcGFkZGluZzogMTBweCAxMHB4IDBweCAxMHB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAuam9iX25hbWUge1xyXG4gICAgYm9yZGVyOiAwcHg7XHJcbiAgICBib3JkZXI6IHNvbGlkIDFweCAjZTdlN2U3O1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHBhZGRpbmc6IDBweCAxMHB4O1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIDp2aXNpdGVkIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5qb2JfbmFtZTpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgOjotd2Via2l0LWlucHV0LXBsYWNlaG9sZGVyIHtcclxuICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLmpvYl9uYW1lOjpwbGFjZWhvbGRlciB7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgfVxyXG5cclxuICBidXR0b24ge1xyXG4gICAgbWFyZ2luOiA1cHggOHB4O1xyXG4gIH1cclxuXHJcbiAgaDMsXHJcbiAgcCB7XHJcbiAgICBtYXJnaW46IDBweCAwcHggMHB4IC03cHg7XHJcbiAgfVxyXG4gIC5jb250ZW50IHtcclxuICAgIHBhZGRpbmc6IDMwcHg7XHJcbiAgICBjb2xvcjogIzc4Nzg3ODtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmY2Y3Zjg7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMDUpO1xyXG4gIH1cclxufVxyXG4jam9iX2NvbnRlbnQge1xyXG4gIG1hcmdpbjogMTBweCAxNXB4O1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGJveC1zaGFkb3c6IDAgMnB4IDVweCByZ2JhKDAsIDAsIDAsIDAuMSk7XHJcbiAgYm9yZGVyLXJhZGl1czogMC4yNXJlbTtcclxuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAuY29ubmVjdGlvbiB7XHJcbiAgICAvLyBib3JkZXI6IHNvbGlkIDFweCAjZjFmMWYxO1xyXG4gICAgaGVpZ2h0OiA3OXZoO1xyXG4gICAgcGFkZGluZzogMHB4IDEwcHg7XHJcbiAgICAuY29ubmVjdGlvbl9oZWFkZXIge1xyXG4gICAgICBwYWRkaW5nOiAwcHggMHB4IDBweCA1cHg7XHJcbiAgICAgIC8vIGJvcmRlci1ib3R0b206IHNvbGlkIDFweCAjZjFmMWYxO1xyXG4gICAgICAvLyBib3gtc2hhZG93OiAwcHggMHB4IDEwcHggMHB4ICNmMWYxZjE7XHJcbiAgICAgIC5jb2wtbGctMTEge1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDBweDtcclxuICAgICAgfVxyXG4gICAgICBrZW5kby1kcm9wZG93bmxpc3Qge1xyXG4gICAgICAgIG1hcmdpbjogOHB4IDVweDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgICAgfVxyXG4gICAgICBidXR0b24ge1xyXG4gICAgICAgIG1hcmdpbjogOHB4IDBweCAwcHggMHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgIHBhZGRpbmc6IDRweDtcclxuICAgICAgICBib3JkZXItd2lkdGg6IDFweDtcclxuICAgICAgfVxyXG4gICAgICBidXR0b246aG92ZXIge1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmMWYxZmY7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5jb25uZWN0aW9uX21lc3NhZ2Uge1xyXG4gICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgIGRpdi5tZXNzYWdlMSxcclxuICAgICAgZGl2Lm1lc3NhZ2UyIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiA0MCU7XHJcbiAgICAgICAgcGFkZGluZzogMHB4IDM1cHg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICB9XHJcbiAgICAgIGRpdi5tZXNzYWdlMSA+IHN2Zy5pY29uX2FkZF9pdGVtIHtcclxuICAgICAgICBtYXJnaW46IDBweCA1cHg7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHdpZHRoOiAxMnB4O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBrZW5kby1wb3B1cC5zb3VyY2UtY29ubmVjdGlvbi1vcHRpb24ge1xyXG4gICAgICByaWdodDogNXB4O1xyXG4gICAgICB3aWR0aDogMTU2cHggIWltcG9ydGFudDtcclxuICAgICAgbGVmdDogdW5zZXQgIWltcG9ydGFudDtcclxuICAgICAgdG9wOiA0NnB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIH1cclxuICAgIGtlbmRvLXBvcHVwLnNvdXJjZS1jb25uZWN0aW9uLW9wdGlvbiA+IC5rLXBvcHVwIHtcclxuICAgICAgd2lkdGg6IDE2MHB4O1xyXG4gICAgfVxyXG4gICAga2VuZG8tcG9wdXAuZGVzdGluYXRpb24tY29ubmVjdGlvbi1vcHRpb24ge1xyXG4gICAgICByaWdodDogNXB4O1xyXG4gICAgICB3aWR0aDogMTU2cHggIWltcG9ydGFudDtcclxuICAgICAgbGVmdDogdW5zZXQgIWltcG9ydGFudDtcclxuICAgICAgdG9wOiA0NnB4ICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5jb2xsZWN0aW9uIHtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICBoZWlnaHQ6IDcwdmg7XHJcbiAgICAvLyBib3JkZXI6IHNvbGlkIDFweCAjZjFmMWYxO1xyXG4gICAgb3ZlcmZsb3cteTogYXV0bztcclxuICAgIG92ZXJmbG93LXg6IGhpZGRlbjtcclxuICAgICY6Oi13ZWJraXQtc2Nyb2xsYmFyLXRyYWNrIHtcclxuICAgICAgLXdlYmtpdC1ib3gtc2hhZG93OiBpbnNldCAwIDAgNnB4ICNmZmZmZmY7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgICB9XHJcblxyXG4gICAgJjo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4gICAgICB3aWR0aDogNnB4O1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgfVxyXG5cclxuICAgICY6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDE1OCwgMTY1LCAxNzEpO1xyXG4gICAgfVxyXG4gICAgPiAubmFtZSB7XHJcbiAgICAgIGJhY2tncm91bmQ6ICNmMGYyZjM7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgd29yZC1icmVhazogYnJlYWstYWxsO1xyXG4gICAgICAub3B0aW9uIHtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICAgICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgICAgIG1hcmdpbi10b3A6IC0xcHg7XHJcbiAgICAgICAgd2lkdGg6IDMwcHg7XHJcbiAgICAgIH1cclxuICAgICAgPiBpbnB1dCB7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMHB4O1xyXG4gICAgICAgIHdpZHRoOiA5NCU7XHJcbiAgICAgIH1cclxuICAgICAgPiBidXR0b24gPiBzdmcge1xyXG4gICAgICAgIGhlaWdodDogMThweDtcclxuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5wYXJhbWV0ZXJzLFxyXG4gICAgLmNvbHVtbnMge1xyXG4gICAgICBtYXJnaW46IDVweDtcclxuICAgICAgbWluLWhlaWdodDogODBweDtcclxuICAgICAgYm9yZGVyOiBzb2xpZCAxcHggI2UxZTFlMTtcclxuICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAmLnBhcmFtZXRlcnM6aG92ZXIge1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XHJcbiAgICAgICAgY3Vyc29yOiBtb3ZlO1xyXG4gICAgICB9XHJcbiAgICAgIC5lZGl0X2ljb24ge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgICAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogLTZweDtcclxuICAgICAgICB3aWR0aDogMzBweDtcclxuICAgICAgfVxyXG4gICAgICAubmFtZSB7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG5cclxuICAgICAgICBidXR0b24ub2tCdG4ge1xyXG4gICAgICAgICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgICBtYXJnaW4tdG9wOiAycHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIC50eXBlIHtcclxuICAgICAgICBmb250LXNpemU6IDExLjVweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiA4cHg7XHJcbiAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuXHJcbiAgICAgICAgYnV0dG9uID4gc3ZnIHtcclxuICAgICAgICAgIGhlaWdodDogMTJweDtcclxuICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgLm1hcHBpbmcge1xyXG4gICAgICAgID4gaW5wdXQge1xyXG4gICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIC5vcHRpb24ge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgICAgICBmbG9hdDogdW5zZXQ7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDVweDtcclxuICAgICAgICB3aWR0aDogMjBweDtcclxuICAgICAgICBwYWRkaW5nOiAxcHg7XHJcbiAgICAgICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgICAgIGJvcmRlcjogMDtcclxuICAgICAgICBkaXNwbGF5OiBjb250ZW50cztcclxuICAgICAgfVxyXG4gICAgICBpbnB1dCxcclxuICAgICAga2VuZG8tZHJvcGRvd25saXN0IHtcclxuICAgICAgICB3aWR0aDogOTUlO1xyXG4gICAgICB9XHJcbiAgICAgIGJ1dHRvbiA+IHN2ZyB7XHJcbiAgICAgICAgaGVpZ2h0OiAxMnB4O1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICBmaWxsOiAjNjY2O1xyXG4gICAgICB9XHJcbiAgICAgIGxhYmVsLnRpdGxlIHtcclxuICAgICAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICAgIH1cclxuICAgICAgaW5wdXQuY29sdW1uX25hbWUsXHJcbiAgICAgIGlucHV0LmNvbHVtbl9tYXBwaW5nIHtcclxuICAgICAgICBib3JkZXI6IHNvbGlkIDFweCAjY2NjY2NjO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDNweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNnB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgb3V0bGluZTogbm9uZTtcclxuICAgICAgICBwYWRkaW5nOiAzcHg7XHJcbiAgICAgICAgd2lkdGg6IDk1JTtcclxuICAgICAgfVxyXG4gICAgICAuY29sdW1uX3R5cGUge1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDZweCAhaW1wb3J0YW50O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAuY29sdW1uczpob3ZlciAub3B0aW9uIHtcclxuICAgICAgZGlzcGxheTogY29udGVudHM7XHJcbiAgICB9XHJcbiAgICBpbnB1dCB7XHJcbiAgICAgIGJvcmRlcjogc29saWQgMXB4ICNjY2NjY2M7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDNweDtcclxuICAgICAgYmFja2dyb3VuZDogI2YxZjFmMTtcclxuICAgICAgbWFyZ2luLXRvcDogNnB4O1xyXG4gICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgICBwYWRkaW5nOiAzcHg7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgfVxyXG4gIH1cclxuICAucGFyYW1lcnRlciA+IC50eXBlIHtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgfVxyXG59XHJcbi5rLWdyaWQge1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxufVxyXG4ubmF2ID4gbGkgPiBhIHtcclxuICBwYWRkaW5nLWxlZnQ6IDMwcHg7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbn1cclxuLm5hdiA+IGxpOmhvdmVyLFxyXG4ubmF2ID4gbGk6aG92ZXIgYSxcclxuLm5hdiA+IGxpOmhvdmVyIGEgPiBzdmcge1xyXG4gIGJhY2tncm91bmQ6ICMwMDY3YzU7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIGZpbGw6IHdoaXRlO1xyXG59XHJcbi5uYXYgPiBsaSA+IGEgPiBzdmcge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBsZWZ0OiA4cHg7XHJcbn1cclxuLm92ZXJsYXkge1xyXG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC4yKTtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDg1dmg7XHJcbiAgei1pbmRleDogMTAwMDAwO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxufVxyXG4ub3ZlcmxheSA+IC5mYSB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogNTAlO1xyXG4gIGxlZnQ6IDQyJTtcclxuICBtYXJnaW4tbGVmdDogLTE1cHg7XHJcbiAgbWFyZ2luLXRvcDogLTE1cHg7XHJcbiAgY29sb3I6ICMwMDA7XHJcbiAgZm9udC1zaXplOiAzMHB4O1xyXG59XHJcbi5vdmVybGF5ID4gcCB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogNjUlO1xyXG4gIGxlZnQ6IDQ5JTtcclxuICBtYXJnaW4tbGVmdDogLTE1cHg7XHJcbiAgbWFyZ2luLXRvcDogLTE1cHg7XHJcbiAgY29sb3I6ICMwMDA7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG59XHJcbi5vdmVybGF5ID4gcC5sb2FkaW5nIHtcclxuICBsZWZ0OiA0OSU7XHJcbn1cclxuXHJcbi5vdmVybGF5ID4gcC5wcm9jZXNzaW5nIHtcclxuICBsZWZ0OiA0OSU7XHJcbn1cclxuXHJcbi5vdmVybGF5ID4gcC52YWxpZGF0aW5nIHtcclxuICBsZWZ0OiA0OSU7XHJcbn1cclxuLnRvcC1NYXJnaW4ge1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDQwM3B4O1xyXG59XHJcbi5hbmNob3Ige1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHdpZHRoOiAxMjBweDtcclxuICBtYXJnaW46IDIwMHB4IGF1dG87XHJcbn1cclxuLnNjcm9sbGluZy13cmFwcGVyLWZsZXhib3gge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC13cmFwOiBub3dyYXA7XHJcbiAgb3ZlcmZsb3cteDogYXV0bztcclxuXHJcbiAgLmNhcmQge1xyXG4gICAgZmxleDogMCAwIGF1dG87XHJcbiAgfVxyXG59XHJcbnRhYmxlLmRfZmlsdGVyIHtcclxuICBmb250LXNpemU6IDEwcHg7XHJcbn1cclxudGFibGUuZF9maWx0ZXIgPiB0Ym9keSA+IHRyID4gdGQge1xyXG4gIHBhZGRpbmc6IDJweDtcclxuICBsaW5lLWhlaWdodDogMi40ZW07XHJcbn1cclxuLmVycm9yIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjU1LCAxOTAsIDE5MCkgIWltcG9ydGFudDtcclxufVxyXG4ucGFkZGluZy1sZWZ0LTUge1xyXG4gIHBhZGRpbmctbGVmdDogNXB4O1xyXG59XHJcbi5wYWRkaW5nLXJpZ2h0LTUge1xyXG4gIHBhZGRpbmctcmlnaHQ6IDVweDtcclxufVxyXG5cclxuLmstZ3JpZCB7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG59XHJcbi5uYXYgPiBsaSA+IGEge1xyXG4gIHBhZGRpbmctbGVmdDogMzBweDtcclxuICBmb250LXNpemU6IDEycHg7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxufVxyXG4ubmF2ID4gbGk6aG92ZXIsXHJcbi5uYXYgPiBsaTpob3ZlciBhLFxyXG4ubmF2ID4gbGk6aG92ZXIgYSA+IHN2ZyB7XHJcbiAgYmFja2dyb3VuZDogIzAwNjdjNTtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgZmlsbDogd2hpdGU7XHJcbn1cclxuLm5hdiA+IGxpID4gYSA+IHN2ZyB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGxlZnQ6IDhweDtcclxufVxyXG4ub3ZlcmxheSB7XHJcbiAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjIpO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogODV2aDtcclxuICB6LWluZGV4OiAxMDAwMDA7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG59XHJcbi5vdmVybGF5ID4gLmZhIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiA1MCU7XHJcbiAgbGVmdDogNDIlO1xyXG4gIG1hcmdpbi1sZWZ0OiAtMTVweDtcclxuICBtYXJnaW4tdG9wOiAtMTVweDtcclxuICBjb2xvcjogIzAwMDtcclxuICBmb250LXNpemU6IDMwcHg7XHJcbn1cclxuLm92ZXJsYXkgPiBwIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiA2NSU7XHJcbiAgbGVmdDogNDklO1xyXG4gIG1hcmdpbi1sZWZ0OiAtMTVweDtcclxuICBtYXJnaW4tdG9wOiAtMTVweDtcclxuICBjb2xvcjogIzAwMDtcclxuICBmb250LXNpemU6IDEycHg7XHJcbn1cclxuLm92ZXJsYXkgPiBwLmxvYWRpbmcge1xyXG4gIGxlZnQ6IDQ5JTtcclxufVxyXG5cclxuLm92ZXJsYXkgPiBwLnByb2Nlc3Npbmcge1xyXG4gIGxlZnQ6IDQ5JTtcclxufVxyXG5cclxuLm92ZXJsYXkgPiBwLnZhbGlkYXRpbmcge1xyXG4gIGxlZnQ6IDQ5JTtcclxufVxyXG4udG9wLU1hcmdpbiB7XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxuICBtYXJnaW4tbGVmdDogNDAzcHg7XHJcbn1cclxuLmFuY2hvciB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgd2lkdGg6IDEyMHB4O1xyXG4gIG1hcmdpbjogMjAwcHggYXV0bztcclxufVxyXG4uc2Nyb2xsaW5nLXdyYXBwZXItZmxleGJveCB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LXdyYXA6IG5vd3JhcDtcclxuICBvdmVyZmxvdy14OiBhdXRvO1xyXG5cclxuICAuY2FyZCB7XHJcbiAgICBmbGV4OiAwIDAgYXV0bztcclxuICB9XHJcbn1cclxudGFibGUuZF9maWx0ZXIge1xyXG4gIGZvbnQtc2l6ZTogMTBweDtcclxufVxyXG50YWJsZS5kX2ZpbHRlciA+IHRib2R5ID4gdHIgPiB0ZCB7XHJcbiAgcGFkZGluZzogMnB4O1xyXG4gIGxpbmUtaGVpZ2h0OiAyLjRlbTtcclxufVxyXG4uZXJyb3Ige1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYigyNTUsIDE5MCwgMTkwKSAhaW1wb3J0YW50O1xyXG59XHJcbi5leGFtcGxlLWNvbnRhaW5lciB7XHJcbiAgd2lkdGg6IDQwMHB4O1xyXG4gIG1heC13aWR0aDogMTAwJTtcclxuICBtYXJnaW46IDAgMjVweCAyNXB4IDA7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIHZlcnRpY2FsLWFsaWduOiB0b3A7XHJcbn1cclxuXHJcbi5jb2xsZWN0aW9uLWxpc3Qge1xyXG4gIGJvcmRlcjogc29saWQgMXB4ICNjY2M7XHJcbiAgbWluLWhlaWdodDogNjBweDtcclxuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBtYXJnaW4tbGVmdDogLTlweDtcclxuICBtYXJnaW4tcmlnaHQ6IC05cHg7XHJcbiAgaGVpZ2h0OiA5MSU7XHJcbiAgYm94LXNoYWRvdzogMHB4IDBweCA1cHggMXB4IGluc2V0ICNlOGUzZTM7XHJcbiAgb3ZlcmZsb3cteTogc2Nyb2xsO1xyXG59XHJcblxyXG4uY29sbGVjdGlvbi1ib3gge1xyXG4gIHBhZGRpbmc6IDEwcHggMTBweDtcclxuICBib3JkZXItYm90dG9tOiBzb2xpZCAxcHggI2NjYztcclxuICBjb2xvcjogcmdiYSgwLCAwLCAwLCAwLjg3KTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICBjdXJzb3I6IG1vdmU7XHJcbiAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIG1hcmdpbjogNXB4O1xyXG4gIHdvcmQtYnJlYWs6IGJyZWFrLWFsbDtcclxufVxyXG5cclxuLmNkay1kcmFnLXByZXZpZXcge1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gIGJveC1zaGFkb3c6IDAgNXB4IDVweCAtM3B4IHJnYmEoMCwgMCwgMCwgMC4yKSwgMCA4cHggMTBweCAxcHggcmdiYSgwLCAwLCAwLCAwLjE0KSwgMCAzcHggMTRweCAycHggcmdiYSgwLCAwLCAwLCAwLjEyKTtcclxufVxyXG5cclxuLmNkay1kcmFnLXBsYWNlaG9sZGVyIHtcclxuICBvcGFjaXR5OiAwO1xyXG59XHJcblxyXG4uY2RrLWRyYWctYW5pbWF0aW5nIHtcclxuICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMjUwbXMgY3ViaWMtYmV6aWVyKDAsIDAsIDAuMiwgMSk7XHJcbn1cclxuXHJcbi5jb2xsZWN0aW9uLWJveDpsYXN0LWNoaWxkIHtcclxuICBib3JkZXI6IG5vbmU7XHJcbn1cclxuXHJcbi5jb2xsZWN0aW9uLWxpc3QuY2RrLWRyb3AtbGlzdC1kcmFnZ2luZyAuY29sbGVjdGlvbi1ib3g6bm90KC5jZGstZHJhZy1wbGFjZWhvbGRlcikge1xyXG4gIHRyYW5zaXRpb246IHRyYW5zZm9ybSAyNTBtcyBjdWJpYy1iZXppZXIoMCwgMCwgMC4yLCAxKTtcclxufVxyXG46Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICB3aWR0aDogMC41ZW07XHJcbn1cclxuOjotd2Via2l0LXNjcm9sbGJhci10cmFjayB7XHJcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiBpbnNldCAwIDAgNnB4IHJnYmEoMCwwLDAsMC4zKTtcclxufVxyXG46Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBkYXJrZ3JleTtcclxuICBvdXRsaW5lOiAxcHggc29saWQgc2xhdGVncmV5O1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/bulk-load/bulk-load.component.ts":
/*!**************************************************!*\
  !*** ./src/app/bulk-load/bulk-load.component.ts ***!
  \**************************************************/
/*! exports provided: Connection, BulkJob, BulkLoadComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Connection", function() { return Connection; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BulkJob", function() { return BulkJob; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BulkLoadComponent", function() { return BulkLoadComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _bulk_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./bulk.service */ "./src/app/bulk-load/bulk.service.ts");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm5/drag-drop.es5.js");
/* harmony import */ var _connections_connection_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../connections/connection-service */ "./src/app/connections/connection-service.ts");
/* harmony import */ var _jobs_job_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../jobs/job.service */ "./src/app/jobs/job.service.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var intro_js_intro_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! intro.js/intro.js */ "./node_modules/intro.js/intro.js");
/* harmony import */ var intro_js_intro_js__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(intro_js_intro_js__WEBPACK_IMPORTED_MODULE_10__);











var Connection = /** @class */ (function () {
    function Connection(connection) {
        connection = connection || {};
        this.connectionId = connection.connectionId || '';
        this.type = connection.type || '';
        this.name = connection.name || '';
        this.aliasName = connection.aliasName || '';
        this.createdDate = connection.createdDate || '';
        this.updatedDate = connection.updatedDate || '';
        this.createdBy = connection.createdBy || '';
        this.updatedBy = connection.updatedBy || '';
        this.toggle = connection.toggle || false;
    }
    return Connection;
}());

var BulkJob = /** @class */ (function () {
    function BulkJob(bulkJob) {
        bulkJob = BulkJob || {};
        this.collectionNames = bulkJob.collectionNames || '';
        this.createdBy = bulkJob.createdBy || '';
        this.destinationConnection = bulkJob.destinationConnection || '';
        this.destinationConnectionName = bulkJob.destinationConnectionName || '';
        this.id = bulkJob.id || '';
        this.name = bulkJob.name || '';
        this.scheduleDateTime = bulkJob.scheduleDateTime || '';
        this.prefix = bulkJob.prefix || '';
        this.sourceConnection = bulkJob.sourceConnection || '';
        this.sourceConnectionName = bulkJob.sourceConnectionName || '';
        this.updatedBy = bulkJob.updatedBy || '';
    }
    return BulkJob;
}());

var BulkLoadComponent = /** @class */ (function () {
    function BulkLoadComponent(connectionService, jobService, router, route, cookieService, bulkService, toastr) {
        this.connectionService = connectionService;
        this.jobService = jobService;
        this.router = router;
        this.route = route;
        this.cookieService = cookieService;
        this.bulkService = bulkService;
        this.toastr = toastr;
        this.cronExpression = '4 3 2 12 1/1 ? *';
        this.cronOptions = {
            formInputClass: 'form-control cron-editor-input',
            formSelectClass: 'form-control cron-editor-select',
            formRadioClass: 'cron-editor-radio',
            formCheckboxClass: 'cron-editor-checkbox',
            defaultTime: '10:00:00',
            use24HourTime: true,
            hideMinutesTab: false,
            hideHourlyTab: false,
            hideDailyTab: false,
            hideWeeklyTab: false,
            hideMonthlyTab: false,
            hideYearlyTab: false,
            hideAdvancedTab: true,
            hideSeconds: false,
            removeSeconds: false,
            removeYears: false
        };
        this.locale = {
            applyLabel: 'Appliquer',
            customRangeLabel: ' - ',
            daysOfWeek: moment__WEBPACK_IMPORTED_MODULE_8__["weekdaysMin"](),
            monthNames: moment__WEBPACK_IMPORTED_MODULE_8__["monthsShort"](),
            firstDay: moment__WEBPACK_IMPORTED_MODULE_8__["localeData"]().firstDayOfWeek(),
        };
        this.todo = [
            'Get to work',
            'Pick up groceries',
            'Go home',
            'Fall asleep'
        ];
        this.done = [];
        this.introJS = intro_js_intro_js__WEBPACK_IMPORTED_MODULE_10__();
        this.alwaysShowCalendars = true;
        this.sourceConnections = Array();
        this.destinationConnections = Array();
        this.bulkJob = new BulkJob();
        this.sourceCollection = new Array();
        if (this.cookieService.get('intro') === 'newbulkjob') {
            this.introJS.setOptions({
                steps: [
                    {
                        element: '#bulkJobName',
                        intro: 'Write Bulk Job Name'
                    },
                    {
                        element: '#sourceConnection',
                        intro: ' Select Source Connection',
                        position: 'left'
                    },
                    {
                        element: '#dragDrop',
                        intro: ' Drag and drop the Collection Here'
                    },
                    {
                        element: '#destinationCollection',
                        intro: '  Select Destination Connection',
                        position: 'left'
                    },
                    {
                        element: '#prefix',
                        intro: '  Write the Prefix'
                    },
                    {
                        element: '#saveBtn',
                        intro: '  Press save job icon '
                    },
                ]
            });
        }
        if (this.cookieService.get('intro') === 'newbulkjobWithSchedular') {
            this.introJS.setOptions({
                steps: [
                    {
                        element: '#bulkJobName',
                        intro: 'Write Bulk Job Name'
                    },
                    {
                        element: '#sourceConnection',
                        intro: ' Select Source Connection',
                        position: 'left'
                    },
                    {
                        element: '#dragDrop',
                        intro: ' Drag and drop the Collection Here'
                    },
                    {
                        element: '#destinationCollection',
                        intro: '  Select Destination Connection',
                        position: 'left'
                    },
                    {
                        element: '#prefix',
                        intro: '  Write the Prefix'
                    },
                    {
                        element: '#schedular',
                        intro: '  Select Schedular',
                        position: 'top'
                    },
                    {
                        element: '#saveBtn',
                        intro: '  Press save job icon '
                    },
                ]
            });
        }
    }
    BulkLoadComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getAllConnection();
        if (this.route.snapshot.queryParams.action === 'intro') {
            this.introJS.start();
            this.introJS.onchange(function (targetElement) {
            });
            this.introJS.oncomplete(function () {
                _this.router.navigate(['bulk-load/new'], { queryParams: {} });
            });
            this.introJS.onexit(function () {
                _this.router.navigate(['bulk-load/new'], { queryParams: {} });
            });
        }
    };
    BulkLoadComponent.prototype.getAllConnection = function () {
        var _this = this;
        this.connectionService.getAllConnection(new Connection(), 0, 10, 'createdDate', 'desc').subscribe(function (data) {
            _this.sourceConnections = Array();
            _this.destinationConnections = Array();
            if (JSON.parse(JSON.stringify(data)).results[0].connectionResponseList !== null) {
                JSON.parse(JSON.stringify(data)).results[0].connectionResponseList.forEach(function (element) {
                    _this.sourceConnections.push(new Connection(element));
                    _this.destinationConnections.push(new Connection(element));
                });
                var sourceObj = new Connection();
                sourceObj.name = 'Select Connection';
                _this.sourceConnections.splice(0, 0, sourceObj);
                _this.connectionsList = _this.sourceConnections;
                _this.sourceConnectionValue = sourceObj;
                var targetObj = new Connection();
                targetObj.name = 'Select Connection';
                targetObj.name = 'Select Target Connection';
                _this.destinationConnectionValue = targetObj;
                // this.route.paramMap.subscribe(params => {
                //   if (params.get('jobId') !== 'new') {
                //     this.isProcessing = false;
                //     this.getJob(params.get('jobId'));
                //   }
                // });
            }
        });
    };
    BulkLoadComponent.prototype.filterSourceConnection = function (value) {
        this.sourceConnections = this.connectionsList.filter(function (n) { return n.name.toLowerCase().indexOf(value.toLowerCase()) !== -1; });
    };
    BulkLoadComponent.prototype.selectionChangeSourceConnection = function (event) {
        this.populateSourceCollection(event.connectionId);
        this.sourceConnectionValue = event;
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'newbulkjob') {
            this.introJS.goToStepNumber(2).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'newbulkjobWithSchedular') {
            this.introJS.goToStepNumber(2).start();
        }
    };
    BulkLoadComponent.prototype.selectionChangeDestinationConnection = function (event) {
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'newbulkjob') {
            this.introJS.goToStepNumber(4).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'newbulkjobWithSchedular') {
            this.introJS.goToStepNumber(4).start();
        }
    };
    BulkLoadComponent.prototype.populateSourceCollection = function (connectionId) {
        var _this = this;
        // this.isProcessing = true;
        this.jobService.getAllEntities(connectionId)
            .subscribe(function (response) {
            // this.isProcessing = false;
            var list = JSON.parse(JSON.stringify(response)).results;
            list.forEach(function (element) {
                _this.sourceCollection.push({ text: element.collectionName + " (" + element.collectionCount + ")", value: element.collectionName });
            });
            _this.sourceCollectionList = _this.sourceCollection;
            var defaultvalue = { text: 'Select Collection', value: '0' };
            // this.sourceCollection.splice(0, 0, defaultvalue);
            _this.sourceCollectionValue = defaultvalue;
            // this.isSourceConnectionConnected = true;
            // this.isSourceCollectionSelected = false;
            // this.route.paramMap.subscribe(params => {
            //   console.log(params);
            //   if (params.get('jobId') !== 'new') {
            //     this.sourceCollectionValue = this.sourceCollection.filter(n => n.value === this.jobDetail.sourceCollection)[0];
            //     this.populateSourceMetadata(this.sourceConnectionValue.connectionId, this.jobDetail.sourceCollection);
            //   }
            // });
        }, function (error) {
            // this.isProcessing = false;
        });
    };
    BulkLoadComponent.prototype.getSubmittedDateTimeRange = function (event) {
        this.datetime = moment__WEBPACK_IMPORTED_MODULE_8__(event['startDate']._d).format('MM/DD/YYYY HH:mm:ss');
    };
    BulkLoadComponent.prototype.saveBulkJob = function () {
        var _this = this;
        this.bulkJob.collectionNames = this.done.map(function (n) { return n.value; }).join(',');
        this.bulkJob.sourceConnection = this.sourceConnectionValue.connectionId;
        this.bulkJob.sourceConnectionName = this.sourceConnectionValue.name;
        this.bulkJob.destinationConnection = this.destinationConnectionValue.connectionId;
        this.bulkJob.destinationConnectionName = this.destinationConnectionValue.name;
        this.bulkJob.scheduleDateTime = this.datetime;
        console.log(this.datetime);
        this.bulkService.createBulkJobs(this.bulkJob).subscribe(function (data) {
            var list = JSON.parse(JSON.stringify(data)).results;
            var message = JSON.parse(JSON.stringify(data)).message;
            _this.toastr.info(message);
            console.log(message);
            console.log(list);
        });
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'newbulkjob') {
            this.introJS.exit();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'newbulkjobWithSchedular') {
            this.introJS.exit();
        }
    };
    BulkLoadComponent.prototype.drop = function (event) {
        if (event.previousContainer === event.container) {
            Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["moveItemInArray"])(event.container.data, event.previousIndex, event.currentIndex);
        }
        else {
            Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["transferArrayItem"])(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
        }
    };
    BulkLoadComponent.prototype.isInvalidDate = function (date) {
        return date.weekday() === 0;
    };
    BulkLoadComponent.prototype.isCustomDate = function (date) {
        return (date.weekday() === 0 ||
            date.weekday() === 6) ? 'mycustomdate' : false;
    };
    BulkLoadComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-bulk-load',
            template: __webpack_require__(/*! ./bulk-load.component.html */ "./src/app/bulk-load/bulk-load.component.html"),
            styles: [__webpack_require__(/*! ./bulk-load.component.scss */ "./src/app/bulk-load/bulk-load.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_connections_connection_service__WEBPACK_IMPORTED_MODULE_4__["ConnectionService"],
            _jobs_job_service__WEBPACK_IMPORTED_MODULE_5__["JobService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_9__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_9__["ActivatedRoute"],
            ngx_cookie_service__WEBPACK_IMPORTED_MODULE_6__["CookieService"],
            _bulk_service__WEBPACK_IMPORTED_MODULE_2__["BulkService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"]])
    ], BulkLoadComponent);
    return BulkLoadComponent;
}());



/***/ }),

/***/ "./src/app/bulk-load/bulk.service.ts":
/*!*******************************************!*\
  !*** ./src/app/bulk-load/bulk.service.ts ***!
  \*******************************************/
/*! exports provided: BulkService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BulkService", function() { return BulkService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/config */ "./src/environments/config.ts");




var BulkService = /** @class */ (function () {
    function BulkService(http) {
        this.http = http;
        this.baseUrl = _environments_config__WEBPACK_IMPORTED_MODULE_3__["Config"].getEnvironmentVariable('apiUrl');
        this.createBulkJobsEndPoint = this.baseUrl + 'jobs/bulkJobs';
    }
    BulkService.prototype.createBulkJobs = function (bulkJob) {
        return this.http.post(this.createBulkJobsEndPoint, bulkJob);
    };
    BulkService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], BulkService);
    return BulkService;
}());



/***/ }),

/***/ "./src/app/common-service.ts":
/*!***********************************!*\
  !*** ./src/app/common-service.ts ***!
  \***********************************/
/*! exports provided: CommonService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommonService", function() { return CommonService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var src_environments_config__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/environments/config */ "./src/environments/config.ts");
/* harmony import */ var _app_session_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.session-service */ "./src/app/app.session-service.ts");






var CommonService = /** @class */ (function () {
    function CommonService(_http, _SessionService) {
        this._http = _http;
        this._SessionService = _SessionService;
        this.baseUrl = src_environments_config__WEBPACK_IMPORTED_MODULE_4__["Config"].getEnvironmentVariable('endPoint');
        this.getGridColumnsEndpoint = this.baseUrl + src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].authApiUrl + 'grid';
        this.filterRequestEndpoint = this.baseUrl + src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].authApiUrl + 'secured/accountChange/request/filter';
    }
    CommonService.prototype.GetGridColumns = function (gridId) {
        this.sessionData = JSON.parse(this._SessionService.getCookie("LD_USER_DETAIL"));
        var uid = this.sessionData.uid;
        if (this._SessionService.getCookie("LA_USER_DETAIL") != "") {
            this.LA_SessionData = JSON.parse(this._SessionService.getCookie("LA_USER_DETAIL"));
            uid = this.LA_SessionData.userId;
        }
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this._http.get(this.getGridColumnsEndpoint + ("/" + gridId + "/columns/" + uid), httpOptions);
    };
    CommonService.prototype.SaveGridColumns = function (gridId, columnsSettings) {
        this.sessionData = JSON.parse(this._SessionService.getCookie("LD_USER_DETAIL"));
        var uid = this.sessionData.uid;
        if (this._SessionService.getCookie("LA_USER_DETAIL") != "") {
            this.LA_SessionData = JSON.parse(this._SessionService.getCookie("LA_USER_DETAIL"));
            uid = this.LA_SessionData.userId;
        }
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        var req = {
            columnProperty: columnsSettings.toString()
        };
        return this._http.post(this.getGridColumnsEndpoint + ("/" + gridId + "/columns/" + uid), req, httpOptions);
    };
    CommonService.prototype.filterRequest = function (requestId, status, somStatus, requestType, startRecord, recordCount, requestor) {
        this.sessionData = JSON.parse(this._SessionService.getCookie("LD_USER_DETAIL"));
        var uid = this.sessionData.uid;
        if (this._SessionService.getCookie("LA_USER_DETAIL") != "") {
            this.LA_SessionData = JSON.parse(this._SessionService.getCookie("LA_USER_DETAIL"));
            uid = this.LA_SessionData.userId;
        }
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this._http.get(this.filterRequestEndpoint + ("/" + uid + "?requestId=" + (requestId == null ? "" : requestId) + "&requestor=" + requestor + "&status=" + ((status == null || status == "0") ? "" : status) + "&somStatus=" + ((somStatus == null || somStatus == "0") ? "" : somStatus) + "&&requestType=" + ((requestType == null || requestType == "0") ? "" : requestType) + "&startRecord=" + startRecord + "&recordCount=" + recordCount), httpOptions);
    };
    CommonService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _app_session_service__WEBPACK_IMPORTED_MODULE_5__["SessionService"]])
    ], CommonService);
    return CommonService;
}());



/***/ }),

/***/ "./src/app/connections/add-connection/add-connection.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/connections/add-connection/add-connection.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"add_connection\">\r\n  <div class=\"row\">\r\n    <div class=\"col-lg-12\">\r\n       \r\n      <kendo-dropdownlist [data]=\"connectors\"  [filterable]=\"true\" [textField]=\"'text'\" [valueField]=\"'value'\" id=\"selectConnector\"\r\n        [value]=\"selectedConnector\" [(ngModel)]=\"selectedConnector\" [valuePrimitive]=\"true\" style=\"width: 100%\"\r\n        (filterChange)=\"filterConnector($event)\" (selectionChange)=\"selectionChangeConnector($event)\">\r\n        <ng-template kendoDropDownListItemTemplate let-dataItem>\r\n          <img [src]=\"dataItem.img\" width=\"40\" /> &nbsp;{{ dataItem.text }}\r\n        </ng-template>\r\n      </kendo-dropdownlist>\r\n   \r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"row\" *ngIf=\"selectedConnector === 0\">\r\n    <div class=\"col-lg-12 text-center\">\r\n      <svg class=\"icon_change_log\" shape-rendering=\"geometricPrecision\" width=\"60\" height=\"60\" style=\"padding: 10px; border: solid 1px #cccccc; border-radius: 50%; margin-top: 12%\">\r\n        <use xlink:href=\"./assets/dist/img/icons.svg#icon_plug\"></use>\r\n      </svg>\r\n      <p>Select connector from above dropdown.</p>\r\n    </div>\r\n  </div>\r\n \r\n  <app-odata-connector *ngIf=\"selectedConnector === 1\" (saveConnection)=\"closeIt($event)\"></app-odata-connector>\r\n  <app-rdbms-connector *ngIf=\"selectedConnector === 2\" (saveConnection)=\"closeIt($event)\"></app-rdbms-connector>\r\n  <app-rdbms-connector *ngIf=\"selectedConnector === 3\" (saveConnection)=\"closeIt($event)\"></app-rdbms-connector>\r\n  <app-rdbms-connector *ngIf=\"selectedConnector === 5\" (saveConnection)=\"closeIt($event)\"></app-rdbms-connector>\r\n  <app-api-connector *ngIf=\"selectedConnector === 4\" (saveConnection)=\"closeIt($event)\"></app-api-connector>\r\n\r\n</div>"

/***/ }),

/***/ "./src/app/connections/add-connection/add-connection.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/connections/add-connection/add-connection.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".k-window-title {\n  font-size: 14px; }\n\n#add_connection .row {\n  margin-bottom: 10px; }\n\n#add_connection p.title {\n  font-weight: 600;\n  font-size: 12px;\n  margin-bottom: 0; }\n\n#add_connection input {\n  width: 100%; }\n\n#add_connection .connector_selection_message {\n  font-size: 16px;\n  background: #c0e1ff;\n  padding: 10px;\n  border-radius: 5px;\n  border: solid 1px #92bfe8; }\n\n#add_connection button {\n  margin: 0px 3px;\n  border-radius: 2px;\n  padding: 4px 8px;\n  box-sizing: border-box;\n  border-width: 1px;\n  border-style: solid;\n  font-size: 14px;\n  line-height: 1.42857;\n  font-family: inherit;\n  text-align: center;\n  text-decoration: none;\n  white-space: nowrap;\n  display: inline-flex;\n  align-items: center;\n  justify-content: center;\n  vertical-align: middle;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  cursor: pointer;\n  outline: none;\n  -webkit-appearance: none;\n  position: relative; }\n\n#add_connection button.test_connection {\n  background-color: #f0ad4e;\n  border: solid 1px #f0ad4e;\n  color: white; }\n\n#add_connection button.cancel_button {\n  background-color: #ffffff;\n  border: solid 1px #999999; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb25uZWN0aW9ucy9hZGQtY29ubmVjdGlvbi9FOlxcVUktUHJvamVjdFxcRVRMXFxzcmNcXHNyYy9hcHBcXGNvbm5lY3Rpb25zXFxhZGQtY29ubmVjdGlvblxcYWRkLWNvbm5lY3Rpb24uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxlQUFlLEVBQUE7O0FBR2pCO0VBRUksbUJBQW1CLEVBQUE7O0FBRnZCO0VBS0ksZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixnQkFBZ0IsRUFBQTs7QUFQcEI7RUFVSSxXQUFXLEVBQUE7O0FBVmY7RUFhSSxlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixrQkFBa0I7RUFDbEIseUJBQXlCLEVBQUE7O0FBakI3QjtFQW9CSSxlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixzQkFBc0I7RUFDdEIsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2Ysb0JBQW9CO0VBQ3BCLG9CQUFvQjtFQUNwQixrQkFBa0I7RUFDbEIscUJBQXFCO0VBQ3JCLG1CQUFtQjtFQUNuQixvQkFBb0I7RUFDcEIsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixzQkFBc0I7RUFDdEIseUJBQXlCO0VBQ3pCLHNCQUFzQjtFQUN0QixxQkFBcUI7RUFDckIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixhQUFhO0VBQ2Isd0JBQXdCO0VBQ3hCLGtCQUFrQixFQUFBOztBQTNDdEI7RUE4Q0kseUJBQXlCO0VBQ3pCLHlCQUF5QjtFQUN6QixZQUFZLEVBQUE7O0FBaERoQjtFQW9ESSx5QkFBeUI7RUFDekIseUJBQXlCLEVBQUEiLCJmaWxlIjoiYXBwL2Nvbm5lY3Rpb25zL2FkZC1jb25uZWN0aW9uL2FkZC1jb25uZWN0aW9uLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmstd2luZG93LXRpdGxlIHtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbn1cclxuXHJcbiNhZGRfY29ubmVjdGlvbiB7XHJcbiAgLnJvdyB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gIH1cclxuICBwLnRpdGxlIHtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gIH1cclxuICBpbnB1dCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbiAgLmNvbm5lY3Rvcl9zZWxlY3Rpb25fbWVzc2FnZSB7XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjYzBlMWZmO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGJvcmRlcjogc29saWQgMXB4ICM5MmJmZTg7XHJcbiAgfVxyXG4gIGJ1dHRvbiB7XHJcbiAgICBtYXJnaW46IDBweCAzcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgICBwYWRkaW5nOiA0cHggOHB4O1xyXG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgIGJvcmRlci13aWR0aDogMXB4O1xyXG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjQyODU3O1xyXG4gICAgZm9udC1mYW1pbHk6IGluaGVyaXQ7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG4gICAgLXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTtcclxuICAgIC1tb3otdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgICAtbXMtdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgICB1c2VyLXNlbGVjdDogbm9uZTtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICAtd2Via2l0LWFwcGVhcmFuY2U6IG5vbmU7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgfVxyXG4gIGJ1dHRvbi50ZXN0X2Nvbm5lY3Rpb24ge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2YwYWQ0ZTtcclxuICAgIGJvcmRlcjogc29saWQgMXB4ICNmMGFkNGU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgfVxyXG5cclxuICBidXR0b24uY2FuY2VsX2J1dHRvbiB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgYm9yZGVyOiBzb2xpZCAxcHggIzk5OTk5OTtcclxuICB9XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/connections/add-connection/add-connection.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/connections/add-connection/add-connection.component.ts ***!
  \************************************************************************/
/*! exports provided: AddConnectionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddConnectionComponent", function() { return AddConnectionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_model_connector__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/model/connector */ "./src/app/model/connector.ts");
/* harmony import */ var src_app_shared_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/data.service */ "./src/app/shared/data.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var intro_js_intro_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! intro.js/intro.js */ "./node_modules/intro.js/intro.js");
/* harmony import */ var intro_js_intro_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(intro_js_intro_js__WEBPACK_IMPORTED_MODULE_5__);






var AddConnectionComponent = /** @class */ (function () {
    function AddConnectionComponent(dataService, router, route) {
        this.dataService = dataService;
        this.router = router;
        this.route = route;
        this.introJS = intro_js_intro_js__WEBPACK_IMPORTED_MODULE_5__();
        this.selectedConnector = 0;
        this.listItems = [
            { text: 'Select Connector', value: 0, img: '' },
            { text: 'ODATA', value: 1, img: 'https://www.accely.com/wp-content/uploads/2018/09/1-88.png' },
            { text: 'ORACLE', value: 2, img: 'https://www.datocms-assets.com/2885/1522106007-oracle.png' },
            { text: 'MYSQL', value: 3, img: 'https://mc.qcloudimg.com/static/img/2742c21902443c72d3b0e198b7c49efb/MySQL.png' },
            { text: 'API', value: 4, img: 'https://mc.qcloudimg.com/static/img/2742c21902443c72d3b0e198b7c49efb/MySQL.png' },
            { text: 'DATAHUG', value: 5, img: 'https://mc.qcloudimg.com/static/img/2742c21902443c72d3b0e198b7c49efb/MySQL.png' },
        ];
        this.closeModel = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.connectionFields = new src_app_model_connector__WEBPACK_IMPORTED_MODULE_2__["Connector"]();
        this.connectors = this.listItems.slice();
        this.introJS.setOptions({
            steps: [
                {
                    element: '#selectConnector',
                    intro: 'Select Connector '
                },
            ]
        });
    }
    AddConnectionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataService.getConnectionDetail().subscribe(function (connection) {
            if (connection.connectionId !== '') {
                _this.connection = connection;
                _this.selectedConnector = _this.listItems.filter(function (n) { return n.text.toLowerCase() === _this.connection.type.toLowerCase(); })[0].value;
            }
        });
        if (this.route.snapshot.queryParams.action === 'intro') {
            this.introJS.start();
            this.introJS.onchange(function (targetElement) {
            });
            this.introJS.oncomplete(function () {
                _this.router.navigate(['/connections'], { queryParams: {} });
            });
            this.introJS.onexit(function () {
                _this.router.navigate(['/connections'], { queryParams: {} });
            });
        }
    };
    AddConnectionComponent.prototype.filterConnector = function (value) {
        this.connectors = this.listItems.filter(function (s) { return s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1; });
    };
    AddConnectionComponent.prototype.saveConnection = function () {
        console.log(this.connectionFields);
    };
    AddConnectionComponent.prototype.onSaveConnection = function (event) {
        console.log(event);
    };
    AddConnectionComponent.prototype.closeIt = function (id) {
        this.closeModel.emit();
        this.selectedConnector = 0;
        this.connectionFields = new src_app_model_connector__WEBPACK_IMPORTED_MODULE_2__["Connector"]();
    };
    AddConnectionComponent.prototype.selectionChangeConnector = function (event) {
        var connectorType = event.text;
        // localStorage.setItem('connectorType', connectorType);
        this.connectorType = connectorType;
        this.introJS.exit();
        this.dataService.setConnector(event.text);
        this.dataService.setAction('connectorVal');
    };
    AddConnectionComponent.prototype.ngOnDestroy = function () {
        this.introJS.exit();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], AddConnectionComponent.prototype, "closeModel", void 0);
    AddConnectionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-add-connection',
            template: __webpack_require__(/*! ./add-connection.component.html */ "./src/app/connections/add-connection/add-connection.component.html"),
            styles: [__webpack_require__(/*! ./add-connection.component.scss */ "./src/app/connections/add-connection/add-connection.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["DataService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
    ], AddConnectionComponent);
    return AddConnectionComponent;
}());



/***/ }),

/***/ "./src/app/connections/connection-service.ts":
/*!***************************************************!*\
  !*** ./src/app/connections/connection-service.ts ***!
  \***************************************************/
/*! exports provided: ConnectionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConnectionService", function() { return ConnectionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_environments_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/config */ "./src/environments/config.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");




var ConnectionService = /** @class */ (function () {
    function ConnectionService(httpClient) {
        this.httpClient = httpClient;
        this.baseUrl = src_environments_config__WEBPACK_IMPORTED_MODULE_2__["Config"].getEnvironmentVariable('apiUrl');
        this.connectionEndPoint = this.baseUrl + 'connection';
        this.connectionsEndPoint = this.baseUrl + 'connections/';
        this.testConnectionEndPoint = this.baseUrl + 'test';
    }
    ConnectionService.prototype.getAllConnection = function (connectionFilter, startRecord, recordCount, sortableColumn, sortingType) {
        // const httpOptions = {
        //     headers: new HttpHeaders({
        //         'Content-Type': 'application/json',
        //         'Authorization': 'Basic YWRtaW46YWRtaW4'
        //     })
        // };
        return this.httpClient.post(this.connectionEndPoint + ("?startRecord=" + startRecord + "&recordCount=" + recordCount + "&sortableColumn=" + sortableColumn + "&sortingType=" + sortingType), connectionFilter); //
    };
    ConnectionService.prototype.getConnectionDetail = function (connectionId) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.httpClient.get(this.connectionsEndPoint + ("/" + connectionId), httpOptions);
    };
    ConnectionService.prototype.delete = function (connectionIds) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            }),
            body: [connectionIds]
        };
        return this.httpClient.delete(this.connectionsEndPoint, httpOptions);
    };
    ConnectionService.prototype.add = function (data) {
        return this.httpClient.post(this.connectionsEndPoint, data);
    };
    ConnectionService.prototype.update = function (connectionId, data) {
        return this.httpClient.put(this.connectionsEndPoint + ("" + connectionId), data);
    };
    ConnectionService.prototype.edit = function (connectionId, data) {
        return this.httpClient.put(this.connectionEndPoint + connectionId, data);
    };
    ConnectionService.prototype.testConnection = function (connection) {
        return this.httpClient.post(this.testConnectionEndPoint, connection);
    };
    ConnectionService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], ConnectionService);
    return ConnectionService;
}());



/***/ }),

/***/ "./src/app/connections/connections.component.html":
/*!********************************************************!*\
  !*** ./src/app/connections/connections.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"connection_header\" class=\"content-header\"\r\n  style=\"padding: 10px;background: white;margin: 15px;box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-4\">\r\n      <h1 style=\"margin:0px;font-size:19px\">\r\n        Connections\r\n      </h1>\r\n      <small>All available connections</small>\r\n    </div>\r\n\r\n    <div class=\"col-sm-8\">\r\n\r\n      <button kendoButton class=\"pull-right\" primary=\"true\" (click)=\"newConnection()\" id=\"connectionBtn\">\r\n        <svg class=\"icon_change_log\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n          <use xlink:href=\"./assets/dist/img/icons.svg#icon_plug\"></use>\r\n        </svg> &nbsp;\r\n        New Connection</button>\r\n    </div>\r\n\r\n  </div>\r\n</section>\r\n\r\n<section id=\"connection_content\" class=\"content\" style=\"min-height:auto;padding-top: 10px\">\r\n  <kendo-grid [data]=\"connectionsGridDataResult\" [height]=\"430\" [skip]=\"state.skip\" [filterable]=\"'menu'\"\r\n    [resizable]=\"true\" (pageChange)=\"pageChangeConnection($event)\" [sortable]=\" { allowUnsort: true}\"\r\n    [pageSize]=\"state.take\" [skip]=\"state.skip\" [sort]=\"state.sort\" [filter]=\"state.filter\" [sortable]=\"true\"\r\n    [pageable]=\"{info: info, type: type,buttonCount:buttonCount, pageSizes: connectionPageSizes, previousNext: previousNext }\"\r\n    [filter]=\"netNewFilter\" [loading]=\"connectionGridLoading\" (filterChange)=\"filterChange($event)\"\r\n    (cellClick)=\"cellClickHandler($event)\" (dataStateChange)=\"dataStateChange($event)\"\r\n    (sortChange)=\"sortChangeConnection($event)\">\r\n\r\n    <kendo-grid-column width=\"40\" title=\"\" [filterable]=\"false\" [locked]=\"true\" [reorderable]=\"false\">\r\n      <ng-template kendoGridCellTemplate kendoTooltip let-dataItem>\r\n        <span kendoTooltip position=\"right\">\r\n          <button #anchor (click)=\"onToggle(dataItem)\" class=\"k-button padding-0\"\r\n            style=\"border: 0px;background: transparent\">\r\n            <img src=\"/assets/dist/img/vertical-dot.png\" alt=\"vertical-dot\" width=\"16px\" />\r\n          </button>\r\n          <kendo-popup [anchor]=\"anchor\" (anchorViewportLeave)=\"dataItem.toggle = false\" *ngIf=\"dataItem.toggle\">\r\n            <ul class=\"nav nav-list\">\r\n              <li>\r\n                <a href=\"javascript:void(0)\" (click)=\"editConnection(dataItem)\" class=\"changelog white\">\r\n                  <svg class=\"icon_change_log\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n                    <use xlink:href=\"./assets/dist/img/icons.svg#icon_change_log\"></use>\r\n                  </svg> Edit Connection\r\n                </a>\r\n              </li>\r\n              <li>\r\n                <a href=\"javascript:void(0)\" (click)=\"isAlertModelVisible = true; selectedConnection = dataItem\"\r\n                  class=\"changelog white\">\r\n                  <svg class=\"icon_update\" shape-rendering=\"geometricPrecision\" width=\"15\" height=\"15\">\r\n                    <use xlink:href=\"./assets/dist/img/icons.svg#icon_update\"></use>\r\n                  </svg> Remove Connection\r\n                </a>\r\n              </li>\r\n            </ul>\r\n          </kendo-popup>\r\n        </span>\r\n      </ng-template>\r\n    </kendo-grid-column>\r\n\r\n    <kendo-grid-column field=\"type\" title=\"Type\" width=\"70\" [filterable]=\"true\" class=\"kendo-cell\" width=\"70\">\r\n      <ng-template kendoGridFilterMenuTemplate let-filter let-column=\"column\" let-filterService=\"filterService\">\r\n        <input kendoTextBox [value]=\"accountIDQF\" (input)=\"filterChangeConnection($event, filterService, 'name')\" />\r\n      </ng-template>>\r\n    </kendo-grid-column>\r\n\r\n    <kendo-grid-column field=\"name\" title=\"Connection Name\" class=\"kendo-cell\" width=\"320\">\r\n      <ng-template kendoGridFilterMenuTemplate let-filter let-column=\"column\" let-filterService=\"filterService\">\r\n        <input kendoTextBox [value]=\"accountIDQF\" (input)=\"filterChangeConnection($event, filterService, 'name')\" />\r\n      </ng-template>\r\n    </kendo-grid-column>\r\n\r\n    <kendo-grid-column field=\"aliasName\" title=\"Alias Name\" width=\"200\" [filterable]=\"false\">\r\n      <ng-template kendoGridFilterMenuTemplate let-filter let-column=\"column\" let-filterService=\"filterService\">\r\n        <input kendoTextBox [value]=\"accountIDQF\"\r\n          (input)=\"filterChangeConnection($event, filterService, 'aliasName')\" />\r\n      </ng-template>\r\n    </kendo-grid-column>\r\n\r\n    <kendo-grid-column field=\"createdDate\" title=\"Created Date (PST)\" width=\"150\" [filterable]=\"false\">\r\n    </kendo-grid-column>\r\n\r\n    <kendo-grid-column field=\"updatedDate\" title=\"Updated Date (PST)\" width=\"150\" [filterable]=\"false\">\r\n    </kendo-grid-column>\r\n\r\n    <kendo-grid-column field=\"createdBy\" title=\"Created By\" width=\"120\" [filterable]=\"false\">\r\n    </kendo-grid-column>\r\n\r\n    <kendo-grid-column field=\"updatedBy\" title=\"Updated By\" width=\"120\" [filterable]=\"false\">\r\n    </kendo-grid-column>\r\n  </kendo-grid>\r\n</section>\r\n\r\n<kendo-dialog class=\"custom_model\" [width]=\"600\" [height]=\"500\" *ngIf=\"isConnectionVisible\"\r\n  (close)=\"isConnectionVisible = !isConnectionVisible\" title=\"Setup Connection\">\r\n  <app-add-connection (closeModel)=\"isConnectionVisible = !isConnectionVisible;this.getAllConnection()\">\r\n  </app-add-connection>\r\n</kendo-dialog>\r\n\r\n\r\n<kendo-dialog class=\"custom_model\" [width]=\"600\" *ngIf=\"isAlertModelVisible\"\r\n  (close)=\"isAlertModelVisible = !isAlertModelVisible\" title=\"Warning!\">\r\n  <span style=\"color:#ffff;\">Information</span>\r\n  <p style=\"padding: 5px 0px;\">Are you sure want to delete this request?</p>\r\n  <button class=\"btn btn-primary\"\r\n    (click)=\"isAlertModelVisible = !isAlertModelVisible;removeConnection(selectedConnection)\">\r\n    Yes\r\n  </button>\r\n  <button class=\"btn btn-danger\" (click)=\"isAlertModelVisible = !isAlertModelVisible\">\r\n    No\r\n  </button>\r\n</kendo-dialog>"

/***/ }),

/***/ "./src/app/connections/connections.component.scss":
/*!********************************************************!*\
  !*** ./src/app/connections/connections.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#connection_header button {\n  border-radius: 3px;\n  margin: 5px 3px 0px 0px; }\n\n#connection_content .k-grid {\n  font-size: 12px; }\n\n.custom_model div {\n  min-height: 300px; }\n\n.nav > li > a {\n  padding-left: 35px;\n  font-size: 12px; }\n\n.nav > li:hover,\n.nav > li:hover a,\n.nav > li:hover a > svg {\n  background: #0067c5;\n  color: white;\n  fill: white; }\n\n.nav > li > a > svg {\n  position: absolute;\n  left: 10px; }\n\n.content {\n  min-height: 250px;\n  padding: 15px;\n  margin-right: auto;\n  margin-left: auto;\n  padding-left: 15px;\n  padding-right: 15px;\n  background: white;\n  margin: 15px;\n  border-radius: 0.25rem;\n  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb25uZWN0aW9ucy9FOlxcVUktUHJvamVjdFxcRVRMXFxzcmNcXHNyYy9hcHBcXGNvbm5lY3Rpb25zXFxjb25uZWN0aW9ucy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLGtCQUFrQjtFQUNsQix1QkFBdUIsRUFBQTs7QUFJM0I7RUFFSSxlQUFlLEVBQUE7O0FBSW5CO0VBRUksaUJBQWlCLEVBQUE7O0FBR3JCO0VBQ0Usa0JBQWtCO0VBQ2xCLGVBQWUsRUFBQTs7QUFFakI7OztFQUdFLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osV0FBVyxFQUFBOztBQUViO0VBQ0Usa0JBQWtCO0VBQ2xCLFVBQVUsRUFBQTs7QUFFWjtFQUNFLGlCQUFpQjtFQUNqQixhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osc0JBQXNCO0VBQ3RCLHdDQUF3QyxFQUFBIiwiZmlsZSI6ImFwcC9jb25uZWN0aW9ucy9jb25uZWN0aW9ucy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNjb25uZWN0aW9uX2hlYWRlciB7XHJcbiAgYnV0dG9uIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDNweDtcclxuICAgIG1hcmdpbjogNXB4IDNweCAwcHggMHB4O1xyXG4gIH1cclxufVxyXG5cclxuI2Nvbm5lY3Rpb25fY29udGVudCB7XHJcbiAgLmstZ3JpZCB7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgfVxyXG59XHJcblxyXG4uY3VzdG9tX21vZGVsIHtcclxuICBkaXYge1xyXG4gICAgbWluLWhlaWdodDogMzAwcHg7XHJcbiAgfVxyXG59XHJcbi5uYXYgPiBsaSA+IGEge1xyXG4gIHBhZGRpbmctbGVmdDogMzVweDtcclxuICBmb250LXNpemU6IDEycHg7XHJcbn1cclxuLm5hdiA+IGxpOmhvdmVyLFxyXG4ubmF2ID4gbGk6aG92ZXIgYSxcclxuLm5hdiA+IGxpOmhvdmVyIGEgPiBzdmcge1xyXG4gIGJhY2tncm91bmQ6ICMwMDY3YzU7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIGZpbGw6IHdoaXRlO1xyXG59XHJcbi5uYXYgPiBsaSA+IGEgPiBzdmcge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBsZWZ0OiAxMHB4O1xyXG59XHJcbi5jb250ZW50IHtcclxuICBtaW4taGVpZ2h0OiAyNTBweDtcclxuICBwYWRkaW5nOiAxNXB4O1xyXG4gIG1hcmdpbi1yaWdodDogYXV0bztcclxuICBtYXJnaW4tbGVmdDogYXV0bztcclxuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XHJcbiAgcGFkZGluZy1yaWdodDogMTVweDtcclxuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICBtYXJnaW46IDE1cHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMC4yNXJlbTtcclxuICBib3gtc2hhZG93OiAwIDJweCA1cHggcmdiYSgwLCAwLCAwLCAwLjEpO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/connections/connections.component.ts":
/*!******************************************************!*\
  !*** ./src/app/connections/connections.component.ts ***!
  \******************************************************/
/*! exports provided: Connection, ConnectionsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Connection", function() { return Connection; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConnectionsComponent", function() { return ConnectionsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _connection_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./connection-service */ "./src/app/connections/connection-service.ts");
/* harmony import */ var _model_connection__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../model/connection */ "./src/app/model/connection.ts");
/* harmony import */ var _shared_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared/data.service */ "./src/app/shared/data.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var intro_js_intro_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! intro.js/intro.js */ "./node_modules/intro.js/intro.js");
/* harmony import */ var intro_js_intro_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(intro_js_intro_js__WEBPACK_IMPORTED_MODULE_6__);







var Connection = /** @class */ (function () {
    function Connection(connection) {
        connection = connection || {};
        this.connectionId = connection.connectionId || '';
        this.type = connection.type || '';
        this.name = connection.name || '';
        this.aliasName = connection.aliasName || '';
        this.createdDate = connection.createdDate || '';
        this.updatedDate = connection.updatedDate || '';
        this.createdBy = connection.createdBy || '';
        this.updatedBy = connection.updatedBy || '';
        this.toggle = connection.toggle || false;
        this.isDisabled = connection.isDisabled || true;
    }
    return Connection;
}());

var ConnectionsComponent = /** @class */ (function () {
    function ConnectionsComponent(connectionService, router, route, dataService, eRef) {
        this.connectionService = connectionService;
        this.router = router;
        this.route = route;
        this.dataService = dataService;
        this.eRef = eRef;
        this.state = {
            skip: 0,
            take: 10
        };
        this.connectionId = new Array();
        this.deleteConnection = Array();
        this.selectedValue = 0;
        this.selectedConnector = '';
        this.connectionName = '';
        this.aliasName = '';
        this.connectionPageSizes = true;
        this.previousNext = true;
        this.info = true;
        this.buttonCount = 5;
        this.type = 'input';
        this.introJS = intro_js_intro_js__WEBPACK_IMPORTED_MODULE_6__();
        this.isConnectionVisible = false;
        this.isAlertModelVisible = false;
        this.connectionFilter = new _model_connection__WEBPACK_IMPORTED_MODULE_3__["ConnectionFilter"]();
        this.connectionFilter.type = '';
        this.connectionFilter.name = '';
        this.connectionFilter.aliasName = '';
        this.connectionFilter.sortingType = 'desc';
        this.connectionFilter.sortableColumn = 'createdDate';
        this.connectionFilter.startRecord = 0;
        this.connectionFilter.recordCount = 10;
        this.connectionPageSize = 10;
        this.connections = Array();
        this.connectionGridLoading = false;
        this.introJS.setOptions({
            steps: [
                {
                    element: '#connectionBtn',
                    intro: 'Press New Connection Button'
                },
            ]
        });
    }
    ConnectionsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getAllConnection();
        if (this.route.snapshot.queryParams.action === 'intro') {
            this.introJS.start();
            this.introJS.onchange(function (targetElement) {
            });
            this.introJS.oncomplete(function () {
                _this.router.navigate(['/connections'], { queryParams: {} });
            });
            this.introJS.onexit(function () {
                _this.router.navigate(['/connections'], { queryParams: {} });
            });
        }
    };
    ConnectionsComponent.prototype.dataStateChange = function (state) {
        this.state = state;
    };
    ConnectionsComponent.prototype.onInputOppIdQF = function (e, filterService) {
        filterService.filter({
            filters: [{
                    field: 'Connection Name',
                    operator: 'contains',
                    value: e.srcElement.value
                }],
            logic: 'and'
        });
    };
    ConnectionsComponent.prototype.getAllConnection = function () {
        var _this = this;
        this.connectionGridLoading = true;
        this.connectionService.getAllConnection(this.connectionFilter, this.connectionFilter.startRecord, this.connectionFilter.recordCount, this.connectionFilter.sortableColumn, this.connectionFilter.sortingType).subscribe(function (data) {
            _this.connectionGridLoading = false;
            _this.connections = Array();
            _this.connectionsGridDataResult = {
                data: _this.connections,
                total: 0
            };
            if (JSON.parse(JSON.stringify(data)).results[0].connectionResponseList !== null) {
                JSON.parse(JSON.stringify(data)).results[0].connectionResponseList.forEach(function (element) {
                    _this.connections.push(new Connection(element));
                });
                _this.connectionsGridDataResult = {
                    data: _this.connections,
                    total: JSON.parse(JSON.stringify(data)).results[0].totalRecordCount
                };
            }
        });
    };
    ConnectionsComponent.prototype.allClear = function () {
        this.connectionFilter = new _model_connection__WEBPACK_IMPORTED_MODULE_3__["ConnectionFilter"]();
        this.getAllConnection();
    };
    ConnectionsComponent.prototype.pageChangeConnection = function (event) {
        this.connectionFilter.startRecord = event.skip;
        this.getAllConnection();
    };
    ConnectionsComponent.prototype.sortChangeConnection = function (sort) {
        this.connectionFilter.sortingType = sort[0]['dir'] === undefined ? 'asc' : this.connectionFilter.sortingType;
        this.connectionFilter.sortableColumn = sort[0]['field'];
        this.connectionFilter.startRecord = 0;
        this.getAllConnection();
    };
    ConnectionsComponent.prototype.filterChange = function (filter) {
        this.connectionFilter.startRecord = 0;
        this.connectionFilter.recordCount = 10;
        this.state.skip = 0;
        this.state.take = 10;
        if (filter.filters.length === 0) {
            // this.connectionFilter = new ConnectionFilter();
            this.connectionFilter.type = "";
            this.connectionFilter = new _model_connection__WEBPACK_IMPORTED_MODULE_3__["ConnectionFilter"]();
        }
        this.getAllConnection();
    };
    ConnectionsComponent.prototype.filterChangeConnection = function (e, filterService, filterOn) {
        this.connectionFilter[filterOn] = e.srcElement.value;
        filterService.filter({
            filters: [{
                    field: filterOn,
                    operator: 'contains',
                    value: e.srcElement.value
                }], logic: 'and'
        });
    };
    ConnectionsComponent.prototype.onToggle = function (account) {
        this.connectionsGridDataResult.data.map(function (n) { return n.toggle = false; });
        account.toggle = !account.toggle;
    };
    ConnectionsComponent.prototype.newConnection = function () {
        this.dataService.setConnectionDetail(new Connection());
        this.dataService.setConnector('0');
        this.isConnectionVisible = !this.isConnectionVisible;
        if (this.route.snapshot.queryParams.action === 'intro') {
            this.introJS.goToStepNumber(1).start();
        }
    };
    ConnectionsComponent.prototype.editConnection = function (connection) {
        var _this = this;
        this.connectionService.getConnectionDetail(connection.connectionId).subscribe(function (data) {
            console.log(data);
            _this.dataService.setConnectionDetail(JSON.parse(JSON.stringify(data)).results[0]);
            _this.isConnectionVisible = !_this.isConnectionVisible;
            // this.selectedConnection;
        }, function (error) {
        });
    };
    ConnectionsComponent.prototype.removeConnection = function (connection) {
        var _this = this;
        this.connectionService.delete(connection.connectionId).subscribe(function (data) {
            _this.getAllConnection();
        }, function (error) {
        });
    };
    ConnectionsComponent.prototype.cellClickHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex, columnIndex = _a.columnIndex, dataItem = _a.dataItem, isEdited = _a.isEdited;
        if (columnIndex > 0) {
            this.editConnection(dataItem);
        }
    };
    ConnectionsComponent.prototype.documentClick = function (event) {
        if (event.srcElement.alt !== undefined && event.srcElement.alt === 'vertical-dot') {
        }
        else if (this.eRef.nativeElement.contains(event.target)) {
            this.connectionsGridDataResult.data.map(function (n) { return n.toggle = false; });
        }
        else {
            this.connectionsGridDataResult.data.map(function (n) { return n.toggle = false; });
        }
    };
    ConnectionsComponent.prototype.ngOnDestroy = function () {
        this.introJS.exit();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('document:click', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], ConnectionsComponent.prototype, "documentClick", null);
    ConnectionsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-connections',
            template: __webpack_require__(/*! ./connections.component.html */ "./src/app/connections/connections.component.html"),
            styles: [__webpack_require__(/*! ./connections.component.scss */ "./src/app/connections/connections.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_connection_service__WEBPACK_IMPORTED_MODULE_2__["ConnectionService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
            _shared_data_service__WEBPACK_IMPORTED_MODULE_4__["DataService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]])
    ], ConnectionsComponent);
    return ConnectionsComponent;
}());



/***/ }),

/***/ "./src/app/connector/api-connector/api-connector.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/connector/api-connector/api-connector.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvY29ubmVjdG9yL2FwaS1jb25uZWN0b3IvYXBpLWNvbm5lY3Rvci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/connector/api-connector/api-connector.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/connector/api-connector/api-connector.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"rdbms_parameters\">\r\n  <form #form=\"ngForm\" (ngSubmit)=\"submitForm(form.value)\">\r\n    <div class=\"form-group\">\r\n      <div class=\"row\">\r\n        <span id=\"connectionName\">\r\n          <div class=\"col-lg-6\">\r\n            <p class=\"title\"><span class=\"astrick\">*</span>Connection Name</p>\r\n            <!-- <input kendoTextBox class=\"kendo-input\" placeholder=\"Connection Name\"\r\n          [(ngModel)]=\"connectorField.connectionName\" /> -->\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Connection Name\" name=\"connectionName\"\r\n              [(ngModel)]=\"connectorField.connectionName\" ngModel required>\r\n          </div>\r\n        </span>\r\n        <span id=\"aliasName\">\r\n          <div class=\"col-lg-6\">\r\n            <p class=\"title\">Alias Name(Optional)</p>\r\n            <!-- <input kendoTextBox class=\"kendo-input\" placeholder=\"Alias Name\" [(ngModel)]=\"connectorField.aliasName\" /> -->\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Alias Name\" name=\"aliasName\" id\r\n              [(ngModel)]=\"connectorField.aliasName\" ngModel>\r\n          </div>\r\n        </span>\r\n      </div>\r\n    </div>\r\n  \r\n    <div class=\"form-group\">\r\n      <div class=\"row\">\r\n        <span id=\"url\">\r\n          <div class=\"col-lg-12\">\r\n            <p class=\"title\"><span class=\"astrick\">*</span>Url</p>\r\n            <!-- <input kendoTextBox class=\"kendo-input\" [(ngModel)]=\"connectorField.host\" placeholder=\"Host\" /> -->\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Url\" name=\"url\" [(ngModel)]=\"connectorField.url\"\r\n              ngModel required>\r\n          </div>\r\n        </span>\r\n      </div>\r\n      <div class=\"row\">\r\n        <span id=\"url\">\r\n          <div class=\"col-lg-12\">\r\n            <p class=\"title\"><span class=\"astrick\">*</span>Health Check</p>\r\n            <!-- <input kendoTextBox class=\"kendo-input\" [(ngModel)]=\"connectorField.host\" placeholder=\"Host\" /> -->\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Url\" name=\"url\" [(ngModel)]=\"connectorField.testUrl\"\r\n              ngModel required>\r\n          </div>\r\n        </span>\r\n      </div>\r\n    </div>\r\n    <!-- <div class=\"form-group\">\r\n      <div class=\"row\">\r\n        <span id=\"userName\">\r\n          <div class=\"col-lg-6\">\r\n            <p class=\"title\"><span class=\"astrick\">*</span>Username</p>\r\n\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Username\" name=\"userName\"\r\n              [(ngModel)]=\"connectorField.userName\" ngModel required>\r\n          </div>\r\n        </span>\r\n        <span id=\"password\">\r\n          <div class=\"col-lg-6\">\r\n            <p class=\"title\"><span class=\"astrick\">*</span>Password </p>\r\n            <input [type]=\"isShow ? 'text' : 'password'\" class=\"form-control\" placeholder=\"Password\" name=\"password\"\r\n              [(ngModel)]=\"connectorField.password\" ngModel required>\r\n            <span toggle=\"#password-field\" (click)=\"this.isShow = !this.isShow\" class=\"fa toggle-password\"\r\n              [ngClass]=\"{ 'fa-eye': isShow, 'fa-eye-slash': !isShow}\"></span>\r\n          </div>\r\n        </span>\r\n      </div>\r\n    </div> -->\r\n    <div class=\"form-group\">\r\n      <div class=\"row\">\r\n        <div class=\"col-lg-12\">\r\n          <span id=\"saveBtn\">\r\n            <button kendoButton class=\"pull-right\" primary=\"true\" (click)=\"onSaveConnection()\"\r\n              [disabled]=\"!form.valid\">Save</button>\r\n          </span>\r\n          <span id=\"closeBtn\">\r\n            <button kendoButton class=\"pull-right cancel_button\" (click)=\"closeIt()\">Cancel</button>\r\n          </span>\r\n          <span id=\"testConnection\">\r\n            <button class=\"pull-right test_connection\" (click)=\"testConnection()\" [disabled]=\"!form.valid\">Test\r\n              Connection</button>\r\n          </span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</div>\r\n<div class=\"overlay\" *ngIf=\"isProcessing\">\r\n  <i class=\"fa fa-refresh fa-spin\"></i>\r\n  <p>{{processingText}}</p>\r\n</div>"

/***/ }),

/***/ "./src/app/connector/api-connector/api-connector.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/connector/api-connector/api-connector.component.ts ***!
  \********************************************************************/
/*! exports provided: ApiConnectorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiConnectorComponent", function() { return ApiConnectorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _connections_connection_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../connections/connection-service */ "./src/app/connections/connection-service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _shared_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/data.service */ "./src/app/shared/data.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _model_connection__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../model/connection */ "./src/app/model/connection.ts");
/* harmony import */ var _model_connector__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../model/connector */ "./src/app/model/connector.ts");








var ApiConnectorComponent = /** @class */ (function () {
    function ApiConnectorComponent(connectionService, toastr, dataService, router, route) {
        var _this = this;
        this.connectionService = connectionService;
        this.toastr = toastr;
        this.dataService = dataService;
        this.router = router;
        this.route = route;
        this.saveConnection = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.isProcessing = false;
        this.processingText = '';
        this.connectionFields = new _model_connection__WEBPACK_IMPORTED_MODULE_6__["Connection"]();
        this.connectorField = new _model_connector__WEBPACK_IMPORTED_MODULE_7__["Connector"]();
        this.dataService.getConnector.subscribe(function (connector) { return _this.connectorField['connectionType'] = connector; });
        this.dataService.getConnectionDetail().subscribe(function (connection) {
            if (connection.connectionId !== '') {
                _this.connectionFields = connection;
                _this.dataService.setConnector(_this.connectionFields.type);
                var parameters = Object.keys(_this.connectionFields.connectionConfigRequestList);
                parameters.forEach(function (element) {
                    _this.connectorField[element] = _this.connectionFields.connectionConfigRequestList[element];
                });
            }
        });
        this.isShow = false;
        this.dataService.invokeEvent.subscribe(function (value) {
            if (value === 'connectorVal') {
                _this.dataService.getConnector.subscribe(function (connector) { return _this.connectorField['connectionType'] = connector; });
            }
        });
    }
    ApiConnectorComponent.prototype.ngOnInit = function () {
    };
    ApiConnectorComponent.prototype.testConnection = function () {
        var _this = this;
        this.isProcessing = true;
        this.processingText = 'Validating...';
        this.dataService.getConnector.subscribe(function (connector) {
            return _this.connectorField['connectionType'] = connector;
        });
        this.connectionService.testConnection(this.connectorField).subscribe(function (response) {
            var res = JSON.parse(JSON.stringify(response));
            if (res.results[0].status) {
                _this.toastr.info(res.results[0].message);
            }
            else {
                _this.toastr.error(res.results[0].message);
            }
            _this.isProcessing = false;
            _this.processingText = '';
        }, function (error) {
            _this.isProcessing = false;
            _this.processingText = '';
        });
    };
    ApiConnectorComponent.prototype.onSaveConnection = function () {
        var _this = this;
        this.isProcessing = true;
        this.processingText = 'Validating...';
        // this.dataService.getConnector.subscribe(connector => this.connectorField['connectionType'] = connector);
        this.connectionService.testConnection(this.connectorField).subscribe(function (response) {
            var res = JSON.parse(JSON.stringify(response));
            if (res.results[0].status) {
                _this.connectionFields.name = _this.connectorField.connectionName;
                _this.connectionFields.aliasName = _this.connectorField.aliasName;
                _this.dataService.getConnector.subscribe(function (connector) { return _this.connectionFields.type = connector; });
                var parameters = Object.keys(_this.connectorField);
                _this.connectionFields.connectionConfigRequestList = [];
                parameters.forEach(function (element) {
                    _this.connectionFields.connectionConfigRequestList.push({ key: element, value: _this.connectorField[element] });
                });
                _this.isProcessing = true;
                _this.processingText = 'Processing...';
                if (_this.connectionFields.connectionId === undefined) {
                    _this.connectionService.add(_this.connectionFields).subscribe(function (response) {
                        var res = JSON.parse(JSON.stringify(response));
                        if (res.sucess === false) {
                            _this.saveConnection.emit('2');
                        }
                        _this.saveConnection.emit('2');
                        _this.connectionFields = new _model_connection__WEBPACK_IMPORTED_MODULE_6__["Connection"]();
                        _this.connectorField = new _model_connector__WEBPACK_IMPORTED_MODULE_7__["Connector"]();
                        _this.dataService.setConnectionDetail(new _model_connection__WEBPACK_IMPORTED_MODULE_6__["Connection"]());
                        _this.dataService.setConnector('0');
                        _this.isProcessing = false;
                        _this.processingText = '';
                    }, function (error) {
                        _this.isProcessing = false;
                        _this.processingText = '';
                    });
                }
                else {
                    _this.connectionService.update(_this.connectionFields.connectionId, _this.connectionFields).subscribe(function (response) {
                        var res = JSON.parse(JSON.stringify(response));
                        if (res.sucess === false) {
                            _this.saveConnection.emit('2');
                        }
                        _this.saveConnection.emit('2');
                        _this.connectionFields = new _model_connection__WEBPACK_IMPORTED_MODULE_6__["Connection"]();
                        _this.connectorField = new _model_connector__WEBPACK_IMPORTED_MODULE_7__["Connector"]();
                        _this.dataService.setConnectionDetail(new _model_connection__WEBPACK_IMPORTED_MODULE_6__["Connection"]());
                        _this.dataService.setConnector('0');
                        _this.isProcessing = false;
                        _this.processingText = '';
                    }, function (error) {
                        _this.isProcessing = false;
                        _this.processingText = '';
                    });
                }
            }
            else {
                _this.toastr.error(res.results[0].message);
            }
            _this.isProcessing = false;
            _this.processingText = '';
        }, function (error) {
            _this.isProcessing = false;
            _this.processingText = '';
        });
    };
    ApiConnectorComponent.prototype.closeIt = function () {
        this.dataService.setConnectionDetail(new _model_connection__WEBPACK_IMPORTED_MODULE_6__["Connection"]());
        this.dataService.setConnector('0');
        this.saveConnection.emit('2');
    };
    ApiConnectorComponent.prototype.ngOnDestroy = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _model_connection__WEBPACK_IMPORTED_MODULE_6__["Connection"])
    ], ApiConnectorComponent.prototype, "connectionFields", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _model_connector__WEBPACK_IMPORTED_MODULE_7__["Connector"])
    ], ApiConnectorComponent.prototype, "connectorField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ApiConnectorComponent.prototype, "saveConnection", void 0);
    ApiConnectorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-api-connector',
            template: __webpack_require__(/*! ./api-connector.component.html */ "./src/app/connector/api-connector/api-connector.component.html"),
            styles: [__webpack_require__(/*! ./api-connector.component.css */ "./src/app/connector/api-connector/api-connector.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_connections_connection_service__WEBPACK_IMPORTED_MODULE_2__["ConnectionService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"],
            _shared_data_service__WEBPACK_IMPORTED_MODULE_4__["DataService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]])
    ], ApiConnectorComponent);
    return ApiConnectorComponent;
}());



/***/ }),

/***/ "./src/app/connector/odata-connector/odata-connector.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/connector/odata-connector/odata-connector.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"odata_parameters\" style=\"margin-top: 5%\">\r\n  <form #form=\"ngForm\" (ngSubmit)=\"submitForm(form.value)\">\r\n    <div class=\"form-group\">\r\n      <div class=\"row\">\r\n        <span id = \"connectionName\">\r\n        <div class=\"col-lg-6\">\r\n          <p class=\"title\"><span class=\"astrick\">*</span>Connection Name </p>\r\n          <!-- <input kendoTextBox class=\"kendo-input\" placeholder=\"Connection Name\"\r\n            [(ngModel)]=\"connectorField.connectionName\" /> -->\r\n          <input type=\"text\" class=\"form-control\" placeholder=\"Connection Name\" name=\"connectionName\"\r\n            [(ngModel)]=\"connectorField.connectionName\" ngModel required>\r\n        </div>\r\n        </span>\r\n        <span id = \"aliasName\">\r\n        <div class=\"col-lg-6\">\r\n          <p class=\"title\">Alias Name(Optional)</p>\r\n          <!-- <input kendoTextBox class=\"kendo-input\" placeholder=\"Alias Name\" [(ngModel)]=\"connectorField.aliasName\" /> -->\r\n          <input type=\"text\" class=\"form-control\" placeholder=\"Alias Name\" name=\"aliasName\"\r\n            [(ngModel)]=\"connectorField.aliasName\" ngModel>\r\n        </div>\r\n        </span>\r\n      </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n      <div class=\"row\">\r\n        <span id=\"connectionUrl\">\r\n        <div class=\"col-lg-12\">\r\n          <p class=\"title\"><span class=\"astrick\">*</span>Connection Url </p>\r\n          <!-- <input kendoTextBox class=\"kendo-input\" [(ngModel)]=\"connectorField.url\" placeholder=\"Connection Url\" /> -->\r\n          <input type=\"text\" class=\"form-control\" placeholder=\"Connection Url\" name=\"connectionUrl\"\r\n            [(ngModel)]=\"connectorField.url\" ngModel required>\r\n        </div>\r\n        </span>\r\n      </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n      <div class=\"row\">\r\n       <span id=\"userName\">\r\n        <div class=\"col-lg-6\">\r\n          <p class=\"title\"><span class=\"astrick\">*</span>Username</p>\r\n          <!-- <input kendoTextBox class=\"kendo-input\" [(ngModel)]=\"connectorField.userName\" placeholder=\"Username\" /> -->\r\n          <input type=\"text\" class=\"form-control\" placeholder=\"Username\" name=\"userName\" \r\n            [(ngModel)]=\"connectorField.userName\" ngModel required>\r\n        </div>\r\n        </span>\r\n       \r\n        <span id=\"password\">\r\n        <div class=\"col-lg-6\">\r\n          <p class=\"title\"><span class=\"astrick\">*</span>Password </p>\r\n          <input [type]=\"isShow ? 'text' : 'password'\" class=\"form-control\" placeholder=\"Password\" name=\"password\"\r\n            [(ngModel)]=\"connectorField.password\" ngModel required>\r\n          <span toggle=\"#password-field\" (click)=\"this.isShow = !this.isShow\" class=\"fa toggle-password\" [ngClass]=\"{ 'fa-eye': isShow, 'fa-eye-slash': !isShow}\"></span>\r\n        </div>\r\n        </span>\r\n      </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n      <div class=\"row\">\r\n        <div class=\"col-lg-12\">\r\n          <span id = \"saveBtn\">\r\n          <button kendoButton class=\"pull-right\" primary=\"true\" (click)=\"onSaveConnection()\"\r\n            [disabled]=\"!form.valid\">Save</button>\r\n            </span>\r\n            <span id=\"closeBtn\">\r\n          <button kendoButton class=\"pull-right cancel_button\" (click)=\"closeIt()\">Close</button>\r\n          </span>\r\n          <span id=\"testConnection\">\r\n          <button class=\"pull-right test_connection\" (click)=\"testConnection()\" [disabled]=\"!form.valid\">Test\r\n            Connection</button>\r\n            </span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </form>\r\n  <div class=\"overlay\" *ngIf=\"isProcessing\">\r\n    <i class=\"fa fa-refresh fa-spin\"></i>\r\n    <p>{{processingText}}</p>\r\n  </div>"

/***/ }),

/***/ "./src/app/connector/odata-connector/odata-connector.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/connector/odata-connector/odata-connector.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#odata_parameters .row {\n  margin-bottom: 10px; }\n\n#odata_parameters p.title {\n  font-weight: 600;\n  font-size: 12px;\n  margin-bottom: 0; }\n\n#odata_parameters .astrick {\n  color: red; }\n\n#odata_parameters input {\n  width: 100%; }\n\n#odata_parameters button {\n  margin: 0px 3px; }\n\n#odata_parameters button.test_connection {\n  background-color: #e3a54c;\n  color: white;\n  border-radius: 2px;\n  padding: 4px 8px;\n  box-sizing: border-box;\n  border-width: 1px;\n  border-style: solid;\n  font-size: 14px;\n  line-height: 1.42857;\n  font-family: inherit;\n  text-align: center;\n  text-decoration: none;\n  white-space: nowrap;\n  display: inline-flex;\n  align-items: center;\n  justify-content: center;\n  vertical-align: middle;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  cursor: pointer;\n  outline: none;\n  -webkit-appearance: none;\n  position: relative;\n  border: solid 1px #c5862b; }\n\n.overlay > .fa {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  margin-left: -15px;\n  margin-top: -15px;\n  color: #000;\n  font-size: 30px; }\n\n.overlay > p {\n  position: absolute;\n  top: 65%;\n  left: 49%;\n  margin-left: -15px;\n  margin-top: -15px;\n  color: #000;\n  font-size: 12px; }\n\n.overlay > p.loading {\n  left: 49%; }\n\n.overlay > p.processing {\n  left: 49%; }\n\n.overlay > p.validating {\n  left: 49%; }\n\n.toggle-password {\n  position: absolute;\n  right: 20px;\n  top: 20px;\n  border: solid 1px #323639;\n  font-size: 16px;\n  padding: 4px;\n  border-radius: 50%;\n  width: 27px;\n  background: #323639;\n  color: white;\n  height: 27px;\n  cursor: pointer; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb25uZWN0b3Ivb2RhdGEtY29ubmVjdG9yL0U6XFxVSS1Qcm9qZWN0XFxFVExcXHNyY1xcc3JjL2FwcFxcY29ubmVjdG9yXFxvZGF0YS1jb25uZWN0b3JcXG9kYXRhLWNvbm5lY3Rvci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLG1CQUFtQixFQUFBOztBQUZ2QjtFQUtJLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsZ0JBQWdCLEVBQUE7O0FBUHBCO0VBVUksVUFBVSxFQUFBOztBQVZkO0VBY0ksV0FBVyxFQUFBOztBQWRmO0VBaUJJLGVBQWUsRUFBQTs7QUFqQm5CO0VBb0JJLHlCQUF5QjtFQUN6QixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixzQkFBc0I7RUFDdEIsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2Ysb0JBQW9CO0VBQ3BCLG9CQUFvQjtFQUNwQixrQkFBa0I7RUFDbEIscUJBQXFCO0VBQ3JCLG1CQUFtQjtFQUNuQixvQkFBb0I7RUFDcEIsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixzQkFBc0I7RUFDdEIseUJBQXlCO0VBQ3pCLHNCQUFzQjtFQUN0QixxQkFBcUI7RUFDckIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixhQUFhO0VBQ2Isd0JBQXdCO0VBQ3hCLGtCQUFrQjtFQUNsQix5QkFBeUIsRUFBQTs7QUFJN0I7RUFDRSxrQkFBa0I7RUFDbEIsUUFBUTtFQUNSLFNBQVM7RUFDVCxrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLFdBQVc7RUFDWCxlQUFlLEVBQUE7O0FBRWpCO0VBQ0Usa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixTQUFTO0VBQ1Qsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixXQUFXO0VBQ1gsZUFBZSxFQUFBOztBQUVqQjtFQUNFLFNBQVMsRUFBQTs7QUFHWDtFQUNFLFNBQVMsRUFBQTs7QUFHWDtFQUNFLFNBQVMsRUFBQTs7QUFFWDtFQUNFLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsU0FBUztFQUNULHlCQUF5QjtFQUN6QixlQUFlO0VBQ2YsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixZQUFZO0VBQ1osZUFBZSxFQUFBIiwiZmlsZSI6ImFwcC9jb25uZWN0b3Ivb2RhdGEtY29ubmVjdG9yL29kYXRhLWNvbm5lY3Rvci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNvZGF0YV9wYXJhbWV0ZXJzIHtcclxuICAucm93IHtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgfVxyXG4gIHAudGl0bGUge1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgfVxyXG4gIC5hc3RyaWNrIHtcclxuICAgIGNvbG9yOiByZWQ7XHJcbiAgfVxyXG5cclxuICBpbnB1dCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbiAgYnV0dG9uIHtcclxuICAgIG1hcmdpbjogMHB4IDNweDtcclxuICB9XHJcbiAgYnV0dG9uLnRlc3RfY29ubmVjdGlvbiB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTNhNTRjO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG4gICAgcGFkZGluZzogNHB4IDhweDtcclxuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICBib3JkZXItd2lkdGg6IDFweDtcclxuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBsaW5lLWhlaWdodDogMS40Mjg1NztcclxuICAgIGZvbnQtZmFtaWx5OiBpbmhlcml0O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgIGRpc3BsYXk6IGlubGluZS1mbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuICAgIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgICAtbW96LXVzZXItc2VsZWN0OiBub25lO1xyXG4gICAgLW1zLXVzZXItc2VsZWN0OiBub25lO1xyXG4gICAgdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgYm9yZGVyOiBzb2xpZCAxcHggI2M1ODYyYjtcclxuICB9XHJcbn1cclxuXHJcbi5vdmVybGF5ID4gLmZhIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiA1MCU7XHJcbiAgbGVmdDogNTAlO1xyXG4gIG1hcmdpbi1sZWZ0OiAtMTVweDtcclxuICBtYXJnaW4tdG9wOiAtMTVweDtcclxuICBjb2xvcjogIzAwMDtcclxuICBmb250LXNpemU6IDMwcHg7XHJcbn1cclxuLm92ZXJsYXkgPiBwIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiA2NSU7XHJcbiAgbGVmdDogNDklO1xyXG4gIG1hcmdpbi1sZWZ0OiAtMTVweDtcclxuICBtYXJnaW4tdG9wOiAtMTVweDtcclxuICBjb2xvcjogIzAwMDtcclxuICBmb250LXNpemU6IDEycHg7XHJcbn1cclxuLm92ZXJsYXkgPiBwLmxvYWRpbmcge1xyXG4gIGxlZnQ6IDQ5JTtcclxufVxyXG5cclxuLm92ZXJsYXkgPiBwLnByb2Nlc3Npbmcge1xyXG4gIGxlZnQ6IDQ5JTtcclxufVxyXG5cclxuLm92ZXJsYXkgPiBwLnZhbGlkYXRpbmcge1xyXG4gIGxlZnQ6IDQ5JTtcclxufVxyXG4udG9nZ2xlLXBhc3N3b3JkIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgcmlnaHQ6IDIwcHg7XHJcbiAgdG9wOiAyMHB4O1xyXG4gIGJvcmRlcjogc29saWQgMXB4ICMzMjM2Mzk7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIHBhZGRpbmc6IDRweDtcclxuICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgd2lkdGg6IDI3cHg7XHJcbiAgYmFja2dyb3VuZDogIzMyMzYzOTtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgaGVpZ2h0OiAyN3B4O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/connector/odata-connector/odata-connector.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/connector/odata-connector/odata-connector.component.ts ***!
  \************************************************************************/
/*! exports provided: ODataConnectorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ODataConnectorComponent", function() { return ODataConnectorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_model_connector__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/model/connector */ "./src/app/model/connector.ts");
/* harmony import */ var src_app_connections_connection_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/connections/connection-service */ "./src/app/connections/connection-service.ts");
/* harmony import */ var src_app_model_connection__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/model/connection */ "./src/app/model/connection.ts");
/* harmony import */ var src_app_shared_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/shared/data.service */ "./src/app/shared/data.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var src_app_jobs_job_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/jobs/job.service */ "./src/app/jobs/job.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var intro_js_intro_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! intro.js/intro.js */ "./node_modules/intro.js/intro.js");
/* harmony import */ var intro_js_intro_js__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(intro_js_intro_js__WEBPACK_IMPORTED_MODULE_9__);











var ODataConnectorComponent = /** @class */ (function () {
    function ODataConnectorComponent(connectionService, jobService, router, route, toastr, dataService) {
        var _this = this;
        this.connectionService = connectionService;
        this.jobService = jobService;
        this.router = router;
        this.route = route;
        this.toastr = toastr;
        this.dataService = dataService;
        this.saveConnection = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.introJS = intro_js_intro_js__WEBPACK_IMPORTED_MODULE_9__();
        this.isProcessing = false;
        this.processingText = '';
        this.connectionFields = new src_app_model_connection__WEBPACK_IMPORTED_MODULE_4__["Connection"]();
        this.connectorField = new src_app_model_connector__WEBPACK_IMPORTED_MODULE_2__["Connector"]();
        this.dataService.getConnectionDetail().subscribe(function (connection) {
            if (connection.connectionId !== '') {
                _this.connectionFields = connection;
                _this.dataService.setConnector(_this.connectionFields.type);
                var parameters = Object.keys(_this.connectionFields.connectionConfigRequestList);
                parameters.forEach(function (element) {
                    _this.connectorField[element] = _this.connectionFields.connectionConfigRequestList[element];
                });
            }
        });
        this.isShow = false;
        this.introJS.setOptions({
            steps: [
                {
                    element: '#connectionName',
                    intro: 'Write Connection Name '
                },
                {
                    element: '#aliasName',
                    intro: 'Write alias name (optional)',
                    position: 'left'
                },
                {
                    element: '#connectionUrl',
                    intro: 'Write Connection Url',
                    position: 'left'
                },
                {
                    element: '#userName',
                    intro: 'Write User Name',
                    position: 'left'
                },
                {
                    element: '#password',
                    intro: 'Write Password',
                    position: 'left'
                },
                {
                    element: '#testConnection',
                    intro: 'Press Test Connection'
                },
                {
                    element: '#saveBtn',
                    intro: 'Press Save Connection'
                },
                {
                    element: '#closeBtn',
                    intro: ' Press Close button'
                },
            ]
        });
    }
    ODataConnectorComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.route.snapshot.queryParams.action === 'intro') {
            this.introJS.start();
            this.introJS.onchange(function (targetElement) {
            });
            this.introJS.oncomplete(function () {
                _this.router.navigate(['/connections'], { queryParams: {} });
            });
            this.introJS.onexit(function () {
                // this.router.navigate(['/connections'], { queryParams: {} });
            });
        }
    };
    ODataConnectorComponent.prototype.testConnection = function () {
        var _this = this;
        this.isProcessing = true;
        this.processingText = 'Validating...';
        this.dataService.getConnector.subscribe(function (connector) { return _this.connectorField['connectionType'] = connector; });
        this.connectionService.testConnection(this.connectorField).subscribe(function (response) {
            var res = JSON.parse(JSON.stringify(response));
            if (res.results[0].status) {
                _this.toastr.info(res.results[0].message);
            }
            else {
                _this.toastr.error(res.results[0].message);
            }
            if (_this.route.snapshot.queryParams.action === 'intro') {
                _this.introJS.goToStepNumber(6).start();
            }
            _this.isProcessing = false;
            _this.processingText = '';
        }, function (error) {
            _this.isProcessing = false;
            _this.processingText = '';
        });
    };
    ODataConnectorComponent.prototype.onSaveConnection = function () {
        var _this = this;
        this.isProcessing = true;
        this.processingText = 'Validating...';
        this.dataService.getConnector.subscribe(function (connector) { return _this.connectorField['connectionType'] = connector; });
        this.connectionService.testConnection(this.connectorField).subscribe(function (response) {
            var res = JSON.parse(JSON.stringify(response));
            if (res.results[0].status === false) {
                _this.connectionFields.name = _this.connectorField.connectionName;
                _this.connectionFields.aliasName = _this.connectorField.aliasName;
                _this.dataService.getConnector.subscribe(function (connector) { return _this.connectionFields.type = connector; });
                var parameters = Object.keys(_this.connectorField);
                _this.connectionFields.connectionConfigRequestList = [];
                parameters.forEach(function (element) {
                    _this.connectionFields.connectionConfigRequestList.push({ key: element, value: _this.connectorField[element] });
                });
                _this.isProcessing = true;
                _this.processingText = 'Processing...';
                if (_this.connectionFields.connectionId === undefined) {
                    _this.connectionService.add(_this.connectionFields).subscribe(function (response) {
                        var res = JSON.parse(JSON.stringify(response));
                        if (res.sucess === false) {
                            _this.saveConnection.emit('2');
                        }
                        _this.saveConnection.emit('2');
                        _this.connectionFields = new src_app_model_connection__WEBPACK_IMPORTED_MODULE_4__["Connection"]();
                        _this.connectorField = new src_app_model_connector__WEBPACK_IMPORTED_MODULE_2__["Connector"]();
                        _this.dataService.setConnectionDetail(new src_app_model_connection__WEBPACK_IMPORTED_MODULE_4__["Connection"]());
                        _this.dataService.setConnector('0');
                        _this.isProcessing = false;
                        _this.processingText = '';
                        _this.jobService.refreshOdataCollectionMetadata(res.results[0].connectionId)
                            .subscribe(function (res1) {
                            console.log(res1);
                        }, function (error) {
                            console.log(error);
                        });
                    }, function (error) {
                        _this.isProcessing = false;
                        _this.processingText = '';
                    });
                }
                else {
                    _this.connectionService.update(_this.connectionFields.connectionId, _this.connectionFields).subscribe(function (response) {
                        var res = JSON.parse(JSON.stringify(response));
                        if (res.sucess === false) {
                            _this.saveConnection.emit('2');
                        }
                        _this.saveConnection.emit('2');
                        _this.connectionFields = new src_app_model_connection__WEBPACK_IMPORTED_MODULE_4__["Connection"]();
                        _this.connectorField = new src_app_model_connector__WEBPACK_IMPORTED_MODULE_2__["Connector"]();
                        _this.dataService.setConnectionDetail(new src_app_model_connection__WEBPACK_IMPORTED_MODULE_4__["Connection"]());
                        _this.dataService.setConnector('0');
                        _this.isProcessing = false;
                        _this.processingText = '';
                        _this.jobService.refreshOdataCollectionMetadata(res.results[0].connectionId)
                            .subscribe(function (res1) {
                            console.log(res1);
                        }, function (error) {
                            console.log(error);
                        });
                    }, function (error) {
                        _this.isProcessing = false;
                        _this.processingText = '';
                    });
                }
            }
            else {
                _this.toastr.error(res.results[0].message);
            }
            _this.isProcessing = false;
            _this.processingText = '';
        }, function (error) {
            _this.isProcessing = false;
            _this.processingText = '';
        });
        if (this.route.snapshot.queryParams.action === 'intro') {
            this.introJS.goToStepNumber(7).start();
        }
    };
    ODataConnectorComponent.prototype.closeIt = function () {
        this.dataService.setConnectionDetail(new src_app_model_connection__WEBPACK_IMPORTED_MODULE_4__["Connection"]());
        this.dataService.setConnector('0');
        this.saveConnection.emit('2');
        if (this.route.snapshot.queryParams.action === 'intro') {
            this.introJS.exit();
        }
    };
    ODataConnectorComponent.prototype.ngOnDestroy = function () {
        this.introJS.exit();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", src_app_model_connection__WEBPACK_IMPORTED_MODULE_4__["Connection"])
    ], ODataConnectorComponent.prototype, "connectionFields", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", src_app_model_connector__WEBPACK_IMPORTED_MODULE_2__["Connector"])
    ], ODataConnectorComponent.prototype, "connectorField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ODataConnectorComponent.prototype, "saveConnection", void 0);
    ODataConnectorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-odata-connector',
            template: __webpack_require__(/*! ./odata-connector.component.html */ "./src/app/connector/odata-connector/odata-connector.component.html"),
            styles: [__webpack_require__(/*! ./odata-connector.component.scss */ "./src/app/connector/odata-connector/odata-connector.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_connections_connection_service__WEBPACK_IMPORTED_MODULE_3__["ConnectionService"],
            src_app_jobs_job_service__WEBPACK_IMPORTED_MODULE_7__["JobService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"],
            src_app_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["DataService"]])
    ], ODataConnectorComponent);
    return ODataConnectorComponent;
}());



/***/ }),

/***/ "./src/app/connector/rdbms-connector/rdbms-connector.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/connector/rdbms-connector/rdbms-connector.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"rdbms_parameters\">\r\n  <form #form=\"ngForm\" (ngSubmit)=\"submitForm(form.value)\">\r\n    <div class=\"form-group\">\r\n      <div class=\"row\">\r\n        <span id=\"connectionName\">\r\n          <div class=\"col-lg-6\">\r\n            <p class=\"title\"><span class=\"astrick\">*</span>Connection Name</p>\r\n            <!-- <input kendoTextBox class=\"kendo-input\" placeholder=\"Connection Name\"\r\n          [(ngModel)]=\"connectorField.connectionName\" /> -->\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Connection Name\" name=\"connectionName\"\r\n              [(ngModel)]=\"connectorField.connectionName\" ngModel required>\r\n          </div>\r\n        </span>\r\n        <span id=\"aliasName\">\r\n          <div class=\"col-lg-6\">\r\n            <p class=\"title\">Alias Name(Optional)</p>\r\n            <!-- <input kendoTextBox class=\"kendo-input\" placeholder=\"Alias Name\" [(ngModel)]=\"connectorField.aliasName\" /> -->\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Alias Name\" name=\"aliasName\" id\r\n              [(ngModel)]=\"connectorField.aliasName\" ngModel>\r\n          </div>\r\n        </span>\r\n      </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n      <div class=\"row\">\r\n        <span id=\"serviceName\">\r\n          <div class=\"col-lg-10\" *ngIf=\"connectorField.connectionType==='ORACLE'\">\r\n            <p class=\"title\"><span class=\"astrick\">*</span>Service Name</p>\r\n            <!-- <input kendoTextBox class=\"kendo-input\" [(ngModel)]=\"connectorField.serviceName\" placeholder=\"Service Name\" /> -->\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Service Name\" name=\"serviceName\"\r\n              [(ngModel)]=\"connectorField.serviceName\" ngModel required>\r\n          </div>\r\n          <div class=\"col-lg-10\" *ngIf=\"connectorField.connectionType==='MYSQL' ||connectorField.connectionType==='DATAHUG'\">\r\n            <p class=\"title\"><span class=\"astrick\">*</span>Schema Name</p>\r\n            <!-- <input kendoTextBox class=\"kendo-input\" [(ngModel)]=\"connectorField.serviceName\" placeholder=\"Service Name\" /> -->\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Schema Name\" name=\"schemaName\"\r\n              [(ngModel)]=\"connectorField.schemaName\" ngModel required>\r\n          </div>\r\n        </span>\r\n        <span id=\"port\">\r\n          <div class=\"col-lg-2\">\r\n            <p class=\"title\"><span class=\"astrick\">*</span>Port</p>\r\n            <!-- <input kendoTextBox class=\"kendo-input\" [(ngModel)]=\"connectorField.port\" placeholder=\"Port\" /> -->\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Port\" name=\"port\" [(ngModel)]=\"connectorField.port\"\r\n              ngModel required>\r\n          </div>\r\n        </span>\r\n      </div>\r\n    </div>\r\n    <div class=\"form-group\" *ngIf = \"connectorField.connectionType==='DATAHUG'\">\r\n      <div class=\"row\">\r\n        <span id=\"host\">\r\n          <div class=\"col-lg-6\">\r\n            <p class=\"title\"><span class=\"astrick\">*</span>Service Name</p>\r\n            <!-- <input kendoTextBox class=\"kendo-input\" [(ngModel)]=\"connectorField.host\" placeholder=\"Host\" /> -->\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Service Name\" name=\"serviceName\" [(ngModel)]=\"connectorField.serviceName\"\r\n              ngModel required>\r\n          </div>\r\n        </span>\r\n        <span id=\"testUrl\">\r\n          <div class=\"col-lg-6\">\r\n            <p class=\"title\"><span class=\"astrick\">*</span>Test Url</p>\r\n            <!-- <input kendoTextBox class=\"kendo-input\" [(ngModel)]=\"connectorField.host\" placeholder=\"Host\" /> -->\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Test Url\" name=\"testUrl\" [(ngModel)]=\"connectorField.testUrl\"\r\n              ngModel required>\r\n          </div>\r\n        </span>\r\n      </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n      <div class=\"row\">\r\n        <span id=\"host\">\r\n          <div class=\"col-lg-12\">\r\n            <p class=\"title\"><span class=\"astrick\">*</span>Host</p>\r\n            <!-- <input kendoTextBox class=\"kendo-input\" [(ngModel)]=\"connectorField.host\" placeholder=\"Host\" /> -->\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Host\" name=\"host\" [(ngModel)]=\"connectorField.host\"\r\n              ngModel required>\r\n          </div>\r\n        </span>\r\n      </div>\r\n    </div>\r\n    \r\n    <div class=\"form-group\" *ngIf = \"connectorField.connectionType==='DATAHUG'\">\r\n      <div class=\"row\">\r\n        <span id=\"tokenUrl\">\r\n          <div class=\"col-lg-6\">\r\n            <p class=\"title\"><span class=\"astrick\">*</span>Token Url</p>\r\n            <!-- <input kendoTextBox class=\"kendo-input\" [(ngModel)]=\"connectorField.host\" placeholder=\"Host\" /> -->\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Token Url\" name=\"tokenUrl\" [(ngModel)]=\"connectorField.tokenUrl\"\r\n              ngModel required>\r\n          </div>\r\n        </span>\r\n        <span id=\"url\">\r\n          <div class=\"col-lg-6\">\r\n            <p class=\"title\"><span class=\"astrick\">*</span>Url</p>\r\n            <!-- <input kendoTextBox class=\"kendo-input\" [(ngModel)]=\"connectorField.host\" placeholder=\"Host\" /> -->\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Url\" name=\"url\" [(ngModel)]=\"connectorField.url\"\r\n              ngModel required>\r\n          </div>\r\n        </span>\r\n      </div>\r\n    </div>\r\n  \r\n    <div class=\"form-group\">\r\n      <div class=\"row\">\r\n        <span id=\"userName\">\r\n          <div class=\"col-lg-6\">\r\n            <p class=\"title\"><span class=\"astrick\">*</span>Username</p>\r\n            <!-- <input kendoTextBox class=\"kendo-input\" [(ngModel)]=\"connectorField.userName\" placeholder=\"Username\" /> -->\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Username\" name=\"userName\"\r\n              [(ngModel)]=\"connectorField.userName\" ngModel required>\r\n          </div>\r\n        </span>\r\n        <span id=\"password\">\r\n          <div class=\"col-lg-6\">\r\n            <p class=\"title\"><span class=\"astrick\">*</span>Password </p>\r\n            <input [type]=\"isShow ? 'text' : 'password'\" class=\"form-control\" placeholder=\"Password\" name=\"password\"\r\n              [(ngModel)]=\"connectorField.password\" ngModel required>\r\n            <span toggle=\"#password-field\" (click)=\"this.isShow = !this.isShow\" class=\"fa toggle-password\"\r\n              [ngClass]=\"{ 'fa-eye': isShow, 'fa-eye-slash': !isShow}\"></span>\r\n          </div>\r\n        </span>\r\n      </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n      <div class=\"row\">\r\n        <div class=\"col-lg-12\">\r\n          <span id=\"saveBtn\">\r\n            <button kendoButton class=\"pull-right\" primary=\"true\" (click)=\"onSaveConnection()\"\r\n              [disabled]=\"!form.valid\">Save</button>\r\n          </span>\r\n          <span id=\"closeBtn\">\r\n            <button kendoButton class=\"pull-right cancel_button\" (click)=\"closeIt()\">Cancel</button>\r\n          </span>\r\n          <span id=\"testConnection\">\r\n            <button class=\"pull-right test_connection\" (click)=\"testConnection()\" [disabled]=\"!form.valid\">Test\r\n              Connection</button>\r\n          </span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</div>\r\n<div class=\"overlay\" *ngIf=\"isProcessing\">\r\n  <i class=\"fa fa-refresh fa-spin\"></i>\r\n  <p>{{processingText}}</p>\r\n</div>"

/***/ }),

/***/ "./src/app/connector/rdbms-connector/rdbms-connector.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/connector/rdbms-connector/rdbms-connector.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#rdbms_parameters .row {\n  margin-bottom: 10px; }\n\n#rdbms_parameters p.title {\n  font-weight: 600;\n  font-size: 12px;\n  margin-bottom: 0; }\n\n#rdbms_parameters .astrick {\n  color: red; }\n\n#rdbms_parameters input {\n  width: 100%; }\n\n#rdbms_parameters button {\n  margin: 0px 3px; }\n\n#rdbms_parameters button.test_connection {\n  background-color: #e3a54c;\n  color: white;\n  border-radius: 2px;\n  padding: 4px 8px;\n  box-sizing: border-box;\n  border-width: 1px;\n  border-style: solid;\n  font-size: 14px;\n  line-height: 1.42857;\n  font-family: inherit;\n  text-align: center;\n  text-decoration: none;\n  white-space: nowrap;\n  display: inline-flex;\n  align-items: center;\n  justify-content: center;\n  vertical-align: middle;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  cursor: pointer;\n  outline: none;\n  -webkit-appearance: none;\n  position: relative;\n  border: solid 1px #c5862b; }\n\n.overlay > .fa {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  margin-left: -15px;\n  margin-top: -15px;\n  color: #000;\n  font-size: 30px; }\n\n.overlay > p {\n  position: absolute;\n  top: 65%;\n  left: 49%;\n  margin-left: -15px;\n  margin-top: -15px;\n  color: #000;\n  font-size: 12px; }\n\n.overlay > p.loading {\n  left: 49%; }\n\n.overlay > p.processing {\n  left: 49%; }\n\n.overlay > p.validating {\n  left: 49%; }\n\n.toggle-password {\n  position: absolute;\n  right: 20px;\n  top: 20px;\n  border: solid 1px #323639;\n  font-size: 16px;\n  padding: 4px;\n  border-radius: 50%;\n  width: 27px;\n  background: #323639;\n  color: white;\n  height: 27px;\n  cursor: pointer; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb25uZWN0b3IvcmRibXMtY29ubmVjdG9yL0U6XFxVSS1Qcm9qZWN0XFxFVExcXHNyY1xcc3JjL2FwcFxcY29ubmVjdG9yXFxyZGJtcy1jb25uZWN0b3JcXHJkYm1zLWNvbm5lY3Rvci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLG1CQUFtQixFQUFBOztBQUZ2QjtFQUtJLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsZ0JBQWdCLEVBQUE7O0FBUHBCO0VBVUksVUFBVSxFQUFBOztBQVZkO0VBYUksV0FBVyxFQUFBOztBQWJmO0VBZ0JJLGVBQWUsRUFBQTs7QUFoQm5CO0VBbUJJLHlCQUF5QjtFQUN6QixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixzQkFBc0I7RUFDdEIsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2Ysb0JBQW9CO0VBQ3BCLG9CQUFvQjtFQUNwQixrQkFBa0I7RUFDbEIscUJBQXFCO0VBQ3JCLG1CQUFtQjtFQUNuQixvQkFBb0I7RUFDcEIsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixzQkFBc0I7RUFDdEIseUJBQXlCO0VBQ3pCLHNCQUFzQjtFQUN0QixxQkFBcUI7RUFDckIsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixhQUFhO0VBQ2Isd0JBQXdCO0VBQ3hCLGtCQUFrQjtFQUNsQix5QkFBeUIsRUFBQTs7QUFJN0I7RUFDRSxrQkFBa0I7RUFDbEIsUUFBUTtFQUNSLFNBQVM7RUFDVCxrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLFdBQVc7RUFDWCxlQUFlLEVBQUE7O0FBRWpCO0VBQ0Usa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixTQUFTO0VBQ1Qsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixXQUFXO0VBQ1gsZUFBZSxFQUFBOztBQUVqQjtFQUNFLFNBQVMsRUFBQTs7QUFHWDtFQUNFLFNBQVMsRUFBQTs7QUFHWDtFQUNFLFNBQVMsRUFBQTs7QUFFWDtFQUNFLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsU0FBUztFQUNULHlCQUF5QjtFQUN6QixlQUFlO0VBQ2YsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixZQUFZO0VBQ1osZUFBZSxFQUFBIiwiZmlsZSI6ImFwcC9jb25uZWN0b3IvcmRibXMtY29ubmVjdG9yL3JkYm1zLWNvbm5lY3Rvci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNyZGJtc19wYXJhbWV0ZXJzIHtcclxuICAucm93IHtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgfVxyXG4gIHAudGl0bGUge1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgfVxyXG4gIC5hc3RyaWNrIHtcclxuICAgIGNvbG9yOiByZWQ7XHJcbiAgfVxyXG4gIGlucHV0IHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuICBidXR0b24ge1xyXG4gICAgbWFyZ2luOiAwcHggM3B4O1xyXG4gIH1cclxuICBidXR0b24udGVzdF9jb25uZWN0aW9uIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNlM2E1NGM7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgICBwYWRkaW5nOiA0cHggOHB4O1xyXG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgIGJvcmRlci13aWR0aDogMXB4O1xyXG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjQyODU3O1xyXG4gICAgZm9udC1mYW1pbHk6IGluaGVyaXQ7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG4gICAgLXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTtcclxuICAgIC1tb3otdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgICAtbXMtdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgICB1c2VyLXNlbGVjdDogbm9uZTtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICAtd2Via2l0LWFwcGVhcmFuY2U6IG5vbmU7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBib3JkZXI6IHNvbGlkIDFweCAjYzU4NjJiO1xyXG4gIH1cclxufVxyXG5cclxuLm92ZXJsYXkgPiAuZmEge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDUwJTtcclxuICBsZWZ0OiA1MCU7XHJcbiAgbWFyZ2luLWxlZnQ6IC0xNXB4O1xyXG4gIG1hcmdpbi10b3A6IC0xNXB4O1xyXG4gIGNvbG9yOiAjMDAwO1xyXG4gIGZvbnQtc2l6ZTogMzBweDtcclxufVxyXG4ub3ZlcmxheSA+IHAge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDY1JTtcclxuICBsZWZ0OiA0OSU7XHJcbiAgbWFyZ2luLWxlZnQ6IC0xNXB4O1xyXG4gIG1hcmdpbi10b3A6IC0xNXB4O1xyXG4gIGNvbG9yOiAjMDAwO1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxufVxyXG4ub3ZlcmxheSA+IHAubG9hZGluZyB7XHJcbiAgbGVmdDogNDklO1xyXG59XHJcblxyXG4ub3ZlcmxheSA+IHAucHJvY2Vzc2luZyB7XHJcbiAgbGVmdDogNDklO1xyXG59XHJcblxyXG4ub3ZlcmxheSA+IHAudmFsaWRhdGluZyB7XHJcbiAgbGVmdDogNDklO1xyXG59XHJcbi50b2dnbGUtcGFzc3dvcmQge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICByaWdodDogMjBweDtcclxuICB0b3A6IDIwcHg7XHJcbiAgYm9yZGVyOiBzb2xpZCAxcHggIzMyMzYzOTtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgcGFkZGluZzogNHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICB3aWR0aDogMjdweDtcclxuICBiYWNrZ3JvdW5kOiAjMzIzNjM5O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBoZWlnaHQ6IDI3cHg7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/connector/rdbms-connector/rdbms-connector.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/connector/rdbms-connector/rdbms-connector.component.ts ***!
  \************************************************************************/
/*! exports provided: RdbmsConnectorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RdbmsConnectorComponent", function() { return RdbmsConnectorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_model_connector__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/model/connector */ "./src/app/model/connector.ts");
/* harmony import */ var src_app_connections_connection_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/connections/connection-service */ "./src/app/connections/connection-service.ts");
/* harmony import */ var src_app_shared_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/data.service */ "./src/app/shared/data.service.ts");
/* harmony import */ var src_app_model_connection__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/model/connection */ "./src/app/model/connection.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var intro_js_intro_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! intro.js/intro.js */ "./node_modules/intro.js/intro.js");
/* harmony import */ var intro_js_intro_js__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(intro_js_intro_js__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");









var RdbmsConnectorComponent = /** @class */ (function () {
    function RdbmsConnectorComponent(connectionService, toastr, dataService, router, route) {
        var _this = this;
        this.connectionService = connectionService;
        this.toastr = toastr;
        this.dataService = dataService;
        this.router = router;
        this.route = route;
        this.saveConnection = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.introJS = intro_js_intro_js__WEBPACK_IMPORTED_MODULE_7__();
        this.isProcessing = false;
        this.processingText = '';
        this.connectionFields = new src_app_model_connection__WEBPACK_IMPORTED_MODULE_5__["Connection"]();
        this.connectorField = new src_app_model_connector__WEBPACK_IMPORTED_MODULE_2__["Connector"]();
        this.dataService.getConnector.subscribe(function (connector) { return _this.connectorField['connectionType'] = connector; });
        this.dataService.getConnectionDetail().subscribe(function (connection) {
            if (connection.connectionId !== '') {
                _this.connectionFields = connection;
                _this.dataService.setConnector(_this.connectionFields.type);
                var parameters = Object.keys(_this.connectionFields.connectionConfigRequestList);
                parameters.forEach(function (element) {
                    _this.connectorField[element] = _this.connectionFields.connectionConfigRequestList[element];
                });
            }
        });
        this.isShow = false;
        this.introJS.setOptions({
            steps: [
                {
                    element: '#connectionName',
                    intro: 'Write Connection Name '
                },
                {
                    element: '#aliasName',
                    intro: 'Write alias name (optional)',
                    position: 'left'
                },
                {
                    element: '#serviceName',
                    intro: 'Write Service Name',
                    position: 'left'
                },
                {
                    element: '#port',
                    intro: 'Write Port Number',
                    position: 'left'
                },
                {
                    element: '#host',
                    intro: 'Write Host',
                    position: 'left'
                },
                {
                    element: '#userName',
                    intro: 'Write User Name',
                    position: 'left'
                },
                {
                    element: '#password',
                    intro: 'Write Password',
                    position: 'left'
                },
                {
                    element: '#testConnection',
                    intro: 'Press Test Connection'
                },
                {
                    element: '#saveBtn',
                    intro: 'Press Save Connection'
                },
                {
                    element: '#closeBtn',
                    intro: ' Press Close button'
                },
            ]
        });
        this.dataService.invokeEvent.subscribe(function (value) {
            if (value === 'connectorVal') {
                _this.dataService.getConnector.subscribe(function (connector) { return _this.connectorField['connectionType'] = connector; });
            }
        });
    }
    RdbmsConnectorComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.route.snapshot.queryParams.action === 'intro') {
            this.introJS.start();
            this.introJS.onchange(function (targetElement) {
            });
            this.introJS.oncomplete(function () {
                _this.router.navigate(['/connections'], { queryParams: {} });
            });
            this.introJS.onexit(function () {
                // this.router.navigate(['/connections'], { queryParams: {} });
            });
        }
    };
    RdbmsConnectorComponent.prototype.testConnection = function () {
        var _this = this;
        this.isProcessing = true;
        this.processingText = 'Validating...';
        this.dataService.getConnector.subscribe(function (connector) {
            return _this.connectorField['connectionType'] = connector;
        });
        this.connectionService.testConnection(this.connectorField).subscribe(function (response) {
            var res = JSON.parse(JSON.stringify(response));
            if (res.results[0].status) {
                _this.toastr.info(res.results[0].message);
            }
            else {
                _this.toastr.error(res.results[0].message);
            }
            if (_this.route.snapshot.queryParams.action === 'intro') {
                _this.introJS.goToStepNumber(8).start();
            }
            _this.isProcessing = false;
            _this.processingText = '';
        }, function (error) {
            _this.isProcessing = false;
            _this.processingText = '';
        });
    };
    RdbmsConnectorComponent.prototype.onSaveConnection = function () {
        var _this = this;
        this.isProcessing = true;
        this.processingText = 'Validating...';
        // this.dataService.getConnector.subscribe(connector => this.connectorField['connectionType'] = connector);
        this.connectionService.testConnection(this.connectorField).subscribe(function (response) {
            var res = JSON.parse(JSON.stringify(response));
            if (res.results[0].status) {
                _this.connectionFields.name = _this.connectorField.connectionName;
                _this.connectionFields.aliasName = _this.connectorField.aliasName;
                _this.dataService.getConnector.subscribe(function (connector) { return _this.connectionFields.type = connector; });
                var parameters = Object.keys(_this.connectorField);
                _this.connectionFields.connectionConfigRequestList = [];
                parameters.forEach(function (element) {
                    _this.connectionFields.connectionConfigRequestList.push({ key: element, value: _this.connectorField[element] });
                });
                _this.isProcessing = true;
                _this.processingText = 'Processing...';
                if (_this.connectionFields.connectionId === undefined) {
                    _this.connectionService.add(_this.connectionFields).subscribe(function (response) {
                        var res = JSON.parse(JSON.stringify(response));
                        if (res.sucess === false) {
                            _this.saveConnection.emit('2');
                        }
                        _this.saveConnection.emit('2');
                        _this.connectionFields = new src_app_model_connection__WEBPACK_IMPORTED_MODULE_5__["Connection"]();
                        _this.connectorField = new src_app_model_connector__WEBPACK_IMPORTED_MODULE_2__["Connector"]();
                        _this.dataService.setConnectionDetail(new src_app_model_connection__WEBPACK_IMPORTED_MODULE_5__["Connection"]());
                        _this.dataService.setConnector('0');
                        _this.isProcessing = false;
                        _this.processingText = '';
                    }, function (error) {
                        _this.isProcessing = false;
                        _this.processingText = '';
                    });
                }
                else {
                    _this.connectionService.update(_this.connectionFields.connectionId, _this.connectionFields).subscribe(function (response) {
                        var res = JSON.parse(JSON.stringify(response));
                        if (res.sucess === false) {
                            _this.saveConnection.emit('2');
                        }
                        _this.saveConnection.emit('2');
                        _this.connectionFields = new src_app_model_connection__WEBPACK_IMPORTED_MODULE_5__["Connection"]();
                        _this.connectorField = new src_app_model_connector__WEBPACK_IMPORTED_MODULE_2__["Connector"]();
                        _this.dataService.setConnectionDetail(new src_app_model_connection__WEBPACK_IMPORTED_MODULE_5__["Connection"]());
                        _this.dataService.setConnector('0');
                        _this.isProcessing = false;
                        _this.processingText = '';
                    }, function (error) {
                        _this.isProcessing = false;
                        _this.processingText = '';
                    });
                }
            }
            else {
                _this.toastr.error(res.results[0].message);
            }
            _this.isProcessing = false;
            _this.processingText = '';
        }, function (error) {
            _this.isProcessing = false;
            _this.processingText = '';
        });
        if (this.route.snapshot.queryParams.action === 'intro') {
            this.introJS.goToStepNumber(9).start();
        }
    };
    RdbmsConnectorComponent.prototype.closeIt = function () {
        this.dataService.setConnectionDetail(new src_app_model_connection__WEBPACK_IMPORTED_MODULE_5__["Connection"]());
        this.dataService.setConnector('0');
        this.saveConnection.emit('2');
        if (this.route.snapshot.queryParams.action === 'intro') {
            this.introJS.exit(10);
        }
    };
    RdbmsConnectorComponent.prototype.ngOnDestroy = function () {
        this.introJS.exit();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", src_app_model_connection__WEBPACK_IMPORTED_MODULE_5__["Connection"])
    ], RdbmsConnectorComponent.prototype, "connectionFields", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", src_app_model_connector__WEBPACK_IMPORTED_MODULE_2__["Connector"])
    ], RdbmsConnectorComponent.prototype, "connectorField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], RdbmsConnectorComponent.prototype, "saveConnection", void 0);
    RdbmsConnectorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-rdbms-connector',
            template: __webpack_require__(/*! ./rdbms-connector.component.html */ "./src/app/connector/rdbms-connector/rdbms-connector.component.html"),
            styles: [__webpack_require__(/*! ./rdbms-connector.component.scss */ "./src/app/connector/rdbms-connector/rdbms-connector.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_connections_connection_service__WEBPACK_IMPORTED_MODULE_3__["ConnectionService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"],
            src_app_shared_data_service__WEBPACK_IMPORTED_MODULE_4__["DataService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"]])
    ], RdbmsConnectorComponent);
    return RdbmsConnectorComponent;
}());



/***/ }),

/***/ "./src/app/core-component/comment/comment-service.ts":
/*!***********************************************************!*\
  !*** ./src/app/core-component/comment/comment-service.ts ***!
  \***********************************************************/
/*! exports provided: CommentService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommentService", function() { return CommentService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_environments_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/config */ "./src/environments/config.ts");




var CommentService = /** @class */ (function () {
    function CommentService(http) {
        this.http = http;
        this.baseUrl = src_environments_config__WEBPACK_IMPORTED_MODULE_3__["Config"].getEnvironmentVariable('apiUrl');
        this.getCommnetEndPoint = this.baseUrl + 'comments/';
        this.addCommentEndPoint = this.baseUrl + 'comments/';
    }
    CommentService.prototype.getAllComment = function (moduleId, entityId, entityValue) {
        return this.http.get(this.getCommnetEndPoint + moduleId + '/' + entityId + '/' + entityValue);
    };
    CommentService.prototype.addComment = function (req) {
        return this.http.post(this.addCommentEndPoint, req);
    };
    CommentService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], CommentService);
    return CommentService;
}());



/***/ }),

/***/ "./src/app/core-component/comment/comment.component.css":
/*!**************************************************************!*\
  !*** ./src/app/core-component/comment/comment.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".animated {\r\n  transition: height 0.2s;\r\n}\r\n\r\ntextarea::-webkit-input-placeholder {\r\n  color: #999999;\r\n  font-size: 1em;\r\n  font-style: normal;\r\n}\r\n\r\ntextarea::-moz-placeholder {\r\n  color: #999999;\r\n  font-size: 1em;\r\n  font-style: normal;\r\n}\r\n\r\ntextarea::-ms-input-placeholder {\r\n  color: #999999;\r\n  font-size: 1em;\r\n  font-style: normal;\r\n}\r\n\r\ntextarea::placeholder {\r\n  color: #999999;\r\n  font-size: 1em;\r\n  font-style: normal;\r\n}\r\n\r\n.no-bg {\r\n  background-color: transparent;\r\n}\r\n\r\n#comment-wrapper {\r\n  margin-top: 20px;\r\n}\r\n\r\n#comment-wrapper .comment-text {\r\n  border: solid 2px #e7e7e7;\r\n}\r\n\r\n#comment-wrapper .comment-box, .comment-list {\r\n  padding: 15px 0px;\r\n}\r\n\r\n#comment-wrapper .comment-text:focus + .comment-submit {\r\n  display: block;\r\n}\r\n\r\n.comment-submit {\r\n  position: relative;\r\n  bottom: 52px;\r\n  right: 10px;\r\n  /* display: none; */\r\n}\r\n\r\n.comment-file-attach {\r\n  position: relative;\r\n  bottom: 52px;\r\n  right: 10px;\r\n  margin-top: 10px;\r\n  height: 34px;\r\n  padding: 5px;\r\n  right: 16px;\r\n}\r\n\r\n.btn-np-primary {\r\n  background-color: #0067c5;\r\n  border-color: #005aac;\r\n  color: white;\r\n}\r\n\r\n.padding-top-0 {\r\n  padding-top: 0px;\r\n}\r\n\r\n.commentor {\r\n  color: #0067c5;\r\n  text-transform: capitalize;\r\n  text-align: left;\r\n}\r\n\r\n.media-comment {\r\n  text-align: left;\r\n}\r\n\r\n.comment-date {\r\n  font-size: 12px;\r\n  color: black;\r\n}\r\n\r\n#comment-wrapper .avatar-container img {\r\n  background: #f7f7f7;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9jb3JlLWNvbXBvbmVudC9jb21tZW50L2NvbW1lbnQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUdFLHVCQUF1QjtBQUN6Qjs7QUFFQTtFQUNFLGNBQWM7RUFDZCxjQUFjO0VBQ2Qsa0JBQWtCO0FBQ3BCOztBQUpBO0VBQ0UsY0FBYztFQUNkLGNBQWM7RUFDZCxrQkFBa0I7QUFDcEI7O0FBSkE7RUFDRSxjQUFjO0VBQ2QsY0FBYztFQUNkLGtCQUFrQjtBQUNwQjs7QUFKQTtFQUNFLGNBQWM7RUFDZCxjQUFjO0VBQ2Qsa0JBQWtCO0FBQ3BCOztBQUNBO0VBQ0UsNkJBQTZCO0FBQy9COztBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCOztBQUNBO0VBQ0UseUJBQXlCO0FBQzNCOztBQUNBO0VBQ0UsaUJBQWlCO0FBQ25COztBQUVBO0VBQ0UsY0FBYztBQUNoQjs7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osV0FBVztFQUNYLG1CQUFtQjtBQUNyQjs7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osV0FBVztFQUNYLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osWUFBWTtFQUNaLFdBQVc7QUFDYjs7QUFDQTtFQUNFLHlCQUF5QjtFQUN6QixxQkFBcUI7RUFDckIsWUFBWTtBQUNkOztBQUNBO0VBQ0UsZ0JBQWdCO0FBQ2xCOztBQUNBO0VBQ0UsY0FBYztFQUNkLDBCQUEwQjtFQUMxQixnQkFBZ0I7QUFDbEI7O0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7O0FBQ0E7RUFDRSxlQUFlO0VBQ2YsWUFBWTtBQUNkOztBQUNBO0VBQ0UsbUJBQW1CO0FBQ3JCIiwiZmlsZSI6ImFwcC9jb3JlLWNvbXBvbmVudC9jb21tZW50L2NvbW1lbnQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5hbmltYXRlZCB7XHJcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBoZWlnaHQgMC4ycztcclxuICAtbW96LXRyYW5zaXRpb246IGhlaWdodCAwLjJzO1xyXG4gIHRyYW5zaXRpb246IGhlaWdodCAwLjJzO1xyXG59XHJcblxyXG50ZXh0YXJlYTo6cGxhY2Vob2xkZXIge1xyXG4gIGNvbG9yOiAjOTk5OTk5O1xyXG4gIGZvbnQtc2l6ZTogMWVtO1xyXG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxufVxyXG4ubm8tYmcge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG59XHJcbiNjb21tZW50LXdyYXBwZXIge1xyXG4gIG1hcmdpbi10b3A6IDIwcHg7XHJcbn1cclxuI2NvbW1lbnQtd3JhcHBlciAuY29tbWVudC10ZXh0IHtcclxuICBib3JkZXI6IHNvbGlkIDJweCAjZTdlN2U3O1xyXG59XHJcbiNjb21tZW50LXdyYXBwZXIgLmNvbW1lbnQtYm94LCAuY29tbWVudC1saXN0IHtcclxuICBwYWRkaW5nOiAxNXB4IDBweDtcclxufVxyXG5cclxuI2NvbW1lbnQtd3JhcHBlciAuY29tbWVudC10ZXh0OmZvY3VzICsgLmNvbW1lbnQtc3VibWl0IHtcclxuICBkaXNwbGF5OiBibG9jaztcclxufVxyXG4uY29tbWVudC1zdWJtaXQge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBib3R0b206IDUycHg7XHJcbiAgcmlnaHQ6IDEwcHg7XHJcbiAgLyogZGlzcGxheTogbm9uZTsgKi9cclxufVxyXG4uY29tbWVudC1maWxlLWF0dGFjaCB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGJvdHRvbTogNTJweDtcclxuICByaWdodDogMTBweDtcclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gIGhlaWdodDogMzRweDtcclxuICBwYWRkaW5nOiA1cHg7XHJcbiAgcmlnaHQ6IDE2cHg7XHJcbn1cclxuLmJ0bi1ucC1wcmltYXJ5IHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDA2N2M1O1xyXG4gIGJvcmRlci1jb2xvcjogIzAwNWFhYztcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuLnBhZGRpbmctdG9wLTAge1xyXG4gIHBhZGRpbmctdG9wOiAwcHg7XHJcbn1cclxuLmNvbW1lbnRvciB7XHJcbiAgY29sb3I6ICMwMDY3YzU7XHJcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxufVxyXG4ubWVkaWEtY29tbWVudCB7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxufVxyXG4uY29tbWVudC1kYXRlIHtcclxuICBmb250LXNpemU6IDEycHg7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG59XHJcbiNjb21tZW50LXdyYXBwZXIgLmF2YXRhci1jb250YWluZXIgaW1nIHtcclxuICBiYWNrZ3JvdW5kOiAjZjdmN2Y3O1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/core-component/comment/comment.component.html":
/*!***************************************************************!*\
  !*** ./src/app/core-component/comment/comment.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"comment-wrapper\">\r\n  <div class=\"row\">\r\n    <div class=\"col-lg-12 no-padding\">\r\n      <div class=\"well well-lg no-bg no-border no-margin comment-box\" style=\"padding-bottom: 0px;\">\r\n        <div class=\"media1\">\r\n          <a class=\"pull-left\" href=\"#\" style=\"margin: 12px 0px\">\r\n            <ngx-avatar name=\"Karnesh Kumar\" [src]=\"'http://cedprod.corp.netapp.com/photos/' + userId + '.jpg'\"\r\n              size=\"50\" cornerRadius=\"5\">\r\n            </ngx-avatar>\r\n          </a>\r\n          <div class=\"media-body\">\r\n            <div class=\"form-group\" style=\"padding:12px;\">\r\n              <textarea class=\"form-control animated comment-text\" [(ngModel)]=\"comment.notes\"\r\n                placeholder=\"Leave your comment...\"></textarea>\r\n              <button class=\"btn btn-np-primary pull-right comment-submit\" (click)=\"addComment()\"\r\n                style=\"margin-top:10px\" type=\"button\">Submit</button>\r\n              <!-- <button class=\"btn btn-default pull-right comment-file-attach no-bg\" (click)=\"file.click()\"\r\n              style=\"margin-top:10px\" type=\"button\"><svg class=\"icon_logout\" shape-rendering=\"geometricPrecision\"\r\n                width=\"20\" height=\"20\" fill=\"#777777\">\r\n                <use xlink:href=\"./assets/dist/img/icons.svg#icon_file_attachment\"></use>\r\n              </svg>\r\n            </button> -->\r\n              <input type=\"file\" accept=\"image/*\" #file style=\"display: none\" />\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"row\">\r\n    <div class=\"col-lg-12 no-padding\">\r\n      <div class=\"well well-lg no-bg no-border comment-list\">\r\n        <ul class=\"media-list\">\r\n          <li class=\"media\" *ngFor=\"let comment of allComments\">\r\n            <a class=\"pull-left\" href=\"#\">\r\n              <ngx-avatar name=\"Karnesh Kumar\" title=\"Karnesh Kumar\"\r\n                [src]=\"'http://cedprod.corp.netapp.com/photos/' + comment.userId + '.jpg'\" size=\"50\" cornerRadius=\"5\">\r\n              </ngx-avatar>\r\n            </a>\r\n            <div class=\"media-body\">\r\n              <div class=\"well well-lg no-bg no-border padding-top-0\"\r\n                style=\"box-shadow: 0px 0px;padding-left: 0px; padding-bottom: 0px\">\r\n                <h4 class=\"media-heading text-uppercase reviews commentor\">{{comment.name}} <span\r\n                    class=\"comment-date\">{{comment.createdDate}}</span></h4>\r\n\r\n                <p class=\"media-comment\">\r\n                  {{comment.notes}}\r\n                </p>\r\n                <a class=\"reply\" href=\"#\" id=\"reply\" style=\"float:left;display: none;\"><span\r\n                    class=\"glyphicon glyphicon-share-alt\"></span> Reply</a>\r\n              </div>\r\n            </div>\r\n            <div class=\"collapse\" id=\"replyOne\">\r\n              <ul class=\"media-list\">\r\n                <li class=\"media media-replied\">\r\n                  <a class=\"pull-left\" href=\"#\">\r\n                    <ngx-avatar name=\"Karnesh Kumar\"\r\n                      [src]=\"'http://cedprod.corp.netapp.com/photos/' + comment.userId + '.jpg'\" size=\"50\"\r\n                      cornerRadius=\"5\">\r\n                    </ngx-avatar>\r\n                  </a>\r\n                  <div class=\"media-body\">\r\n                    <div class=\"well well-lg\">\r\n                      <h4 class=\"media-heading text-uppercase reviews\"><span\r\n                          class=\"glyphicon glyphicon-share-alt\"></span>\r\n                        The Hipster</h4>\r\n                      <ul class=\"media-date text-uppercase reviews list-inline\">\r\n                        <li class=\"dd\">22</li>\r\n                        <li class=\"mm\">09</li>\r\n                        <li class=\"aaaa\">2014</li>\r\n                      </ul>\r\n                      <p class=\"media-comment\">\r\n                        Nice job Maria.\r\n                      </p>\r\n                      <a class=\"btn btn-info btn-circle text-uppercase\" href=\"#\" id=\"reply\"><span\r\n                          class=\"glyphicon glyphicon-share-alt\"></span> Reply</a>\r\n                    </div>\r\n                  </div>\r\n                </li>\r\n                <li class=\"media media-replied\" id=\"replied\">\r\n                  <a class=\"pull-left\" href=\"#\">\r\n                    <ngx-avatar name=\"Karnesh Kumar\" src=\"./assets/dist/img/noavatar92.png\" size=\"50\" cornerRadius=\"5\">\r\n                    </ngx-avatar>\r\n                  </a>\r\n                  <div class=\"media-body\">\r\n                    <div class=\"well well-lg no-bg no-border\">\r\n                      <h4 class=\"media-heading text-uppercase reviews\"><span\r\n                          class=\"glyphicon glyphicon-share-alt\"></span>\r\n                        Mary</h4>\r\n                      <ul class=\"media-date text-uppercase reviews list-inline\">\r\n                        <li class=\"dd\">22</li>\r\n                        <li class=\"mm\">09</li>\r\n                        <li class=\"aaaa\">2014</li>\r\n                      </ul>\r\n                      <p class=\"media-comment\">\r\n                        Thank you Guys!\r\n                      </p>\r\n                      <a class=\"btn btn-info btn-circle text-uppercase\" href=\"#\" id=\"reply\"><span\r\n                          class=\"glyphicon glyphicon-share-alt\"></span> Reply</a>\r\n                    </div>\r\n                  </div>\r\n                </li>\r\n              </ul>\r\n            </div>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/core-component/comment/comment.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/core-component/comment/comment.component.ts ***!
  \*************************************************************/
/*! exports provided: CommentComponent, Comment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommentComponent", function() { return CommentComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Comment", function() { return Comment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _comment_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./comment-service */ "./src/app/core-component/comment/comment-service.ts");



var CommentComponent = /** @class */ (function () {
    function CommentComponent(commentService) {
        this.commentService = commentService;
        this.comment = new Comment();
        this.allComments = new Array();
    }
    CommentComponent.prototype.ngOnInit = function () {
        load();
        this.getAllComment();
    };
    CommentComponent.prototype.getAllComment = function () {
        var _this = this;
        this.commentService.getAllComment(this.moduleId, this.entityId, this.entityValue)
            .subscribe(function (response) {
            _this.allComments = JSON.parse(JSON.stringify(response)).results;
        }, function (error) {
            console.log(error);
        });
    };
    CommentComponent.prototype.addComment = function () {
        var _this = this;
        this.comment.entityValue = this.entityValue;
        this.comment.moduleId = parseInt(this.moduleId, 0);
        this.comment.entityId = parseInt(this.entityId, 0);
        this.comment.userId = this.userId;
        this.commentService.addComment(this.comment).subscribe(function (data) {
            _this.comment.notes = '';
            _this.getAllComment();
        }, function (error) {
            console.log(error);
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], CommentComponent.prototype, "moduleId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], CommentComponent.prototype, "entityId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], CommentComponent.prototype, "entityValue", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], CommentComponent.prototype, "userId", void 0);
    CommentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-comment',
            template: __webpack_require__(/*! ./comment.component.html */ "./src/app/core-component/comment/comment.component.html"),
            providers: [
                _comment_service__WEBPACK_IMPORTED_MODULE_2__["CommentService"]
            ],
            styles: [__webpack_require__(/*! ./comment.component.css */ "./src/app/core-component/comment/comment.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_comment_service__WEBPACK_IMPORTED_MODULE_2__["CommentService"]])
    ], CommentComponent);
    return CommentComponent;
}());

var Comment = /** @class */ (function () {
    function Comment() {
    }
    return Comment;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.component.css":
/*!***************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.html":
/*!****************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"connection_header\" class=\"content-header\"\r\n    style=\"padding: 10px;background: white;margin: 15px;box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);\">\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-4\">\r\n            <h1 style=\"margin:0px;font-size:19px\">\r\n                Dashboard\r\n            </h1>\r\n        </div>\r\n        <div class=\"col-sm-8\">\r\n\r\n        </div>\r\n    </div>\r\n</section>\r\n\r\n<section class=\"content\">\r\n    <!-- Small boxes (Stat box) -->\r\n    <div class=\"row\">\r\n        <div class=\"col-lg-3 col-xs-6\" data-step=\"1\" data-intro=\"This is a tooltip 1!\" data-position='right'>\r\n            <!-- small box -->\r\n            <div class=\"small-box\">\r\n                <div class=\"inner\">\r\n                    <h3>150</h3>\r\n                    <p>Total Connections</p>\r\n                </div>\r\n                <div class=\"icon\">\r\n                    <svg class=\"icon_change_log\" shape-rendering=\"geometricPrecision\" width=\"60\" height=\"60\">\r\n                        <use xlink:href=\"./assets/dist/img/icons.svg#icon_plug\"></use>\r\n                    </svg>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-lg-3 col-xs-6\" data-step=\"2\" data-intro=\"This is a tooltip 2!\" data-position='right'>\r\n            <!-- small box -->\r\n            <div class=\"small-box\">\r\n                <div class=\"inner\">\r\n                    <h3>150</h3>\r\n                    <p>Total Jobs</p>\r\n                </div>\r\n                <div class=\"icon\">\r\n                    <svg class=\"icon_change_log\" shape-rendering=\"geometricPrecision\" width=\"60\" height=\"60\">\r\n                        <use xlink:href=\"./assets/dist/img/icons.svg#icon_job\"></use>\r\n                    </svg>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-lg-3 col-xs-6\" data-step=\"3\" data-intro=\"This is a tooltip 3!\" data-position='right'>\r\n            <!-- small box -->\r\n            <div class=\"small-box\">\r\n                <div class=\"inner\">\r\n                    <h3>150</h3>\r\n                    <p>Running Jobs</p>\r\n                </div>\r\n                <div class=\"icon\">\r\n                    <svg class=\"icon_change_log\" shape-rendering=\"geometricPrecision\" width=\"60\" height=\"60\"\r\n                        style=\"fill: green\">\r\n                        <use xlink:href=\"./assets/dist/img/icons.svg#icon_job\"></use>\r\n                    </svg>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-lg-3 col-xs-6\" data-step=\"4\" data-intro=\"This is a tooltip 4!\" data-position='right'>\r\n            <!-- small box -->\r\n            <div class=\"small-box\">\r\n                <div class=\"inner\">\r\n                    <h3>150</h3>\r\n                    <p>Failed Jobs</p>\r\n                </div>\r\n                <div class=\"icon\">\r\n                    <svg class=\"icon_change_log\" shape-rendering=\"geometricPrecision\" width=\"60\" height=\"60\"\r\n                        style=\"fill: rgb(231, 76, 60)\">\r\n                        <use xlink:href=\"./assets/dist/img/icons.svg#icon_job\"></use>\r\n                    </svg>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- /.row -->\r\n</section>"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.ts":
/*!**************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.ts ***!
  \**************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var intro_js_intro_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! intro.js/intro.js */ "./node_modules/intro.js/intro.js");
/* harmony import */ var intro_js_intro_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(intro_js_intro_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(route) {
        this.route = route;
    }
    DashboardComponent.prototype.ngOnInit = function () {
        if (this.route.snapshot.queryParams.action === 'intro') {
            intro_js_intro_js__WEBPACK_IMPORTED_MODULE_2__().start();
        }
    };
    DashboardComponent.prototype.ngOnDestroy = function () {
        intro_js_intro_js__WEBPACK_IMPORTED_MODULE_2__().exit();
    };
    DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.css */ "./src/app/dashboard/dashboard.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/features/features.service.ts":
/*!**********************************************!*\
  !*** ./src/app/features/features.service.ts ***!
  \**********************************************/
/*! exports provided: FeatureService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeatureService", function() { return FeatureService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _environments_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/config */ "./src/environments/config.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_session_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../app.session-service */ "./src/app/app.session-service.ts");






//import { Observable} from 'rxjs';
var FeatureService = /** @class */ (function () {
    function FeatureService(_http, _SessionService) {
        this._http = _http;
        this._SessionService = _SessionService;
        this.baseUrl = _environments_config__WEBPACK_IMPORTED_MODULE_3__["Config"].getEnvironmentVariable('endPoint');
        this.AddFeatureEndpoint = this.baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].authApiUrl + "features";
        this.UpdateFeatureEndpoint = this.baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].authApiUrl + "features" + "/featureid";
        this.DeleteFeatureEndpoint = this.baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].authApiUrl + "features/";
        this.GetFeatureEndpoint = this.baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].authApiUrl + "features/";
        this.GetUserEndpoint = this.baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].authApiUrl + "features" + "/userid";
        this.GetAllFeatureEndpoint = this.baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].authApiUrl + "features";
        this.GetAllFeatureNodeEndpoint = this.baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].authApiUrl + "features/linkId";
        this.GetAllFeatureEndPoint = this.baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].authApiUrl + "features/";
        this.GetAllMenuEndPoint = this.baseUrl + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].authApiUrl + "menu/";
    }
    FeatureService.prototype.AddFeature = function (data) {
        return this._http.post(this.AddFeatureEndpoint, data);
    };
    FeatureService.prototype.UpdateFeature = function (data) {
        return this._http.put(this.UpdateFeatureEndpoint, data);
    };
    FeatureService.prototype.DeleteFeature = function (id) {
        return this._http.delete(this.DeleteFeatureEndpoint + id);
    };
    FeatureService.prototype.GetFeature = function (featureID) {
        return this._http.get(this.GetFeatureEndpoint + featureID);
    };
    FeatureService.prototype.GetUser = function (menuid) {
        return this._http.get(this.GetUserEndpoint);
    };
    FeatureService.prototype.GetAllFeature = function () {
        return this._http.get(this.GetAllFeatureEndpoint);
    };
    FeatureService.prototype.GetAllFeatureNode = function () {
        return this._http.get(this.GetAllFeatureNodeEndpoint);
    };
    FeatureService.prototype.GetAllFeatureList = function (linkID) {
        return this._http.get(this.GetAllFeatureEndPoint + linkID);
    };
    FeatureService.prototype.GetAllMenu = function (linkID) {
        var uid = this._SessionService.getSessionValue("uid");
        if (this._SessionService.getCookie("LA_USER_DETAIL") != "") {
            var LA_SessionData = JSON.parse(this._SessionService.getCookie("LA_USER_DETAIL"));
            uid = LA_SessionData.userId;
        }
        return this._http.get(this.GetAllMenuEndPoint + linkID + '/' + uid);
    };
    FeatureService.prototype.GetAllMenuByLinkId = function (linkID) {
        return this._http.get(this.GetAllMenuEndPoint + linkID);
    };
    FeatureService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"], _app_session_service__WEBPACK_IMPORTED_MODULE_5__["SessionService"]])
    ], FeatureService);
    return FeatureService;
}());



/***/ }),

/***/ "./src/app/jobs/job.service.ts":
/*!*************************************!*\
  !*** ./src/app/jobs/job.service.ts ***!
  \*************************************/
/*! exports provided: JobService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobService", function() { return JobService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/config */ "./src/environments/config.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");





var JobService = /** @class */ (function () {
    function JobService(http) {
        this.http = http;
        this.baseUrl = _environments_config__WEBPACK_IMPORTED_MODULE_3__["Config"].getEnvironmentVariable('apiUrl');
        this.allJobsEndPoint = this.baseUrl + 'job';
        this.deleteJobsEndPoint = this.baseUrl + 'jobs';
        this.jobsFilterEndPoint = this.baseUrl + 'jobs';
        this.collectionsEndPoint = this.baseUrl + 'collections';
        this.jobsEndPoint = this.baseUrl + 'jobs';
        this.queryEndPoint = this.baseUrl + 'collections/odata';
        this.viewLogEndPoint = this.baseUrl + 'jobs';
    }
    JobService.prototype.getAllJobs = function (filter, jobName, startRecord, recordCount, sortableColumn, sortingType) {
        return this.http.post(this.allJobsEndPoint + ("?jobName=" + (jobName ? jobName : '') + "&startRecord=" + startRecord + "&recordCount=" + recordCount + "&sortableColumn=" + sortableColumn + "&sortingType=" + sortingType), filter);
    };
    JobService.prototype.deleteJob = function (mapIds) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            }),
            body: mapIds
        };
        return this.http.delete(this.deleteJobsEndPoint, httpOptions);
    };
    JobService.prototype.getAllEntities = function (connectionId) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.get(this.collectionsEndPoint + ("/" + connectionId), httpOptions);
    };
    JobService.prototype.getCollectionMetadata = function (connectionId) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.get(this.collectionsEndPoint + ("/" + connectionId + "/tableMetadata"), httpOptions);
    };
    JobService.prototype.refreshOdataCollectionMetadata = function (connectionId) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.get(this.collectionsEndPoint + ("/odata/" + connectionId + "/refreshMetadata"), httpOptions);
    };
    JobService.prototype.getOdataCollection = function (connectionId, collectionName) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.get(this.collectionsEndPoint + ("/odata/" + connectionId + "/tableMetadata?collectionName=" + collectionName), httpOptions);
    };
    JobService.prototype.getTableMetadata = function (connectionId, collectionName, type) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.get(this.collectionsEndPoint + ("/" + connectionId + "/" + collectionName + "/metadata/?type=" + type), httpOptions);
    };
    JobService.prototype.createTable = function (connectionId, table) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.post(this.collectionsEndPoint + ("/odata/" + connectionId + "/tableMetadata"), table, httpOptions);
    };
    JobService.prototype.createjob = function (jobRequest) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.post(this.jobsEndPoint, jobRequest, httpOptions);
    };
    JobService.prototype.updateJob = function (jobRequest) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.put(this.jobsEndPoint + ("/" + jobRequest.jobId), jobRequest, httpOptions);
    };
    JobService.prototype.runJob = function (jobId) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.get(this.jobsEndPoint + ("/" + jobId + "/run"), httpOptions);
    };
    JobService.prototype.stopJob = function (jobId) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.get(this.jobsEndPoint + ("/" + jobId + "/stop"), httpOptions);
    };
    JobService.prototype.syncMetadata = function (jobRequest) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.post(this.jobsEndPoint + '/difference', jobRequest, httpOptions);
    };
    JobService.prototype.getUpdatedTableMetadata = function (jobId) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.get(this.queryEndPoint + ("/" + jobId + "/updatedTableMetadata"), httpOptions);
    };
    JobService.prototype.saveJobsMetadataDifference = function (connectionId, jobRequest) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.post(this.queryEndPoint + ("/" + connectionId + "/alterTableMetadata"), jobRequest, httpOptions);
    };
    JobService.prototype.updateTableMetadata = function (jobId, jobRequest) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.post(this.jobsEndPoint + ("/" + jobId + "/difference"), jobRequest, httpOptions);
    };
    JobService.prototype.logView = function (jobId) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.get(this.jobsEndPoint + ("/" + jobId + "/logs"), httpOptions);
    };
    JobService.prototype.scheduleJob = function (jobId, schedule) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.post(this.jobsEndPoint + ("/" + jobId + "/schedule"), schedule, httpOptions);
    };
    JobService.prototype.testFilterQuery = function (connectionId, collectionName, testFilterRequest) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.post(this.queryEndPoint + ("/" + connectionId + "/testFilterQuery?collectionName=" + collectionName), testFilterRequest, httpOptions);
    };
    JobService.prototype.gettJobStatus = function (jobId) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.get(this.jobsEndPoint + ("/" + jobId + "/progress"), httpOptions);
    };
    JobService.prototype.gettJob = function (jobId) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.get(this.jobsEndPoint + ("/" + jobId), httpOptions);
    };
    JobService.prototype.gettJobsInProgress = function () {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.get(this.jobsEndPoint + "/InProgress", httpOptions);
    };
    JobService.prototype.getJobLog = function (jobId) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.get(this.viewLogEndPoint + ("/" + jobId) + '/logs');
    };
    JobService.prototype.getJobLogStatus = function (jobId, jobStatusId) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.get(this.viewLogEndPoint + ("/" + jobId) + '/logs' + ("/" + jobStatusId));
    };
    JobService.prototype.getDataForPreview = function (connectionId, collectionName, collectionType, startRecord, recordCount, filter) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.http.post(this.collectionsEndPoint + ("/" + connectionId) + ("/" + collectionName) + ("/data?collectionType=" + collectionType + "&startRecord=" + startRecord + "&recordCount=" + recordCount), filter);
    };
    JobService.prototype.getExcelExport = function (jobId) {
        return this.http.get(this.baseUrl + ("jobs/" + jobId + "/run/export"), { responseType: 'blob' });
    };
    JobService.prototype.upLoad = function (jobId, file) {
        this.headers = new Headers({ 'enctype': 'multipart/form-data' });
        // headers.append('Accept', 'application/json');
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["RequestOptions"]({ headers: this.headers });
        var formData = new FormData();
        formData.append('file', file, file.name);
        console.log('before hist the service' + formData);
        return this.http.post(this.baseUrl + ("jobs/" + jobId + "/run/upload"), formData, this.options);
    };
    JobService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], JobService);
    return JobService;
}());

// upload(fileToUpload: File) {
//   const headers = new Headers({'enctype': 'multipart/form-data'});
//   // headers.append('Accept', 'application/json');
//   const options = new RequestOptions({headers: headers});
//   const formData: FormData = new FormData();
//   formData.append('file', fileToUpload, fileToUpload.name);
//   console.log('before hist the service' + formData);
//   return this.http
//     .post(`${this.appSettings.baseUrl}/Containers/avatar/upload/`, formData , options).map(
//       res => {
//         const data = res.json();
//         return data;
//       }
//     ).catch(this.handleError);
// }


/***/ }),

/***/ "./src/app/jobs/jobs.component.html":
/*!******************************************!*\
  !*** ./src/app/jobs/jobs.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"jobs_header\" class=\"content-header\"\r\n  style=\"padding: 10px;background: white;margin: 15px;box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-4\">\r\n      <h1 style=\"margin:0px;font-size:19px\">Jobs</h1>\r\n      <small>Displaying all available jobs</small>\r\n    </div>\r\n    <div class=\"col-sm-8\">\r\n      <a [routerLink]=\"['/jobs/new']\">\r\n        <button kendoButton class=\"pull-right\" primary=\"true\" data-position='bottom'>\r\n          <svg class=\"icon_change_log\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n            <use xlink:href=\"./assets/dist/img/icons.svg#icon_job\"></use>\r\n          </svg> &nbsp;New Job</button>\r\n      </a>\r\n      <a [routerLink]=\"['/bulk-load/new']\">\r\n        <button kendoButton class=\"pull-right\" primary=\"true\">\r\n          <svg class=\"icon_change_log\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n            <use xlink:href=\"./assets/dist/img/icons.svg#icon_job\"></use>\r\n          </svg> &nbsp;New Bulk Job</button>\r\n      </a>\r\n      <button kendoButton class=\"pull-right\" (click)=\"allClear()\">\r\n        All Clear</button>\r\n      <button kendoButton class=\"pull-right\" (click)=\"syncMetadata()\">\r\n        Sync Metadata</button>\r\n    </div>\r\n  </div>\r\n</section>\r\n<section id=\"jobs_body\" class=\"content content-body\">\r\n  <kendo-grid [data]=\"jobGridDataResult\" [height]=\"460\" [filterable]=\"'menu'\" [resizable]=\"true\"\r\n    (pageChange)=\"pageChangeJob($event)\" [sortable]=\" { allowUnsort: true}\" [pageSize]=\"state.take\" [skip]=\"state.skip\"\r\n    [sort]=\"state.sort\" [filter]=\"state.filter\" [sortable]=\"true\"\r\n    [pageable]=\"{info: info, type: type,buttonCount:buttonCount, pageSizes: jobPageSizes, previousNext: previousNext }\"\r\n    [loading]=\"jobGridLoading\" (filterChange)=\"filterChange($event)\" (dataStateChange)=\"dataStateChange($event)\"\r\n    (sortChange)=\"sortChangeConnection($event)\" (cellClick)=\"cellClickHandler($event)\">\r\n\r\n    <kendo-grid-column field=\"action\" title=\"\" width=\"42\" [filterable]=\"flase\" [locked]=\"true\">\r\n      <ng-template kendoGridCellTemplate kendoTooltip let-dataItem>\r\n          <span id=\"dotBtn\">\r\n  \r\n          <button #anchor (click)=\"onToggle(dataItem)\" class=\"k-button padding-0\"\r\n            style=\"border: 0px;background: transparent\">\r\n            <img src=\"/assets/dist/img/vertical-dot.png\" alt=\"vertical-dot\"  width=\"16px\" />\r\n          </button>\r\n       </span>\r\n       \r\n          <span kendoTooltip position=\"right\">\r\n\r\n            <kendo-popup [anchor]=\"anchor\" (anchorViewportLeave)=\"dataItem.toggle = false\" *ngIf=\"dataItem.toggle\">\r\n              <ul class=\"nav nav-list\">\r\n\r\n                <li>\r\n                  <a href=\"javascript:void(0)\" routerLink=\"/jobs/{{dataItem.jobId}}\" class=\"changelog white\"\r\n                    id=\"editJob\">\r\n                    <svg class=\"icon_update\" shape-rendering=\"geometricPrecision\" width=\"15\" height=\"15\">\r\n                      <use xlink:href=\"./assets/dist/img/icons.svg#icon_update\"></use>\r\n                    </svg> Edit Job\r\n                  </a>\r\n                </li>\r\n\r\n                <li>\r\n                  <a href=\"javascript:void(0)\" (click)=\"runJob(dataItem)\" class=\"changelog white\" id=\"runJob\">\r\n                    <svg class=\"icon_update\" shape-rendering=\"geometricPrecision\" width=\"15\" height=\"15\">\r\n                      <use xlink:href=\"./assets/dist/img/icons.svg#icon_update\"></use>\r\n                    </svg> Run Job\r\n                  </a>\r\n                </li>\r\n                <li>\r\n                  <a href=\"javascript:void(0)\" (click)=\"getAllAccountOfTreeitory(dataItem.territoryId)\"\r\n                    class=\"changelog white\">\r\n                    <svg class=\"icon_copy\" shape-rendering=\"geometricPrecision\" width=\"15\" height=\"15\">\r\n                      <use xlink:href=\"./assets/dist/img/icons.svg#icon_copy\"></use>\r\n                    </svg> Clone Job\r\n                  </a>\r\n                </li>\r\n                <li>\r\n                <li>\r\n                  <a (click)=\"exportJob(dataItem)\" [href]=\"downloadJsonHref\" download=\"download.json\"\r\n                    class=\"changelog white\">\r\n                    <svg class=\"icon_export\" shape-rendering=\"geometricPrecision\" width=\"15\" height=\"15\">\r\n                      <use xlink:href=\"./assets/dist/img/icons.svg#icon_export\"></use>\r\n                    </svg> Export Job\r\n                  </a>\r\n                </li>\r\n                <li>\r\n                  <a href=\"javascript:void(0)\" *ngIf=\"!openedLog\" (click)=\"openLog(dataItem)\">\r\n                    <svg class=\"icon_change_log\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n                      <use xlink:href=\"./assets/dist/img/icons.svg#icon_change_log\"></use>\r\n                    </svg> View log\r\n                  </a>\r\n                </li>\r\n                <li>\r\n                  <a href=\"javascript:void(0)\" (click)=\"removeJob(dataItem.jobId)\" class=\"changelog white\"\r\n                    id=\"removeJob\">\r\n                    <svg class=\"icon_delete\" shape-rendering=\"geometricPrecision\" width=\"15\" height=\"15\">\r\n                      <use xlink:href=\"./assets/dist/img/icons.svg#icon_delete\"></use>\r\n                    </svg> Remove Job\r\n                  </a>\r\n                </li>\r\n              </ul>\r\n            </kendo-popup>\r\n          </span>\r\n      </ng-template>\r\n    </kendo-grid-column>\r\n    <kendo-grid-column title=\"\" width=\"48x\"\r\n      [style]=\"{'overflow': 'hidden','text-overflow': 'ellipsis', 'white-space': 'nowrap'}\" [locked]=\"true\">\r\n      <ng-template kendoGridCellTemplate let-dataItem>\r\n        <button #info (mouseover)='showInfo(dataItem)'\r\n          [ngClass]=\"{'infoBtnDisabled': dataItem.viewJobDifferenceDetailsResponseList === undefined || dataItem?.viewJobDifferenceDetailsResponseList?.length === 0}\"\r\n          [disabled]=\"dataItem.viewJobDifferenceDetailsResponseList === undefined || dataItem?.viewJobDifferenceDetailsResponseList?.length === 0\"\r\n          class=\"k-button padding-0\" style=\"border: 0px;background: transparent\">\r\n          <svg title=\"Stop Job\" class=\"icon_connection\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n            <use xlink:href=\"./assets/dist/img/icons.svg#icon_info\"></use>\r\n          </svg>\r\n        </button>\r\n        <kendo-popup [anchor]=\"info\" (anchorViewportLeave)=\"dataItem.toggleInfo = false\" *ngIf=\"dataItem.toggleInfo\">\r\n          <div style=\"width: 200px; height: 100px; overflow: auto;\">\r\n            <table style=\"font-size: 10px\">\r\n              <tr *ngFor=\"let item of dataItem.viewJobDifferenceDetailsResponseList\">\r\n                <td>\r\n                  {{item.fieldName}}\r\n                </td>\r\n                <td>\r\n                  {{item.status}}\r\n                </td>\r\n              </tr>\r\n            </table>\r\n          </div>\r\n        </kendo-popup>\r\n      </ng-template>\r\n    </kendo-grid-column>\r\n    <!-- <kendo-grid-column title=\"\" width=\"45x\" [locked]=\"true\"\r\n    [style]=\"{'overflow': 'hidden','text-overflow': 'ellipsis', 'white-space': 'nowrap'}\">\r\n    <ng-template kendoGridCellTemplate let-dataItem>\r\n        <input type=\"checkbox\"/>\r\n    </ng-template>\r\n  </kendo-grid-column> -->\r\n    <kendo-grid-column field=\"jobName\" title=\"Job Name\" class=\"kendo-cell\" width=\"320\"\r\n      [style]=\"{'overflow': 'hidden','text-overflow': 'ellipsis', 'white-space': 'nowrap'}\">\r\n      <ng-template kendoGridFilterMenuTemplate let-filter let-column=\"column\" let-filterService=\"filterService\">\r\n        <input kendoTextBox (input)=\"filterChangeConnection($event, filterService, 'jobName')\" />\r\n      </ng-template>\r\n    </kendo-grid-column>\r\n\r\n    <kendo-grid-column field=\"sourceConnectionName\" title=\"Source Connection\" width=\"150\"\r\n      [style]=\"{'overflow': 'hidden','text-overflow': 'ellipsis', 'white-space': 'nowrap'}\" [filterable]=\"true\">\r\n      <ng-template kendoGridFilterMenuTemplate let-filter let-column=\"column\" let-filterService=\"filterService\">\r\n        <kendo-dropdownlist [data]=\"connections\" class=\"custom_dpd\" height=\"40\" [filterable]=\"true\" [textField]=\"'name'\"\r\n          [valueField]=\"'name'\" [(ngModel)]=\"jobFilter.sourceConnectionName\" [value]=\"'name'\" [valuePrimitive]=\"true\"\r\n          [valuePrimitive]=\"true\" (valueChange)=\"DropDownFilterChange($event,filterService, 'sourceConnectionName')\">\r\n        </kendo-dropdownlist>\r\n      </ng-template>\r\n    </kendo-grid-column>\r\n\r\n    <kendo-grid-column field=\"destinationConnectionName\" title=\"Destination Connection\" width=\"150\"\r\n      [style]=\"{'overflow': 'hidden','text-overflow': 'ellipsis', 'white-space': 'nowrap'}\" [filterable]=\"true\">\r\n      <ng-template kendoGridFilterMenuTemplate let-filter let-column=\"column\" let-filterService=\"filterService\">\r\n        <kendo-dropdownlist [data]=\"connections\" class=\"custom_dpd\" height=\"40\" [filterable]=\"true\" [textField]=\"'name'\"\r\n          [valueField]=\"'name'\" [(ngModel)]=\"jobFilter.destinationConnectionName\" [value]=\"'name'\"\r\n          [valuePrimitive]=\"true\"\r\n          (valueChange)=\"DropDownFilterChange($event,filterService, 'destinationConnectionName')\">\r\n        </kendo-dropdownlist>\r\n      </ng-template>\r\n    </kendo-grid-column>\r\n\r\n    <kendo-grid-column field=\"status\" title=\"Status\" width=\"150\"\r\n      [style]=\"{'overflow': 'hidden','text-overflow': 'ellipsis', 'white-space': 'nowrap'}\" [filterable]=\"true\">\r\n      <ng-template kendoGridFilterMenuTemplate let-filter let-column=\"column\" let-filterService=\"filterService\">\r\n        <kendo-dropdownlist [data]=\"statusDropDownFilter\" class=\"kendo-input\" [filterable]=\"true\"\r\n          [defaultItem]=\"{ description: 'Status State', code: '0' }\" [textField]=\"'description'\" [valueField]=\"'code'\"\r\n          [valuePrimitive]=\"true\" [value]=\"'code'\" [(ngModel)]=\"jobFilter.status\"\r\n          (valueChange)=\"DropDownFilterChange($event, filterService,'status')\">\r\n        </kendo-dropdownlist>\r\n      </ng-template>\r\n    </kendo-grid-column>\r\n\r\n    <kendo-grid-column field=\"state\" title=\"Total Record Count\" width=\"150\"\r\n      [style]=\"{'overflow': 'hidden','text-overflow': 'ellipsis', 'white-space': 'nowrap'}\" [filterable]=\"false\">\r\n    </kendo-grid-column>\r\n    <kendo-grid-column field=\"lastRun\" title=\"Last Run (PST)\" width=\"150\"\r\n      [style]=\"{'overflow': 'hidden','text-overflow': 'ellipsis', 'white-space': 'nowrap'}\" [filterable]=\"false\">\r\n      <ng-template kendoGridFilterMenuTemplate let-filter let-column=\"column\" let-filterService=\"filterService\">\r\n        <input kendoTextBox [value]=\"jobModal.destinationConnection\"\r\n          (input)=\"onInputFilterValue($event, filterService,'lastRun')\" />\r\n      </ng-template>\r\n      <ng-template kendoGridCellTemplate let-dataItem>\r\n        <span kendoTooltip position=\"left\" title=\"{{dataItem.lastRun}}\">\r\n          {{dataItem.lastRun}}\r\n        </span>\r\n      </ng-template>\r\n    </kendo-grid-column>\r\n\r\n    <kendo-grid-column field=\"created\" title=\"Created Date (PST)\" width=\"150\"\r\n      [style]=\"{'overflow': 'hidden','text-overflow': 'ellipsis', 'white-space': 'nowrap'}\" [filterable]=\"false\">\r\n      <ng-template kendoGridFilterMenuTemplate let-filter let-column=\"column\" let-filterService=\"filterService\">\r\n        <input kendoTextBox [value]=\"jobModal.created\" (input)=\"onInputFilterValue($event, filterService,'created')\" />\r\n      </ng-template>\r\n      <ng-template kendoGridCellTemplate let-dataItem>\r\n        <span kendoTooltip position=\"left\" title=\"{{dataItem.created}}\">\r\n          {{dataItem.created}}\r\n        </span>\r\n      </ng-template>\r\n    </kendo-grid-column>\r\n\r\n    <kendo-grid-column field=\"updated\" title=\"Updated Date (PST)\" width=\"150\"\r\n      [style]=\"{'overflow': 'hidden','text-overflow': 'ellipsis', 'white-space': 'nowrap'}\" [filterable]=\"false\">\r\n      <ng-template kendoGridFilterMenuTemplate let-filter let-column=\"column\" let-filterService=\"filterService\">\r\n        <input kendoTextBox [value]=\"jobModal.updated\" (input)=\"onInputFilterValue($event, filterService,'updated')\" />\r\n      </ng-template>\r\n      <ng-template kendoGridCellTemplate let-dataItem>\r\n        <span kendoTooltip position=\"left\" title=\"{{dataItem.updated}}\">\r\n          {{dataItem.updated}}\r\n        </span>\r\n      </ng-template>\r\n    </kendo-grid-column>\r\n\r\n    <kendo-grid-column field=\"createdBy\" title=\"Created By\" width=\"150\"\r\n      [style]=\"{'overflow': 'hidden','text-overflow': 'ellipsis', 'white-space': 'nowrap'}\" [filterable]=\"false\">\r\n      <ng-template kendoGridFilterMenuTemplate let-filter let-column=\"column\" let-filterService=\"filterService\">\r\n        <input kendoTextBox [value]=\"jobModal.createdBy\"\r\n          (input)=\"onInputFilterValue($event, filterService,'createdBy')\" />\r\n      </ng-template>\r\n      <ng-template kendoGridCellTemplate let-dataItem>\r\n        <span kendoTooltip position=\"left\" title=\"{{dataItem.createdBy}}\">\r\n          {{dataItem.createdBy}}\r\n        </span>\r\n      </ng-template>\r\n    </kendo-grid-column>\r\n\r\n    <kendo-grid-column field=\"updatedBy\" title=\"Updated By\" width=\"150\"\r\n      [style]=\"{'overflow': 'hidden','text-overflow': 'ellipsis', 'white-space': 'nowrap'}\" [filterable]=\"false\">\r\n      <ng-template kendoGridFilterMenuTemplate let-filter let-column=\"column\" let-filterService=\"filterService\">\r\n        <input kendoTextBox [value]=\"jobModal.updatedBy\"\r\n          (input)=\"onInputFilterValue($event, filterService,'updatedBy')\" />\r\n      </ng-template>\r\n      <ng-template kendoGridCellTemplate let-dataItem>\r\n        <span kendoTooltip position=\"left\" title=\"{{dataItem.updatedBy}}\">\r\n          {{dataItem.updatedBy}}\r\n        </span>\r\n      </ng-template>\r\n    </kendo-grid-column>\r\n\r\n  </kendo-grid>\r\n  <kendo-dialog title=\"View Log\" *ngIf=\"isOpenedLog\" (close)=\"close('cancel')\" [minWidth]=\"800\" [width]=\"1000\">\r\n    <app-log [job]=\"job\"></app-log>\r\n  </kendo-dialog>\r\n</section>"

/***/ }),

/***/ "./src/app/jobs/jobs.component.scss":
/*!******************************************!*\
  !*** ./src/app/jobs/jobs.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#jobs_header {\n  padding: 5px 15px 10px 15px;\n  margin: 14px 0px 0px 17px; }\n  #jobs_header .job_name {\n    border: 0px;\n    width: 100%; }\n  #jobs_header .job_name:hover {\n    background-color: #ecf6ff; }\n  #jobs_header .job_name:hover ::-webkit-input-placeholder {\n      color: white; }\n  #jobs_header .job_name::-webkit-input-placeholder {\n    font-size: 18px; }\n  #jobs_header .job_name::-moz-placeholder {\n    font-size: 18px; }\n  #jobs_header .job_name::-ms-input-placeholder {\n    font-size: 18px; }\n  #jobs_header .job_name::placeholder {\n    font-size: 18px; }\n  #jobs_header button {\n    margin: 7px 3px 0px 5px; }\n  #jobs_header h3, #jobs_header p {\n    margin: 0px 0px 0px -7px; }\n  #jobs_header .content {\n    padding: 30px;\n    color: #787878;\n    background-color: #fcf7f8;\n    border: 1px solid rgba(0, 0, 0, 0.05); }\n  .infoBtnDisabled svg {\n  opacity: 0.2; }\n  .k-grid tbody tr:hover td:nth-child(3) {\n  color: #0067c5;\n  font-weight: bold; }\n  .k-grid {\n  font-size: 12px; }\n  .nav > li > a {\n  padding-left: 35px;\n  font-size: 12px; }\n  .nav > li:hover,\n.nav > li:hover a,\n.nav > li:hover a > svg {\n  background: #0067c5;\n  color: white;\n  fill: white; }\n  .nav > li > a > svg {\n  position: absolute;\n  left: 10px; }\n  .content {\n  min-height: 250px;\n  padding: 15px;\n  margin-right: auto;\n  margin-left: auto;\n  padding-left: 15px;\n  padding-right: 15px;\n  background: white;\n  margin: 15px;\n  border-radius: 0.25rem;\n  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9qb2JzL0U6XFxVSS1Qcm9qZWN0XFxFVExcXHNyY1xcc3JjL2FwcFxcam9ic1xcam9icy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLDJCQUEyQjtFQUMzQix5QkFBeUIsRUFBQTtFQUYzQjtJQUtJLFdBQVc7SUFDWCxXQUFXLEVBQUE7RUFOZjtJQVVJLHlCQUF5QixFQUFBO0VBVjdCO01BWU0sWUFBWSxFQUFBO0VBWmxCO0lBaUJJLGVBQWUsRUFBQTtFQWpCbkI7SUFpQkksZUFBZSxFQUFBO0VBakJuQjtJQWlCSSxlQUFlLEVBQUE7RUFqQm5CO0lBaUJJLGVBQWUsRUFBQTtFQWpCbkI7SUFxQkksdUJBQXVCLEVBQUE7RUFyQjNCO0lBMEJJLHdCQUF3QixFQUFBO0VBMUI1QjtJQTZCSSxhQUFhO0lBQ2IsY0FBYztJQUNkLHlCQUF5QjtJQUN6QixxQ0FBaUMsRUFBQTtFQUlyQztFQUVJLFlBQVksRUFBQTtFQUdoQjtFQUNFLGNBQWM7RUFDZCxpQkFBaUIsRUFBQTtFQUduQjtFQUNFLGVBQWUsRUFBQTtFQUdqQjtFQUNFLGtCQUFrQjtFQUNsQixlQUFlLEVBQUE7RUFFakI7OztFQUdFLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osV0FBVyxFQUFBO0VBRWI7RUFDRSxrQkFBa0I7RUFDbEIsVUFBVSxFQUFBO0VBRVo7RUFDRSxpQkFBaUI7RUFDakIsYUFBYTtFQUNiLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsWUFBWTtFQUNaLHNCQUFzQjtFQUN0Qix3Q0FBd0MsRUFBQSIsImZpbGUiOiJhcHAvam9icy9qb2JzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2pvYnNfaGVhZGVyIHtcclxuICBwYWRkaW5nOiA1cHggMTVweCAxMHB4IDE1cHg7XHJcbiAgbWFyZ2luOiAxNHB4IDBweCAwcHggMTdweDtcclxuICBcclxuICAuam9iX25hbWUge1xyXG4gICAgYm9yZGVyOiAwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcblxyXG4gIC5qb2JfbmFtZTpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWNmNmZmO1xyXG4gICAgOjotd2Via2l0LWlucHV0LXBsYWNlaG9sZGVyIHtcclxuICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLmpvYl9uYW1lOjpwbGFjZWhvbGRlciB7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgfVxyXG5cclxuICBidXR0b24ge1xyXG4gICAgbWFyZ2luOiA3cHggM3B4IDBweCA1cHg7XHJcbiAgfVxyXG5cclxuICBoMyxwXHJcbiAge1xyXG4gICAgbWFyZ2luOiAwcHggMHB4IDBweCAtN3B4O1xyXG4gIH1cclxuICAuY29udGVudCB7XHJcbiAgICBwYWRkaW5nOiAzMHB4O1xyXG4gICAgY29sb3I6ICM3ODc4Nzg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmNmN2Y4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLDAsMCwuMDUpO1xyXG4gIH1cclxuIFxyXG59XHJcbi5pbmZvQnRuRGlzYWJsZWQge1xyXG4gIHN2ZyB7XHJcbiAgICBvcGFjaXR5OiAwLjI7XHJcbiAgfVxyXG59XHJcbi5rLWdyaWQgdGJvZHkgdHI6aG92ZXIgdGQ6bnRoLWNoaWxkKDMpIHtcclxuICBjb2xvcjogIzAwNjdjNTtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG5cclxuLmstZ3JpZCB7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG59XHJcblxyXG4ubmF2ID4gbGkgPiBhIHtcclxuICBwYWRkaW5nLWxlZnQ6IDM1cHg7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG59XHJcbi5uYXYgPiBsaTpob3ZlcixcclxuLm5hdiA+IGxpOmhvdmVyIGEsXHJcbi5uYXYgPiBsaTpob3ZlciBhID4gc3ZnIHtcclxuICBiYWNrZ3JvdW5kOiAjMDA2N2M1O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBmaWxsOiB3aGl0ZTtcclxufVxyXG4ubmF2ID4gbGkgPiBhID4gc3ZnIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgbGVmdDogMTBweDtcclxufVxyXG4uY29udGVudCB7XHJcbiAgbWluLWhlaWdodDogMjUwcHg7XHJcbiAgcGFkZGluZzogMTVweDtcclxuICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgcGFkZGluZy1sZWZ0OiAxNXB4O1xyXG4gIHBhZGRpbmctcmlnaHQ6IDE1cHg7XHJcbiAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgbWFyZ2luOiAxNXB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDAuMjVyZW07XHJcbiAgYm94LXNoYWRvdzogMCAycHggNXB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/jobs/jobs.component.ts":
/*!****************************************!*\
  !*** ./src/app/jobs/jobs.component.ts ***!
  \****************************************/
/*! exports provided: JobsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobsComponent", function() { return JobsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _model_job__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/job */ "./src/app/model/job.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var src_app_connections_connection_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/connections/connection-service */ "./src/app/connections/connection-service.ts");
/* harmony import */ var src_app_connections_connections_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/connections/connections.component */ "./src/app/connections/connections.component.ts");
/* harmony import */ var _job_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./job.service */ "./src/app/jobs/job.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var intro_js_intro_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! intro.js/intro.js */ "./node_modules/intro.js/intro.js");
/* harmony import */ var intro_js_intro_js__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(intro_js_intro_js__WEBPACK_IMPORTED_MODULE_10__);












var JobsComponent = /** @class */ (function () {
    function JobsComponent(jobService, toastr, cookieService, eRef, router, sanitizer, connectionService, route) {
        this.jobService = jobService;
        this.toastr = toastr;
        this.cookieService = cookieService;
        this.eRef = eRef;
        this.router = router;
        this.sanitizer = sanitizer;
        this.connectionService = connectionService;
        this.route = route;
        this.jobPageSizes = true;
        this.previousNext = true;
        this.info = true;
        this.buttonCount = 5;
        this.type = 'input';
        this.requestFilterObj = new src_app_connections_connections_component__WEBPACK_IMPORTED_MODULE_5__["Connection"]();
        this.deleteJob = Array();
        this.jobFilterModel = new _model_job__WEBPACK_IMPORTED_MODULE_2__["Job"]();
        this.state = {
            skip: 0,
            take: 10
        };
        this.statusDropDownFilter = Array();
        this.introJS = intro_js_intro_js__WEBPACK_IMPORTED_MODULE_10__();
        this.jobs = new Array();
        this.jobFilter = new _model_job__WEBPACK_IMPORTED_MODULE_2__["JobFilter"]();
        this.jobFilter.recordCount = 10;
        this.jobStartIndex = 0;
        this.jobSkip = 0;
        this.jobPageSize = 10;
        this.jobGridLoading = false;
        this.isAlertModelVisible = false;
        this.isOpenedLog = false;
        this.sourceCollection = new Array();
        this.getAllConnection();
        this.statusDropDownFilter.push({ description: 'In Process', code: 'In Process' }, { description: 'Failed', code: 'Failed' }, { description: 'Completed', code: 'Completed' });
        if (this.cookieService.get('intro') === 'runJobs') {
            this.introJS.setOptions({
                steps: [
                    {
                        element: '#dotBtn',
                        intro: 'Press three vertical dot '
                    },
                    {
                        element: '#runJob',
                        intro: 'Press Run Job Text'
                    },
                ]
            });
        }
        else if (this.cookieService.get('intro') === 'editJobs') {
            this.introJS.setOptions({
                steps: [
                    {
                        element: '#dotBtn',
                        intro: 'Press three vertical dot '
                    },
                    {
                        element: '#editJob',
                        intro: 'Press Edit Job Text'
                    },
                ]
            });
        }
        else if (this.cookieService.get('intro') === 'removeJobs') {
            this.introJS.setOptions({
                steps: [
                    {
                        element: '#dotBtn',
                        intro: 'Press three vertical dot '
                    },
                    {
                        element: '#removeJob',
                        intro: 'Press Remove Job Text'
                    },
                ]
            });
        }
    }
    JobsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getAllJobs();
        this.checkProgress();
        if (this.route.snapshot.queryParams.action === 'intro') {
            this.introJS.start();
            this.introJS.onchange(function (targetElement) {
            });
            this.introJS.oncomplete(function () {
                _this.router.navigate(['/jobs'], { queryParams: {} });
            });
            this.introJS.onexit(function () {
                _this.router.navigate(['/jobs'], { queryParams: {} });
            });
        }
    };
    JobsComponent.prototype.cellClickHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex, columnIndex = _a.columnIndex, dataItem = _a.dataItem, isEdited = _a.isEdited;
        if (columnIndex > 1) {
            this.router.navigate(['jobs/', dataItem.jobId]);
        }
    };
    JobsComponent.prototype.onInputFilterValue = function (e, filterService, fieldName) {
        filterService.filter({
            filters: [{
                    field: fieldName,
                    operator: 'contains',
                    value: e.srcElement.value
                }],
            logic: 'and'
        });
    };
    JobsComponent.prototype.getAllConnection = function () {
        var _this = this;
        this.connectionService.getAllConnection(new src_app_connections_connections_component__WEBPACK_IMPORTED_MODULE_5__["Connection"](), 0, 10, 'createdDate', 'desc').subscribe(function (data) {
            _this.connections = Array();
            if (JSON.parse(JSON.stringify(data)).results[0].connectionResponseList !== null) {
                JSON.parse(JSON.stringify(data)).results[0].connectionResponseList.forEach(function (element) {
                    _this.connections.push(new src_app_connections_connections_component__WEBPACK_IMPORTED_MODULE_5__["Connection"](element));
                });
                var obj = new src_app_connections_connections_component__WEBPACK_IMPORTED_MODULE_5__["Connection"]();
                obj.name = 'Select Connection';
                _this.connections.splice(0, 0, obj);
                _this.sourceConnectionValue = obj;
                _this.destinationConnectionValue = obj;
            }
        });
    };
    JobsComponent.prototype.jobStateChange = function (state) {
    };
    JobsComponent.prototype.pageJobChange = function (event) {
        this.jobStartIndex = event.skip;
        this.jobPageSize = event.take;
        this.getAllJobs();
    };
    JobsComponent.prototype.getAllJobs = function () {
        var _this = this;
        this.jobGridLoading = true;
        this.jobs = [];
        this.jobService.getAllJobs(this.jobFilter, this.jobFilter.jobName, this.jobFilter.startRecord, this.jobFilter.recordCount, this.jobFilter.sortableColumn, this.jobFilter.sortingType).subscribe(function (data) {
            _this.jobGridLoading = false;
            _this.jobs = new Array();
            var jobList = JSON.parse(JSON.stringify(data)).results[0].jobResponseList;
            _this.jobs = JSON.parse(JSON.stringify(data)).results[0].jobResponseList;
            _this.jobGridDataResult = {
                data: _this.jobs,
                total: JSON.parse(JSON.stringify(data)).results[0].totalRecordCount
            };
        });
    };
    JobsComponent.prototype.DropDownFilterChange = function (event, filterService, filterOn) {
        this.jobFilter.name = event;
        filterService.filter({
            filters: [{
                    field: filterOn,
                    operator: 'contains',
                    value: event
                }],
            logic: 'and'
        });
    };
    JobsComponent.prototype.close = function (status) {
        console.log("Dialog result: " + status);
        this.isOpenedLog = false;
    };
    JobsComponent.prototype.openLog = function (job) {
        this.job = job;
        this.isOpenedLog = true;
    };
    JobsComponent.prototype.allClear = function () {
        this.state.filter = {
            logic: 'and',
            filters: []
        };
        this.jobFilter = new _model_job__WEBPACK_IMPORTED_MODULE_2__["JobFilter"]();
        this.getAllJobs();
    };
    JobsComponent.prototype.dataStateChange = function (state) {
        this.state = state;
    };
    JobsComponent.prototype.pageChangeJob = function (event) {
        this.jobFilter.startRecord = event.skip;
        this.getAllJobs();
    };
    JobsComponent.prototype.onToggle = function (job) {
        this.jobGridDataResult.data.map(function (n) { return n.toggle = false; });
        job.toggle = !job.toggle;
        if (this.route.snapshot.queryParams.action === 'intro') {
            this.introJS.goToStepNumber(1).start();
        }
    };
    JobsComponent.prototype.showInfo = function (job) {
        this.jobGridDataResult.data.map(function (n) { return n.toggleInfo = false; });
        job.toggleInfo = !job.toggleInfo === null ? true : !job.toggleInfo;
    };
    JobsComponent.prototype.filterChange = function (filter) {
        this.jobFilter.startRecord = 0;
        this.jobFilter.recordCount = 10;
        this.state.skip = 0;
        this.state.take = 10;
        if (filter.filters.length === 0) {
            this.jobFilter = new _model_job__WEBPACK_IMPORTED_MODULE_2__["JobFilter"]();
            this.jobFilter.jobName = '';
        }
        this.getAllJobs();
    };
    JobsComponent.prototype.sortChangeConnection = function (sort) {
        this.jobFilter.sortingType = sort[0]['dir'] === undefined ? 'asc' : 'desc';
        this.jobFilter.sortableColumn = sort[0]['field'];
        this.jobFilter.startRecord = 0;
        this.getAllJobs();
    };
    JobsComponent.prototype.filterChangeConnection = function (e, filterService, filterOn) {
        this.jobFilter[filterOn] = e.srcElement.value;
        filterService.filter({
            filters: [{
                    field: filterOn,
                    operator: 'contains',
                    value: e.srcElement.value
                }], logic: 'and'
        });
    };
    JobsComponent.prototype.runJob = function (dataItem) {
        var _this = this;
        this.jobService.runJob(dataItem.jobId)
            .subscribe(function (response) {
            var res = JSON.parse(JSON.stringify(response));
            _this.toastr.warning(res.message);
        }, function (error) {
            console.log(error);
        });
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'runJobs') {
            this.introJS.exit();
        }
    };
    JobsComponent.prototype.removeJob = function (jobId) {
        var _this = this;
        this.deleteJob = [];
        this.deleteJob.push(jobId);
        this.jobService.deleteJob(this.deleteJob).subscribe(function (data) {
            _this.deleteMessage = JSON.parse(JSON.stringify(data)).message;
            _this.getAllJobs();
            _this.toastr.success(_this.deleteMessage);
            console.log(data);
            if (_this.route.snapshot.queryParams.action === 'intro' && _this.cookieService.get('intro') === 'removeJobs') {
                _this.introJS.exit();
            }
        });
    };
    JobsComponent.prototype.exportJob = function (jobId) {
        var v1 = jobId;
        var theJSON = JSON.stringify(v1);
        var uri = this.sanitizer.bypassSecurityTrustUrl('data:text/json;charset=UTF-8,' + encodeURIComponent(theJSON));
        this.downloadJsonHref = uri;
    };
    JobsComponent.prototype.checkProgress = function () {
        var _this = this;
        this.intervalObj = setInterval(function () {
            _this.jobService.gettJobsInProgress()
                .subscribe(function (response) {
                var stats = JSON.parse(JSON.stringify(response)).results;
                stats.forEach(function (job) {
                    _this.jobGridDataResult.data.filter(function (n) { return n.jobId === job.jobId; }).map(function (n) { n.state = "Copied " + job.processedRecord + " of " + job.totalRecordCount; n.status = job.status; });
                });
            }, function (error) {
            });
        }, 3000);
    };
    JobsComponent.prototype.syncMetadata = function () {
        var _this = this;
        this.jobGridLoading = true;
        var jobRequest = new _model_job__WEBPACK_IMPORTED_MODULE_2__["JobRequest"]();
        this.jobService.syncMetadata(jobRequest)
            .subscribe(function (response) {
            _this.jobGridLoading = false;
            var metadataDifference = JSON.parse(JSON.stringify(response)).results;
            _this.jobGridDataResult.data.forEach(function (element) {
                metadataDifference.filter(function (n) { return n.jobId === element.jobId; }).forEach(function (e) {
                    element.viewJobDifferenceDetailsResponseList = new Array();
                    element.viewJobDifferenceDetailsResponseList = e.viewJobDifferenceDetailsResponseList;
                });
            });
        }, function (error) {
            _this.jobGridLoading = false;
            console.log(error);
        });
    };
    JobsComponent.prototype.documentClick = function (event) {
        if (event.srcElement.alt !== undefined && event.srcElement.alt === 'vertical-dot') {
        }
        else if (this.eRef.nativeElement.contains(event.target)) {
            this.jobGridDataResult.data.map(function (n) { return n.toggle = false; });
        }
        else {
            this.jobGridDataResult.data.map(function (n) { return n.toggle = false; });
        }
        this.jobGridDataResult.data.map(function (n) { return n.toggleInfo = false; });
    };
    JobsComponent.prototype.ngOnDestroy = function () {
        clearInterval(this.intervalObj);
        intro_js_intro_js__WEBPACK_IMPORTED_MODULE_10__().exit();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('document:click', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], JobsComponent.prototype, "documentClick", null);
    JobsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-jobs',
            template: __webpack_require__(/*! ./jobs.component.html */ "./src/app/jobs/jobs.component.html"),
            styles: [__webpack_require__(/*! ./jobs.component.scss */ "./src/app/jobs/jobs.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_job_service__WEBPACK_IMPORTED_MODULE_6__["JobService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"],
            ngx_cookie_service__WEBPACK_IMPORTED_MODULE_3__["CookieService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_9__["DomSanitizer"],
            src_app_connections_connection_service__WEBPACK_IMPORTED_MODULE_4__["ConnectionService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"]])
    ], JobsComponent);
    return JobsComponent;
}());



/***/ }),

/***/ "./src/app/jobs/log-status/log-status.component.css":
/*!**********************************************************!*\
  !*** ./src/app/jobs/log-status/log-status.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvam9icy9sb2ctc3RhdHVzL2xvZy1zdGF0dXMuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/jobs/log-status/log-status.component.html":
/*!***********************************************************!*\
  !*** ./src/app/jobs/log-status/log-status.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<kendo-grid [data]=\"logStatus\" [height]=\"410\">\r\n  <kendo-grid-column field=\"skipRecordCount\" title=\"Skip Record Count\" width=\"100\">\r\n  </kendo-grid-column>\r\n  <kendo-grid-column field=\"message\" title=\"Message\" width=\"500\">\r\n  </kendo-grid-column>\r\n  <!-- <div *kendoGridDetailTemplate=\"let dataItem\">\r\n      <h1>ddd</h1>\r\n  </div> -->\r\n</kendo-grid>"

/***/ }),

/***/ "./src/app/jobs/log-status/log-status.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/jobs/log-status/log-status.component.ts ***!
  \*********************************************************/
/*! exports provided: LogStatusComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogStatusComponent", function() { return LogStatusComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _job_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../job.service */ "./src/app/jobs/job.service.ts");
/* harmony import */ var src_app_model_log__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/model/log */ "./src/app/model/log.ts");




var LogStatusComponent = /** @class */ (function () {
    function LogStatusComponent(jobService) {
        this.jobService = jobService;
        this.logStatus = new Array();
    }
    LogStatusComponent.prototype.ngOnInit = function () {
        this.jobStatusId();
    };
    LogStatusComponent.prototype.jobStatusId = function () {
        var _this = this;
        this.jobService.getJobLogStatus(this.jobStatus.jobId, this.jobStatus.jobStatusId).subscribe(function (response) {
            var res = JSON.parse(JSON.stringify(response)).results;
            res.forEach(function (element) {
                _this.logStatus.push({ skipRecordCount: element.skipRecordCount, message: element.message });
            });
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", src_app_model_log__WEBPACK_IMPORTED_MODULE_3__["Log"])
    ], LogStatusComponent.prototype, "jobStatus", void 0);
    LogStatusComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-log-status',
            template: __webpack_require__(/*! ./log-status.component.html */ "./src/app/jobs/log-status/log-status.component.html"),
            styles: [__webpack_require__(/*! ./log-status.component.css */ "./src/app/jobs/log-status/log-status.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_job_service__WEBPACK_IMPORTED_MODULE_2__["JobService"]])
    ], LogStatusComponent);
    return LogStatusComponent;
}());



/***/ }),

/***/ "./src/app/jobs/log/log.component.css":
/*!********************************************!*\
  !*** ./src/app/jobs/log/log.component.css ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".k-grid {\r\n    font-size: 12px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9qb2JzL2xvZy9sb2cuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGVBQWU7QUFDbkIiLCJmaWxlIjoiYXBwL2pvYnMvbG9nL2xvZy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmstZ3JpZCB7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/jobs/log/log.component.html":
/*!*********************************************!*\
  !*** ./src/app/jobs/log/log.component.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<kendo-grid [data]=\"gridData\" [height]=\"410\" [loading]=\"gridData.loading\" [pageSize]=\"state.take\" [skip]=\"state.skip\"\r\n  [sort]=\"state.sort\" [sortable]=\"true\" [pageable]=\"true\" [height]=\"550\" [navigable]=\"true\"\r\n  (dataStateChange)=\"dataStateChange($event)\">\r\n  <kendo-grid-column field=\"jobStatusId\" title=\"Job Status Id\" width=\"250\">\r\n  </kendo-grid-column>\r\n  <kendo-grid-column field=\"jobId\" title=\"Job Id\" width=\"250\">\r\n  </kendo-grid-column>\r\n  <kendo-grid-column field=\"totalRecordCount\" title=\"Total Record Count\" width=\"200\">\r\n  </kendo-grid-column>\r\n  <kendo-grid-column field=\"totalRecordProcessed\" title=\"Total Record Processed\" width=\"200\">\r\n  </kendo-grid-column>\r\n  <kendo-grid-column field=\"status\" title=\"Status\" width=\"150\">\r\n  </kendo-grid-column>\r\n  <div *kendoGridDetailTemplate=\"let dataItem\">\r\n    <app-log-status [jobStatus]=\"dataItem\"></app-log-status>\r\n  </div>\r\n</kendo-grid>"

/***/ }),

/***/ "./src/app/jobs/log/log.component.ts":
/*!*******************************************!*\
  !*** ./src/app/jobs/log/log.component.ts ***!
  \*******************************************/
/*! exports provided: LogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogComponent", function() { return LogComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _job_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../job.service */ "./src/app/jobs/job.service.ts");
/* harmony import */ var _model_job__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../model/job */ "./src/app/model/job.ts");
/* harmony import */ var _progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @progress/kendo-data-query */ "./node_modules/@progress/kendo-data-query/dist/es/main.js");
/* harmony import */ var _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @progress/kendo-angular-grid */ "./node_modules/@progress/kendo-angular-grid/dist/fesm5/index.js");






var LogComponent = /** @class */ (function () {
    function LogComponent(jobService) {
        this.jobService = jobService;
        this.log = new Array();
        this.state = {
            skip: 0,
            take: 5,
        };
        this.gridData = Object(_progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_4__["process"])(this.log, this.state);
    }
    LogComponent.prototype.ngOnInit = function () {
        this.getJobLog();
        this.jobStatusId();
    };
    LogComponent.prototype.ngAfterViewInit = function () {
        // Expand the first row initially
        // this.grid.expandRow(0);
    };
    LogComponent.prototype.getJobLog = function () {
        var _this = this;
        this.jobService.getJobLog(this.job.jobId).subscribe(function (response) {
            console.log(response);
            var res = JSON.parse(JSON.stringify(response)).results;
            console.log(res);
            _this.gridData = res;
            _this.id = res[0].jobStatusId;
            console.log(_this.id);
        });
    };
    LogComponent.prototype.jobStatusId = function () {
        var _this = this;
        this.jobService.getJobLog(this.job.jobId).subscribe(function (response) {
            console.log(response);
            var res = JSON.parse(JSON.stringify(response)).results;
            console.log(res);
            _this.gridData = res;
            _this.id = res[0].jobStatusId;
            console.log(_this.id);
            _this.jobService.getJobLogStatus(_this.job.jobId, _this.id).subscribe(function (response) {
                console.log(response);
            });
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_5__["GridComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _progress_kendo_angular_grid__WEBPACK_IMPORTED_MODULE_5__["GridComponent"])
    ], LogComponent.prototype, "grid", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _model_job__WEBPACK_IMPORTED_MODULE_3__["Job"])
    ], LogComponent.prototype, "job", void 0);
    LogComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            // providers: [JobsComponent],
            selector: 'app-log',
            template: __webpack_require__(/*! ./log.component.html */ "./src/app/jobs/log/log.component.html"),
            styles: [__webpack_require__(/*! ./log.component.css */ "./src/app/jobs/log/log.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_job_service__WEBPACK_IMPORTED_MODULE_2__["JobService"]])
    ], LogComponent);
    return LogComponent;
}());



/***/ }),

/***/ "./src/app/jobs/new-job/new-job.component.html":
/*!*****************************************************!*\
  !*** ./src/app/jobs/new-job/new-job.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"job_header\" class=\"content-header\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-6 padding-left-5\">\r\n      <input kendoInput type=\"text\" id=\"job_name\" class=\"job_name\" placeholder=\"New Jobs\"\r\n        [(ngModel)]=\"jobRequest.jobName\" />\r\n    </div>\r\n    <div kendoTooltip class=\"col-sm-6 padding-right-5\">\r\n      <button kendoButton class=\"pull-right\" (click)=\"runJob()\" *ngIf=\"!isRunEnabled\">\r\n        <svg title=\"Run Job\" class=\"icon_connection\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n          <use xlink:href=\"./assets/dist/img/icons.svg#icon_play\"></use>\r\n        </svg>\r\n      </button>\r\n      <button kendoButton class=\"pull-right k-button-default\" (click)=\"stopJob()\" *ngIf=\"isRunEnabled\">\r\n        <svg title=\"Stop Job\" class=\"icon_connection\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n          <use xlink:href=\"./assets/dist/img/icons.svg#icon_stop\"></use>\r\n        </svg>\r\n      </button>\r\n      <span>\r\n        <button kendoButton id=\"saveBtn\" class=\"pull-right k-button-default\" (click)=\"saveJob()\"\r\n          [disabled]=\"jobRequest.jobName == ''\">\r\n          <svg title=\"Save Job\" class=\"icon_connection\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n            <use xlink:href=\"./assets/dist/img/icons.svg#icon_save\"></use>\r\n          </svg>\r\n        </button>\r\n      </span>\r\n      <button #scheduler title=\"Schedule Job\" kendoTooltip kendoButton class=\"pull-right k-button-default\"\r\n        (click)=\"isSchedulerHide = !isSchedulerHide\" [disabled]=\"jobRequest.jobId == ''\">\r\n        <svg class=\"icon_connection\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n          <use xlink:href=\"./assets/dist/img/icons.svg#icon_schedule\"></use>\r\n        </svg>\r\n      </button>\r\n      <button kendoTooltip title=\"View Log\" kendoButton class=\"pull-right k-button-default\" (click)=\"openTab('logView')\"\r\n        [disabled]=\"jobRequest.jobId == ''\">\r\n        <svg class=\"icon_connection\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n          <use xlink:href=\"./assets/dist/img/icons.svg#icon_log\"></use>\r\n        </svg>\r\n      </button>\r\n      <span class=\"pull-right\" (click)=\"checkProgress(jobRequest.jobId)\" style=\"margin: 10px\">{{statsMessage}}</span>\r\n      <kendo-popup [anchor]=\"scheduler\" (anchorViewportLeave)=\"isSchedulerHide = false\" *ngIf=\"isSchedulerHide\"\r\n        class=\"scheduler-popup\" style=\"top: unset !important\">\r\n        <div>\r\n          <cron-editor [(cron)]=\"cronExpression\" [disabled]=\"isCronDisabled\" [(options)]=\"cronOptions\"></cron-editor>\r\n          <hr />\r\n          <p>Cron Expression: <strong>{{cronExpression}}</strong></p>\r\n          <button kendoButton class=\"pull-right k-button-default\" (click)=\"schduleJob()\">Save</button>\r\n        </div>\r\n      </kendo-popup>\r\n    </div>\r\n  </div>\r\n</section>\r\n<section id=\"job_content\">\r\n  <div class=\"box box-warning box-solid\" *ngIf=\"isSyncAble\" style=\"box-shadow: 0px 5px 17px 1px rgba(0,0 ,0, 0.5);\r\n      position: absolute;z-index: 100;width: 400px;left: 45%;\">\r\n    <div class=\"box-body\" style=\"margin: 0;font-size: 12px;\">\r\n      <div *ngIf=\"isSyncAble\">\r\n        <p>Found few column in source table. Do you want to sync with target table metadata?\r\n        </p>\r\n        <div class=\"box-tools pull-right\">\r\n          <button kendoButton [primary]=\"true\" (click)=\"getUpdatedTableMetadata()\">Ok\r\n          </button>&nbsp;\r\n          <button type=\"button\" class=\"btn btn-default btn-box-tool\" (click)=\"isSyncAble = !isSyncAble\"\r\n            data-widget=\"collapse\">Cancel\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- /.box-body -->\r\n  </div>\r\n  <div class=\"col-sm-6 connection\">\r\n    <div class=\"row connection_header\">\r\n      <div class=\"col-lg-11\">\r\n        <span id=\"sourceConnections\" data-position='top'>\r\n          <kendo-dropdownlist [data]=\"sourceConnections\" class=\"custom_dpd\" height=\"40\" [filterable]=\"true\"\r\n            [textField]=\"'name'\" [valueField]=\"'connectionId'\" (filterChange)=\"filterSourceConnection($event)\"\r\n            [(ngModel)]=\"sourceConnectionValue\" *ngIf=\"!isSourceConnectionConnected\"\r\n            (selectionChange)=\"selectionChangeSourceConnection($event)\">\r\n          </kendo-dropdownlist>\r\n        </span>\r\n        <span id=\"sourceCollection\">\r\n          <kendo-dropdownlist [data]=\"sourceCollection\" class=\"custom_dpd\" height=\"40\" [filterable]=\"true\"\r\n            [textField]=\"'text'\" [valueField]=\"'value'\" (filterChange)=\"filterSourceCollection($event)\"\r\n            [(ngModel)]=\"sourceCollectionValue\" (selectionChange)=\"selectionChangeSourceCollection($event)\"\r\n            *ngIf=\"isSourceConnectionConnected\" [disabled]= \"sourceValueType === 'EXCEL'\">\r\n          </kendo-dropdownlist>\r\n        </span>\r\n      </div>\r\n      <div class=\"col-lg-1 no-padding text-center\">\r\n\r\n        <span id=\"sourceConnectionsBtn\">\r\n          <button kendoButton class=\"k-button-default\" *ngIf=\"!isSourceConnectionConnected\"\r\n            (click)=\"selectionChangeSourceConnection()\">\r\n            <svg class=\"icon_ok\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\" fill=\"#0067C5\">\r\n              <use xlink:href=\"./assets/dist/img/icons.svg#icon_ok\"></use>\r\n            </svg>\r\n          </button>\r\n        </span>\r\n        <span id=\"openFilterBtn\">\r\n          <button #sourceConnection kendoButton class=\"k-button-default option\"\r\n            (click)=\"onToggleSourceConnection(isSourceConnectionOption)\" *ngIf=\"isSourceConnectionConnected\">\r\n            <svg class=\"icon_connection\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n              <use xlink:href=\"./assets/dist/img/icons.svg#icon_vertical_dots\"></use>\r\n            </svg>\r\n          </button>\r\n        </span>\r\n        <span id=\"filterBtn\">\r\n          <kendo-popup [anchor]=\"sourceConnection\" (anchorViewportLeave)=\"isSourceConnectionOption = false\"\r\n            *ngIf=\"isSourceConnectionOption\" class=\"source-connection-option\" style=\"top: unset !important\">\r\n            <ul class=\"nav nav-list\">\r\n              <li>\r\n                <a href=\"javascript:void(0)\" (click)=\"changeSourceConnection()\" class=\"changelog white\">\r\n                  <svg class=\"icon_change_log\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n                    <use xlink:href=\"./assets/dist/img/icons.svg#icon_change_log\"></use>\r\n                  </svg> Change Connection\r\n                </a>\r\n              </li>\r\n              <li>\r\n                <a href=\"javascript:void(0)\" *ngIf=\"isSourceConnectionConnected && isSourceCollectionSelected\"\r\n                  (click)=\"openTab('filter');onToggleSourceConnection(isSourceConnectionOption)\"\r\n                  class=\"changelog white\">\r\n                  <svg class=\"icon_update\" shape-rendering=\"geometricPrecision\" width=\"15\" height=\"15\">\r\n                    <use xlink:href=\"./assets/dist/img/icons.svg#icon_filter\"></use>\r\n                  </svg> Filter\r\n                </a>\r\n              </li>\r\n              <li>\r\n                <a href=\"javascript:void(0)\" *ngIf=\"isSourceConnectionConnected && isSourceCollectionSelected\"\r\n                  (click)=\"openTab('netChange');onToggleSourceConnection(isSourceConnectionOption)\"\r\n                  class=\"changelog white\">\r\n                  <svg class=\"icon_update\" shape-rendering=\"geometricPrecision\" width=\"15\" height=\"15\">\r\n                    <use xlink:href=\"./assets/dist/img/icons.svg#icon_settings\"></use>\r\n                  </svg> Net Change\r\n                </a>\r\n              </li>\r\n              <li>\r\n                <a href=\"javascript:void(0)\" *ngIf=\"isSourceConnectionConnected && isSourceCollectionSelected\"\r\n                  (click)=\"openTab('dataPreviewer')\" class=\"changelog white\">\r\n                  <svg class=\"icon_update\" shape-rendering=\"geometricPrecision\" width=\"15\" height=\"15\">\r\n                    <use xlink:href=\"./assets/dist/img/icons.svg#icon_viwer\"></use>\r\n                  </svg> Data Previewer\r\n                </a>\r\n              </li>\r\n              <li>\r\n                <a href=\"javascript:void(0)\" (click)=\"syncMetadata()\" class=\"changelog white\">\r\n                  <svg class=\"icon_update\" shape-rendering=\"geometricPrecision\" width=\"15\" height=\"15\">\r\n                    <use xlink:href=\"./assets/dist/img/icons.svg#icon_refresh\"></use>\r\n                  </svg> Sync Metadata\r\n                </a>\r\n              </li>\r\n              <li>\r\n                <a href=\"javascript:void(0)\" *ngIf=\"isSourceConnectionConnected && isSourceCollectionSelected\"\r\n                  (click)=\"refreshSourceMetadata()\" class=\"changelog white\">\r\n                  <svg class=\"icon_update\" shape-rendering=\"geometricPrecision\" width=\"15\" height=\"15\">\r\n                    <use xlink:href=\"./assets/dist/img/icons.svg#icon_refresh\"></use>\r\n                  </svg> Refresh Metadata\r\n                </a>\r\n              </li>\r\n            </ul>\r\n          </kendo-popup>\r\n        </span>\r\n      </div>\r\n    </div>\r\n    <div class=\"connection_message\">\r\n      <div class=\"message1\" *ngIf=\"!isSourceConnectionConnected\">\r\n        Choose source connection by clicking above icon\r\n        <svg class=\"icon_add_item\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\" fill=\"#0067C5\">\r\n          <use xlink:href=\"./assets/dist/img/icons.svg#icon_add_item\"></use>\r\n        </svg>\r\n\r\n\r\n      </div>\r\n      <div class=\"message2\" *ngIf=\"(isSourceConnectionConnected && !isSourceCollectionSelected) && sourceValueType!=='EXCEL'\">\r\n        Select collection name from above collection dropdown or Create new table.\r\n      </div>\r\n      <div class=\"message2\" *ngIf=\"(isSourceConnectionConnected && !isSourceCollectionSelected) && sourceValueType==='EXCEL'\">\r\n          \r\n          <input type='file' (change)=\"onSelectFile($event)\">\r\n      </div>\r\n    </div>\r\n    <div *ngIf=\"isSourceCollectionSelected\">\r\n      <div class=\"collection\" *ngFor=\"let obj of selectedSourceCollection\">\r\n        <div class=\"name\">\r\n          <!-- <input type=\"checkbox\" class=\"pull-left\" style=\"width: unset\" />  -->\r\n          {{obj.collectionName}}\r\n\r\n          <!-- <div class=\"pull-right\">\r\n              <input for=\"childObj\" type=\"radio\" [(ngModel)]=\"obj.toggleChildObject\" [value]=\"true\"\r\n                (change)=\"obj.toggleChildObject = !obj.toggleChildObject\" style=\" width: unset\" /><label\r\n                id=\"childObj\">Child\r\n                Object</label>\r\n            </div> -->\r\n        </div>\r\n        <div mwlDraggable [dropData]=\"metadata.name\" (dragEnd)=\"dragEnd($event)\" class=\"parameters\"\r\n          *ngFor=\"let metadata of obj.collectionMetadataResponseList\">\r\n          <div class=\"name\">\r\n            {{metadata.name}}\r\n            <small class=\"label label-success\" *ngIf=\"metadata.isNewColumn\">New</small>\r\n            <small class=\"label label-danger\" *ngIf=\"metadata.isDeletedColumn\">Delete</small>\r\n          </div>\r\n          <div class=\"type\">\r\n            {{metadata.type}}\r\n          </div>\r\n          <!-- <div mwlDraggable   (dragEnd)=\"dragEnd($event)\" *ngFor=\"let track of obj.collectionMetadataResponseList\">\r\n            {{track}}\r\n          </div> -->\r\n        </div>\r\n\r\n        <!-- {{obj.toggleChildObject}} -->\r\n        <div *ngIf=\"obj.toggleChildObject\">\r\n          <div *ngFor=\"let child of obj.navigableProperties\">\r\n            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{child}}\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-sm-6 connection\">\r\n    <div class=\"row connection_header\">\r\n      <div class=\"col-lg-11\">\r\n        <span id='destinationConnections'>\r\n          <kendo-dropdownlist [data]=\"destinationConnections\" class=\"custom_dpd\" height=\"40\" [filterable]=\"true\"\r\n            [textField]=\"'name'\" [valueField]=\"'connectionId'\" (filterChange)=\"filterDestinationConnection($event)\"\r\n            [(ngModel)]=\"destinationConnectionValue\" *ngIf=\"!isDestinationConnectionConnected\"\r\n            (selectionChange)=\"selectionChangeDestinationConnection($event)\" >\r\n          </kendo-dropdownlist>\r\n        </span>\r\n        <span id=\"selectTableCollection\">\r\n          <kendo-dropdownlist [data]=\"destinationCollection\" [filterable]=\"true\" [textField]=\"'text'\"\r\n            [valueField]=\"'value'\" (filterChange)=\"filterDestinationCollection($event)\"\r\n            [(ngModel)]=\"destinationCollectionValue\" (selectionChange)=\"selectionChangeDestinationCollection($event)\"\r\n            *ngIf=\"isDestinationConnectionConnected\" [disabled]=\"destinationConnectionValue.type==='EXCEL'\">\r\n          </kendo-dropdownlist>\r\n        </span>\r\n      </div>\r\n      <div class=\"col-lg-1 no-padding text-center\">\r\n        <span id='destinationConnectionsBtn'>\r\n          <button kendoButton class=\"k-button-default\" *ngIf=\"!isDestinationConnectionConnected\"\r\n            (click)=\"selectionChangeDestinationConnection()\">\r\n            <svg class=\"icon_ok\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\" fill=\"#0067C5\">\r\n              <use xlink:href=\"./assets/dist/img/icons.svg#icon_ok\"></use>\r\n            </svg>\r\n          </button>\r\n        </span>\r\n        <span id=\"createTableBtn\">\r\n          <button #destiConnection kendoButton (click)=\"onToggleDestinationConnection(isDestinationConnectionOption)\"\r\n            class=\"k-button-default option\" *ngIf=\"isDestinationConnectionConnected\">\r\n            <svg class=\"icon_connection\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n              <use xlink:href=\"./assets/dist/img/icons.svg#icon_vertical_dots\"></use>\r\n            </svg>\r\n          </button>\r\n        </span>\r\n        <span id=\"createTableOption\">\r\n          <kendo-popup [anchor]=\"destiConnection\" *ngIf=\"isDestinationConnectionOption\"\r\n            class=\"destination-connection-option\" style=\"left: 0px !important\">\r\n            <ul class=\"nav nav-list\">\r\n              <li>\r\n                <a href=\"javascript:void(0)\" (click)=\"changeDestinationConnection()\" class=\"changelog white\">\r\n                  <svg class=\"icon_change_log\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n                    <use xlink:href=\"./assets/dist/img/icons.svg#icon_change_log\"></use>\r\n                  </svg> Change Connection\r\n                </a>\r\n              </li>\r\n\r\n              <li *ngIf = \"destinationConnectionValue.type !== 'API' && (destinationValueType!=='EXCEL') && (destinationValueType!=='DataHug')\">\r\n                <a href=\"javascript:void(0)\" [attr.disabled]=\"isDestinationCollectionSelected ? true: null\"\r\n                  (click)=\"copyMetadata();onToggleDestinationConnection(isDestinationConnectionOption)\"\r\n                  class=\"changelog white\">\r\n                  <svg class=\"icon_change_log\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n                    <use xlink:href=\"./assets/dist/img/icons.svg#icon_table\"></use>\r\n                  </svg> Create Table\r\n                </a>\r\n              </li>\r\n\r\n              <li *ngIf = \"destinationValueType!=='EXCEL' && (destinationValueType!=='DataHug')\"> \r\n                <a href=\"javascript:void(0)\"\r\n                  (click)=\"autoLinking();onToggleDestinationConnection(isDestinationConnectionOption)\"\r\n                  class=\"changelog white\">\r\n                  <svg class=\"icon_change_log\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n                    <use xlink:href=\"./assets/dist/img/icons.svg#icon_linking\"></use>\r\n                  </svg> Auto Link\r\n                </a>\r\n              </li>\r\n              <li *ngIf = \"destinationValueType!=='EXCEL' && (destinationValueType!=='DataHug')\">\r\n                <a href=\"javascript:void(0)\" (click)=\"openTab('dataPreviewer')\" class=\"changelog white\">\r\n                  <svg class=\"icon_update\" shape-rendering=\"geometricPrecision\" width=\"15\" height=\"15\">\r\n                    <use xlink:href=\"./assets/dist/img/icons.svg#icon_viwer\"></use>\r\n                  </svg> Data Previewer\r\n                </a>\r\n              </li>\r\n              <li *ngIf = \"destinationValueType!=='EXCEL' && (destinationValueType!=='DataHug')\">\r\n                <a href=\"javascript:void(0)\" (click)=\"refreshMetadata()\" (click)=\"refreshDestinationMetadata()\"\r\n                  class=\"changelog white\">\r\n                  <svg class=\"icon_update\" shape-rendering=\"geometricPrecision\" width=\"15\" height=\"15\">\r\n                    <use xlink:href=\"./assets/dist/img/icons.svg#icon_refresh\"></use>\r\n                  </svg> Refresh Metadata\r\n                </a>\r\n              </li>\r\n            </ul>\r\n          </kendo-popup>\r\n        </span>\r\n      </div>\r\n    </div>\r\n    <div class=\"connection_message\">\r\n      <div class=\"message1\" *ngIf=\"!isDestinationConnectionConnected\">\r\n        Choose destination connection by clicking above icon\r\n        <svg class=\"icon_add_item\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\" fill=\"#0067C5\">\r\n          <use xlink:href=\"./assets/dist/img/icons.svg#icon_add_item\"></use>\r\n        </svg>\r\n      </div>\r\n      <div class=\"message2\" *ngIf=\"(isDestinationConnectionConnected && !isDestinationCollectionSelected) &&  (destinationValueType!=='EXCEL') && (destinationValueType!=='DataHug')\">\r\n        Select collection name from above collection dropdown or Create new table.\r\n      </div>\r\n    </div>\r\n    <span id=\"OkBtn\">\r\n      <div class=\"row\" *ngIf=\"isCreateTableRequested\">\r\n        <div class=\"col-md-12\">\r\n\r\n          <div class=\"box box-warning box-solid\"\r\n            style=\"margin-bottom: 0;margin-top: 3px;box-shadow: 0px 0px 5px 0px #666;\">\r\n            <div class=\"box-header with-border\" style=\"padding: 4px 10px\">\r\n              <h3 class=\"box-title\" style=\"font-size: 12px\">Alert</h3>\r\n              <!-- /.box-tools -->\r\n            </div>\r\n            <!-- /.box-header -->\r\n\r\n            <div class=\"box-body\" style=\"margin: 0;font-size: 12px;\">\r\n\r\n              <p>Are you sure want to create new table with name of {{selectedDestinationCollection[0].collectionName}}\r\n                <span class=\"label label-warning\"\r\n                  style=\"margin-left: 5px\">({{selectedDestinationCollection[0].collectionName.length}}) </span>\r\n              </p>\r\n\r\n              <div class=\"box-tools pull-right\">\r\n                <button type=\"button\" class=\"btn btn-default btn-box-tool\" data-widget=\"collapse\"\r\n                  [disabled]=\"selectedDestinationCollection[0].collectionName.length > 30\" (click)=\"createTable()\">Ok\r\n                </button>&nbsp;\r\n                <button type=\"button\" class=\"btn btn-default btn-box-tool\" (click)=\"clearCopyTable()\"\r\n                  data-widget=\"collapse\">Cancel\r\n                </button>\r\n              </div>\r\n\r\n            </div>\r\n\r\n            <!-- /.box-body -->\r\n          </div>\r\n\r\n          <!-- /.box -->\r\n        </div>\r\n      </div>\r\n    </span>\r\n\r\n    <div *ngIf=\"isDestinationConnectionConnected && isDestinationCollectionSelected\">\r\n      <div class=\"collection\" *ngFor=\"let obj of selectedDestinationCollection\">\r\n        <div class=\"name\" [ngClass]=\"{'error' : obj.collectionName.length >30 ? obj.isNameError : ''}\">\r\n          <span *ngIf=\"!obj.toggleName && !obj.isNameError\">{{obj.collectionName}}</span>\r\n          <input kendoInput type=\"text\" *ngIf=\"obj.toggleName || obj.isNameError\" placeholder=\"Collection Name\"\r\n            class=\"column_name\" [(ngModel)]=\"obj.collectionName\"   (input)=\"tableName(obj)\" />\r\n          <button kendoButton class=\"k-button-default option\" *ngIf=\"(destinationConnectionValue.type !== 'API')  &&  (!obj.toggleName || obj.isNameError)\"\r\n            (click)=\"obj.toggleName = !obj.toggleName\">\r\n            <svg class=\"icon_edit pull-right\" shape-rendering=\"geometricPrecision\" width=\"15\" height=\"15\">\r\n              <use xlink:href=\"./assets/dist/img/icons.svg#icon_edit\"></use>\r\n            </svg>\r\n          </button>\r\n          <button kendoButton class=\"k-button-default option okBtn\" (click)=\"obj.toggleName = !obj.toggleName\"\r\n            *ngIf=\"obj.toggleName || obj.isNameError\">\r\n            <svg class=\"icon_ok pull-right\" shape-rendering=\"geometricPrecision\" width=\"15\" height=\"15\">\r\n              <use xlink:href=\"./assets/dist/img/icons.svg#icon_ok\"></use>\r\n            </svg>\r\n          </button>\r\n          <span class=\"label label-warning\" style=\"margin-left: 5px\">({{obj.collectionName.length}}) </span>\r\n        </div>\r\n        <div *ngIf=\"!isCreatedTableRequest\">\r\n          <!-- [ngClass]=\"{'error' : metadata.name.length > 30, 'error' : metadata.isNameError === true}\" -->\r\n          <div [ngClass]=\"{'error' : metadata.name.length > 30, 'error' : metadata.isNameError === true}\"\r\n            class=\"columns\" mwlDroppable (drop)=\"onDrop($event, i)\"\r\n            *ngFor=\"let metadata of obj.collectionMetadataResponseList;let i = index\">\r\n            <div class=\"name\">\r\n              <span *ngIf=\"!metadata.isNameError && !metadata.toggleName\">{{metadata.name}}</span>\r\n              <small class=\"label label-success\" *ngIf=\"metadata.isNewColumn\">New</small>\r\n              <input *ngIf=\"metadata.isNameError || metadata.toggleName\" kendoInput type=\"text\"\r\n                placeholder=\"COLUMN NAME\" class=\"column_name\" [(ngModel)]=\"metadata.name\"\r\n                (input)=\"checkColumnName(metadata)\" />\r\n              <button kendoButton class=\"k-button-default option\" *ngIf=\"(destinationConnectionValue.type !== 'API') && (destinationConnectionValue.type !== 'ODATA') && (!metadata.toggleName || metadata.isNameError)\"\r\n                (click)=\"metadata.toggleName = !metadata.toggleName\">\r\n                <svg class=\"icon_edit pull-right\" shape-rendering=\"geometricPrecision\" width=\"15\" height=\"15\">\r\n                  <use xlink:href=\"./assets/dist/img/icons.svg#icon_edit\"></use>\r\n                </svg>\r\n              </button>\r\n              <!-- <button kendoButton kendoTooltip class=\"k-button-default option\"\r\n                  *ngIf=\"!metadata.toggleName && metadata.isNameError\">\r\n                  <svg class=\"icon_edit pull-right\" shape-rendering=\"geometricPrecision\" width=\"15\" height=\"15\"\r\n                    [attr.title]=\"metadata.name + ' should not exceed the 30 character length'\">\r\n                    <use xlink:href=\"./assets/dist/img/icons.svg#icon_error\"></use>\r\n                  </svg>\r\n                </button> -->\r\n              <button kendoButton class=\"k-button-default option okBtn\"\r\n                (click)=\"metadata.toggleName = !metadata.toggleName\"\r\n                *ngIf=\"metadata.toggleName || metadata.isNameError\">\r\n                <svg class=\"icon_ok pull-right\" shape-rendering=\"geometricPrecision\" width=\"15\" height=\"15\">\r\n                  <use xlink:href=\"./assets/dist/img/icons.svg#icon_ok\"></use>\r\n                </svg>\r\n              </button>\r\n              <span class=\"label label-warning\" style=\"margin-left: 5px\">({{metadata.name.length}})</span>\r\n            </div>\r\n            <div class=\"type\">\r\n              <span *ngIf=\"!metadata.toggleType\">{{metadata.type}}</span>\r\n              <kendo-dropdownlist [data]=\"typeCollection\" [filterable]=\"true\" [textField]=\"'text'\"\r\n                [valueField]=\"'value'\" (filterChange)=\"handleFilter($event)\" class=\"column_type\"\r\n                *ngIf=\"metadata.toggleType\">\r\n              </kendo-dropdownlist>\r\n            </div>\r\n            <div class=\"mapping\">\r\n              <input [disabled]=\"metadata.updatable==='false'\" kendoInput type=\"text\" class=\"job_name\" (input)=\"changeMetaMapping(metadata.mapping, i)\" [ngModel]=\"metadata.mapping\">\r\n              <small class=\"label label-success\" *ngIf=\"metadata.updatable==='false'\">Non Editable </small>\r\n            \r\n            </div>\r\n\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n<kendo-dialog title=\"\" *ngIf=\"opened\" (close)=\"close('cancel')\" [width]=\"1300\" [height]=\"height\">\r\n  <kendo-dialog-titlebar>\r\n    <div style=\"font-size: 18px; line-height: 1.3em;\">\r\n      <span class=\"k-icon k-i-warning\"></span> Delete Data\r\n    </div>\r\n  </kendo-dialog-titlebar>\r\n  <kendo-tabstrip (tabSelect)=\"onTabSelect($event)\">\r\n    <kendo-tabstrip-tab [title]=\"'Net Change'\" [selected]=\"selectedModelTabName === 'netChange'\">\r\n      <ng-template kendoTabContent>\r\n        <!-- <input type=\"radio\" name=\"allProcess\" id=\"allProcess\" class=\"k-radio\" (change)=\"isIncrementParameter = false\"\r\n            [(ngModel)]=\"incrementValue\" value=\"singleProcess\"> -->\r\n        <div class=\"k-form-field\">\r\n          <input type=\"radio\" name=\"allProcess\" id=\"allProcess\" class=\"k-radio\" [(ngModel)]=\"incrementValue\"\r\n            value=\"singleProcess\" (change)=\"handleChange($event)\">\r\n          <label class=\"k-radio-label\" for=\"allProcess\">Process all records on each run.</label>\r\n          <div class=\"top-Margin\"></div>\r\n          <span id=\"radioBtn\">\r\n            <input type=\"radio\" name=\"allProcess\" id=\"incrementProcess\" class=\"k-radio\" (change)=\"handleChange($event)\"\r\n              [(ngModel)]=\"incrementValue\" value=\"allProcess\">\r\n          </span>\r\n          <label class=\"k-radio-label\" for=\"incrementProcess\">Process only records created or updated since the\r\n            last\r\n            run, based on the value of the specified field.</label>\r\n        </div>\r\n        <div class=\"top-Margin\"></div>\r\n        <span id=\"selectNetChaneValue\">\r\n          <kendo-dropdownlist [data]=\"incrementalColumns\" style=\"width: 100%;\"\r\n            [(ngModel)]=\"incrementalColumnSelectedValue\" *ngIf=\"isIncrementParameter\">\r\n          </kendo-dropdownlist>\r\n        </span>\r\n        <div class=\"top-Margin\"></div>\r\n        <div *ngIf=\"isIncrementParameter\">\r\n          <p>Most Recent Record Processed:<strong (click)='incrementColumnDateTimeValue = false'>\r\n              Never</strong>&nbsp;<a href=\"javascript:void(0)\" (click)='incrementColumnDateTimeValue = true'\r\n              *ngIf=\"incrementalColumnSelectedValue\">DateTime</a>\r\n        </div>\r\n        <span id=\"selectIncrementDate\">\r\n          <kendo-datepicker [(value)]=\"incrementColumnValue\" *ngIf=\"incrementalColumnSelectedValue\">\r\n          </kendo-datepicker>\r\n        </span>\r\n        <span id=\"selectIncrementTime\">\r\n          <kendo-timepicker [(value)]=\"incrementColumnValue\" *ngIf=\"incrementalColumnSelectedValue\">\r\n          </kendo-timepicker>\r\n        </span>\r\n        <div class=\"top-Margin\"></div>\r\n        <p *ngIf=\"isIncrementParameter\"><strong>Note</strong>: Net Change field and datetime filters are appended\r\n          to the\r\n          list\r\n          of\r\n          filters configured on the Filter tab as an\r\n          AND.</p>\r\n      </ng-template>\r\n    </kendo-tabstrip-tab>\r\n    <kendo-tabstrip-tab [title]=\"'Filter'\" [selected]=\"selectedModelTabName === 'filter'\">\r\n      <ng-template kendoTabContent>\r\n        <table class=\"table table-striped d_filter\" *ngIf=\"sourceCollectionValue.subcategory!=='procedure'\">\r\n          <tr>\r\n            <th></th>\r\n            <th>And/Or</th>\r\n            <th>Field</th>\r\n            <th>Data Type</th>\r\n            <th>Operator</th>\r\n            <th>Value</th>\r\n\r\n          </tr>\r\n          <tr *ngFor=\"let filter of dataFilters; let i = index\">\r\n            <td class=\"control\" width=\"100px\">\r\n              <button kendoButton>\r\n                <svg class=\"icon_add_item\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\" fill=\"#0067C5\"\r\n                  (click)=\"addFilter()\">\r\n                  <use xlink:href=\"./assets/dist/img/icons.svg#icon_add_item\"></use>\r\n                </svg>\r\n              </button>\r\n              <button kendoButton *ngIf=\"i !== 0\">\r\n                <svg class=\"icon_add_item\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\"\r\n                  style=\"fill: #C50000\" (click)=\"removeFilter(i)\">\r\n                  <use xlink:href=\"./assets/dist/img/icons.svg#icon_delete\"></use>\r\n                </svg>\r\n              </button>\r\n            </td>\r\n\r\n            <td class=\"input\" width=\"100px\">\r\n              <kendo-dropdownlist [data]=\"logicalOperatorCollections\" class=\"custom_dpd\" height=\"40\"\r\n                [defaultItem]=\"{ text: 'Select Operator', value: 'null' }\" [textField]=\"'text'\" [valueField]=\"'text'\"\r\n                [valuePrimitive]=\"true\" [(ngModel)]=\"filter.logicalOperator\" [disabled]=\"i == 0\">\r\n              </kendo-dropdownlist>\r\n\r\n            </td>\r\n\r\n            <td class=\"input\" width=\"100px\">\r\n              <span id=\"filterField\">\r\n                <kendo-dropdownlist [data]=\"sourceFilterableColumns\" [textField]=\"'name'\" [valueField]=\"'name'\"\r\n                  (selectionChange)=\"selectionChangeDataType($event, filter)\" [(ngModel)]=\"filter.field\"\r\n                  [valuePrimitive]=\"true\">\r\n                </kendo-dropdownlist>\r\n              </span>\r\n            </td>\r\n            <td class=\"input\" width=\"100px\">\r\n              <input kendoInput placeholder=\"Data Type\" [(ngModel)]=\"filter.dataType\" />\r\n            </td>\r\n            <td class=\"input\" id=\"selectOperator\" width=\"100px\">\r\n              <kendo-dropdownlist [data]=\"comparisonOperatorCollections\" class=\"custom_dpd\" height=\"40\"\r\n                [filterable]=\"true\" [textField]=\"'value'\" [valueField]=\"'text'\" [valuePrimitive]=\"true\"\r\n                [(ngModel)]=\"filter.comparisonOperator\">\r\n              </kendo-dropdownlist>\r\n            </td>\r\n            <td class=\"input\" width=\"100px\">\r\n              <input kendoInput placeholder=\"Value\" [(ngModel)]=\"filter.value\" />\r\n            </td>\r\n          </tr>\r\n        </table>\r\n        <table class=\"table table-striped d_filter\" *ngIf=\"sourceCollectionValue.subcategory==='procedure'\">\r\n          <tr>\r\n\r\n            <th>And/Or</th>\r\n            <th>Field</th>\r\n            <th>Data Type</th>\r\n            <th>Operator</th>\r\n            <th>Value</th>\r\n\r\n          </tr>\r\n          <tr *ngFor=\"let filter of dataFilters; let i = index\">\r\n\r\n\r\n            <td class=\"input\" width=\"100px\">\r\n              <kendo-dropdownlist [data]=\"logicalOperatorCollections\" class=\"custom_dpd\" height=\"40\"\r\n                [defaultItem]=\"{ text: 'Select Operator', value: 'null' }\" [textField]=\"'text'\" [valueField]=\"'text'\"\r\n                [valuePrimitive]=\"true\" [(ngModel)]=\"filter.logicalOperator\" [disabled]=\"i == 0\">\r\n              </kendo-dropdownlist>\r\n\r\n            </td>\r\n\r\n            <td class=\"input\" width=\"100px\">\r\n              <span id=\"filterField\">\r\n                <kendo-dropdownlist [data]=\"sourceFilterableColumns\" [textField]=\"'name'\" [valueField]=\"'name'\"\r\n                  (selectionChange)=\"selectionChangeDataType($event, filter)\" [(ngModel)]=\"filter.field\"\r\n                  [valuePrimitive]=\"true\" disabled>\r\n                </kendo-dropdownlist>\r\n              </span>\r\n            </td>\r\n            <td class=\"input\" width=\"100px\">\r\n              <input kendoInput placeholder=\"Data Type\" [(ngModel)]=\"filter.dataType\" disabled />\r\n            </td>\r\n            <td class=\"input\" id=\"selectOperator\" width=\"100px\">\r\n              <kendo-dropdownlist [data]=\"comparisonOperatorCollections\" class=\"custom_dpd\" height=\"40\"\r\n                [filterable]=\"true\" [textField]=\"'value'\" [valueField]=\"'text'\" [valuePrimitive]=\"true\"\r\n                [(ngModel)]=\"filter.comparisonOperator\">\r\n              </kendo-dropdownlist>\r\n            </td>\r\n            <td class=\"input\" width=\"100px\">\r\n              <input kendoInput placeholder=\"Value\" [(ngModel)]=\"filter.value\" />\r\n            </td>\r\n          </tr>\r\n        </table>\r\n\r\n\r\n        <!-- <span  id = \"Validate Filter\" > -->\r\n        <button kendoButton id=\"validate\" (click)=\"validateFilter()\" [primary]=\"true\">Validate Filter</button>\r\n        <!-- </span> -->\r\n      </ng-template>\r\n    </kendo-tabstrip-tab>\r\n    <kendo-tabstrip-tab [title]=\"'Data Previewer'\" [selected]=\"selectedModelTabName === 'dataPreviewer'\">\r\n      <ng-template kendoTabContent>\r\n        <table class=\"table table-striped d_filter\" style=\"margin:0px\">\r\n          <tr>\r\n            <th></th>\r\n            <th>And/Or</th>\r\n            <th>Field</th>\r\n            <th>Data Type</th>\r\n            <th>Operator</th>\r\n            <th>Value</th>\r\n          </tr>\r\n          <tr *ngFor=\"let filter of dataPreviewerFilters; let i = index\">\r\n            <td class=\"control\" width=\"100px\">\r\n              <button kendoButton>\r\n                <svg class=\"icon_add_item\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\" fill=\"#0067C5\"\r\n                  (click)=\"addDataPreviewerFilters()\">\r\n                  <use xlink:href=\"./assets/dist/img/icons.svg#icon_add_item\"></use>\r\n                </svg>\r\n              </button>\r\n              <button kendoButton *ngIf=\"i !== 0\">\r\n                <svg class=\"icon_add_item\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\"\r\n                  style=\"fill: #C50000\" (click)=\"removeDataPreviewerFilters(i)\">\r\n                  <use xlink:href=\"./assets/dist/img/icons.svg#icon_delete\"></use>\r\n                </svg>\r\n              </button>\r\n            </td>\r\n            <td class=\"input\" width=\"100px\">\r\n              <kendo-dropdownlist [data]=\"logicalOperatorCollections\" class=\"custom_dpd\" height=\"40\" [filterable]=\"true\"\r\n                [textField]=\"'text'\" [valueField]=\"'text'\" [valuePrimitive]=\"true\" [(ngModel)]=\"filter.logicalOperator\">\r\n              </kendo-dropdownlist>\r\n            </td>\r\n            <td class=\"input\" width=\"100px\">\r\n              <span id=\"filterField\">\r\n                <kendo-dropdownlist [data]=\"sourceFilterableColumns\" [textField]=\"'name'\" [valueField]=\"'name'\"\r\n                  (selectionChange)=\"selectionChangeDataType($event, filter)\" [(ngModel)]=\"filter.field\"\r\n                  [valuePrimitive]=\"true\">\r\n                </kendo-dropdownlist>\r\n              </span>\r\n            </td>\r\n            <td class=\"input\" width=\"100px\">\r\n              <input kendoInput placeholder=\"Data Type\" [(ngModel)]=\"filter.dataType\" disabled />\r\n            </td>\r\n            <td class=\"input\" width=\"100px\">\r\n\r\n              <kendo-dropdownlist [data]=\"comparisonOperatorCollections\" class=\"custom_dpd\" height=\"40\"\r\n                [filterable]=\"true\" [textField]=\"'value'\" [valueField]=\"'text'\" [valuePrimitive]=\"true\"\r\n                [(ngModel)]=\"filter.comparisonOperator\">\r\n              </kendo-dropdownlist>\r\n            </td>\r\n            <td class=\"input\" width=\"100px\">\r\n              <input kendoInput id=\"writeValue\" placeholder=\"Value\" [(ngModel)]=\"filter.value\" />\r\n            </td>\r\n          </tr>\r\n        </table>\r\n        <button kendoButton type=\"button\" (click)=\"getDataForPreview()\" [primary]=\"true\">Filter</button>\r\n        <kendo-tabstrip>\r\n          <kendo-tabstrip-tab\r\n            [title]=\"sourceCollection?.length > 0 ? sourceCollectionValue?.value + ' (' +sourceConnectionValue.name + ')':''\"\r\n            [selected]=\"true\" [disabled]=\"sourceCollectionValue?.value == 0?true:false\">\r\n            <ng-template kendoTabContent>\r\n              <kendo-grid [data]=\"sourceGridDataResult\" [height]=\"430\" [pageable]=\"true\"\r\n                (pageChange)=\"sourceItemsPageChange($event)\" [sortable]=\" { allowUnsort: true}\" [pageSize]=\"state.take\"\r\n                [skip]=\"state.skip\" [sort]=\"state.sort\" [filter]=\"state.filter\" [sortable]=\"true\" [pageable]=\"{\r\n                    buttonCount: buttonCount,\r\n                    info: infoDP,\r\n                    type: typeDP,\r\n                    pageSizes: pageSizesDP,\r\n                    previousNext: previousNextDP\r\n                  }\" [filter]=\"netNewFilter\" [loading]=\"sourceGridLoading\">\r\n                <kendo-grid-column kendoTooltip position=\"right\" field=\"Name\" title=\"Name\">\r\n                </kendo-grid-column>\r\n                <kendo-grid-column kendoTooltip position=\"right\" field=\"Value\" title=\"Value\">\r\n                </kendo-grid-column>\r\n              </kendo-grid>\r\n            </ng-template>\r\n          </kendo-tabstrip-tab>\r\n          <kendo-tabstrip-tab\r\n            [title]=\"destinationCollection?.length > 0 ? destinationCollectionValue?.value + ' (' + destinationConnectionValue?.name + ')':''\"\r\n            [disabled]=\"destinationCollectionValue?.value == 0?true:false\">\r\n            <ng-template kendoTabContent>\r\n              <kendo-grid [data]=\"destinationGridDataResult\" [height]=\"430\" [pageable]=\"true\" [resizable]=\"true\"\r\n                (pageChange)=\"destinationItemsPageChange($event)\" [sortable]=\" { allowUnsort: true}\"\r\n                [pageSize]=\"state.take\" [skip]=\"state.skip\" [sort]=\"state.sort\" [filter]=\"state.filter\"\r\n                [sortable]=\"true\" [pageable]=\"{\r\n                    buttonCount: buttonCount,\r\n                    info: infoDP,\r\n                    type: typeDP,\r\n                    pageSizes: pageSizesDP,\r\n                    previousNext: previousNextDP\r\n                  }\" [filter]=\"netNewFilter\" [loading]=\"destinationGridLoading\">\r\n                <kendo-grid-column kendoTooltip position=\"right\" field=\"Name\" title=\"Name\">\r\n                </kendo-grid-column>\r\n                <kendo-grid-column kendoTooltip position=\"right\" field=\"Value\" title=\"Value\">\r\n                </kendo-grid-column>\r\n              </kendo-grid>\r\n            </ng-template>\r\n          </kendo-tabstrip-tab>\r\n        </kendo-tabstrip>\r\n      </ng-template>\r\n    </kendo-tabstrip-tab>\r\n    <kendo-tabstrip-tab [title]=\"'View Log'\" [selected]=\"selectedModelTabName === 'logView'\">\r\n      <ng-template kendoTabContent>\r\n        <app-log [job]=\"jobRequest\"></app-log>\r\n      </ng-template>\r\n    </kendo-tabstrip-tab>\r\n  </kendo-tabstrip>\r\n  <kendo-dialog-actions layout=\"normal\">\r\n    <!-- <button kendoButton (click)=\"validateFilter()\" [primary]=\"true\">Validate Filter</button> -->\r\n\r\n    <button kendoButton (click)=\"saveTabOptions()\" id=\"OkBtn\" [primary]=\"true\">Ok</button>\r\n\r\n  </kendo-dialog-actions>\r\n</kendo-dialog>\r\n<!-- <div mwlDraggable (dragEnd)=\"dragEnd($event)\" *ngFor=\"let track of metadatName\">\r\n  {{track}}\r\n</div>\r\n<div mwlDroppable (drop)=\"onDrop($event, i)\" *ngFor=\"let track of metadataName; let i = index\">\r\n  <div style=\"width: 100%\">{{track}}</div>\r\n  <input type=\"text\" [(ngModel)]=\"track\" />\r\n</div> -->"

/***/ }),

/***/ "./src/app/jobs/new-job/new-job.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/jobs/new-job/new-job.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".content-wrapper {\n  padding-top: 0px !important; }\n\n#job_header {\n  margin: 10px 15px;\n  background: #fff;\n  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);\n  border-radius: 0.25rem;\n  padding: 5px 15px 4px 15px; }\n\n#job_header kendo-popup {\n    position: absolute;\n    width: 185px;\n    top: 39px;\n    left: 15px; }\n\n#job_header kendo-popup.scheduler-popup {\n    right: 5px;\n    width: 550px !important;\n    left: unset !important;\n    top: 46px !important;\n    position: absolute;\n    padding: 10px;\n    background: #fff;\n    box-shadow: 0px 3px 3px 0px #ccc;\n    right: 20%; }\n\n#job_header kendo-popup.scheduler-popup hr {\n      margin: 0px; }\n\n#job_header kendo-popup.scheduler-popup p {\n      margin: 0px;\n      padding: 10px 10px 0px 10px; }\n\n#job_header .job_name {\n    border: 0px;\n    border: solid 1px #e7e7e7;\n    font-size: 20px;\n    height: 40px;\n    outline: none;\n    padding: 0px 10px;\n    text-transform: capitalize;\n    width: 100%; }\n\n#job_header .job_name :visited {\n      background-color: #ffffff; }\n\n#job_header .job_name:hover {\n    background-color: #ffffff; }\n\n#job_header .job_name:hover ::-webkit-input-placeholder {\n      color: white; }\n\n#job_header .job_name::-webkit-input-placeholder {\n    font-size: 20px; }\n\n#job_header .job_name::-moz-placeholder {\n    font-size: 20px; }\n\n#job_header .job_name::-ms-input-placeholder {\n    font-size: 20px; }\n\n#job_header .job_name::placeholder {\n    font-size: 20px; }\n\n#job_header button {\n    margin: 5px 8px; }\n\n#job_header h3,\n  #job_header p {\n    margin: 0px 0px 0px -7px; }\n\n#job_header .content {\n    padding: 30px;\n    color: #787878;\n    background-color: #fcf7f8;\n    border: 1px solid rgba(0, 0, 0, 0.05); }\n\n#job_content {\n  margin: 10px 15px;\n  position: relative;\n  display: flex;\n  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);\n  border-radius: 0.25rem;\n  background: white; }\n\n#job_content .connection {\n    height: 80vh;\n    padding: 0px 10px; }\n\n#job_content .connection .connection_header {\n      padding: 0px 0px 0px 5px; }\n\n#job_content .connection .connection_header .col-lg-11 {\n        padding-left: 5px;\n        padding-right: 0px; }\n\n#job_content .connection .connection_header kendo-dropdownlist {\n        margin: 8px 5px;\n        width: 100%;\n        background: transparent; }\n\n#job_content .connection .connection_header button {\n        margin: 8px 0px 0px 0px;\n        background: transparent;\n        padding: 4px;\n        border-width: 1px; }\n\n#job_content .connection .connection_header button:hover {\n        background-color: #f1f1ff; }\n\n#job_content .connection .connection_message {\n      height: auto; }\n\n#job_content .connection .connection_message div.message1,\n      #job_content .connection .connection_message div.message2 {\n        margin-top: 40%;\n        padding: 0px 35px;\n        text-align: center; }\n\n#job_content .connection .connection_message div.message1 > svg.icon_add_item {\n        margin: 0px 5px;\n        position: absolute;\n        width: 12px; }\n\n#job_content .connection kendo-popup.source-connection-option {\n      right: 5px;\n      width: 156px !important;\n      left: unset !important;\n      top: 46px !important;\n      position: absolute; }\n\n#job_content .connection kendo-popup.source-connection-option > .k-popup {\n      width: 160px; }\n\n#job_content .connection kendo-popup.destination-connection-option {\n      right: 5px;\n      width: 156px !important;\n      left: unset !important;\n      top: 46px !important; }\n\n#job_content .collection {\n    margin-top: 10px;\n    height: 70vh;\n    overflow-y: auto;\n    overflow-x: hidden; }\n\n#job_content .collection::-webkit-scrollbar-track {\n      -webkit-box-shadow: inset 0 0 6px #ffffff;\n      background-color: #ffffff; }\n\n#job_content .collection::-webkit-scrollbar {\n      width: 6px;\n      background-color: #ffffff; }\n\n#job_content .collection::-webkit-scrollbar-thumb {\n      background-color: #9ea5ab; }\n\n#job_content .collection > .name {\n      background: #f0f2f3;\n      font-size: 13px;\n      padding: 10px;\n      word-break: break-all; }\n\n#job_content .collection > .name .option {\n        border-radius: 50%;\n        float: right;\n        margin-top: -1px;\n        width: 30px; }\n\n#job_content .collection > .name > input {\n        background-color: white;\n        margin-top: 0px;\n        width: 94%; }\n\n#job_content .collection > .name > button > svg {\n        height: 18px;\n        cursor: pointer; }\n\n#job_content .collection .parameters,\n    #job_content .collection .columns {\n      margin: 5px;\n      min-height: 80px;\n      border: solid 1px #e1e1e1;\n      border-radius: 5px;\n      padding: 10px;\n      z-index: 10;\n      background: white; }\n\n#job_content .collection .parameters.parameters:hover,\n      #job_content .collection .columns.parameters:hover {\n        background-color: #f9f9f9; }\n\n#job_content .collection .parameters .edit_icon,\n      #job_content .collection .columns .edit_icon {\n        border-radius: 50%;\n        float: right;\n        margin-top: -6px;\n        width: 30px; }\n\n#job_content .collection .parameters .name,\n      #job_content .collection .columns .name {\n        font-weight: bold;\n        font-size: 12px; }\n\n#job_content .collection .parameters .name button.okBtn,\n        #job_content .collection .columns .name button.okBtn {\n          float: right;\n          margin-right: 0px;\n          margin-top: 2px; }\n\n#job_content .collection .parameters .type,\n      #job_content .collection .columns .type {\n        font-size: 11.5px;\n        margin-top: 8px;\n        text-transform: uppercase; }\n\n#job_content .collection .parameters .type button > svg,\n        #job_content .collection .columns .type button > svg {\n          height: 12px;\n          cursor: pointer; }\n\n#job_content .collection .parameters .mapping > input,\n      #job_content .collection .columns .mapping > input {\n        width: 100%;\n        font-size: 12px; }\n\n#job_content .collection .parameters .option,\n      #job_content .collection .columns .option {\n        border-radius: 50%;\n        float: unset;\n        margin-left: 5px;\n        width: 20px;\n        padding: 1px;\n        height: 20px;\n        border: 0;\n        display: contents; }\n\n#job_content .collection .parameters input,\n      #job_content .collection .parameters kendo-dropdownlist,\n      #job_content .collection .columns input,\n      #job_content .collection .columns kendo-dropdownlist {\n        width: 95%; }\n\n#job_content .collection .parameters button > svg,\n      #job_content .collection .columns button > svg {\n        height: 12px;\n        cursor: pointer;\n        fill: #666; }\n\n#job_content .collection .parameters label.title,\n      #job_content .collection .columns label.title {\n        font-size: 10px;\n        margin-bottom: 0px;\n        width: 100%;\n        text-transform: uppercase; }\n\n#job_content .collection .parameters input.column_name,\n      #job_content .collection .parameters input.column_mapping,\n      #job_content .collection .columns input.column_name,\n      #job_content .collection .columns input.column_mapping {\n        border: solid 1px #cccccc;\n        border-radius: 3px;\n        background: #ffffff;\n        font-weight: normal;\n        margin-top: 0px !important;\n        margin-bottom: 6px !important;\n        outline: none;\n        padding: 3px;\n        width: 95%; }\n\n#job_content .collection .parameters .column_type,\n      #job_content .collection .columns .column_type {\n        width: 100%;\n        margin-bottom: 6px !important; }\n\n#job_content .collection .columns:hover .option {\n      display: contents; }\n\n#job_content .collection input {\n      border: solid 1px #cccccc;\n      border-radius: 3px;\n      background: #f1f1f1;\n      margin-top: 6px;\n      outline: none;\n      padding: 3px;\n      width: 100%; }\n\n#job_content .paramerter > .type {\n    font-size: 12px;\n    margin-top: 10px; }\n\n.k-grid {\n  font-size: 12px; }\n\n.nav > li > a {\n  padding-left: 30px;\n  font-size: 12px;\n  text-align: left; }\n\n.nav > li:hover,\n.nav > li:hover a,\n.nav > li:hover a > svg {\n  background: #38414a;\n  color: white;\n  fill: white; }\n\n.nav > li > a > svg {\n  position: absolute;\n  left: 8px; }\n\n.overlay {\n  background: rgba(0, 0, 0, 0.2);\n  width: 100%;\n  height: 85vh;\n  z-index: 100000;\n  position: absolute; }\n\n.overlay > .fa {\n  position: absolute;\n  top: 50%;\n  left: 42%;\n  margin-left: -15px;\n  margin-top: -15px;\n  color: #000;\n  font-size: 30px; }\n\n.overlay > p {\n  position: absolute;\n  top: 65%;\n  left: 49%;\n  margin-left: -15px;\n  margin-top: -15px;\n  color: #000;\n  font-size: 12px; }\n\n.overlay > p.loading {\n  left: 49%; }\n\n.overlay > p.processing {\n  left: 49%; }\n\n.overlay > p.validating {\n  left: 49%; }\n\n.top-Margin {\n  margin-top: 10px;\n  margin-left: 403px; }\n\n.anchor {\n  display: block;\n  width: 120px;\n  margin: 200px auto; }\n\n.scrolling-wrapper-flexbox {\n  display: flex;\n  flex-wrap: nowrap;\n  overflow-x: auto; }\n\n.scrolling-wrapper-flexbox .card {\n    flex: 0 0 auto; }\n\ntable.d_filter {\n  font-size: 10px; }\n\ntable.d_filter .control {\n    text-align: center; }\n\ntable.d_filter .control svg {\n      margin: 0px 3px; }\n\ntable.d_filter .input kendo-dropdownlist,\n  table.d_filter .input input {\n    width: 100%; }\n\ntable.d_filter > tbody > tr > td {\n  padding: 2px;\n  line-height: 2.4em; }\n\n.error {\n  background-color: #ffbebe !important; }\n\n.padding-left-5 {\n  padding-left: 5px; }\n\n.padding-right-5 {\n  padding-right: 5px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9qb2JzL25ldy1qb2IvRTpcXFVJLVByb2plY3RcXEVUTFxcc3JjXFxzcmMvYXBwXFxqb2JzXFxuZXctam9iXFxuZXctam9iLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsMkJBQTJCLEVBQUE7O0FBRTdCO0VBQ0UsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQix3Q0FBd0M7RUFDeEMsc0JBQXNCO0VBQ3RCLDBCQUEwQixFQUFBOztBQUw1QjtJQU9JLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osU0FBUztJQUNULFVBQVUsRUFBQTs7QUFWZDtJQWFJLFVBQVU7SUFDVix1QkFBdUI7SUFDdkIsc0JBQXNCO0lBQ3RCLG9CQUFvQjtJQUNwQixrQkFBa0I7SUFDbEIsYUFBYTtJQUNiLGdCQUFnQjtJQUNoQixnQ0FBZ0M7SUFDaEMsVUFBVSxFQUFBOztBQXJCZDtNQXVCTSxXQUFXLEVBQUE7O0FBdkJqQjtNQTBCTSxXQUFXO01BQ1gsMkJBQTJCLEVBQUE7O0FBM0JqQztJQStCSSxXQUFXO0lBQ1gseUJBQXlCO0lBQ3pCLGVBQWU7SUFDZixZQUFZO0lBQ1osYUFBYTtJQUNiLGlCQUFpQjtJQUNqQiwwQkFBMEI7SUFDMUIsV0FBVyxFQUFBOztBQXRDZjtNQXdDTSx5QkFBeUIsRUFBQTs7QUF4Qy9CO0lBNkNJLHlCQUF5QixFQUFBOztBQTdDN0I7TUErQ00sWUFBWSxFQUFBOztBQS9DbEI7SUFvREksZUFBZSxFQUFBOztBQXBEbkI7SUFvREksZUFBZSxFQUFBOztBQXBEbkI7SUFvREksZUFBZSxFQUFBOztBQXBEbkI7SUFvREksZUFBZSxFQUFBOztBQXBEbkI7SUF3REksZUFBZSxFQUFBOztBQXhEbkI7O0lBNkRJLHdCQUF3QixFQUFBOztBQTdENUI7SUFnRUksYUFBYTtJQUNiLGNBQWM7SUFDZCx5QkFBeUI7SUFDekIscUNBQXFDLEVBQUE7O0FBR3pDO0VBQ0UsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2Isd0NBQXdDO0VBQ3hDLHNCQUFzQjtFQUN0QixpQkFBaUIsRUFBQTs7QUFObkI7SUFTSSxZQUFZO0lBQ1osaUJBQWlCLEVBQUE7O0FBVnJCO01BWU0sd0JBQXdCLEVBQUE7O0FBWjlCO1FBZ0JRLGlCQUFpQjtRQUNqQixrQkFBa0IsRUFBQTs7QUFqQjFCO1FBb0JRLGVBQWU7UUFDZixXQUFXO1FBQ1gsdUJBQXVCLEVBQUE7O0FBdEIvQjtRQXlCUSx1QkFBdUI7UUFDdkIsdUJBQXVCO1FBQ3ZCLFlBQVk7UUFDWixpQkFBaUIsRUFBQTs7QUE1QnpCO1FBK0JRLHlCQUF5QixFQUFBOztBQS9CakM7TUFtQ00sWUFBWSxFQUFBOztBQW5DbEI7O1FBc0NRLGVBQWU7UUFDZixpQkFBaUI7UUFDakIsa0JBQWtCLEVBQUE7O0FBeEMxQjtRQTJDUSxlQUFlO1FBQ2Ysa0JBQWtCO1FBQ2xCLFdBQVcsRUFBQTs7QUE3Q25CO01BaURNLFVBQVU7TUFDVix1QkFBdUI7TUFDdkIsc0JBQXNCO01BQ3RCLG9CQUFvQjtNQUNwQixrQkFBa0IsRUFBQTs7QUFyRHhCO01Bd0RNLFlBQVksRUFBQTs7QUF4RGxCO01BMkRNLFVBQVU7TUFDVix1QkFBdUI7TUFDdkIsc0JBQXNCO01BQ3RCLG9CQUFvQixFQUFBOztBQTlEMUI7SUFrRUksZ0JBQWdCO0lBQ2hCLFlBQVk7SUFFWixnQkFBZ0I7SUFDaEIsa0JBQWtCLEVBQUE7O0FBdEV0QjtNQXdFTSx5Q0FBeUM7TUFDekMseUJBQXlCLEVBQUE7O0FBekUvQjtNQTZFTSxVQUFVO01BQ1YseUJBQXlCLEVBQUE7O0FBOUUvQjtNQWtGTSx5QkFBb0MsRUFBQTs7QUFsRjFDO01BcUZNLG1CQUFtQjtNQUNuQixlQUFlO01BQ2YsYUFBYTtNQUNiLHFCQUFxQixFQUFBOztBQXhGM0I7UUEwRlEsa0JBQWtCO1FBQ2xCLFlBQVk7UUFDWixnQkFBZ0I7UUFDaEIsV0FBVyxFQUFBOztBQTdGbkI7UUFnR1EsdUJBQXVCO1FBQ3ZCLGVBQWU7UUFDZixVQUFVLEVBQUE7O0FBbEdsQjtRQXFHUSxZQUFZO1FBQ1osZUFBZSxFQUFBOztBQXRHdkI7O01BMkdNLFdBQVc7TUFDWCxnQkFBZ0I7TUFDaEIseUJBQXlCO01BQ3pCLGtCQUFrQjtNQUNsQixhQUFhO01BQ2IsV0FBVztNQUNYLGlCQUFpQixFQUFBOztBQWpIdkI7O1FBbUhRLHlCQUF5QixFQUFBOztBQW5IakM7O1FBdUhRLGtCQUFrQjtRQUNsQixZQUFZO1FBQ1osZ0JBQWdCO1FBQ2hCLFdBQVcsRUFBQTs7QUExSG5COztRQTZIUSxpQkFBaUI7UUFDakIsZUFBZSxFQUFBOztBQTlIdkI7O1VBaUlVLFlBQVk7VUFDWixpQkFBaUI7VUFDakIsZUFBZSxFQUFBOztBQW5JekI7O1FBdUlRLGlCQUFpQjtRQUNqQixlQUFlO1FBQ2YseUJBQXlCLEVBQUE7O0FBeklqQzs7VUE0SVUsWUFBWTtVQUNaLGVBQWUsRUFBQTs7QUE3SXpCOztRQWtKVSxXQUFXO1FBQ1gsZUFBZSxFQUFBOztBQW5KekI7O1FBdUpRLGtCQUFrQjtRQUNsQixZQUFZO1FBQ1osZ0JBQWdCO1FBQ2hCLFdBQVc7UUFDWCxZQUFZO1FBQ1osWUFBWTtRQUNaLFNBQVM7UUFDVCxpQkFBaUIsRUFBQTs7QUE5SnpCOzs7O1FBa0tRLFVBQVUsRUFBQTs7QUFsS2xCOztRQXFLUSxZQUFZO1FBQ1osZUFBZTtRQUNmLFVBQVUsRUFBQTs7QUF2S2xCOztRQTBLUSxlQUFlO1FBQ2Ysa0JBQWtCO1FBQ2xCLFdBQVc7UUFDWCx5QkFBeUIsRUFBQTs7QUE3S2pDOzs7O1FBaUxRLHlCQUF5QjtRQUN6QixrQkFBa0I7UUFDbEIsbUJBQW1CO1FBQ25CLG1CQUFtQjtRQUNuQiwwQkFBMEI7UUFDMUIsNkJBQTZCO1FBQzdCLGFBQWE7UUFDYixZQUFZO1FBQ1osVUFBVSxFQUFBOztBQXpMbEI7O1FBNExRLFdBQVc7UUFDWCw2QkFBNkIsRUFBQTs7QUE3THJDO01BaU1NLGlCQUFpQixFQUFBOztBQWpNdkI7TUFvTU0seUJBQXlCO01BQ3pCLGtCQUFrQjtNQUNsQixtQkFBbUI7TUFDbkIsZUFBZTtNQUNmLGFBQWE7TUFDYixZQUFZO01BQ1osV0FBVyxFQUFBOztBQTFNakI7SUE4TUksZUFBZTtJQUNmLGdCQUFnQixFQUFBOztBQUdwQjtFQUNFLGVBQWUsRUFBQTs7QUFFakI7RUFDRSxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGdCQUFnQixFQUFBOztBQUVsQjs7O0VBR0UsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixXQUFXLEVBQUE7O0FBRWI7RUFDRSxrQkFBa0I7RUFDbEIsU0FBUyxFQUFBOztBQUVYO0VBQ0UsOEJBQThCO0VBQzlCLFdBQVc7RUFDWCxZQUFZO0VBQ1osZUFBZTtFQUNmLGtCQUFrQixFQUFBOztBQUVwQjtFQUNFLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsU0FBUztFQUNULGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsV0FBVztFQUNYLGVBQWUsRUFBQTs7QUFFakI7RUFDRSxrQkFBa0I7RUFDbEIsUUFBUTtFQUNSLFNBQVM7RUFDVCxrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLFdBQVc7RUFDWCxlQUFlLEVBQUE7O0FBRWpCO0VBQ0UsU0FBUyxFQUFBOztBQUdYO0VBQ0UsU0FBUyxFQUFBOztBQUdYO0VBQ0UsU0FBUyxFQUFBOztBQUVYO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQixFQUFBOztBQUVwQjtFQUNFLGNBQWM7RUFDZCxZQUFZO0VBQ1osa0JBQWtCLEVBQUE7O0FBRXBCO0VBQ0UsYUFBYTtFQUNiLGlCQUFpQjtFQUNqQixnQkFBZ0IsRUFBQTs7QUFIbEI7SUFNSSxjQUFjLEVBQUE7O0FBR2xCO0VBQ0UsZUFBZSxFQUFBOztBQURqQjtJQUdJLGtCQUFrQixFQUFBOztBQUh0QjtNQUtNLGVBQWUsRUFBQTs7QUFMckI7O0lBV00sV0FBVyxFQUFBOztBQUlqQjtFQUNFLFlBQVk7RUFDWixrQkFBa0IsRUFBQTs7QUFFcEI7RUFDRSxvQ0FBK0MsRUFBQTs7QUFFakQ7RUFDRSxpQkFBaUIsRUFBQTs7QUFFbkI7RUFDRSxrQkFBa0IsRUFBQSIsImZpbGUiOiJhcHAvam9icy9uZXctam9iL25ldy1qb2IuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGVudC13cmFwcGVyIHtcclxuICBwYWRkaW5nLXRvcDogMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuI2pvYl9oZWFkZXIge1xyXG4gIG1hcmdpbjogMTBweCAxNXB4O1xyXG4gIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgYm94LXNoYWRvdzogMCAycHggNXB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcclxuICBib3JkZXItcmFkaXVzOiAwLjI1cmVtO1xyXG4gIHBhZGRpbmc6IDVweCAxNXB4IDRweCAxNXB4O1xyXG4gIGtlbmRvLXBvcHVwIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHdpZHRoOiAxODVweDtcclxuICAgIHRvcDogMzlweDtcclxuICAgIGxlZnQ6IDE1cHg7XHJcbiAgfVxyXG4gIGtlbmRvLXBvcHVwLnNjaGVkdWxlci1wb3B1cCB7XHJcbiAgICByaWdodDogNXB4O1xyXG4gICAgd2lkdGg6IDU1MHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBsZWZ0OiB1bnNldCAhaW1wb3J0YW50O1xyXG4gICAgdG9wOiA0NnB4ICFpbXBvcnRhbnQ7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgIGJveC1zaGFkb3c6IDBweCAzcHggM3B4IDBweCAjY2NjO1xyXG4gICAgcmlnaHQ6IDIwJTtcclxuICAgIGhyIHtcclxuICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICB9XHJcbiAgICBwIHtcclxuICAgICAgbWFyZ2luOiAwcHg7XHJcbiAgICAgIHBhZGRpbmc6IDEwcHggMTBweCAwcHggMTBweDtcclxuICAgIH1cclxuICB9XHJcbiAgLmpvYl9uYW1lIHtcclxuICAgIGJvcmRlcjogMHB4O1xyXG4gICAgYm9yZGVyOiBzb2xpZCAxcHggI2U3ZTdlNztcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICBwYWRkaW5nOiAwcHggMTBweDtcclxuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICA6dmlzaXRlZCB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuam9iX25hbWU6aG92ZXIge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICAgIDo6LXdlYmtpdC1pbnB1dC1wbGFjZWhvbGRlciB7XHJcbiAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5qb2JfbmFtZTo6cGxhY2Vob2xkZXIge1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gIH1cclxuXHJcbiAgYnV0dG9uIHtcclxuICAgIG1hcmdpbjogNXB4IDhweDtcclxuICB9XHJcblxyXG4gIGgzLFxyXG4gIHAge1xyXG4gICAgbWFyZ2luOiAwcHggMHB4IDBweCAtN3B4O1xyXG4gIH1cclxuICAuY29udGVudCB7XHJcbiAgICBwYWRkaW5nOiAzMHB4O1xyXG4gICAgY29sb3I6ICM3ODc4Nzg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmNmN2Y4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjA1KTtcclxuICB9XHJcbn1cclxuI2pvYl9jb250ZW50IHtcclxuICBtYXJnaW46IDEwcHggMTVweDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBib3gtc2hhZG93OiAwIDJweCA1cHggcmdiYSgwLCAwLCAwLCAwLjEpO1xyXG4gIGJvcmRlci1yYWRpdXM6IDAuMjVyZW07XHJcbiAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgLmNvbm5lY3Rpb24ge1xyXG4gICAgLy8gYm9yZGVyOiBzb2xpZCAxcHggI2YxZjFmMTtcclxuICAgIGhlaWdodDogODB2aDtcclxuICAgIHBhZGRpbmc6IDBweCAxMHB4O1xyXG4gICAgLmNvbm5lY3Rpb25faGVhZGVyIHtcclxuICAgICAgcGFkZGluZzogMHB4IDBweCAwcHggNXB4O1xyXG4gICAgICAvLyBib3JkZXItYm90dG9tOiBzb2xpZCAxcHggI2YxZjFmMTtcclxuICAgICAgLy8gYm94LXNoYWRvdzogMHB4IDBweCAxMHB4IDBweCAjZjFmMWYxO1xyXG4gICAgICAuY29sLWxnLTExIHtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDVweDtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcbiAgICAgIH1cclxuICAgICAga2VuZG8tZHJvcGRvd25saXN0IHtcclxuICAgICAgICBtYXJnaW46IDhweCA1cHg7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICAgIH1cclxuICAgICAgYnV0dG9uIHtcclxuICAgICAgICBtYXJnaW46IDhweCAwcHggMHB4IDBweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgICAgICBwYWRkaW5nOiA0cHg7XHJcbiAgICAgICAgYm9yZGVyLXdpZHRoOiAxcHg7XHJcbiAgICAgIH1cclxuICAgICAgYnV0dG9uOmhvdmVyIHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjFmMWZmO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAuY29ubmVjdGlvbl9tZXNzYWdlIHtcclxuICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICBkaXYubWVzc2FnZTEsXHJcbiAgICAgIGRpdi5tZXNzYWdlMiB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogNDAlO1xyXG4gICAgICAgIHBhZGRpbmc6IDBweCAzNXB4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgfVxyXG4gICAgICBkaXYubWVzc2FnZTEgPiBzdmcuaWNvbl9hZGRfaXRlbSB7XHJcbiAgICAgICAgbWFyZ2luOiAwcHggNXB4O1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICB3aWR0aDogMTJweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAga2VuZG8tcG9wdXAuc291cmNlLWNvbm5lY3Rpb24tb3B0aW9uIHtcclxuICAgICAgcmlnaHQ6IDVweDtcclxuICAgICAgd2lkdGg6IDE1NnB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgIGxlZnQ6IHVuc2V0ICFpbXBvcnRhbnQ7XHJcbiAgICAgIHRvcDogNDZweCAhaW1wb3J0YW50O1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB9XHJcbiAgICBrZW5kby1wb3B1cC5zb3VyY2UtY29ubmVjdGlvbi1vcHRpb24gPiAuay1wb3B1cCB7XHJcbiAgICAgIHdpZHRoOiAxNjBweDtcclxuICAgIH1cclxuICAgIGtlbmRvLXBvcHVwLmRlc3RpbmF0aW9uLWNvbm5lY3Rpb24tb3B0aW9uIHtcclxuICAgICAgcmlnaHQ6IDVweDtcclxuICAgICAgd2lkdGg6IDE1NnB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgIGxlZnQ6IHVuc2V0ICFpbXBvcnRhbnQ7XHJcbiAgICAgIHRvcDogNDZweCAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG4gIH1cclxuICAuY29sbGVjdGlvbiB7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgaGVpZ2h0OiA3MHZoO1xyXG4gICAgLy8gYm9yZGVyOiBzb2xpZCAxcHggI2YxZjFmMTtcclxuICAgIG92ZXJmbG93LXk6IGF1dG87XHJcbiAgICBvdmVyZmxvdy14OiBoaWRkZW47XHJcbiAgICAmOjotd2Via2l0LXNjcm9sbGJhci10cmFjayB7XHJcbiAgICAgIC13ZWJraXQtYm94LXNoYWRvdzogaW5zZXQgMCAwIDZweCAjZmZmZmZmO1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgfVxyXG5cclxuICAgICY6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICAgICAgd2lkdGg6IDZweDtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICAgIH1cclxuXHJcbiAgICAmOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigxNTgsIDE2NSwgMTcxKTtcclxuICAgIH1cclxuICAgID4gLm5hbWUge1xyXG4gICAgICBiYWNrZ3JvdW5kOiAjZjBmMmYzO1xyXG4gICAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgIHdvcmQtYnJlYWs6IGJyZWFrLWFsbDtcclxuICAgICAgLm9wdGlvbiB7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgIGZsb2F0OiByaWdodDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAtMXB4O1xyXG4gICAgICAgIHdpZHRoOiAzMHB4O1xyXG4gICAgICB9XHJcbiAgICAgID4gaW5wdXQge1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDBweDtcclxuICAgICAgICB3aWR0aDogOTQlO1xyXG4gICAgICB9XHJcbiAgICAgID4gYnV0dG9uID4gc3ZnIHtcclxuICAgICAgICBoZWlnaHQ6IDE4cHg7XHJcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAucGFyYW1ldGVycyxcclxuICAgIC5jb2x1bW5zIHtcclxuICAgICAgbWFyZ2luOiA1cHg7XHJcbiAgICAgIG1pbi1oZWlnaHQ6IDgwcHg7XHJcbiAgICAgIGJvcmRlcjogc29saWQgMXB4ICNlMWUxZTE7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgei1pbmRleDogMTA7XHJcbiAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgICAmLnBhcmFtZXRlcnM6aG92ZXIge1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XHJcbiAgICAgICAgLy8gY3Vyc29yOiBtb3ZlO1xyXG4gICAgICB9XHJcbiAgICAgIC5lZGl0X2ljb24ge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgICAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogLTZweDtcclxuICAgICAgICB3aWR0aDogMzBweDtcclxuICAgICAgfVxyXG4gICAgICAubmFtZSB7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG5cclxuICAgICAgICBidXR0b24ub2tCdG4ge1xyXG4gICAgICAgICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiAwcHg7XHJcbiAgICAgICAgICBtYXJnaW4tdG9wOiAycHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIC50eXBlIHtcclxuICAgICAgICBmb250LXNpemU6IDExLjVweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiA4cHg7XHJcbiAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuXHJcbiAgICAgICAgYnV0dG9uID4gc3ZnIHtcclxuICAgICAgICAgIGhlaWdodDogMTJweDtcclxuICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgLm1hcHBpbmcge1xyXG4gICAgICAgID4gaW5wdXQge1xyXG4gICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIC5vcHRpb24ge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgICAgICBmbG9hdDogdW5zZXQ7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDVweDtcclxuICAgICAgICB3aWR0aDogMjBweDtcclxuICAgICAgICBwYWRkaW5nOiAxcHg7XHJcbiAgICAgICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgICAgIGJvcmRlcjogMDtcclxuICAgICAgICBkaXNwbGF5OiBjb250ZW50cztcclxuICAgICAgfVxyXG4gICAgICBpbnB1dCxcclxuICAgICAga2VuZG8tZHJvcGRvd25saXN0IHtcclxuICAgICAgICB3aWR0aDogOTUlO1xyXG4gICAgICB9XHJcbiAgICAgIGJ1dHRvbiA+IHN2ZyB7XHJcbiAgICAgICAgaGVpZ2h0OiAxMnB4O1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICBmaWxsOiAjNjY2O1xyXG4gICAgICB9XHJcbiAgICAgIGxhYmVsLnRpdGxlIHtcclxuICAgICAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICAgIH1cclxuICAgICAgaW5wdXQuY29sdW1uX25hbWUsXHJcbiAgICAgIGlucHV0LmNvbHVtbl9tYXBwaW5nIHtcclxuICAgICAgICBib3JkZXI6IHNvbGlkIDFweCAjY2NjY2NjO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDNweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNnB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgb3V0bGluZTogbm9uZTtcclxuICAgICAgICBwYWRkaW5nOiAzcHg7XHJcbiAgICAgICAgd2lkdGg6IDk1JTtcclxuICAgICAgfVxyXG4gICAgICAuY29sdW1uX3R5cGUge1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDZweCAhaW1wb3J0YW50O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAuY29sdW1uczpob3ZlciAub3B0aW9uIHtcclxuICAgICAgZGlzcGxheTogY29udGVudHM7XHJcbiAgICB9XHJcbiAgICBpbnB1dCB7XHJcbiAgICAgIGJvcmRlcjogc29saWQgMXB4ICNjY2NjY2M7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDNweDtcclxuICAgICAgYmFja2dyb3VuZDogI2YxZjFmMTtcclxuICAgICAgbWFyZ2luLXRvcDogNnB4O1xyXG4gICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgICBwYWRkaW5nOiAzcHg7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgfVxyXG4gIH1cclxuICAucGFyYW1lcnRlciA+IC50eXBlIHtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgfVxyXG59XHJcbi5rLWdyaWQge1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxufVxyXG4ubmF2ID4gbGkgPiBhIHtcclxuICBwYWRkaW5nLWxlZnQ6IDMwcHg7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbn1cclxuLm5hdiA+IGxpOmhvdmVyLFxyXG4ubmF2ID4gbGk6aG92ZXIgYSxcclxuLm5hdiA+IGxpOmhvdmVyIGEgPiBzdmcge1xyXG4gIGJhY2tncm91bmQ6ICMzODQxNGE7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIGZpbGw6IHdoaXRlO1xyXG59XHJcbi5uYXYgPiBsaSA+IGEgPiBzdmcge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBsZWZ0OiA4cHg7XHJcbn1cclxuLm92ZXJsYXkge1xyXG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC4yKTtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDg1dmg7XHJcbiAgei1pbmRleDogMTAwMDAwO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxufVxyXG4ub3ZlcmxheSA+IC5mYSB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogNTAlO1xyXG4gIGxlZnQ6IDQyJTtcclxuICBtYXJnaW4tbGVmdDogLTE1cHg7XHJcbiAgbWFyZ2luLXRvcDogLTE1cHg7XHJcbiAgY29sb3I6ICMwMDA7XHJcbiAgZm9udC1zaXplOiAzMHB4O1xyXG59XHJcbi5vdmVybGF5ID4gcCB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogNjUlO1xyXG4gIGxlZnQ6IDQ5JTtcclxuICBtYXJnaW4tbGVmdDogLTE1cHg7XHJcbiAgbWFyZ2luLXRvcDogLTE1cHg7XHJcbiAgY29sb3I6ICMwMDA7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG59XHJcbi5vdmVybGF5ID4gcC5sb2FkaW5nIHtcclxuICBsZWZ0OiA0OSU7XHJcbn1cclxuXHJcbi5vdmVybGF5ID4gcC5wcm9jZXNzaW5nIHtcclxuICBsZWZ0OiA0OSU7XHJcbn1cclxuXHJcbi5vdmVybGF5ID4gcC52YWxpZGF0aW5nIHtcclxuICBsZWZ0OiA0OSU7XHJcbn1cclxuLnRvcC1NYXJnaW4ge1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDQwM3B4O1xyXG59XHJcbi5hbmNob3Ige1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHdpZHRoOiAxMjBweDtcclxuICBtYXJnaW46IDIwMHB4IGF1dG87XHJcbn1cclxuLnNjcm9sbGluZy13cmFwcGVyLWZsZXhib3gge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC13cmFwOiBub3dyYXA7XHJcbiAgb3ZlcmZsb3cteDogYXV0bztcclxuXHJcbiAgLmNhcmQge1xyXG4gICAgZmxleDogMCAwIGF1dG87XHJcbiAgfVxyXG59XHJcbnRhYmxlLmRfZmlsdGVyIHtcclxuICBmb250LXNpemU6IDEwcHg7XHJcbiAgLmNvbnRyb2wge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgc3ZnIHtcclxuICAgICAgbWFyZ2luOiAwcHggM3B4O1xyXG4gICAgfVxyXG4gIH1cclxuICAuaW5wdXQge1xyXG4gICAga2VuZG8tZHJvcGRvd25saXN0LFxyXG4gICAgaW5wdXQge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxuICB9XHJcbn1cclxudGFibGUuZF9maWx0ZXIgPiB0Ym9keSA+IHRyID4gdGQge1xyXG4gIHBhZGRpbmc6IDJweDtcclxuICBsaW5lLWhlaWdodDogMi40ZW07XHJcbn1cclxuLmVycm9yIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjU1LCAxOTAsIDE5MCkgIWltcG9ydGFudDtcclxufVxyXG4ucGFkZGluZy1sZWZ0LTUge1xyXG4gIHBhZGRpbmctbGVmdDogNXB4O1xyXG59XHJcbi5wYWRkaW5nLXJpZ2h0LTUge1xyXG4gIHBhZGRpbmctcmlnaHQ6IDVweDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/jobs/new-job/new-job.component.ts":
/*!***************************************************!*\
  !*** ./src/app/jobs/new-job/new-job.component.ts ***!
  \***************************************************/
/*! exports provided: NewJobComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewJobComponent", function() { return NewJobComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_connections_connections_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/connections/connections.component */ "./src/app/connections/connections.component.ts");
/* harmony import */ var src_app_connections_connection_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/connections/connection-service */ "./src/app/connections/connection-service.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _job_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../job.service */ "./src/app/jobs/job.service.ts");
/* harmony import */ var src_app_model_collection__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/model/collection */ "./src/app/model/collection.ts");
/* harmony import */ var src_app_model_job__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/model/job */ "./src/app/model/job.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_data_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../shared/data.service */ "./src/app/shared/data.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _progress_kendo_file_saver__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @progress/kendo-file-saver */ "./node_modules/@progress/kendo-file-saver/dist/es/main.js");
/* harmony import */ var src_app_model_schdule__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! src/app/model/schdule */ "./src/app/model/schdule.ts");
/* harmony import */ var src_app_model_data_filter__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! src/app/model/data-filter */ "./src/app/model/data-filter.ts");
/* harmony import */ var _shared_tour_guide_tour_guide_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../shared/tour-guide/tour-guide.component */ "./src/app/shared/tour-guide/tour-guide.component.ts");
/* harmony import */ var _progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @progress/kendo-data-query */ "./node_modules/@progress/kendo-data-query/dist/es/main.js");
/* harmony import */ var intro_js_intro_js__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! intro.js/intro.js */ "./node_modules/intro.js/intro.js");
/* harmony import */ var intro_js_intro_js__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(intro_js_intro_js__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _app_session_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../app.session-service */ "./src/app/app.session-service.ts");
/* harmony import */ var _settings_users_users_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../../settings/users/users.service */ "./src/app/settings/users/users.service.ts");
/* harmony import */ var _settings_roles_roles_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../../settings/roles/roles.service */ "./src/app/settings/roles/roles.service.ts");





















var NewJobComponent = /** @class */ (function () {
    // = groupBy(this.groupedData, [{ field: 'subcategory' }])
    function NewJobComponent(connectionService, cookieService, tourGuideComponents, router, jobService, route, eRef, toastr, dataService, sessionService, userService, roleService) {
        var _this = this;
        this.connectionService = connectionService;
        this.cookieService = cookieService;
        this.tourGuideComponents = tourGuideComponents;
        this.router = router;
        this.jobService = jobService;
        this.route = route;
        this.eRef = eRef;
        this.toastr = toastr;
        this.dataService = dataService;
        this.sessionService = sessionService;
        this.userService = userService;
        this.roleService = roleService;
        this.source = [
            { text: 'Select Connection', value: 1 }
        ];
        this.types = [
            { text: 'Select Type', value: 1 },
            { text: 'VARCHAR2', value: 2 },
            { text: 'NCHAR', value: 3 },
            { text: 'NVARCHAR2', value: 3 },
            { text: 'CLOB', value: 3 },
            { text: 'NCLOB', value: 3 },
            { text: 'LONG', value: 3 },
            { text: 'NUMBER', value: 3 },
            { text: 'DATE', value: 3 },
            { text: 'BLOB', value: 3 },
            { text: 'BFILE', value: 3 },
            { text: 'RAW', value: 3 },
            { text: 'LONG ROW', value: 3 },
        ];
        this.logicalOperators = [
            // { text: 'Select Operator', value: 'null' },
            { text: 'and', value: 'and' },
            { text: 'or', value: 'or' },
        ];
        this.comparisonOperators = [
            { text: 'Select Type', value: '' },
            { text: 'equal', value: 'equal' },
            { text: 'not equal', value: 'not equal' },
            { text: 'less than', value: 'less than' },
            { text: 'less than or equal', value: 'less than or equal' },
            { text: 'greater than', value: 'greater than' },
            { text: 'Greater than or equal', value: 'Greater than or equal' }
        ];
        this.dataFilters = new Array();
        this.dataPreviewerFilters = new Array();
        this.selectedRoles = new Array();
        this.roles = new Array();
        this.inResponseName = [];
        this.inResponseType = [];
        this.myFiles = [];
        this.uploadSaveUrl = 'saveUrl'; // should represent an actual API endpoint
        this.uploadRemoveUrl = 'removeUrl';
        this.sourceDataPreviewGridHeader = [];
        this.destinationDataPreviewGridHeader = [];
        this.dataPreviewGridHeader = [];
        this.testFilterRequest = {};
        this.opened = false;
        this.cronExpression = '4 3 2 12 1/1 ? *';
        this.isCronDisabled = false;
        this.isfilterValue = 0;
        this.cronOptions = {
            formInputClass: 'form-control cron-editor-input',
            formSelectClass: 'form-control cron-editor-select',
            formRadioClass: 'cron-editor-radio',
            formCheckboxClass: 'cron-editor-checkbox',
            defaultTime: '10:00:00',
            use24HourTime: true,
            hideMinutesTab: false,
            hideHourlyTab: false,
            hideDailyTab: false,
            hideWeeklyTab: false,
            hideMonthlyTab: false,
            hideYearlyTab: false,
            hideAdvancedTab: true,
            hideSeconds: false,
            removeSeconds: false,
            removeYears: false
        };
        this.sourceItemsSkip = 0;
        this.destinationItemsSkip = 0;
        this.accountRequestPageSizes = true;
        this.info = true;
        this.previousNext = true;
        this.buttonCount = 5;
        this.state = {
            skip: 0,
            take: 1
        };
        this.buttonCountDP = 1;
        this.infoDP = true;
        this.typeDP = 'numeric';
        this.pageSizesDP = true;
        this.previousNextDP = true;
        this.introJS = intro_js_intro_js__WEBPACK_IMPORTED_MODULE_16__();
        this.data = [
            { name: 'Pork', category: 'Food', subcategory: 'Tables' },
            { name: 'Pepper', category: 'Food', subcategory: 'Procedure' },
        ];
        if (this.sessionService.getCookie('LA_USER_DETAIL') !== '') {
            this.loginData = JSON.parse(JSON.stringify(this.sessionService.getCookie('LA_USER_DETAIL')));
        }
        else {
            this.loginData = JSON.parse(JSON.stringify(this.sessionService.getCookie('LOGIN').results[0]));
        }
        this.GetUserRoles(this.loginData.userId);
        this.isSourceConnectionConnected = false;
        this.isSourceCollectionSelected = false;
        this.isDestinationConnectionConnected = false;
        this.isDestinationCollectionSelected = false;
        this.isConnectionVisibleOption = false;
        this.editIconVisibility = false;
        this.isCollectionVisibleOption = false;
        this.isDestinationConnectionOption = false;
        this.isSourceConnectionOption = false;
        this.isCreatedTableRequest = false;
        this.isCreateTableRequested = false;
        this.isNewSyncMetadataUpdated = false;
        this.isSyncAble = false;
        this.showSpinner = false;
        this.isRunEnabled = false;
        this.incrementColumnDateTimeValue = false;
        this.isSchedulerHide = false;
        this.sourceConnections = Array();
        this.destinationConnections = Array();
        this.selectedSourceCollection = new Array();
        this.filterResponseProcedure = new Array();
        this.selectedDestinationCollection = new Array();
        this.sourceCollectionItems = new Array();
        this.sourceCollectionItemsUrl = new Array();
        this.destinationCollectionItems = new Array();
        this.sourceFilterableColumns = new Array();
        this.dataFilters = new Array();
        this.dataPreviewerFilters = new Array();
        // this.dataFilters.push(new DataFilter());
        this.dataPreviewerFilters.push(new src_app_model_data_filter__WEBPACK_IMPORTED_MODULE_13__["DataFilter"]());
        this.jobRequest = new src_app_model_job__WEBPACK_IMPORTED_MODULE_7__["JobRequest"]();
        this.jobFilter = new src_app_model_job__WEBPACK_IMPORTED_MODULE_7__["JobFilter"]();
        this.jobFilter.recordCount = 10;
        this.height = 700;
        this.i = 1;
        this.j = 0;
        this.stats = { status: '', processedRecord: '', totalRecordCount: '' };
        // this.sourceCollection = new Array<{ text: string, value: string }>();
        // this.destinationCollection = new Array<{ text: string, value: string }>();
        this.incrementColumnValue = new Date();
        this.incrementValue = 'singleProcess';
        this.intervalObj = setInterval(function () {
            _this.route.paramMap.subscribe(function (params) {
                if (params.get('jobId') !== 'new') {
                    _this.checkProgress(params.get('jobId'));
                }
            });
        }, 1000);
        if (this.cookieService.get('intro') === 'createNewJob') {
            this.introJS.setOptions({
                steps: [
                    {
                        element: '#job_name',
                        intro: 'Write the job name '
                    },
                    {
                        element: '#sourceConnections',
                        intro: 'Select Source Connection',
                        position: 'right'
                    },
                    {
                        element: '#sourceConnectionsBtn',
                        intro: 'Press button'
                    },
                    {
                        element: '#sourceCollection',
                        intro: 'Select Source Collection.',
                        position: 'right'
                    },
                    {
                        element: '#destinationConnections',
                        intro: 'Select Destination Connection.',
                        position: 'left'
                    },
                    {
                        element: '#destinationConnectionsBtn',
                        intro: 'Press button'
                    },
                    {
                        element: '#createTableBtn',
                        intro: 'Press vertical three dot'
                    },
                    {
                        element: '#createTableOption',
                        intro: 'Select Create Table',
                        position: 'top'
                    },
                    {
                        element: '#OkBtn',
                        intro: 'Press the Ok button'
                    },
                    {
                        element: '#saveBtn',
                        intro: 'Press Save Icon'
                    },
                ]
            });
        }
        else if (this.cookieService.get('intro') === 'createNewJobWithExistingTable') {
            this.introJS.setOptions({
                steps: [
                    {
                        element: '#job_name',
                        intro: 'Write the job name '
                    },
                    {
                        element: '#sourceConnections',
                        intro: 'Select Source Connection',
                        position: 'right'
                    },
                    {
                        element: '#sourceConnectionsBtn',
                        intro: 'Press button'
                    },
                    {
                        element: '#sourceCollection',
                        intro: 'Select Source Collection.',
                        position: 'right'
                    },
                    {
                        element: '#destinationConnections',
                        intro: 'Select Destination Connection.',
                        position: 'left'
                    },
                    {
                        element: '#destinationConnectionsBtn',
                        intro: 'Press button'
                    },
                    {
                        element: '#selectTableCollection',
                        intro: 'Select the table ',
                        position: 'top'
                    },
                    {
                        element: '#saveBtn',
                        intro: 'Press Save Icon'
                    },
                ]
            });
        }
        else if (this.cookieService.get('intro') === 'createNewJobWithFilter') {
            this.introJS.setOptions({
                steps: [
                    {
                        element: '#job_name',
                        intro: 'Write the job name '
                    },
                    {
                        element: '#sourceConnections',
                        intro: 'Select Source Connection',
                        position: 'left'
                    },
                    {
                        element: '#sourceConnectionsBtn',
                        intro: 'Press button'
                    },
                    {
                        element: '#sourceCollection',
                        intro: 'Select Source Collection.',
                        position: 'left'
                    },
                    {
                        element: '#openFilterBtn',
                        intro: 'Press Vertical Three Dot',
                        position: 'top'
                    },
                    {
                        element: '#filterBtn',
                        intro: 'Press Filter Text.',
                        position: 'right'
                    },
                    {
                        element: '#filterField',
                        intro: 'Select Filterable field.'
                    },
                    {
                        element: '#selectOperator',
                        intro: 'Select Operator.'
                    },
                    {
                        element: '#writeValue',
                        intro: 'Write value.'
                    },
                    {
                        element: '#validate',
                        intro: 'Press Validate Filter.'
                    },
                    {
                        element: '#OkBtn',
                        intro: 'Press Ok.'
                    },
                    {
                        element: '#destinationConnections',
                        intro: 'Select Destination Connection.'
                    },
                    {
                        element: '#destinationConnectionsBtn',
                        intro: 'Press button'
                    },
                    {
                        element: '#createTableBtn',
                        intro: 'Press vertical three dot'
                    },
                    {
                        element: '#createTableOption',
                        intro: 'Select Create Table'
                    },
                    {
                        element: '#OkBtn',
                        intro: 'Press the Ok button'
                    },
                    {
                        element: '#saveBtn',
                        intro: 'Press Save Icon'
                    },
                ]
            });
        }
        else if (this.cookieService.get('intro') === 'createNewJobWithFilterWithExistingTable') {
            this.introJS.setOptions({
                steps: [
                    {
                        element: '#job_name',
                        intro: 'Write the job name '
                    },
                    {
                        element: '#sourceConnections',
                        intro: 'Select Source Connection'
                    },
                    {
                        element: '#sourceConnectionsBtn',
                        intro: 'Press button'
                    },
                    {
                        element: '#sourceCollection',
                        intro: 'Select Source Collection.'
                    },
                    {
                        element: '#openFilterBtn',
                        intro: 'Press Vertical Three Dot'
                    },
                    {
                        element: '#filterBtn',
                        intro: 'Press Filter Text.'
                    },
                    {
                        element: '#filterField',
                        intro: 'Select Filterable field.'
                    },
                    {
                        element: '#selectOperator',
                        intro: 'Select Operator.'
                    },
                    {
                        element: '#writeValue',
                        intro: 'Write value.'
                    },
                    {
                        element: '#validate',
                        intro: 'Press Validate Filter.'
                    },
                    {
                        element: '#OkBtn',
                        intro: 'Press Ok.'
                    },
                    {
                        element: '#destinationConnections',
                        intro: 'Select Destination Connection.'
                    },
                    {
                        element: '#destinationConnectionsBtn',
                        intro: 'Press button'
                    },
                    {
                        element: '#selectTableCollection',
                        intro: 'Select the table '
                    },
                    {
                        element: '#saveBtn',
                        intro: 'Press Save Icon'
                    },
                ]
            });
        }
        else if (this.cookieService.get('intro') === 'createNewJobWithIncrementValue') {
            this.introJS.setOptions({
                steps: [
                    {
                        element: '#job_name',
                        intro: 'Write the job name '
                    },
                    {
                        element: '#sourceConnections',
                        intro: 'Select Source Connection'
                    },
                    {
                        element: '#sourceConnectionsBtn',
                        intro: 'Press button'
                    },
                    {
                        element: '#sourceCollection',
                        intro: 'Select Source Collection.'
                    },
                    {
                        element: '#openFilterBtn',
                        intro: 'Press Vertical Three Dot'
                    },
                    {
                        element: '#filterBtn',
                        intro: 'Press Net Change Text.'
                    },
                    {
                        element: '#radioBtn',
                        intro: 'Select Second Radio Butoon.'
                    },
                    {
                        element: '#selectNetChaneValue',
                        intro: 'Select Net Change Field.'
                    },
                    {
                        element: '#selectIncrementDate',
                        intro: 'Select Date.'
                    },
                    {
                        element: '#selectIncrementTime',
                        intro: 'Select Time.'
                    },
                    {
                        element: '#OkBtn',
                        intro: 'Press Ok.'
                    },
                    {
                        element: '#destinationConnections',
                        intro: 'Select Destination Connection.'
                    },
                    {
                        element: '#destinationConnectionsBtn',
                        intro: 'Press button'
                    },
                    {
                        element: '#createTableBtn',
                        intro: 'Press vertical three dot'
                    },
                    {
                        element: '#createTableOption',
                        intro: 'Select Create Table'
                    },
                    {
                        element: '#OkBtn',
                        intro: 'Press the Ok button'
                    },
                    {
                        element: '#saveBtn',
                        intro: 'Press Save Icon'
                    },
                ]
            });
        }
        else if (this.cookieService.get('intro') === 'createNewJobWithIncrementValueExistingtable') {
            this.introJS.setOptions({
                steps: [
                    {
                        element: '#job_name',
                        intro: 'Write the job name '
                    },
                    {
                        element: '#sourceConnections',
                        intro: 'Select Source Connection'
                    },
                    {
                        element: '#sourceConnectionsBtn',
                        intro: 'Press button'
                    },
                    {
                        element: '#sourceCollection',
                        intro: 'Select Source Collection.'
                    },
                    {
                        element: '#openFilterBtn',
                        intro: 'Press Vertical Three Dot'
                    },
                    {
                        element: '#filterBtn',
                        intro: 'Press Net Change Text.'
                    },
                    {
                        element: '#radioBtn',
                        intro: 'Select Second Radio Butoon.'
                    },
                    {
                        element: '#selectNetChaneValue',
                        intro: 'Select Net Change Field.'
                    },
                    {
                        element: '#selectIncrementDate',
                        intro: 'Select Date.'
                    },
                    {
                        element: '#selectIncrementTime',
                        intro: 'Select Time.'
                    },
                    {
                        element: '#OkBtn',
                        intro: 'Press Ok.'
                    },
                    {
                        element: '#destinationConnections',
                        intro: 'Select Destination Connection.'
                    },
                    {
                        element: '#destinationConnectionsBtn',
                        intro: 'Press button'
                    },
                    {
                        element: '#selectTableCollection',
                        intro: 'Select the table '
                    },
                    {
                        element: '#saveBtn',
                        intro: 'Press Save Icon'
                    },
                ]
            });
        }
        this.introJS.oncomplete(function () {
            _this.router.navigate(['jobs/new'], { queryParams: {} });
        });
        this.introJS.onexit(function () {
            _this.router.navigate(['jobs/new'], { queryParams: {} });
        });
    }
    NewJobComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.typeCollection = this.types.slice();
        this.logicalOperatorCollections = this.logicalOperators.slice();
        this.comparisonOperatorCollections = this.comparisonOperators.slice();
        if (this.route.snapshot.queryParams.action === 'intro') {
            this.introJS.start();
            this.introJS.onchange(function (targetElement) {
            });
            this.introJS.oncomplete(function () {
                _this.router.navigate(['/jobs/new'], { queryParams: {} });
            });
            this.introJS.onexit(function () {
                // this.router.navigate(['/jobs/new'], { queryParams: {} });
            });
        }
    };
    NewJobComponent.prototype.GetUserRoles = function (userid) {
        var _this = this;
        this.userService.GetUserRoles(userid).subscribe(function (data) {
            _this.GetAllRoles();
            var response = JSON.parse(JSON.stringify(data));
            if (response.success === true && response.results[0].roleDetailsResponse.length > 0) {
                _this.selectedRoles = response.results[0].roleDetailsResponse;
            }
        }, function (err) {
        });
    };
    NewJobComponent.prototype.GetAllRoles = function () {
        var _this = this;
        this.roleService.GetAllRoles().subscribe(function (data) {
            _this.roles = JSON.parse(JSON.stringify(data)).results;
            _this.getAllConnection();
            _this.selectedRoles.forEach(function (elemen) {
                var obj = _this.roles.filter(function (n) { return n.roleId === elemen.roleId; })[0];
                var index = _this.roles.indexOf(obj);
                if (index > -1) {
                    _this.roles.splice(index, 1);
                }
            });
            console.log(_this.roles);
        }, function (err) {
            console.error(err);
        });
    };
    NewJobComponent.prototype.onSelectFile = function (event) {
        this.files = event.target.files[0];
    };
    NewJobComponent.prototype.handleChange = function (evt) {
        this.isIncrementParameter = !this.isIncrementParameter;
        this.incrementColumnDateTimeValue = false;
        this.incrementalColumnSelectedValue = '';
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithIncrementValue') {
            this.introJS.goToStepNumber(7).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithIncrementValueExistingtable') {
            this.introJS.goToStepNumber(7).start();
        }
    };
    NewJobComponent.prototype.selectionChangeSourceConnection = function () {
        this.populateSourceCollection(this.sourceConnectionValue.connectionId);
        if (this.route.snapshot.queryParams.action === 'intro') {
            this.introJS.goToStepNumber(2).start();
        }
    };
    // public itemDisabled(itemArgs: { dataItem: string, index: number }) {
    //     return itemArgs.dataItem.type !== '' && this.sourceConnectionValue != undefined && itemArgs.dataItem.type !== this.sourceConnectionValue.type;
    // }
    NewJobComponent.prototype.changeSourceConnection = function () {
        this.isSourceConnectionConnected = false;
    };
    NewJobComponent.prototype.itemDisabled = function (itemArgs) {
        return !itemArgs.dataItem.isDisabled;
    };
    NewJobComponent.prototype.selectionChangeSourceCollection = function (event) {
        this.sourceCollectionValue.text = event.text;
        this.sourceCollectionValue.value = event.value;
        this.sourceCollectionValue.subcategory = event.subcategory;
        this.populateSourceMetadata(this.sourceConnectionValue.connectionId, event.value);
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJob') {
            this.introJS.goToStepNumber(4).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithExistingTable') {
            this.introJS.goToStepNumber(4).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilter') {
            this.introJS.goToStepNumber(4).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilterWithExistingTable') {
            this.introJS.goToStepNumber(4).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithIncrementValue' && this.isSourceConnectionOption === false) {
            this.introJS.goToStepNumber(4).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithIncrementValueExistingtable' && this.isSourceConnectionOption === false) {
            this.introJS.goToStepNumber(4).start();
        }
    };
    NewJobComponent.prototype.selectionChangeDestinationConnection = function () {
        this.populateDestinationCollection(this.destinationConnectionValue.connectionId);
        this.populateDestinationMetadata(this.destinationConnectionValue.connectionId, '');
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJob') {
            this.introJS.goToStepNumber(5).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithExistingTable') {
            this.introJS.goToStepNumber(5).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilter') {
            this.introJS.goToStepNumber(12).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilterWithExistingTable' && this.i === 1) {
            this.introJS.goToStepNumber(13).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilterWithExistingTable' && this.i === 0) {
            this.introJS.goToStepNumber(12).start();
        }
        this.i = this.i + 1;
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithIncrementValue') {
            this.introJS.goToStepNumber(12).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithIncrementValueExistingtable') {
            this.introJS.goToStepNumber(12).start();
        }
    };
    NewJobComponent.prototype.changeDestinationConnection = function () {
        this.isDestinationConnectionConnected = false;
        this.destinationCollection = [];
    };
    NewJobComponent.prototype.selectionChangeDataType = function (event, filter) {
        var _this = this;
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJob') {
            this.introJS.goToStepNumber(6).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilter') {
            this.introJS.goToStepNumber(7).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilterWithExistingTable') {
            this.introJS.goToStepNumber(7).start();
        }
        if (this.sourceCollectionValue.subcategory !== 'procedure') {
            filter.dataType = event.type;
        }
        if (this.sourceCollectionValue.subcategory === 'procedure') {
            this.dataFilters = [];
            event.forEach(function (element) {
                _this.procedureType = element.type;
                _this.procedureField = element.name;
                _this.dataFilters.push({ comparisonOperator: '', field: _this.procedureField, logicalOperator: '', value: '', dataType: _this.procedureType });
            });
        }
    };
    NewJobComponent.prototype.populateSourceCollection = function (connectionId) {
        var _this = this;
        if (this.sourceConnectionValue.type === 'EXCEL') {
            this.sourceValueType = 'EXCEL';
            this.isSourceConnectionConnected = true;
            // return false;
        }
        console.log(this.sourceConnectionValue.type);
        this.sourceValueType = this.sourceConnectionValue.name;
        this.isProcessing = true;
        this.connectionId = connectionId;
        this.jobService.getAllEntities(connectionId)
            .subscribe(function (response) {
            _this.isProcessing = false;
            if (_this.sourceConnectionValue.type !== 'EXCEL') {
                var metadataTableResponse = JSON.parse(JSON.stringify(response)).results[0].metadataTableResponse === undefined ? [] : JSON.parse(JSON.stringify(response)).results[0].metadataTableResponse;
                var metadataProcedureResponse = JSON.parse(JSON.stringify(response)).results[0].metadataProcedureResponse === undefined ? [] : JSON.parse(JSON.stringify(response)).results[0].metadataProcedureResponse;
                //  this.data
                _this.sourceCollectionItems = new Array();
                if (_this.sourceConnectionValue.type === 'ODATA') {
                    metadataTableResponse.forEach(function (element) {
                        _this.tableOrProcedure = 'table';
                        _this.sourceCollectionItems.push({ text: element.collectionName + " (" + element.collectionCount + ")", value: element.collectionName, subcategory: 'table' });
                    });
                }
                else {
                    metadataTableResponse.forEach(function (element) {
                        _this.tableOrProcedure = 'table';
                        _this.sourceCollectionItems.push({ text: "" + element.collectionName, value: element.collectionName, subcategory: 'table' });
                    });
                }
                if (_this.sourceConnectionValue.type !== 'ODATA') {
                    metadataProcedureResponse.forEach(function (element) {
                        _this.tableOrProcedure = 'procedure';
                        _this.sourceCollectionItems.push({ text: element.collectionName, value: element.collectionName, subcategory: 'procedure' });
                    });
                }
                else {
                    metadataTableResponse.forEach(function (element) {
                        _this.sourceCollectionItems.push({ text: element.collectionName, value: element.collectionName, subcategory: 'procedure' });
                    });
                }
            }
            _this.sourceCollectionList = _this.sourceCollectionItems;
            _this.sourceCollection = Object(_progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_15__["groupBy"])(_this.sourceCollectionItems, [{ field: 'subcategory' }]);
            if (_this.sourceConnectionValue.type === 'EXCEL') {
                _this.sourceCollectionItems.push({ text: '', value: '', subcategory: '' });
            }
            var defaultvalue = { text: 'Select Collection', value: '0' };
            // this.sourceCollection.splice(0, 0, defaultvalue);
            // = groupBy(this.groupedData, [{ field: 'subcategory' }])
            _this.sourceCollectionValue = defaultvalue;
            _this.isSourceConnectionConnected = true;
            if (_this.isSourceConnectionConnected && _this.route.snapshot.queryParams.action === 'intro') {
                _this.introJS.goToStepNumber(3).start();
            }
            _this.isSourceCollectionSelected = false;
            _this.route.paramMap.subscribe(function (params) {
                console.log(params);
                if (params.get('jobId') !== 'new') {
                    _this.sourceCollectionValue = _this.sourceCollectionItems.filter(function (n) { return n.value.toLowerCase() === _this.jobDetail.sourceCollection.toLowerCase(); })[0];
                    _this.populateSourceMetadata(_this.sourceConnectionValue.connectionId, _this.jobDetail.sourceCollection);
                }
            });
        }, function (error) {
            _this.isProcessing = false;
        });
    };
    NewJobComponent.prototype.populateDestinationCollection = function (connectionId) {
        var _this = this;
        console.log(this.destinationConnectionValue.type);
        if (this.destinationConnectionValue.type === 'EXCEL') {
            this.isDestinationConnectionConnected = true;
            // return false;
        }
        this.isProcessing = true;
        this.jobService.getAllEntities(connectionId)
            .subscribe(function (response) {
            _this.isProcessing = false;
            if (_this.destinationConnectionValue.type !== 'EXCEL') {
                var metadataTableResponse = JSON.parse(JSON.stringify(response)).results[0].metadataTableResponse === undefined ? [] : JSON.parse(JSON.stringify(response)).results[0].metadataTableResponse;
                var metadataProcedureResponse = JSON.parse(JSON.stringify(response)).results[0].metadataProcedureResponse === undefined ? [] : JSON.parse(JSON.stringify(response)).results[0].metadataProcedureResponse;
                var list = JSON.parse(JSON.stringify(response)).results;
                // this.destinationCollection = new Array<{ text: string, value: string }>();
                _this.destinationCollectionItems = new Array();
                if (_this.sourceConnectionValue.type === 'OData') {
                    metadataTableResponse.forEach(function (element) {
                        _this.destinationCollectionItems.push({ text: element.collectionName + " (" + element.collectionCount + ")", value: element.collectionName, subcategory: 'table' });
                    });
                }
                else {
                    metadataTableResponse.forEach(function (element) {
                        _this.destinationCollectionItems.push({ text: "" + element.collectionName, value: element.collectionName, subcategory: 'table', url: element.apiUrl });
                        // this.destinationCollectionItems.push({ text: `${element.apiUrl}`, value: element.apiUrl, subcategory: 'table' });
                        _this.sourceCollectionItemsUrl.push({ text: "" + element.collectionName, value: element.collectionName, subcategory: 'table', apiUrl: element.apiUrl });
                    });
                }
            }
            // metadataProcedureResponse.forEach(element => {
            //   this.destinationCollectionItems.push({ text: element.collectionName, value: element.collectionName, subcategory: 'procedure' });
            // });
            //  this.destinationCollectionList = this.destinationCollection;
            _this.destinationCollectionList = _this.destinationCollectionItems;
            // this.sourceCollectionUrlList = this.sourceCollectionItemsUrl;
            _this.destinationCollection = Object(_progress_kendo_data_query__WEBPACK_IMPORTED_MODULE_15__["groupBy"])(_this.destinationCollectionItems, [{ field: 'subcategory' }]);
            var defaultvalue = { text: 'Select Collection', value: '0' };
            //  this.destinationCollection.splice(0, 0, defaultvalue);
            _this.destinationCollectionValue = defaultvalue;
            _this.isDestinationConnectionConnected = true;
            if (_this.isDestinationConnectionConnected && _this.route.snapshot.queryParams.action === 'intro') {
                _this.introJS.goToStepNumber(6).start();
            }
            _this.introJS.onexit(function () {
                _this.router.navigate(['jobs/new'], { queryParams: {} });
            });
            _this.isDestinationCollectionSelected = false;
            console.log(response);
            _this.route.paramMap.subscribe(function (params) {
                console.log(params);
                if (params.get('jobId') !== 'new' && (_this.sourceConnectionValue.type !== 'ODATA') && params.get('jobId') !== 'new' && (_this.sourceConnectionValue.type !== 'ORACLE')) {
                    _this.destinationCollectionValue = _this.destinationCollectionItems.
                        filter(function (n) { return n.url.toLowerCase() === _this.jobDetail.destinationCollection.toLowerCase(); })[0];
                    console.log(_this.sourceCollectionItemsUrl);
                    var destinationColl = _this.sourceCollectionItemsUrl.filter(function (x) { return x.apiUrl === _this.jobDetail.destinationCollection; }).map(function (n) { return n.text; });
                    _this.populateDestinationMetadata(_this.destinationConnectionValue.connectionId, destinationColl[0]);
                }
                else {
                    console.log(_this.destinationCollection);
                    _this.destinationCollectionValue = _this.destinationCollectionItems.filter(function (n) { return n.value.toLowerCase() === _this.jobDetail.destinationCollection.toLowerCase(); })[0];
                    _this.populateDestinationMetadata(_this.destinationConnectionValue.connectionId, _this.jobDetail.destinationCollection);
                }
                //   this.destinationCollectionValue = this.destinationCollectionItems.
                //     filter(n => n.value.toLowerCase() === this.jobDetail.destinationCollection.toLowerCase())[0];
                //   console.log(this.sourceCollectionItemsUrl);
                //   const destinationColl = this.sourceCollectionItemsUrl.filter(x => x.apiUrl === this.jobDetail.destinationCollection).map(n => n.text);
                //   this.populateDestinationMetadata(this.destinationConnectionValue.connectionId, destinationColl[0]);
                // }
                // this.getJob(params.get('jobId'));
            });
        }, function (error) {
            _this.isProcessing = false;
            console.log(error);
        });
    };
    NewJobComponent.prototype.populateSourceMetadata = function (connectionId, collectionName) {
        var _this = this;
        // this.connectionType = this.sourceCollectionValue.subcategory;
        // collectionName = collectionName;
        this.filterResponseProcedure = [];
        this.connectionType = this.sourceCollectionValue.subcategory;
        this.isProcessing = true;
        this.jobService.getTableMetadata(connectionId, collectionName, this.sourceCollectionValue.subcategory)
            .subscribe(function (response) {
            _this.isProcessing = false;
            var list = JSON.parse(JSON.stringify(response)).results;
            var inResponse = JSON.parse(JSON.stringify(response)).results[0].inResponseList;
            _this.selectedSourceCollection = new Array();
            list.forEach(function (element) {
                _this.selectedSourceCollection.push({
                    collectionName: element.collectionName,
                    collectionNameToggle: element.collectionNameToggle,
                    isNameError: false,
                    collectionMetadataResponseList: element.collectionMetadataResponseList,
                    toggleName: false,
                    toggleChildObject: false
                });
            });
            if (inResponse !== undefined) {
                inResponse.forEach(function (element) {
                    _this.filterResponseProcedure.push({
                        name: element.name,
                        type: element.type
                    });
                });
            }
            if (_this.sourceConnectionValue.type === 'ODATA') {
                _this.incrementalColumns = _this.selectedSourceCollection[0].collectionMetadataResponseList.filter(function (n) { return n.type === 'DateTimeOffset' && n.filterable === 'true'; }).map(function (n) { return n.name; });
                _this.incrementalColumns.splice(0, 0, '');
            }
            else if (_this.sourceConnectionValue.type !== 'ODATA' && _this.sourceCollectionValue.subcategory === 'table') {
                _this.incrementalColumns = _this.selectedSourceCollection[0].collectionMetadataResponseList.filter(function (n) { return n.type === 'datetime'; }).map(function (n) { return n.name; });
                _this.incrementalColumns.splice(0, 0, '');
            }
            else if (_this.sourceConnectionValue.type !== 'ODATA' && _this.sourceCollectionValue.subcategory === 'procedure') {
                _this.incrementalColumns = _this.filterResponseProcedure.filter(function (n) { return n.type === 'datetime'; }).map(function (n) { return n.name; });
            }
            if (_this.sourceConnectionValue.type === 'ODATA') {
                _this.sourceFilterableColumns = _this.selectedSourceCollection[0].collectionMetadataResponseList.filter(function (n) { return n.filterable === 'true'; }).map(function (n) { return n; });
            }
            else if (_this.sourceCollectionValue.subcategory === 'procedure' && _this.sourceConnectionValue.type !== 'ODATA') {
                _this.sourceFilterableColumns = _this.filterResponseProcedure.filter(function (n) { return n.name; }).map(function (n) { return n; });
            }
            else if (_this.sourceCollectionValue.subcategory === 'table' && _this.sourceConnectionValue.type !== 'ODATA') {
                _this.sourceFilterableColumns = _this.selectedSourceCollection[0].collectionMetadataResponseList.filter(function (n) { return n.name; });
            }
            _this.isSourceCollectionSelected = true;
        }, function (error) {
            _this.isProcessing = false;
            console.log(error);
        });
    };
    NewJobComponent.prototype.populateDestinationMetadata = function (connectionId, collectionName) {
        var _this = this;
        this.destinationConnectionType = localStorage.setItem('destinationConnectionValue', this.destinationConnectionValue.type);
        this.destinationValueType = this.destinationConnectionValue.name;
        this.apiUrl = this.sourceCollectionItemsUrl.filter(function (x) { return x.text === collectionName; }).map(function (n) { return n.apiUrl; });
        this.isProcessing = true;
        this.jobService.getTableMetadata(connectionId, collectionName, this.destinationCollectionValue.subcategory)
            .subscribe(function (response) {
            _this.isProcessing = false;
            var list = JSON.parse(JSON.stringify(response)).results;
            _this.selectedDestinationCollection = new Array();
            list.forEach(function (element) {
                if (_this.jobDetail === undefined || _this.jobDetail === null) {
                    _this.selectedDestinationCollection.push({
                        collectionName: element.collectionName,
                        collectionNameToggle: element.collectionNameToggle || false,
                        isNameError: element.isNameError || false,
                        collectionMetadataResponseList: element.collectionMetadataResponseList.map(function (n) { return new src_app_model_collection__WEBPACK_IMPORTED_MODULE_6__["Metadata"](n); }),
                        toggleName: false,
                        toggleChildObject: false
                    });
                }
                else {
                    var mapping_1 = JSON.parse(_this.jobDetail.mapping);
                    _this.selectedDestinationCollection.push({
                        collectionName: element.collectionName,
                        collectionNameToggle: element.collectionNameToggle || false,
                        isNameError: element.isNameError || false,
                        collectionMetadataResponseList: element.collectionMetadataResponseList.map(function (n) { return new src_app_model_collection__WEBPACK_IMPORTED_MODULE_6__["Metadata"](n, mapping_1); }),
                        toggleName: false,
                        toggleChildObject: false
                    });
                }
            });
            console.log(_this.selectedDestinationCollection);
            _this.isDestinationCollectionSelected = true;
            _this.collectionName = list[0].collectionName;
            _this.metadataName = list[0].collectionMetadataResponseList.map(function (n) { return n.name; });
            console.log(_this.collectionName);
            console.log(_this.metadataName);
            console.log(response);
        }, function (error) {
            _this.isProcessing = false;
            console.log(error);
        });
    };
    NewJobComponent.prototype.dragEnd = function (event) {
        console.log('Element was dragged', event);
    };
    NewJobComponent.prototype.onDrop = function (_a, index) {
        var dropData = _a.dropData;
        if (this.destinationConnectionValue.type === 'ODATA') {
            if (this.selectedDestinationCollection[0].collectionMetadataResponseList[index].updatable === 'false') {
            }
            else {
                this.selectedDestinationCollection[0].collectionMetadataResponseList[index].mapping = this.selectedSourceCollection[0].collectionName + "." + dropData;
            }
        }
        else {
            this.selectedDestinationCollection[0].collectionMetadataResponseList[index].mapping = this.selectedSourceCollection[0].collectionName + "." + dropData;
        }
    };
    NewJobComponent.prototype.selectionChangeDestinationCollection = function (event) {
        this.populateDestinationMetadata(this.destinationConnectionValue.connectionId, event.value);
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJob') {
            this.introJS.goToStepNumber(7).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithExistingTable') {
            this.introJS.goToStepNumber(7).start();
        }
        // if (this.route.snapshot.queryParams.action === 'intro' &&  this.cookieService.get('intro') === 'createNewJobw') {
        //   this.introJS.goToStepNumber(7).start();
        // }
    };
    NewJobComponent.prototype.getAllConnection = function () {
        var _this = this;
        var roles = this.selectedRoles;
        this.connectionService.getAllConnection(new src_app_connections_connections_component__WEBPACK_IMPORTED_MODULE_2__["Connection"](), 0, 36, 'createdDate', 'desc').subscribe(function (data) {
            _this.sourceConnections = Array();
            _this.destinationConnections = Array();
            _this.allConnectiona = Array();
            if (JSON.parse(JSON.stringify(data)).results[0].connectionResponseList !== null) {
                JSON.parse(JSON.stringify(data)).results[0].connectionResponseList.forEach(function (element) {
                    _this.allConnectiona.push(new src_app_connections_connections_component__WEBPACK_IMPORTED_MODULE_2__["Connection"](element));
                    if (roles.filter(function (n) { return n.role === 'Datahug'; }) && (roles.filter(function (n) { return n.role === 'Admin'; }).length === 0)) {
                        if (element.type === 'EXCEL' || element.type === 'DATAHUG') {
                            _this.sourceConnections.push(new src_app_connections_connections_component__WEBPACK_IMPORTED_MODULE_2__["Connection"](element));
                            _this.destinationConnections.push(new src_app_connections_connections_component__WEBPACK_IMPORTED_MODULE_2__["Connection"](element));
                        }
                    }
                    // else if(roles.filter(n => n.role ==='Admin')) {
                    else {
                        _this.sourceConnections.push(new src_app_connections_connections_component__WEBPACK_IMPORTED_MODULE_2__["Connection"](element));
                        _this.destinationConnections.push(new src_app_connections_connections_component__WEBPACK_IMPORTED_MODULE_2__["Connection"](element));
                    }
                });
                var obj = new src_app_connections_connections_component__WEBPACK_IMPORTED_MODULE_2__["Connection"]();
                obj.name = 'Select Connection';
                _this.sourceConnections.splice(0, 0, obj);
                _this.connectionsList = _this.sourceConnections;
                _this.sourceConnectionValue = obj;
                _this.destinationConnectionValue = obj;
                _this.route.paramMap.subscribe(function (params) {
                    if (params.get('jobId') !== 'new') {
                        _this.isProcessing = false;
                        _this.getJob(params.get('jobId'));
                    }
                });
            }
        });
    };
    NewJobComponent.prototype.onToggleSourceConnection = function (isSourceConnectionOption) {
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') !== 'createNewJobWithFilter') {
            this.introJS.goToStepNumber(5).start();
        }
        this.isSourceConnectionOption = !isSourceConnectionOption;
        // if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilter' && this.isSourceConnectionOption ===true) {
        //   this.introJS.goToStepNumber(5).start();
        // }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilter' && this.isSourceConnectionOption === false) {
            this.introJS.goToStepNumber(6).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilterWithExistingTable' && this.isSourceConnectionOption === false) {
            this.introJS.goToStepNumber(6).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithIncrementValue' && this.isSourceConnectionOption === false) {
            this.introJS.goToStepNumber(6).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithIncrementValueExistingtable' && this.isSourceConnectionOption === false) {
            this.introJS.goToStepNumber(6).start();
        }
    };
    NewJobComponent.prototype.onToggleDestinationConnection = function (isDestinationConnectionOption) {
        this.isDestinationConnectionOption = !isDestinationConnectionOption;
        if (this.route.snapshot.queryParams.action === 'intro' && this.isDestinationConnectionOption === true && this.cookieService.get('intro') === 'createNewJob') {
            this.introJS.goToStepNumber(7).start();
        }
        // this.createTable();
    };
    NewJobComponent.prototype.copyMetadata = function () {
        var _this = this;
        this.jobService.getOdataCollection(this.sourceConnectionValue.connectionId, this.sourceCollectionValue.value)
            .subscribe(function (response) {
            _this.isCreateTableRequested = true;
            var list = JSON.parse(JSON.stringify(response)).results;
            _this.selectedDestinationCollection = new Array();
            list.forEach(function (element) {
                _this.selectedDestinationCollection.push({
                    collectionName: element.collectionName,
                    collectionNameToggle: element.collectionNameToggle,
                    isNameError: element.collectionName.length > 30 ? true : false,
                    collectionMetadataResponseList: element.collectionMetadataResponseList.map(function (n) { return new src_app_model_collection__WEBPACK_IMPORTED_MODULE_6__["Metadata"](n); }),
                    toggleName: false,
                    toggleChildObject: false
                });
                _this.metadataName = element.collectionMetadataResponseList.map(function (n) { return new src_app_model_collection__WEBPACK_IMPORTED_MODULE_6__["Metadata"](n); });
            });
            if (_this.sourceCollectionValue.value.length > 30 && _this.destinationConnectionValue.type === 'ORACLE') {
                _this.toastr.warning('Table name length is more than 30 character');
            }
            if (_this.route.snapshot.queryParams.action === 'intro' && _this.isDestinationConnectionOption === false && _this.cookieService.get('intro') === 'createNewJob') {
                _this.introJS.goToStepNumber(8).start();
            }
            _this.selectedDestinationCollection.filter(function (n) { return n.collectionName.length > 30; }).map(function (n) { return n.isNameError = true; });
            _this.selectedDestinationCollection.map(function (n) { return n.collectionMetadataResponseList.filter(function (n) { return n.name.length > 30; }).map(function (n) { return n.isNameError = true; }); });
            console.log(_this.selectedDestinationCollection.map(function (n) { return n.collectionMetadataResponseList.filter(function (n) { return n.name.length > 30; }).map(function (n) { return n.isNameError = true; }); }));
            console.log((_this.selectedDestinationCollection.filter(function (n) { return n.collectionMetadataResponseList.filter(function (j) { return j.isNameError === true; }); })[0].collectionMetadataResponseList.length > 30));
            if (_this.selectedDestinationCollection.filter(function (n) { return n.collectionMetadataResponseList.filter(function (j) { return j.isNameError === true; }); })[0].collectionMetadataResponseList.length > 30 && _this.destinationConnectionValue.type === 'ORACLE') {
                _this.toastr.warning('Some column length is greater than 30, please scroll and change the column name.');
            }
            _this.isDestinationCollectionSelected = true;
        }, function (error) {
            console.log(error);
        });
    };
    NewJobComponent.prototype.clearCopyTable = function () {
        this.isCreateTableRequested = false;
        this.isDestinationCollectionSelected = false;
        this.selectedDestinationCollection = new Array();
    };
    NewJobComponent.prototype.createTable = function () {
        var _this = this;
        if (this.selectedDestinationCollection[0].collectionMetadataResponseList.filter(function (j) { return j.isNameError === true; }).length > 30) {
            this.toastr.warning('Some column length is greater than 30, please scroll and change the column name.');
            return false;
        }
        var table = {};
        table['tableName'] = this.selectedDestinationCollection[0].collectionName;
        table['tableMetadataList'] = [];
        this.selectedDestinationCollection[0].collectionMetadataResponseList.forEach(function (element) {
            var obj = new Object();
            obj['name'] = element.name;
            obj['type'] = element.type;
            table['tableMetadataList'].push(obj);
        });
        this.jobService.createTable(this.destinationConnectionValue.connectionId, table)
            .subscribe(function (response) {
            _this.isCreateTableRequested = false;
            var res = JSON.parse(JSON.stringify(response));
            _this.toastr.info(res.message);
            _this.isCreateTableRequested = !res.success;
            if (res.success) {
                if (_this.route.snapshot.queryParams.action === 'intro' && _this.cookieService.get('intro') === 'createNewJob') {
                    _this.introJS.goToStepNumber(9).start();
                }
            }
            console.log(response);
        }, function (error) {
            console.log(error);
        });
    };
    NewJobComponent.prototype.runJob = function () {
        var _this = this;
        // this.isDestinationCollectionSelected = false;
        if (this.destinationValueType === 'EXCEL') {
            this.jobService.getExcelExport(this.jobRequest.jobId).subscribe(function (res) {
                Object(_progress_kendo_file_saver__WEBPACK_IMPORTED_MODULE_11__["saveAs"])(res, 'export.xlsx');
            });
        }
        else if (this.sourceValueType === 'EXCEL') {
            this.jobService.upLoad(this.jobRequest.jobId, this.files).subscribe(function (res) {
                console.log(res);
            });
        }
        else {
            this.jobService.runJob(this.jobRequest.jobId)
                .subscribe(function (response) {
                var res = JSON.parse(JSON.stringify(response));
                _this.toastr.warning(res.message);
                if (res.message === 'Job Run Successfully') {
                    _this.isRunEnabled = true;
                }
                else {
                    _this.isRunEnabled = false;
                }
                _this.isDestinationCollectionSelected = true;
                _this.intervalObj = setInterval(function () {
                    _this.route.paramMap.subscribe(function (params) {
                        if (params.get('jobId') !== 'new') {
                            _this.checkProgress(params.get('jobId'));
                        }
                    });
                }, 1000);
            }, function (error) {
                console.log(error);
            });
        }
    };
    NewJobComponent.prototype.stopJob = function () {
        var _this = this;
        this.jobService.stopJob(this.jobRequest.jobId)
            .subscribe(function (response) {
            var res = JSON.parse(JSON.stringify(response));
            _this.toastr.warning(res.message);
            _this.isRunEnabled = false;
        }, function (error) {
            console.log(error);
        });
    };
    NewJobComponent.prototype.schduleJob = function () {
        var _this = this;
        var data = new Date();
        var schduleJob = new src_app_model_schdule__WEBPACK_IMPORTED_MODULE_12__["Schdule"]();
        // schduleJob.cronExpression = `0 ${data.getMinutes() + 5} ${data.getHours()} * * *`;
        schduleJob.cronExpression = this.cronExpression.substring(0, this.cronExpression.length - 2);
        this.jobService.scheduleJob(this.jobRequest.jobId, schduleJob)
            .subscribe(function (response) {
            var res = JSON.parse(JSON.stringify(response));
            _this.toastr.warning(res.message);
        }, function (error) {
            console.log(error);
        });
    };
    NewJobComponent.prototype.saveJob = function () {
        var _this = this;
        console.log(this.myFiles);
        if (this.jobRequest.jobName === '') {
            this.toastr.warning('Please enter the job name.');
            return false;
        }
        if (this.sourceConnectionValue.type === 'ODATA') {
            this.jobRequest.destinationCollection = this.selectedDestinationCollection.length > 0 ? this.selectedDestinationCollection[0].collectionName : '';
        }
        else if (this.sourceConnectionValue.type === 'ORACLE') {
            this.jobRequest.destinationCollection = this.selectedDestinationCollection.length > 0 ? this.selectedDestinationCollection[0].collectionName : '';
        }
        else {
            this.jobRequest.destinationCollection = this.apiUrl[0];
        }
        this.jobRequest.destinationConnection = this.destinationConnectionValue.connectionId;
        this.jobRequest.destinationConnectionName = this.destinationConnectionValue.name;
        if (this.jobRequest.sourceCollectionType !== '') {
            this.jobRequest.sourceCollectionType = this.connectionType.toUpperCase();
        }
        this.jobRequest.sourceCollection = this.selectedSourceCollection.length > 0 ? this.selectedSourceCollection[0].collectionName : '';
        this.jobRequest.sourceConnection = this.sourceConnectionValue.connectionId;
        this.jobRequest.sourceConnectionName = this.sourceConnectionValue.name;
        this.jobRequest.incrementalParameter = this.incrementalColumnSelectedValue;
        this.jobRequest.jobFilter = this.testFilterRequest['filterJson'];
        this.jobRequest.incrementalParameterValue = this.incrementColumnValue.getMonth() + 1 + "/" + this.incrementColumnValue.getDate() + "/" + this.incrementColumnValue.getFullYear() + " " + this.incrementColumnValue.getHours() + ":" + this.incrementColumnValue.getMinutes() + ":" + this.incrementColumnValue.getSeconds();
        console.log(this.incrementColumnValue);
        if (this.incrementalColumnSelectedValue === '') {
            this.jobRequest.incrementalParameterValue = '';
        }
        var object = {};
        if (this.selectedDestinationCollection.length > 0) {
            this.selectedDestinationCollection[0].collectionMetadataResponseList.forEach(function (element) {
                object[element.name] = element.mapping;
            });
        }
        this.jobRequest.mapping = JSON.stringify(object);
        if (this.jobRequest.jobId === '') {
            this.jobService.createjob(this.jobRequest)
                .subscribe(function (response) {
                _this.toastr.success('Job created successfully');
                var jobId = JSON.parse(JSON.stringify(response)).results[0].jobId;
                _this.jobRequest.jobId = jobId;
            }, function (error) {
                console.log(error);
            });
            if (this.route.snapshot.queryParams.action === 'intro') {
                this.introJS.goToStepNumber(9).start();
                this.introJS.exit();
            }
        }
        else {
            this.alterTableMetadata();
            //  this.jobRequest.mapping = this.jobDetail.mapping;
            this.jobService.updateJob(this.jobRequest)
                .subscribe(function (response) {
                _this.toastr.success('Job updated successfully');
                var jobId = JSON.parse(JSON.stringify(response)).results[0].jobId;
                _this.jobRequest.jobId = jobId;
            }, function (error) {
                console.log(error);
            });
        }
    };
    //clear() {
    // this.incrementColumnValue = new Date(this.jobRequest.incrementalParameterValue);
    // console.log(this.incrementColumnValue);
    //this.incrementColumnValue = new Date();
    //}
    NewJobComponent.prototype.saveTabOptions = function () {
        var _this = this;
        switch (this.selectedModelTabName) {
            case 'netChange':
                this.jobRequest.incrementalParameter = this.incrementalColumnSelectedValue;
                this.jobRequest.incrementalParameterValue = this.incrementColumnValue.getMonth() + 1 + "/" + this.incrementColumnValue.getDate() + "/" + this.incrementColumnValue.getFullYear() + " " + this.incrementColumnValue.getHours() + ":" + this.incrementColumnValue.getMinutes() + ":" + this.incrementColumnValue.getSeconds();
                break;
            case 'filter':
                if (this.dataFilters.length === 1 && this.dataFilters.filter(function (n) { return n.field === '' || n.comparisonOperator === '' || n.value === ''; }).length > 0 && this.connectionType !== 'procedure') {
                    this.opened = !this.opened;
                    return false;
                }
                if (this.dataFilters.filter(function (n) { return n.field === '' || n.comparisonOperator === '' || n.value === ''; }).length > 0 && this.connectionType === 'procedure') {
                    this.opened = !this.opened;
                }
                // const testFilterRequest = {};
                this.dataFilters.forEach(function (element) {
                    var parameters = Object.keys(element);
                    parameters.forEach(function (parameter) {
                        if (element[parameter] !== '') {
                            element[parameter] = element[parameter] === null ? '' : element[parameter];
                        }
                    });
                });
                this.testFilterRequest['filterJson'] = JSON.stringify(this.dataFilters);
                this.jobService.testFilterQuery(this.sourceConnectionValue.connectionId, this.selectedSourceCollection[0].collectionName, this.testFilterRequest)
                    .subscribe(function (response) {
                    var res = JSON.parse(JSON.stringify(response));
                    if (res.success === 'true') {
                        _this.jobRequest.jobFilter = _this.dataFilters.length > 0 ? JSON.stringify(_this.dataFilters) : '';
                        _this.dataPreviewerFilters = _this.dataFilters;
                    }
                    else {
                        _this.toastr.warning(res.message);
                    }
                });
                break;
            case 'dataPreviewer':
                break;
            default:
                break;
        }
        // this.opened = !this.opened;
        this.opened = false;
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilter') {
            this.introJS.goToStepNumber(11).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilterWithExistingTable') {
            this.introJS.goToStepNumber(11).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithIncrementValue') {
            this.introJS.goToStepNumber(11).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithIncrementValueExistingtable') {
            this.introJS.goToStepNumber(11).start();
        }
    };
    NewJobComponent.prototype.validateFilter = function () {
        var _this = this;
        if (this.dataFilters.length === 1 && this.dataFilters.filter(function (n) { return n.field === '' || n.comparisonOperator === '' || n.value === ''; }).length > 0) {
            return false;
        }
        var testFilterRequest = {};
        testFilterRequest['collectionName'] = this.sourceCollectionValue.value;
        this.dataFilters.forEach(function (element) {
            var parameters = Object.keys(element);
            parameters.forEach(function (parameter) {
                if (element[parameter] !== '') {
                    // this.isfilterValue = 1;
                    element[parameter] = element[parameter] === null ? '' : element[parameter];
                }
            });
        });
        testFilterRequest['filterJson'] = JSON.stringify(this.dataFilters);
        this.jobService.testFilterQuery(this.sourceConnectionValue.connectionId, this.selectedSourceCollection[0].collectionName, testFilterRequest)
            .subscribe(function (response) {
            console.log(response);
            var res = JSON.parse(JSON.stringify(response));
            if (res.success === 'true') {
                _this.toastr.success(res.message);
            }
            else {
                _this.toastr.warning(res.message);
            }
        });
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithExistingTable') {
            this.introJS.goToStepNumber(10).start();
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilterWithExistingTable') {
            this.introJS.goToStepNumber(10).start();
        }
    };
    NewJobComponent.prototype.checkProgress = function (jobId) {
        var _this = this;
        var progress = '0';
        this.showSpinner = true;
        this.jobService.gettJobStatus(jobId)
            .subscribe(function (response) {
            _this.stats = JSON.parse(JSON.stringify(response)).results[0];
            if (_this.stats.status === 'Completed') {
                clearInterval(_this.intervalObj);
                _this.isRunEnabled = false;
                _this.statsMessage = '';
                _this.showSpinner = false;
                progress = _this.getPercentageChange(_this.stats.totalRecordCount, _this.stats.processedRecord).toFixed(2);
                _this.statsMessage = "Copied " + _this.stats.processedRecord + " of " + _this.stats.totalRecordCount;
            }
            if (_this.stats.status === 'In Process') {
                _this.isRunEnabled = true;
                progress = _this.getPercentageChange(_this.stats.totalRecordCount, _this.stats.processedRecord).toFixed(2);
                _this.statsMessage = "Copied " + _this.stats.processedRecord + " of " + _this.stats.totalRecordCount;
            }
            if (_this.stats.status === 'Failed') {
                _this.isRunEnabled = false;
            }
        }, function (error) {
        });
    };
    NewJobComponent.prototype.getJob = function (jobId) {
        var _this = this;
        // if (jobId !== '' && this.destinationConnectionValue.type === 'ORACLE') {
        //   this.editIconVisibility = true;
        // }
        this.jobService.gettJob(jobId)
            .subscribe(function (response) {
            _this.jobDetail = JSON.parse(JSON.stringify(response)).results[0];
            _this.jobRequest.jobName = _this.jobDetail.jobName;
            _this.sourceConnectionValue = _this.sourceConnections.filter(function (n) { return n.name === _this.jobDetail.sourceConnectionName; })[0];
            _this.destinationConnectionValue = _this.sourceConnections.filter(function (n) { return n.name === _this.jobDetail.destinationConnectionName; })[0];
            _this.sourceConnections.forEach(function (element) {
                if (element.connectionId !== '' && element.type !== _this.sourceConnectionValue.type) {
                    element.isDisabled = false;
                }
            });
            _this.populateSourceCollection(_this.sourceConnectionValue.connectionId);
            _this.populateDestinationCollection(_this.destinationConnectionValue.connectionId);
            _this.jobRequest.jobId = _this.jobDetail.jobId;
            _this.jobRequest.incrementalParameter = _this.jobDetail.incrementalParameter;
            _this.jobRequest.incrementalParameterValue = _this.jobDetail.incrementalParameterValue;
            _this.incrementColumnValue = new Date(_this.jobRequest.incrementalParameterValue);
            _this.incrementalColumnSelectedValue = _this.jobRequest.incrementalParameter;
            if (_this.jobRequest.incrementalParameterValue !== '') {
                _this.isIncrementParameter = true;
                _this.incrementValue = 'allProcess';
            }
            if (_this.jobDetail.jobFilter !== '') {
                _this.dataFilters = JSON.parse(_this.jobDetail.jobFilter);
                _this.dataPreviewerFilters = JSON.parse(_this.jobDetail.jobFilter);
            }
        }, function (error) {
            console.log(error);
        });
    };
    NewJobComponent.prototype.close = function (status) {
        console.log("Dialog result: " + status);
        this.opened = false;
    };
    NewJobComponent.prototype.openTab = function (tabName) {
        var _this = this;
        // this.dataFilters = [];
        var columnLength = this.sourceFilterableColumns.length;
        this.inResponseName = [];
        this.inResponseType = [];
        this.selectedModelTabName = tabName;
        if (tabName === 'dataPreviewer') {
            this.dataPreviewerFilters = this.dataFilters;
            this.getDataForPreview();
        }
        else if (tabName === 'logView') {
            this.height = 500;
            this.getLogView();
        }
        else if (tabName === 'filter' && this.sourceCollectionValue.subcategory === 'table') {
            if (this.dataFilters.length === 0) {
                this.dataFilters.push(new src_app_model_data_filter__WEBPACK_IMPORTED_MODULE_13__["DataFilter"]());
            }
        }
        else if (tabName === 'filter' && this.sourceCollectionValue.subcategory === 'procedure') {
            // this.filterResponseProcedure = [];
            // this.filterResponseProcedure.forEach(element => {
            //   this.inResponseName.push({ comparisonOperator: '', field: '', logicalOperator: '', value: '', dataType: '' });
            //   this.inResponseType.push({ name: element.name, type: element.type });
            // });
            // this.selectionChangeDataType(this.inResponseType, this.inResponseName);
            if (columnLength > this.dataFilters.length) {
                this.sourceFilterableColumns.map(function (column) {
                    _this.dataFilters.push({ comparisonOperator: '', field: column.name, logicalOperator: '', value: '', dataType: column.type });
                });
            }
        }
        this.opened = true;
    };
    NewJobComponent.prototype.refreshSourceMetadata = function () {
        var _this = this;
        this.showSpinner = true;
        if (this.sourceConnectionValue.type === 'Odata' || this.sourceConnectionValue.type === 'OData_Working') {
            this.jobService.refreshOdataCollectionMetadata(this.sourceConnectionValue.connectionId).subscribe(function (data) {
                console.log(data);
                var refreshMetadata = JSON.parse(JSON.stringify(data)).message;
                _this.toastr.warning(refreshMetadata);
                _this.showSpinner = false;
            });
        }
    };
    NewJobComponent.prototype.destinationRefreshMetadata = function () {
        var _this = this;
        this.showSpinner = true;
        if (this.destinationConnectionValue.type === 'Odata' || this.sourceConnectionValue.type === 'OData_Working') {
            this.jobService.refreshOdataCollectionMetadata(this.destinationConnectionValue.connectionId).subscribe(function (data) {
                console.log(data);
                var refreshMetadata = JSON.parse(JSON.stringify(data)).message;
                _this.toastr.warning(refreshMetadata);
                _this.showSpinner = false;
            });
        }
    };
    NewJobComponent.prototype.addFilter = function () {
        this.dataFilters.push(new src_app_model_data_filter__WEBPACK_IMPORTED_MODULE_13__["DataFilter"]());
    };
    NewJobComponent.prototype.removeFilter = function (index) {
        this.dataFilters.splice(index, 1);
    };
    NewJobComponent.prototype.addDataPreviewerFilters = function () {
        this.dataPreviewerFilters.push(new src_app_model_data_filter__WEBPACK_IMPORTED_MODULE_13__["DataFilter"]());
    };
    NewJobComponent.prototype.removeDataPreviewerFilters = function (index) {
        this.dataPreviewerFilters.splice(index, 1);
    };
    NewJobComponent.prototype.getPercentageChange = function (oldNumber, newNumber) {
        var decreaseValue = oldNumber - newNumber;
        return (decreaseValue / oldNumber) * .1;
    };
    NewJobComponent.prototype.getDataForPreview = function () {
        var _this = this;
        this.sourceGridLoading = true;
        var testFilterRequest = {};
        // this.dataPreviewerFilters = this.dataFilters;
        this.dataPreviewerFilters.forEach(function (element) {
            var parameters = Object.keys(element);
            parameters.forEach(function (parameter) {
                if (element[parameter] !== '') {
                    element[parameter] = element[parameter] === null ? '' : element[parameter];
                }
            });
        });
        testFilterRequest['previewFilter'] = JSON.stringify(this.dataPreviewerFilters);
        if (this.dataFilters.length === 1 && this.dataFilters.filter(function (n) { return n.field === '' || n.comparisonOperator === '' || n.value === ''; }).length > 0) {
            testFilterRequest['previewFilter'] = '';
        }
        this.jobService.getDataForPreview(this.sourceConnectionValue.connectionId, this.selectedSourceCollection[0].collectionName, this.sourceCollectionValue.subcategory.toUpperCase(), this.jobFilter.startRecord, this.jobFilter.recordCount, testFilterRequest)
            .subscribe(function (response) {
            var res = JSON.parse(JSON.stringify(response));
            console.log(res);
            console.log(res.success);
            if (res.success === true) {
                var list = JSON.parse(JSON.stringify(response)).results[0];
                console.log(list.reviewList);
                _this.sourceGridDataResult = list.reviewList;
                _this.sourceGridLoading = true;
                _this.destinationGridLoading = true;
                _this.previewSourceData();
                _this.previewDestinationData();
            }
            else {
                _this.toastr.warning(res.message);
            }
        });
    };
    NewJobComponent.prototype.getLogView = function () {
        var _this = this;
        this.jobService.logView(this.jobRequest.jobId)
            .subscribe(function (response) {
            var res = JSON.parse(JSON.stringify(response));
            //this.toastr.warning(res.message);
            var list = JSON.parse(JSON.stringify(response)).results;
            var dataPreviewGridHeader = Object.keys(list[0]);
            _this.logViewGridDataResult = {
                data: list,
                total: list.length
            };
        }, function (error) {
            console.log(error);
        });
    };
    NewJobComponent.prototype.previewSourceData = function () {
        var _this = this;
        this.sourceGridLoading = true;
        this.dataPreviewerFilters.forEach(function (element) {
            var parameters = Object.keys(element);
            parameters.forEach(function (parameter) {
                element[parameter] = element[parameter] === null ? '' : element[parameter];
            });
        });
        // testFilterRequest['filterJson'] = JSON.stringify(this.dataPreviewerFilters);
        var filter = new Object();
        filter['previewFilter'] = this.dataPreviewerFilters.filter(function (n) { return n.field !== ''; }).length > 0 ? JSON.stringify(this.dataPreviewerFilters) : '';
        this.jobService.getDataForPreview(this.sourceConnectionValue.connectionId, this.sourceCollectionValue.value, this.sourceCollectionValue.subcategory.toUpperCase(), this.sourceItemsSkip, 1, filter)
            .subscribe(function (response) {
            var list = JSON.parse(JSON.stringify(response)).results[0];
            var parameters = Object.keys(list.reviewList[0]);
            _this.sourceDataPreviewGridHeader = ['Name', 'Value'];
            var dataCollection = [];
            list.reviewList.forEach(function (data) {
                parameters.forEach(function (parameter) {
                    dataCollection.push({ Name: parameter, Value: data[parameter] });
                });
            });
            _this.sourceGridDataResult = {
                data: dataCollection,
                total: list.totalRecordCount
            };
            _this.sourceGridLoading = false;
        });
    };
    NewJobComponent.prototype.previewDestinationData = function () {
        var _this = this;
        this.destinationGridLoading = true;
        var filter = new Object();
        filter['previewFilter'] = this.dataPreviewerFilters.filter(function (n) { return n.field !== ''; }).length > 0 ? JSON.stringify(this.dataPreviewerFilters) : '';
        this.jobService.getDataForPreview(this.destinationConnectionValue.connectionId, this.destinationCollectionValue.value, this.sourceCollectionValue.subcategory.toUpperCase(), this.destinationItemsSkip, 10, filter)
            .subscribe(function (response) {
            var list = JSON.parse(JSON.stringify(response)).results[0];
            var parameters = Object.keys(list.reviewList[0]);
            _this.destinationDataPreviewGridHeader = ['Name', 'Value'];
            var dataCollection = [];
            list.reviewList.forEach(function (data) {
                parameters.forEach(function (parameter) {
                    dataCollection.push({ Name: parameter, Value: data[parameter] });
                });
            });
            _this.destinationGridDataResult = {
                data: dataCollection,
                total: list.totalRecordCount
            };
            _this.destinationGridLoading = false;
        });
    };
    NewJobComponent.prototype.sourceItemsPageChange = function (event) {
        this.sourceItemsSkip = event.skip;
        this.previewSourceData();
    };
    NewJobComponent.prototype.destinationItemsPageChange = function (event) {
        this.destinationItemsSkip = event.skip;
        this.previewDestinationData();
    };
    NewJobComponent.prototype.autoLinking = function () {
        var _this = this;
        if (this.sourceConnectionValue.type === 'ORACLE') {
            this.selectedDestinationCollection[0].collectionMetadataResponseList.forEach(function (element) {
                if (element.updatable === 'false') {
                    // this.selectedDestinationCollection[0].collectionMetadataResponseList = this.selectedDestinationCollection[0].collectionMetadataResponseList.filter(n => n.creatable = element.updatable);
                }
                else {
                    _this.selectedDestinationCollection[0].collectionMetadataResponseList = _this.selectedDestinationCollection[0].collectionMetadataResponseList.filter(function (n) { return n.creatable = element.updatable; });
                    element.mapping = _this.selectedSourceCollection[0].collectionName + "." + element.name.toUpperCase();
                }
            });
        }
        else {
            this.selectedDestinationCollection[0].collectionMetadataResponseList.map(function (m) { return m.mapping = _this.selectedSourceCollection[0].collectionMetadataResponseList.filter(function (n) { return n.name.toLocaleLowerCase() === m.name.toLocaleLowerCase(); }).length > 0 ? _this.selectedSourceCollection[0].collectionName + "." + _this.selectedSourceCollection[0].collectionMetadataResponseList.filter(function (n) { return n.name.toLocaleLowerCase() === m.name.toLocaleLowerCase(); })[0].name : ''; });
        }
        // const mapping = map = this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === name.toLocaleLowerCase()).length > 0 ? `${this.selectedSourceCollection[0].collectionName}.${this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === name.toLocaleLowerCase())[0].name}` : `${this.selectedSourceCollection[0].collectionName}.${name}`;
        // metadata.isNameError = name.length > 30 || this.selectedDestinationCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === name.toLocaleLowerCase()).length > 1 ? true : false;
        // const mapping = metadata.mapping = this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === metadata.name.toLocaleLowerCase()).length > 0 ? `${this.selectedSourceCollection[0].collectionName}.${this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === metadata.name.toLocaleLowerCase())[0].name}` : `${this.selectedSourceCollection[0].collectionName}.${metadata.name}`;
        // metadata.isNameError =  metadata.name.length > 30 || this.selectedDestinationCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() ===  metadata.name.toLocaleLowerCase()).length > 1 ? true : false;
        //  const mapping =  metadata.map(m => m.mapping = this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === m.name.toLocaleLowerCase()).length > 0 ? `${this.selectedSourceCollection[0].collectionName}.${this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() ===  m.name.toLocaleLowerCase())[0].name}` : `${this.selectedSourceCollection[0].collectionName}.${ m.name}`);
        // console.log(metadata.mapping);
        //   if(this.selectedDestinationCollection[0].collectionMetadataResponseList.filter(n => n.creatable === 'false' ) && this.selectedDestinationCollection[0].collectionMetadataResponseList.filter(n => n.updatable === 'false' )) {
        //   } else {
        //   this.selectedDestinationCollection[0].collectionMetadataResponseList.map(m => m.mapping = this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === m.name.toLocaleLowerCase()).length > 0 ? `${this.selectedSourceCollection[0].collectionName}.${this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === m.name.toLocaleLowerCase())[0].name}` : '');
        // }
    };
    NewJobComponent.prototype.changeMetaMapping = function (e, index) {
        this.selectedDestinationCollection[0].collectionMetadataResponseList[index].mapping = '';
    };
    // autoLinking(metadata: Metadata) {
    //   console.log(this.selectedDestinationCollection[0].collectionMetadataResponseList);
    // }
    NewJobComponent.prototype.checkColumnName = function (metadata) {
        if (this.selectedSourceCollection[0].collectionMetadataResponseList.filter(function (n) { return n.name.toLocaleLowerCase() === metadata.name.toLocaleLowerCase(); }).length > 1) {
            this.toastr.warning(this.selectedSourceCollection[0].collectionMetadataResponseList.filter(function (n) { return n.name.toLocaleLowerCase() === metadata.name.toLocaleLowerCase(); })[0].name + " column exist multiple times");
        }
        if (metadata.name.length <= 30 && this.destinationConnectionValue.type === 'ORACLE') {
            //const checkCondition = this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === metadata.name).map(n => n.isNameError);
            console.log(metadata.name);
            metadata.isNameError = false;
            this.selectedSourceCollection[0].collectionMetadataResponseList.filter(function (n) { return n.name.toLocaleLowerCase() === metadata.name; });
        }
        else if (metadata.name.length > 30 && this.destinationConnectionValue.type === 'ORACLE') {
            metadata.isNameError = true;
        }
    };
    NewJobComponent.prototype.tableName = function (tableName) {
        if (this.destinationConnectionValue.type === 'ORACLE' && tableName.length <= 30) {
            tableName.toggleName = true;
        }
        else if (this.destinationConnectionValue.type === 'ORACLE' && tableName.length > 30) {
            tableName.toggleName = false;
        }
    };
    NewJobComponent.prototype.filterSourceCollection = function (value) {
        this.sourceCollection = this.sourceCollectionList.filter(function (s) { return s.value.toLowerCase().indexOf(value.toLowerCase()) !== -1; });
    };
    NewJobComponent.prototype.filterDestinationCollection = function (value) {
        this.destinationCollection = this.destinationCollectionList.filter(function (s) { return s.value.toLowerCase().indexOf(value.toLowerCase()) !== -1; });
    };
    NewJobComponent.prototype.filterDestinationConnection = function (value) {
        this.destinationConnections = this.connectionsList.filter(function (n) { return n.name.toLowerCase().indexOf(value.toLowerCase()) !== -1; });
    };
    NewJobComponent.prototype.filterSourceConnection = function (value) {
        this.sourceConnections = this.connectionsList.filter(function (n) { return n.name.toLowerCase().indexOf(value.toLowerCase()) !== -1; });
    };
    NewJobComponent.prototype.validateCollection = function (collection) {
        collection.collectionNameToggle = collection.collectionName.length > 30 ? true : false;
    };
    NewJobComponent.prototype.validateCollectionParameter = function (metadata) {
        return metadata.isNameError = metadata.name.length > 30 ? true : false;
    };
    NewJobComponent.prototype.syncMetadata = function () {
        var _this = this;
        var jobRequest = new src_app_model_job__WEBPACK_IMPORTED_MODULE_7__["JobRequest"]();
        jobRequest.jobId = this.jobRequest.jobId;
        this.jobService.syncMetadata(jobRequest)
            .subscribe(function (response) {
            _this.metadataDifference = JSON.parse(JSON.stringify(response)).results[0];
            _this.selectedSourceCollection.forEach(function (element) {
                element.collectionMetadataResponseList.forEach(function (metadata) {
                    metadata.isNewColumn = _this.metadataDifference.viewJobDifferenceDetailsResponseList.filter(function (n) { return n.fieldName === metadata.name && n.status === 'New'; }).length > 0;
                    metadata.isDeletedColumn = _this.metadataDifference.viewJobDifferenceDetailsResponseList.filter(function (n) { return n.fieldName === metadata.name && n.status === 'Delete'; }).length > 0;
                });
            });
            if (_this.metadataDifference.viewJobDifferenceDetailsResponseList.filter(function (n) { return n.status === 'New'; }).length > 0) {
                _this.isSyncAble = true;
            }
            else {
                _this.toastr.success('No change found in metadata');
            }
        }, function (error) {
            console.log(error);
        });
    };
    NewJobComponent.prototype.getUpdatedTableMetadata = function () {
        var _this = this;
        this.isProcessing = true;
        this.jobService.getUpdatedTableMetadata(this.jobRequest.jobId)
            .subscribe(function (response) {
            var list = JSON.parse(JSON.stringify(response)).results;
            _this.selectedDestinationCollection = new Array();
            list.forEach(function (element) {
                _this.selectedDestinationCollection.push({
                    collectionName: element.collectionName,
                    collectionNameToggle: element.collectionNameToggle || false,
                    isNameError: element.isNameError || false,
                    collectionMetadataResponseList: element.collectionMetadataResponseList.map(function (n) { return new src_app_model_collection__WEBPACK_IMPORTED_MODULE_6__["Metadata"](n); }),
                    toggleName: false,
                    toggleChildObject: false
                });
            });
            _this.selectedDestinationCollection.forEach(function (n) {
                n.collectionMetadataResponseList.forEach(function (j) {
                    j.isNewColumn = _this.selectedSourceCollection.filter(function (s) { return s.collectionName.toLocaleLowerCase() === n.collectionName.toLocaleLowerCase(); })[0].collectionMetadataResponseList.filter(function (k) { return k.name.toLocaleLowerCase() === j.name.toLocaleLowerCase() && k.isNewColumn === true; }).length > 0;
                });
            });
            _this.toastr.success('Target metadata synced');
            _this.isSyncAble = false;
            _this.isProcessing = false;
            _this.isNewSyncMetadataUpdated = true;
        }, function (error) {
            console.log(error);
        });
    };
    NewJobComponent.prototype.alterTableMetadata = function () {
        var _this = this;
        if (this.selectedDestinationCollection[0].collectionMetadataResponseList.filter(function (n) { return n.isNewColumn === true; }).length === 0) {
            return false;
        }
        this.isProcessing = true;
        var table = {};
        table['tableName'] = this.selectedDestinationCollection[0].collectionName;
        table['tableMetadataList'] = [];
        this.selectedDestinationCollection[0].collectionMetadataResponseList.filter(function (n) { return n.isNewColumn === true; }).forEach(function (element) {
            var obj = new Object();
            obj['name'] = element.name;
            obj['type'] = element.type;
            table['tableMetadataList'].push(obj);
        });
        this.jobService.saveJobsMetadataDifference(this.destinationConnectionValue.connectionId, table)
            .subscribe(function (response) {
            _this.isProcessing = false;
            _this.isNewSyncMetadataUpdated = true;
            _this.updateTableMetadata();
        }, function (error) {
            _this.isProcessing = false;
        });
    };
    NewJobComponent.prototype.updateTableMetadata = function () {
        var _this = this;
        if (this.selectedDestinationCollection[0].collectionMetadataResponseList.filter(function (n) { return n.isNewColumn === true; }).length === 0) {
            return false;
        }
        this.isProcessing = true;
        var job = {};
        job['jobId'] = this.jobRequest.jobId;
        var object = {};
        this.selectedDestinationCollection[0].collectionMetadataResponseList.filter(function (n) { return n.isNewColumn === true; }).forEach(function (element) {
            object[element.name] = _this.selectedSourceCollection[0].collectionName + "." + _this.selectedSourceCollection[0].collectionMetadataResponseList.filter(function (n) { return n.name.toLocaleLowerCase() === element.name.toLocaleLowerCase(); })[0].name;
        });
        job['newMapping'] = JSON.stringify(object);
        this.jobService.updateTableMetadata(this.jobRequest.jobId, job)
            .subscribe(function (response) {
            _this.isProcessing = false;
        }, function (error) {
            _this.isProcessing = false;
        });
    };
    NewJobComponent.prototype.editColumnName = function (metadata) {
        var mapping = this.selectedSourceCollection[0].collectionMetadataResponseList.filter(function (n) { return n.name.toLocaleLowerCase() === metadata.name.toLocaleLowerCase(); }).length > 0 ? this.selectedSourceCollection[0].collectionName + "." + this.selectedSourceCollection[0].collectionMetadataResponseList.filter(function (n) { return n.name.toLocaleLowerCase() === metadata.name.toLocaleLowerCase(); })[0].name : this.selectedSourceCollection[0].collectionName + "." + metadata.name;
        console.log(metadata.name.length);
        // this.editColumn = metadata.name.length;
    };
    NewJobComponent.prototype.onTabSelect = function (e) {
        switch (e.title) {
            case 'Data Previewer':
                // this.getDataForPreview();
                break;
            case 'View Log':
                this.getLogView();
                break;
            default:
                break;
        }
    };
    NewJobComponent.prototype.documentClick = function (event) {
        if (event.srcElement.nodeName !== 'svg' && event.srcElement.className.baseVal !== 'icon_connection') {
            this.isDestinationConnectionOption = false;
            this.isSourceConnectionOption = false;
            // this.isSchedulerHide = false;
        }
        else {
        }
    };
    NewJobComponent.prototype.ngOnDestroy = function () {
        clearInterval(this.intervalObj);
        this.introJS.exit();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('document:click', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], NewJobComponent.prototype, "documentClick", null);
    NewJobComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-new-job',
            template: __webpack_require__(/*! ./new-job.component.html */ "./src/app/jobs/new-job/new-job.component.html"),
            styles: [__webpack_require__(/*! ./new-job.component.scss */ "./src/app/jobs/new-job/new-job.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_connections_connection_service__WEBPACK_IMPORTED_MODULE_3__["ConnectionService"],
            ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"],
            _shared_tour_guide_tour_guide_component__WEBPACK_IMPORTED_MODULE_14__["TourGuideComponent"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"],
            _job_service__WEBPACK_IMPORTED_MODULE_5__["JobService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_10__["ToastrService"],
            _shared_data_service__WEBPACK_IMPORTED_MODULE_9__["DataService"],
            _app_session_service__WEBPACK_IMPORTED_MODULE_17__["SessionService"],
            _settings_users_users_service__WEBPACK_IMPORTED_MODULE_18__["UserService"],
            _settings_roles_roles_service__WEBPACK_IMPORTED_MODULE_19__["RolesService"]])
    ], NewJobComponent);
    return NewJobComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"login-box\">\n  <div class=\"login-logo\">\n    <a href=\"#\">Login</a>\n  </div>\n  <!-- /.login-logo -->\n  <div class=\"login-box-body\">\n    <p class=\"login-box-msg\">Login</p>\n\n    <form action=\"../../index2.html\" method=\"post\">\n      <div class=\"form-group has-feedback\">\n        <input type=\"email\" class=\"form-control\" placeholder=\"Email\" [(ngModel)]=\"username\" name=\"username\">\n        <span class=\"glyphicon glyphicon-envelope form-control-feedback\"></span>\n      </div>\n      <div class=\"form-group has-feedback\">\n        <input type=\"password\" class=\"form-control\" placeholder=\"Password\" [(ngModel)]=\"password\" name=\"password\">\n        <span class=\"glyphicon glyphicon-lock form-control-feedback\"></span>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-12\">\n          <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\" (click)=\"doSignIn()\">Sign In</button>\n        </div>\n        <!-- /.col -->\n      </div>\n    </form>\n\n  </div>\n  <!-- /.login-box-body -->\n</div>"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _model_connector__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../model/connector */ "./src/app/model/connector.ts");
/* harmony import */ var _settings_users_users_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../settings/users/users.service */ "./src/app/settings/users/users.service.ts");
/* harmony import */ var _shared_shared_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared/shared.service */ "./src/app/shared/shared.service.ts");
/* harmony import */ var _app_session_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../app.session-service */ "./src/app/app.session-service.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../app.component */ "./src/app/app.component.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");









var LoginComponent = /** @class */ (function () {
    function LoginComponent(route, userService, sharedService, sessionSevice, appcomponent, toastr) {
        this.route = route;
        this.userService = userService;
        this.sharedService = sharedService;
        this.sessionSevice = sessionSevice;
        this.appcomponent = appcomponent;
        this.toastr = toastr;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.connectorField = new _model_connector__WEBPACK_IMPORTED_MODULE_3__["Connector"]();
        this.username = '';
        this.password = '';
    };
    LoginComponent.prototype.doSignIn = function () {
        var _this = this;
        this.userService.GetSignInUser(this.username).subscribe(function (data) {
            var response = JSON.parse(JSON.stringify(data));
            if (_this.password !== 'welcome1') {
                _this.toastr.info('Password is incorrect');
            }
            if (response.success === false) {
                _this.toastr.info(response.message);
            }
            if (response.success && _this.password === 'welcome1') {
                _this.sessionSevice.setCookie('LOGIN', JSON.stringify(response));
                _this.sharedService.isFullScreen = false;
                _this.sessionSevice.setData();
                _this.appcomponent.loginAs();
                _this.route.navigate(['dashboard']);
            }
        }, function (error) {
        });
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _settings_users_users_service__WEBPACK_IMPORTED_MODULE_4__["UserService"],
            _shared_shared_service__WEBPACK_IMPORTED_MODULE_5__["SharedService"],
            _app_session_service__WEBPACK_IMPORTED_MODULE_6__["SessionService"],
            _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_8__["ToastrService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/model/EnumType.ts":
/*!***********************************!*\
  !*** ./src/app/model/EnumType.ts ***!
  \***********************************/
/*! exports provided: EnumType, EnumPromotion */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnumType", function() { return EnumType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnumPromotion", function() { return EnumPromotion; });
var EnumType;
(function (EnumType) {
    EnumType["SUPER_ADMIN"] = "Admin";
    EnumType["USER"] = "User";
    EnumType["SOM"] = "SOM";
    EnumType["L2"] = "L2";
    EnumType["DATA_HUG"] = "DATAHUG";
})(EnumType || (EnumType = {}));
var EnumPromotion;
(function (EnumPromotion) {
    EnumPromotion[EnumPromotion["DRAFT"] = 1] = "DRAFT";
    EnumPromotion[EnumPromotion["SUBMITTED"] = 2] = "SUBMITTED";
    EnumPromotion[EnumPromotion["APPROVED"] = 3] = "APPROVED";
    EnumPromotion[EnumPromotion["REJECTED"] = 4] = "REJECTED";
    EnumPromotion[EnumPromotion["APPEALED"] = 5] = "APPEALED";
    EnumPromotion[EnumPromotion["CLONE"] = 12] = "CLONE";
})(EnumPromotion || (EnumPromotion = {}));


/***/ }),

/***/ "./src/app/model/collection.ts":
/*!*************************************!*\
  !*** ./src/app/model/collection.ts ***!
  \*************************************/
/*! exports provided: Collection, Metadata, Difference */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Collection", function() { return Collection; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Metadata", function() { return Metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Difference", function() { return Difference; });
var Collection = /** @class */ (function () {
    function Collection(collection) {
        collection = collection || {};
        this.collectionName = collection.collectionName || '';
        // this.filterable = collection.type || '';
        this.collectionNameToggle = collection.collectionNameToggle || false;
        this.isNameError = collection.isNameError || false;
        this.collectionMetadataResponseList = collection.collectionMetadataResponseList || Array();
        this.toggleChildObject = this.toggleChildObject || false;
        this.toggleName = this.toggleName || false;
    }
    return Collection;
}());

var Metadata = /** @class */ (function () {
    function Metadata(metadata, mappingObject) {
        var _this = this;
        metadata = metadata || {};
        this.creatable = metadata.creatable || '';
        this.filterable = metadata.filterable || 'false';
        this.maxLength = metadata.maxLength || '';
        this.name = metadata.name || '';
        this.nullable = metadata.nullable || '';
        this.type = metadata.type || '';
        this.updatable = metadata.updatable || '';
        this.toggleName = metadata.toggleName || false;
        this.toggleType = metadata.toggleType || false;
        if (localStorage.getItem('destinationConnectionValue') === 'destinationConnectionValue') {
            this.isNameError = metadata.name.length > 30 ? true : false;
        }
        else {
            this.isNameError = false;
        }
        this.isNewColumn = metadata.isNewColumn || false;
        this.isDeletedColumn = metadata.isDeletedColumn || false;
        this.mapping = metadata.mapping || '';
        if (mappingObject !== undefined && mappingObject !== null) {
            var keys = Object.keys(mappingObject);
            keys.forEach(function (key) {
                if (key.toUpperCase() === metadata.name.toUpperCase()) {
                    _this.mapping = mappingObject[key];
                }
            });
        }
    }
    return Metadata;
}());

var Difference = /** @class */ (function () {
    function Difference() {
    }
    return Difference;
}());



/***/ }),

/***/ "./src/app/model/connection.ts":
/*!*************************************!*\
  !*** ./src/app/model/connection.ts ***!
  \*************************************/
/*! exports provided: Connection, ConnectionConfigRequestList, ConnectionFilter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Connection", function() { return Connection; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConnectionConfigRequestList", function() { return ConnectionConfigRequestList; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConnectionFilter", function() { return ConnectionFilter; });
var Connection = /** @class */ (function () {
    function Connection() {
        this.connectionConfigRequestList = new Array();
    }
    return Connection;
}());

var ConnectionConfigRequestList = /** @class */ (function () {
    function ConnectionConfigRequestList() {
    }
    return ConnectionConfigRequestList;
}());

var ConnectionFilter = /** @class */ (function () {
    function ConnectionFilter(connectionFilter) {
        connectionFilter = connectionFilter || {};
        this.type = connectionFilter.connectionName || '';
        this.name = connectionFilter.aliasName || '';
        this.aliasName = connectionFilter.serviceName || '';
        this.startRecord = connectionFilter.host || 0;
        this.recordCount = connectionFilter.port || 10;
        this.sortableColumn = connectionFilter.sortableColumn || 'createdDate';
        this.sortingType = connectionFilter.password || 'desc';
    }
    return ConnectionFilter;
}());



/***/ }),

/***/ "./src/app/model/connector.ts":
/*!************************************!*\
  !*** ./src/app/model/connector.ts ***!
  \************************************/
/*! exports provided: Connector */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Connector", function() { return Connector; });
var Connector = /** @class */ (function () {
    function Connector(connector) {
        connector = connector || {};
        this.connectionName = connector.connectionName || '';
        this.aliasName = connector.aliasName || '';
        this.serviceName = connector.serviceName || '';
        this.host = connector.host || '';
        this.port = connector.port || '';
        this.userName = connector.userName || '';
        this.password = connector.password || '';
        this.url = connector.url || '';
        this.schemaName = connector.schemaName || '';
    }
    return Connector;
}());



/***/ }),

/***/ "./src/app/model/data-filter.ts":
/*!**************************************!*\
  !*** ./src/app/model/data-filter.ts ***!
  \**************************************/
/*! exports provided: DataFilter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataFilter", function() { return DataFilter; });
var DataFilter = /** @class */ (function () {
    function DataFilter(dataFilter) {
        dataFilter = dataFilter || {};
        this.comparisonOperator = dataFilter.comparisonOperator || '';
        this.field = dataFilter.field || '';
        this.logicalOperator = dataFilter.logicalOperator || '';
        this.value = dataFilter.value || '';
        this.dataType = dataFilter.dataType || '';
    }
    return DataFilter;
}());



/***/ }),

/***/ "./src/app/model/feature.ts":
/*!**********************************!*\
  !*** ./src/app/model/feature.ts ***!
  \**********************************/
/*! exports provided: Feature, FeatureId */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Feature", function() { return Feature; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeatureId", function() { return FeatureId; });
var Feature = /** @class */ (function () {
    function Feature() {
    }
    return Feature;
}());

var FeatureId = /** @class */ (function () {
    function FeatureId() {
        this.featureId = Array();
    }
    return FeatureId;
}());



/***/ }),

/***/ "./src/app/model/job.ts":
/*!******************************!*\
  !*** ./src/app/model/job.ts ***!
  \******************************/
/*! exports provided: Job, JobDifferenceDetails, JobFilter, JobRequest */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Job", function() { return Job; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobDifferenceDetails", function() { return JobDifferenceDetails; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobFilter", function() { return JobFilter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobRequest", function() { return JobRequest; });
var Job = /** @class */ (function () {
    function Job() {
        this.toggleInfo = false;
    }
    return Job;
}());

var JobDifferenceDetails = /** @class */ (function () {
    function JobDifferenceDetails() {
    }
    return JobDifferenceDetails;
}());

var JobFilter = /** @class */ (function () {
    function JobFilter(jobFilter) {
        jobFilter = jobFilter || {};
        this.jobName = jobFilter.jobName || '';
        this.startRecord = jobFilter.host || 0;
        this.recordCount = jobFilter.port || 10;
        this.sortableColumn = jobFilter.sortableColumn || 'created';
        this.sortingType = jobFilter.password || 'desc';
    }
    return JobFilter;
}());

var JobRequest = /** @class */ (function () {
    function JobRequest(jobRequest) {
        jobRequest = jobRequest || {};
        this.createdBy = jobRequest.createdBy || '';
        this.destinationCollection = jobRequest.destinationCollection || '';
        this.destinationConnection = jobRequest.destinationConnection || '';
        this.destinationConnectionName = jobRequest.destinationConnectionName || '';
        this.jobId = jobRequest.jobId || '';
        this.sourceCollectionType = jobRequest.sourceCollectionType || '';
        this.jobFilter = jobRequest.jobFilter || '';
        this.jobName = jobRequest.jobName || '';
        this.mapping = jobRequest.mapping || '';
        this.sourceCollection = jobRequest.sourceCollection || '';
        this.sourceConnection = jobRequest.sourceConnection || '';
        this.sourceConnectionName = jobRequest.sourceConnectionName || '';
        this.updatedBy = jobRequest.updatedBy || '';
        this.incrementalParameter = jobRequest.incrementalParameter || '';
        this.incrementalParameterValue = jobRequest.incrementalParameterValue || '';
    }
    return JobRequest;
}());



/***/ }),

/***/ "./src/app/model/log.ts":
/*!******************************!*\
  !*** ./src/app/model/log.ts ***!
  \******************************/
/*! exports provided: Log */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Log", function() { return Log; });
var Log = /** @class */ (function () {
    function Log() {
    }
    return Log;
}());



/***/ }),

/***/ "./src/app/model/role.ts":
/*!*******************************!*\
  !*** ./src/app/model/role.ts ***!
  \*******************************/
/*! exports provided: Role, RoleId */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Role", function() { return Role; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleId", function() { return RoleId; });
var Role = /** @class */ (function () {
    function Role() {
    }
    return Role;
}());

var RoleId = /** @class */ (function () {
    function RoleId() {
        this.roleId = Array();
    }
    return RoleId;
}());



/***/ }),

/***/ "./src/app/model/schdule.ts":
/*!**********************************!*\
  !*** ./src/app/model/schdule.ts ***!
  \**********************************/
/*! exports provided: Schdule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Schdule", function() { return Schdule; });
var Schdule = /** @class */ (function () {
    function Schdule() {
    }
    return Schdule;
}());



/***/ }),

/***/ "./src/app/model/user.ts":
/*!*******************************!*\
  !*** ./src/app/model/user.ts ***!
  \*******************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var User = /** @class */ (function () {
    function User() {
        this.isExternal = false;
    }
    return User;
}());



/***/ }),

/***/ "./src/app/pipe/search.pipe.ts":
/*!*************************************!*\
  !*** ./src/app/pipe/search.pipe.ts ***!
  \*************************************/
/*! exports provided: SearchPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPipe", function() { return SearchPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SearchPipe = /** @class */ (function () {
    function SearchPipe() {
    }
    SearchPipe.prototype.transform = function (value, keys, term) {
        if (!term)
            return value;
        return (value || []).filter(function (item) { return keys.split(',').some(function (key) { return item.hasOwnProperty(key) && new RegExp(term, 'gi').test(item[key]); }); });
    };
    SearchPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'search'
        })
    ], SearchPipe);
    return SearchPipe;
}());



/***/ }),

/***/ "./src/app/settings/roles/roles.component.html":
/*!*****************************************************!*\
  !*** ./src/app/settings/roles/roles.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"content-header\">\r\n  <a [routerLink]=\"['/settings']\">\r\n    <svg class=\"icon_user_management\" shape-rendering=\"geometricPrecision\" width=\"10\" height=\"10\">\r\n      <use xlink:href=\"./assets/dist/img/icons.svg#icon_back\"></use>\r\n    </svg> Settings</a>\r\n  <h1 style=\"margin-top:10px\">\r\n    Roles\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li>\r\n      <input type=\"button\" class=\"btn btn-net-primary\" (click)=\"ShowRoleForm(roleModal)\" value=\"Add Role\" />\r\n    </li>\r\n  </ol>\r\n</section>\r\n\r\n\r\n<section class=\"content\" *ngIf=\"roles.length > 0 && loading == false\">\r\n  <div class=\"box\">\r\n    <div class=\"box-body\" style=\"padding: 0px 10px;border: solid 1px #ccc;border-radius: 3px;\">\r\n      <div id=\"example1_wrapper\" class=\"dataTables_wrapper form-inline dt-bootstrap\">\r\n        <div class=\"row\">\r\n          <div class=\"col-lg-12\" style=\"padding: 0px 5px\">\r\n            <table class=\"table table-bordered table-striped dataTable table-hover\" style=\"margin-bottom: 0px\">\r\n              <thead>\r\n                <tr>\r\n                  <th width=\"100\">\r\n                  </th>\r\n                  <th>Role </th>\r\n                  <th>CreatedBy</th>\r\n                  <th>UpdatedBy</th>\r\n                </tr>\r\n              </thead>\r\n              <tbody>\r\n                <tr *ngFor=\"let r of roles\">\r\n                  <td>\r\n                    <a (click)=\"ShowFeatureModel(featureModal, r.roleId)\" title=\"Assign Feature\">\r\n                      <svg class=\"icon_edit\" shape-rendering=\"geometricPrecision\" width=\"25\" height=\"25\" class=\"btn-xs\">\r\n                        <use xlink:href=\"./assets/dist/img/icons.svg#icon_edit\"></use>\r\n                      </svg>\r\n                    </a>\r\n                    <a (click)=\"ShowDeleteModal(alertModal, r.roleId)\" title=\"Delete Role\">\r\n                      <svg class=\"icon_delete\" shape-rendering=\"geometricPrecision\" width=\"25\" height=\"25\" class=\"btn-xs\">\r\n                        <use xlink:href=\"./assets/dist/img/icons.svg#icon_delete\"></use>\r\n                      </svg>\r\n                    </a>\r\n                  </td>\r\n                  <td>\r\n                    {{r.role}}\r\n                  </td>\r\n                  <td>{{r.createdBy}}</td>\r\n                  <td>{{r.updatedBy}}</td>\r\n                </tr>\r\n                <tr *ngIf=\"roles.length == 0 && loading == true\">\r\n                  <td colspan=\"4\" class=\"text-center\">\r\n                    <svg id=\"icon_loading\" width=\"60\" height=\"60\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 100 100\"\r\n                      preserveAspectRatio=\"xMidYMid\" class=\"lds-eclipse\" style=\"background:0 0\">\r\n                      <path d=\"M9.998 50.08a40 40 0 0 0 80-.16 40 42-.115 0 1-80 .16\" fill=\"#0067C5\">\r\n                        <animateTransform attributeName=\"transform\" type=\"rotate\" calcMode=\"linear\" values=\"0 50 51;360 50 51\"\r\n                          keyTimes=\"0;1\" dur=\"1s\" begin=\"0s\" repeatCount=\"indefinite\" />\r\n                      </path>\r\n                    </svg>\r\n                  </td>\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n<section class=\"content\" *ngIf=\"roles.length == 0 && loading == true\">\r\n  <div class=\"box bg-shade\">\r\n    <div class=\"box-body\" style=\"padding: 0px 10px;border: solid 1px #ccc;border-radius: 3px;\">\r\n      <div id=\"example1_wrapper\" class=\"dataTables_wrapper form-inline dt-bootstrap\">\r\n        <div class=\"row\">\r\n          <div class=\"col-lg-12\" style=\"padding: 0px 5px\">\r\n            <table>\r\n              <thead>\r\n                <tr>\r\n                  <td class=\"td-5\">\r\n                    <span></span>\r\n                  </td>\r\n                </tr>\r\n              </thead>\r\n              <tbody>\r\n                <tr>\r\n                  <td class=\"td-3\">\r\n                    <span></span>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td class=\"td-3\">\r\n                    <span></span>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td class=\"td-3\">\r\n                    <span></span>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td class=\"td-3\">\r\n                    <span></span>\r\n                  </td>\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- /.box-body -->\r\n  </div>\r\n</section>\r\n<modal #roleModal>\r\n  <modal-header>\r\n    <span>Add Role</span>\r\n  </modal-header>\r\n  <modal-content>\r\n    <div class=\"row\" style=\"margin-left:0px\">\r\n      <form #Loginform=\"ngForm\" class=\"form-horizontal\" novalidate (ngSubmit)=\"OnSubmit(roleModal)\">\r\n        <!-- form start -->\r\n        <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error' : role.error?.required && role.touched}\">\r\n          <label>Role</label>\r\n          <input type=\"text\" class=\"form-control\" [(ngModel)]=\"roleModel.role\" name=\"role\" placeholder=\"Role name\"\r\n            #role=\"ngModel\" required placeholder=\"Role name\" />\r\n          <span class=\"help-block small\" *ngIf=\"role.errors?.required && role.touched\">\r\n            Required </span>\r\n        </div>\r\n      </form>\r\n    </div>\r\n    <!-- end form -->\r\n  </modal-content>\r\n  <modal-footer>\r\n    <div class=\"col-md-2 pull-right\">\r\n      <button type=\"submit\" class=\"btn btn-net-primary\" (click)=\"OnSubmit(roleModal)\" [disabled]=\"!Loginform.valid\">\r\n        Submit\r\n      </button>\r\n    </div>\r\n    <div class=\"col-md-2 pull-right\">\r\n      <button class=\"btn btn-default\" (click)=\"roleModal.close()\">\r\n        Cancel\r\n      </button>\r\n    </div>\r\n  </modal-footer>\r\n</modal>\r\n\r\n<modal #alertModal>\r\n  <modal-header>\r\n    <span>Alert!</span>\r\n  </modal-header>\r\n  <modal-content>\r\n    Are you sure, you want to delete?\r\n  </modal-content>\r\n  <modal-footer>\r\n    <button class=\"btn btn-default\" (click)=\"Cancel(alertModal)\">\r\n      No\r\n    </button>\r\n    <button type=\"submit\" class=\"btn btn-danger\" (click)=\"DeleteRole(alertModal)\">Yes</button>\r\n  </modal-footer>\r\n</modal>\r\n\r\n<modal #featureModal>\r\n  <modal-header>\r\n    <span style=\"margin:0px\">Assign Feature</span>\r\n  </modal-header>\r\n  <modal-content>\r\n    <div class=\"row\">\r\n      <div class=\"col-md-12\">\r\n        <div class=\"ui-select__wrapper next-input--has-content\">\r\n          <select class=\"ui-select\" [(ngModel)]=\"selectedFeature\" (change)=\"SelectApplication(selectedFeature)\">\r\n            <option *ngFor=\"let f of features\" [ngValue]=\"f\">{{f.featureName}}</option>\r\n          </select>\r\n          <svg class=\"icon_up_down_select\" shape-rendering=\"geometricPrecision\" class=\"next-icon next-icon--size-16\"\r\n            width=\"13\" height=\"13\">\r\n            <use xlink:href=\"./assets/dist/img/icons.svg#icon_up_down_select\"></use>\r\n          </svg>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-12\">&nbsp;</div>\r\n      <div class=\"col-md-12\">\r\n        <div class=\"box\">\r\n          <div class=\"box-body\">\r\n            <div class=\"form-group col-md-5 margin-0 padding-0\">\r\n              <span>\r\n                Assign Feature\r\n              </span>\r\n              <select multiple=\"multiple\" [(ngModel)]=\"addNewFeature\" style=\"width:100%;height:200px\">\r\n                <option value=\"{{f.featureId}}\" *ngFor=\"let f of featuresList\">{{f.featureName}}</option>\r\n              </select>\r\n            </div>\r\n            <div class=\"form-group col-md-2 margin-0 text-center\" style=\"height:200px\">\r\n              <div class=\"col-md-12 padding-0\" style=\"vertical-align:middle;height:150px\">\r\n                <span>\r\n                  <svg class=\"icon_double_right_arrows_angles\" shape-rendering=\"geometricPrecision\" width=\"25\" height=\"25\"\r\n                    (click)=\"AddRole()\" style=\"border: solid 1px #ddd;padding: 5px;border-radius: 50%;margin-top:55px;background: #f7f7f7\">\r\n                    <use xlink:href=\"./assets/dist/img/icons.svg#icon_double_right_arrows_angles\"></use>\r\n                  </svg>\r\n                </span>\r\n              </div>\r\n              <div class=\"col-md-12 padding-0\">\r\n                <span>\r\n                  <svg class=\"icon_double_left_arrows_angles\" shape-rendering=\"geometricPrecision\" width=\"25\" height=\"25\"\r\n                    (click)=\"RemoveRole()\" style=\"border: solid 1px #ddd;padding: 5px;border-radius: 50%;background: #f7f7f7\">\r\n                    <use xlink:href=\"./assets/dist/img/icons.svg#icon_double_left_arrows_angles\"></use>\r\n                  </svg>\r\n                </span>\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group col-md-5 margin-0 padding-0\">\r\n              <span>\r\n                Assigned Feature\r\n              </span>\r\n              <select multiple=\"multiple\" [(ngModel)]=\"removeSelectedFeatures\" style=\"width:100%;height:200px\">\r\n                <option value=\"{{f.featureId}}\" *ngFor=\"let f of selectedFeatures\">{{f.featureName}}</option>\r\n              </select>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </modal-content>\r\n</modal>"

/***/ }),

/***/ "./src/app/settings/roles/roles.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/settings/roles/roles.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "tr td .user-quick-actions {\n  display: block; }\n\ntr:hover td .user-quick-actions {\n  display: block; }\n\ntr td .user-quick-actions a {\n  font-size: 12px; }\n\n@-webkit-keyframes moving-gradient {\n  0% {\n    background-position: -250px 0; }\n  100% {\n    background-position: 250px 0; } }\n\n.dataTable > thead > tr > th {\n  color: #333333; }\n\n.dataTable > tbody > tr > td {\n  font-size: 12px; }\n\ntable {\n  width: 100%; }\n\ntable tr {\n    border-bottom: 1px solid rgba(0, 0, 0, 0.1); }\n\ntable tr td {\n      vertical-align: middle;\n      padding: 8px; }\n\ntable tr td span {\n        display: block; }\n\ntable tr td.td-1 {\n        width: 20px; }\n\ntable tr td.td-1 span {\n          width: 20px;\n          height: 20px; }\n\ntable tr td.td-2 {\n        width: 50px; }\n\ntable tr td.td-2 span {\n          background-color: rgba(0, 0, 0, 0.15);\n          width: 50px;\n          height: 50px; }\n\ntable tr td.td-3 {\n        width: 400px; }\n\ntable tr td.td-3 span {\n          height: 12px;\n          background: linear-gradient(to right, #eee 20%, #ddd 50%, #eee 80%);\n          background-size: 500px 100px;\n          -webkit-animation-name: moving-gradient;\n                  animation-name: moving-gradient;\n          -webkit-animation-duration: 1s;\n                  animation-duration: 1s;\n          -webkit-animation-iteration-count: infinite;\n                  animation-iteration-count: infinite;\n          -webkit-animation-timing-function: linear;\n                  animation-timing-function: linear;\n          -webkit-animation-fill-mode: forwards;\n                  animation-fill-mode: forwards; }\n\ntable tr td.td-5 {\n        width: 100px; }\n\ntable tr td.td-5 span {\n          background-color: rgba(0, 0, 0, 0.15);\n          width: 100%;\n          height: 30px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXR0aW5ncy9yb2xlcy9FOlxcVUktUHJvamVjdFxcRVRMXFxzcmNcXHNyYy9hcHBcXHNldHRpbmdzXFxyb2xlc1xccm9sZXMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFBNEIsY0FBYyxFQUFBOztBQUMxQztFQUFrQyxjQUFjLEVBQUE7O0FBQ2hEO0VBQThCLGVBQWUsRUFBQTs7QUFHN0M7RUFDSTtJQUFLLDZCQUE2QixFQUFBO0VBQ2xDO0lBQU8sNEJBQTRCLEVBQUEsRUFBQTs7QUFFdkM7RUFDRSxjQUFjLEVBQUE7O0FBRWhCO0VBQ0UsZUFBYyxFQUFBOztBQUVoQjtFQUNFLFdBQVcsRUFBQTs7QUFEYjtJQUdJLDJDQUF1QyxFQUFBOztBQUgzQztNQU1NLHNCQUFzQjtNQUN0QixZQUFZLEVBQUE7O0FBUGxCO1FBU1EsY0FBYyxFQUFBOztBQVR0QjtRQVlRLFdBQVcsRUFBQTs7QUFabkI7VUFjVSxXQUFXO1VBQ1gsWUFBWSxFQUFBOztBQWZ0QjtRQW1CUSxXQUFXLEVBQUE7O0FBbkJuQjtVQXFCVSxxQ0FBaUM7VUFDakMsV0FBVztVQUNYLFlBQVksRUFBQTs7QUF2QnRCO1FBMkJRLFlBQVksRUFBQTs7QUEzQnBCO1VBOEJVLFlBQVk7VUFDWixtRUFBbUU7VUFDbkUsNEJBQTRCO1VBQzVCLHVDQUErQjtrQkFBL0IsK0JBQStCO1VBQy9CLDhCQUFzQjtrQkFBdEIsc0JBQXNCO1VBQ3RCLDJDQUFtQztrQkFBbkMsbUNBQW1DO1VBQ25DLHlDQUFpQztrQkFBakMsaUNBQWlDO1VBQ2pDLHFDQUE2QjtrQkFBN0IsNkJBQTZCLEVBQUE7O0FBckN2QztRQTJDUSxZQUFZLEVBQUE7O0FBM0NwQjtVQTZDVSxxQ0FBaUM7VUFDakMsV0FBVztVQUNYLFlBQ0YsRUFBQSIsImZpbGUiOiJhcHAvc2V0dGluZ3Mvcm9sZXMvcm9sZXMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0ciB0ZCAudXNlci1xdWljay1hY3Rpb25zIHsgZGlzcGxheTogYmxvY2s7IH1cclxudHI6aG92ZXIgdGQgLnVzZXItcXVpY2stYWN0aW9ucyB7IGRpc3BsYXk6IGJsb2NrOyB9XHJcbnRyIHRkIC51c2VyLXF1aWNrLWFjdGlvbnMgYSB7IGZvbnQtc2l6ZTogMTJweH1cclxuXHJcblxyXG5ALXdlYmtpdC1rZXlmcmFtZXMgbW92aW5nLWdyYWRpZW50IHtcclxuICAgIDAlIHsgYmFja2dyb3VuZC1wb3NpdGlvbjogLTI1MHB4IDA7IH1cclxuICAgIDEwMCUgeyBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAyNTBweCAwOyB9XHJcbn1cclxuLmRhdGFUYWJsZT50aGVhZD50cj50aHtcclxuICBjb2xvcjogIzMzMzMzMzsgICBcclxufVxyXG4uZGF0YVRhYmxlPnRib2R5PnRyPnRke1xyXG4gIGZvbnQtc2l6ZToxMnB4O1xyXG59XHJcbnRhYmxlIHtcclxuICB3aWR0aDogMTAwJTsgIFxyXG4gIHRyIHtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2JhKDAsMCwwLC4xKTtcclxuICAgICB0ZCB7XHJcbiAgICAgXHJcbiAgICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbiAgICAgIHBhZGRpbmc6IDhweDtcclxuICAgICAgc3BhbiB7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgIH1cclxuICAgICAgJi50ZC0xIHtcclxuICAgICAgICB3aWR0aDogMjBweDtcclxuICAgICAgICBzcGFuIHtcclxuICAgICAgICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgICAgICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAmLnRkLTIge1xyXG4gICAgICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgICAgIHNwYW4ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwuMTUpO1xyXG4gICAgICAgICAgd2lkdGg6IDUwcHg7XHJcbiAgICAgICAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgICYudGQtMyB7XHJcbiAgICAgICAgd2lkdGg6IDQwMHB4O1xyXG4gICAgICAgIC8vIHBhZGRpbmctcmlnaHQ6IDEwMHB4O1xyXG4gICAgICAgIHNwYW4ge1xyXG4gICAgICAgICAgaGVpZ2h0OiAxMnB4O1xyXG4gICAgICAgICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjZWVlIDIwJSwgI2RkZCA1MCUsICNlZWUgODAlKTtcclxuICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogNTAwcHggMTAwcHg7XHJcbiAgICAgICAgICBhbmltYXRpb24tbmFtZTogbW92aW5nLWdyYWRpZW50O1xyXG4gICAgICAgICAgYW5pbWF0aW9uLWR1cmF0aW9uOiAxcztcclxuICAgICAgICAgIGFuaW1hdGlvbi1pdGVyYXRpb24tY291bnQ6IGluZmluaXRlO1xyXG4gICAgICAgICAgYW5pbWF0aW9uLXRpbWluZy1mdW5jdGlvbjogbGluZWFyO1xyXG4gICAgICAgICAgYW5pbWF0aW9uLWZpbGwtbW9kZTogZm9yd2FyZHM7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgICYudGQtNCB7XHJcbiAgICAgIH1cclxuICAgICAgJi50ZC01IHtcclxuICAgICAgICB3aWR0aDogMTAwcHg7XHJcbiAgICAgICAgc3BhbiB7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLC4xNSk7XHJcbiAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgIGhlaWdodDogMzBweFxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufSAgIl19 */"

/***/ }),

/***/ "./src/app/settings/roles/roles.component.ts":
/*!***************************************************!*\
  !*** ./src/app/settings/roles/roles.component.ts ***!
  \***************************************************/
/*! exports provided: RolesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RolesComponent", function() { return RolesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _roles_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./roles.service */ "./src/app/settings/roles/roles.service.ts");
/* harmony import */ var ngx_treeview__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-treeview */ "./node_modules/ngx-treeview/src/index.js");
/* harmony import */ var _model_role__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../model/role */ "./src/app/model/role.ts");
/* harmony import */ var _model_feature__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../model/feature */ "./src/app/model/feature.ts");



// import { FeatureService } from '../features/features.service';



var RolesComponent = /** @class */ (function () {
    function RolesComponent(RoleService, roleService) {
        this.RoleService = RoleService;
        this.roleService = roleService;
        this.roleModel = new _model_role__WEBPACK_IMPORTED_MODULE_4__["Role"]();
        this.roles = new Array();
        this.features = new Array();
        this.featuresList = new Array();
        this.selectedFeatures = new Array();
        this.menu = new Array();
        this.config = ngx_treeview__WEBPACK_IMPORTED_MODULE_3__["TreeviewConfig"].create({
            hasAllCheckBox: true,
            hasFilter: true,
            hasCollapseExpand: true,
            decoupleChildFromParent: false,
            maxHeight: 400
        });
        this.loading = false;
    }
    RolesComponent.prototype.ngOnInit = function () {
        this.loading = true;
        this.GetAllRoles();
    };
    /**
     * Add new role
     */
    RolesComponent.prototype.OnSubmit = function (roleModal) {
        var _this = this;
        roleModal.close();
        if (this.roleModel.roleId == null) {
            this.RoleService.AddRole(this.roleModel).subscribe(function (data) {
                _this.GetAllRoles();
            }, function (err) {
                console.error(err);
            });
        }
        else {
            this.RoleService.UpdateRole(this.roleModel).subscribe(function (data) {
                _this.GetAllRoles();
            }, function (err) {
                console.error(err);
            });
        }
    };
    /**
     * Update existing role
     */
    RolesComponent.prototype.UpdateRole = function () {
    };
    /**
     * Delete Role
     */
    RolesComponent.prototype.DeleteRole = function (alertModal) {
        var _this = this;
        alertModal.close();
        if (this.selectRoleId !== '') {
            this.RoleService.DeleteRole(this.selectRoleId).subscribe(function (data) {
                _this.GetAllRoles();
            }, function (err) {
                console.error(err);
            });
        }
    };
    /**
     * Get all role assigned featueres
     * @param roleID RoleId Parameter
     */
    RolesComponent.prototype.GetRoleFeatures = function (roleID, linkId) {
        var _this = this;
        this.RoleService.GetRoleFeatures(roleID, linkId).subscribe(function (data) {
            if (JSON.parse(JSON.stringify(data)).results != null &&
                JSON.parse(JSON.stringify(data)).results.length > 0) {
                _this.selectedFeatures = JSON.parse(JSON.stringify(data)).results;
            }
            else {
            }
            _this.featuresList = [];
            // this.featureService.GetAllFeatureList(linkId)
            //   .subscribe(res => {
            //     this.loading = false;
            //     if (JSON.parse(JSON.stringify(res)).results != null &&
            //       JSON.parse(JSON.stringify(res)).results.length > 0) {
            //       this.featuresList = JSON.parse(JSON.stringify(res)).results;
            //       this.selectedFeatures.forEach(element => {
            //         const obj = this.featuresList.filter(n => n.featureId === element.featureId)[0];
            //         const index = this.featuresList.indexOf(obj);
            //         if (index > -1) {
            //           this.featuresList.splice(index, 1);
            //         }
            //       });
            //     } else {
            //     }
            //   }, err => {
            //     console.log(err);
            //   });
        }, function (err) {
            console.error(err);
        });
    };
    /**
     * Get all roles
     */
    RolesComponent.prototype.GetAllRoles = function () {
        var _this = this;
        this.RoleService.GetAllRoles().subscribe(function (data) {
            _this.loading = false;
            _this.roles = JSON.parse(JSON.stringify(data)).results;
        }, function (err) {
            _this.roles = new Array();
            _this.loading = false;
        });
    };
    /**
     *
     * @param roleModal Modal role object
     * @param roleId RoleId Parameter
     */
    RolesComponent.prototype.ShowRoleForm = function (roleModal, roleId) {
        roleModal.open();
        this.roleModel = new _model_role__WEBPACK_IMPORTED_MODULE_4__["Role"]();
        if (roleId != null) {
            this.GetRole(roleId);
        }
    };
    /**
     * Prompt Yes/No model to ask user to delete role.
     * @param alertModal Modal alert object
     * @param roleId RoleId Parameter
     */
    RolesComponent.prototype.ShowDeleteModel = function (alertModal, roleId) {
        alertModal.open();
        this.selectRoleId = roleId;
    };
    /**
     * Cancel to perform delete action
     * @param alertModal Model alert object
     */
    RolesComponent.prototype.Cancel = function (alertModal) {
        alertModal.close();
        this.selectRoleId = '';
    };
    /**
     * Get role detail
     * @param roleId Role Id
     */
    RolesComponent.prototype.GetRole = function (roleId) {
        var _this = this;
        this.RoleService.GetRole(roleId)
            .subscribe(function (response) {
            _this.roleModel = JSON.parse(JSON.stringify(response)).results[0];
        }, function (err) {
            console.error(err);
        });
    };
    RolesComponent.prototype.ShowDeleteModal = function (alertModal, roleId) {
        alertModal.open();
        this.selectRoleId = roleId;
    };
    // GetAllFeatureNode() {
    //   this.features = [];
    //   this.featureService.GetAllFeatureNode().subscribe(data => {
    //     this.features = JSON.parse(JSON.stringify(data)).results;
    //     this.selectedFeature = this.features[0];
    //     if (this.features.length > 0) {
    //       this.SelectApplication(this.selectedFeature);
    //     }
    //   }, err => {
    //     console.error(err);
    //   });
    // }
    RolesComponent.prototype.SelectApplication = function (selectedFeature) {
        this.loading = true;
        this.featuresList = new Array();
        this.selectedFeatures = [];
        this.GetRoleFeatures(this.selectRoleId, selectedFeature.linkId);
    };
    RolesComponent.prototype.AddRole = function () {
        var _this = this;
        this.addNewFeature.forEach(function (e) {
            if (_this.selectedFeatures.filter(function (n) { return n = e; }).length === 0) {
                var features = _this.featuresList.filter(function (n) { return n.featureId === parseInt(e, 10); });
                features.forEach(function (element) {
                    _this.selectedFeatures.push(element);
                });
            }
        });
        var featureIds = new Array();
        this.selectedFeatures.forEach(function (element) {
            featureIds.push(element.featureId);
        });
        var req = new _model_feature__WEBPACK_IMPORTED_MODULE_5__["FeatureId"]();
        req.featureId = featureIds;
        this.roleService.AssignRoleFeatured(this.selectRoleId, req)
            .subscribe(function (response) {
            // this.GetAllFeatureNode();
            console.log(response);
        }, function (error) { return console.log(error); });
    };
    RolesComponent.prototype.RemoveRole = function () {
        var _this = this;
        this.removeSelectedFeatures.forEach(function (role) {
            var indexs = _this.selectedFeatures.indexOf(_this.selectedFeatures.filter(function (n) { return n.featureId === role; })[0]);
            _this.selectedFeatures.splice(indexs, 1);
        });
        var featureIds = new Array();
        var index = 0;
        this.removeSelectedFeatures.forEach(function (element) {
            _this.roleService.RevokeRoleFeatured(_this.selectRoleId, element)
                .subscribe(function (response) {
                index++;
                // if (index === this.removeSelectedFeatures.length) {
                //   this.GetAllFeatureNode();
                // }
            }, function (error) { return console.log(error); });
        });
    };
    RolesComponent.prototype.ShowFeatureModel = function (featureModal, roleId) {
        this.selectRoleId = roleId;
        featureModal.open();
        // this.GetAllFeatureNode();
    };
    RolesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-roles',
            template: __webpack_require__(/*! ./roles.component.html */ "./src/app/settings/roles/roles.component.html"),
            styles: [__webpack_require__(/*! ./roles.component.scss */ "./src/app/settings/roles/roles.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_roles_service__WEBPACK_IMPORTED_MODULE_2__["RolesService"], _roles_service__WEBPACK_IMPORTED_MODULE_2__["RolesService"]])
    ], RolesComponent);
    return RolesComponent;
}());



/***/ }),

/***/ "./src/app/settings/roles/roles.service.ts":
/*!*************************************************!*\
  !*** ./src/app/settings/roles/roles.service.ts ***!
  \*************************************************/
/*! exports provided: RolesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RolesService", function() { return RolesService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/config */ "./src/environments/config.ts");
/* harmony import */ var _app_session_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../app.session-service */ "./src/app/app.session-service.ts");





var RolesService = /** @class */ (function () {
    function RolesService(http, sessionService) {
        this.http = http;
        this.sessionService = sessionService;
        this.baseUrl = _environments_config__WEBPACK_IMPORTED_MODULE_3__["Config"].getEnvironmentVariable('endPoint');
        this.AddRolesEndpoint = this.baseUrl + 'roles';
        this.UpdateRoleEndpoint = this.baseUrl + 'roles/';
        this.DeleteRoleEndpoint = this.baseUrl + 'roles/';
        this.GetRoleEndpoint = this.baseUrl + 'roles/';
        this.GetAllRoleEndpoint = this.baseUrl + 'roles/';
        this.GetRoleFeaturesEndpoint = this.baseUrl + 'roles' + '/roleid' + '/features';
    }
    RolesService.prototype.AddRole = function (data) {
        data.createdBy = this.sessionService.getSessionValue('uid');
        data.updatedBy = this.sessionService.getSessionValue('uid');
        return this.http.post(this.AddRolesEndpoint, data);
    };
    RolesService.prototype.UpdateRole = function (data) {
        data.updatedBy = this.sessionService.getSessionValue('uid');
        return this.http.put(this.UpdateRoleEndpoint, data);
    };
    RolesService.prototype.DeleteRole = function (id) {
        return this.http.delete(this.DeleteRoleEndpoint + id);
    };
    RolesService.prototype.GetRole = function (userid) {
        return this.http.get(this.GetRoleEndpoint + userid);
    };
    RolesService.prototype.GetRoleFeatures = function (roleid, linkId) {
        return this.http.get(this.baseUrl + ("roles/" + roleid + "/features?linkId=") + linkId);
    };
    RolesService.prototype.AssignRoleFeatured = function (roleId, data) {
        data.createdBy = this.sessionService.getSessionValue('uid');
        return this.http.post(this.baseUrl + ("roles/" + roleId + "/features"), data);
    };
    RolesService.prototype.RevokeRoleFeatured = function (roleId, featureId) {
        return this.http.delete(this.baseUrl + ("roles/" + roleId + "/features/" + featureId));
    };
    RolesService.prototype.GetAllRoles = function () {
        return this.http.get(this.GetAllRoleEndpoint);
    };
    RolesService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _app_session_service__WEBPACK_IMPORTED_MODULE_4__["SessionService"]])
    ], RolesService);
    return RolesService;
}());



/***/ }),

/***/ "./src/app/settings/setting-module.ts":
/*!********************************************!*\
  !*** ./src/app/settings/setting-module.ts ***!
  \********************************************/
/*! exports provided: SettingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingModule", function() { return SettingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var ngx_modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-modal */ "./node_modules/ngx-modal/index.js");
/* harmony import */ var ngx_modal__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ngx_modal__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _progress_kendo_angular_inputs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @progress/kendo-angular-inputs */ "./node_modules/@progress/kendo-angular-inputs/dist/fesm5/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _user_managment_user_managment_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./user-managment/user-managment.component */ "./src/app/settings/user-managment/user-managment.component.ts");
/* harmony import */ var _users_users_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./users/users.component */ "./src/app/settings/users/users.component.ts");
/* harmony import */ var _users_edit_edit_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./users/edit/edit.component */ "./src/app/settings/users/edit/edit.component.ts");
/* harmony import */ var _roles_roles_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./roles/roles.component */ "./src/app/settings/roles/roles.component.ts");
/* harmony import */ var _users_users_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./users/users.service */ "./src/app/settings/users/users.service.ts");
/* harmony import */ var _app_admin_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../app.admin-auth-guard.service */ "./src/app/app.admin-auth-guard.service.ts");
/* harmony import */ var _app_user_auth_guard_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../app.user-auth-guard.service */ "./src/app/app.user-auth-guard.service.ts");



// import { UsersRoutingModule } from './users-routing.module';











// import { EditRoutingModule } from './edit-routing.module';
// import { UserManagementRoutingModule } from './user-management-routing.module';
// import { RolesRoutingModule } from './roles-routing.module';
var routes = [
    {
        path: 'settings',
        component: _user_managment_user_managment_component__WEBPACK_IMPORTED_MODULE_7__["UserManagmentComponent"],
        canActivate: [_app_admin_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__["AdminAuthGuardService"]]
    },
    {
        path: 'settings/users',
        component: _users_users_component__WEBPACK_IMPORTED_MODULE_8__["UsersComponent"],
        canActivate: [_app_admin_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__["AdminAuthGuardService"]]
    },
    {
        path: 'settings/users/edit/:id',
        component: _users_edit_edit_component__WEBPACK_IMPORTED_MODULE_9__["EditComponent"],
        resolve: {
            data: _users_users_service__WEBPACK_IMPORTED_MODULE_11__["UserService"]
        }
    },
    {
        path: 'roles',
        component: _roles_roles_component__WEBPACK_IMPORTED_MODULE_10__["RolesComponent"],
        canActivate: [_app_user_auth_guard_service__WEBPACK_IMPORTED_MODULE_13__["UserAuthGuardService"]]
    },
];
var SettingModule = /** @class */ (function () {
    function SettingModule() {
    }
    SettingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                // UsersRoutingModule,
                ngx_modal__WEBPACK_IMPORTED_MODULE_3__["ModalModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _progress_kendo_angular_inputs__WEBPACK_IMPORTED_MODULE_5__["InputsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forChild(routes)
            ],
            declarations: [
                _users_users_component__WEBPACK_IMPORTED_MODULE_8__["UsersComponent"],
                _users_users_component__WEBPACK_IMPORTED_MODULE_8__["UsersComponent"],
                _users_edit_edit_component__WEBPACK_IMPORTED_MODULE_9__["EditComponent"],
                _user_managment_user_managment_component__WEBPACK_IMPORTED_MODULE_7__["UserManagmentComponent"],
                _roles_roles_component__WEBPACK_IMPORTED_MODULE_10__["RolesComponent"]
            ]
        })
    ], SettingModule);
    return SettingModule;
}());



/***/ }),

/***/ "./src/app/settings/user-managment/user-managment.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/settings/user-managment/user-managment.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"content-header\">\r\n</section>\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <br />\r\n    </div>\r\n  </div>\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <br />\r\n    </div>\r\n  </div>\r\n  <div class=\"row\">\r\n    <div class=\"col-md-8 col-md-offset-2\">\r\n      <div class=\"box box-solid\">\r\n        <!-- /.box-header -->\r\n        <div class=\"box-body\" style=\"border: solid 1px #ddd\">\r\n          <h3>\r\n            Settings\r\n          </h3>\r\n          <section class=\"ui-card\">\r\n            <nav>\r\n              <ul class=\"area-settings-nav\">\r\n                <li class=\"area-settings-nav__item\">\r\n                  <a class=\"area-settings-nav__action\" [routerLink]=\"['users']\" aria-disabled=\"false\">\r\n                    <div class=\"area-settings-nav__media\">\r\n                      <svg class=\"icon_user_management\" shape-rendering=\"geometricPrecision\" width=\"25\" height=\"25\">\r\n                        <use xlink:href=\"./assets/dist/img/icons.svg#icon_user_management\"></use>\r\n                      </svg>\r\n                    </div>\r\n                    <div>\r\n                      <p class=\"area-settings-nav__title\">\r\n                        User Management\r\n                      </p>\r\n                      <p class=\"area-settings-nav__description\">\r\n                        View and add user to user management\r\n                      </p>\r\n                    </div>\r\n                  </a>\r\n                </li>\r\n                <li class=\"area-settings-nav__item\">\r\n\r\n                  <a class=\"area-settings-nav__action\" [routerLink]=\"['/roles']\" aria-disabled=\"false\">\r\n                    <div class=\"area-settings-nav__media\">\r\n                      <svg class=\"icon_role_management\" shape-rendering=\"geometricPrecision\" width=\"25\" height=\"25\">\r\n                        <use xlink:href=\"./assets/dist/img/icons.svg#icon_role_management\"></use>\r\n                      </svg>\r\n                    </div>\r\n                    <div>\r\n                      <p class=\"area-settings-nav__title\">\r\n                        Role Management\r\n                      </p>\r\n                      <p class=\"area-settings-nav__description\">\r\n                        Manage and assign roles to users\r\n                      </p>\r\n                    </div>\r\n                  </a>\r\n                </li>\r\n                <li class=\"area-settings-nav__item\" style=\"display:none\">\r\n\r\n                  <a class=\"area-settings-nav__action\" [routerLink]=\"['/features']\" aria-disabled=\"false\">\r\n                    <div class=\"area-settings-nav__media\">\r\n                      <svg class=\"icon_feature_management\" shape-rendering=\"geometricPrecision\" width=\"25\" height=\"25\">\r\n                        <use xlink:href=\"./assets/dist/img/icons.svg#icon_feature_management\"></use>\r\n                      </svg>\r\n                    </div>\r\n                    <div>\r\n                      <p class=\"area-settings-nav__title\">\r\n                        Feature Management\r\n                      </p>\r\n                      <p class=\"area-settings-nav__description\">\r\n                        Manage and assign feature to feature\r\n                      </p>\r\n                    </div>\r\n                  </a>\r\n                </li>\r\n                <li class=\"area-settings-nav__item\" style=\"display:none\">\r\n                  <a class=\"area-settings-nav__action\" [routerLink]=\"['/menu']\" aria-disabled=\"false\">\r\n                    <div class=\"area-settings-nav__media\">\r\n                      <svg class=\"icon_hamburger\" shape-rendering=\"geometricPrecision\" width=\"25\" height=\"25\">\r\n                        <use xlink:href=\"./assets/dist/img/icons.svg#icon_hamburger\"></use>\r\n                      </svg>\r\n                    </div>\r\n                    <div>\r\n                      <p class=\"area-settings-nav__title\">\r\n                        Menu Management\r\n                      </p>\r\n                      <p class=\"area-settings-nav__description\">\r\n                        Manage and assign feature to feature\r\n                      </p>\r\n                    </div>\r\n                  </a>\r\n                </li>\r\n              </ul>\r\n            </nav>\r\n          </section>\r\n        </div>\r\n        <!-- /.box-body -->\r\n      </div>\r\n      <!-- /.box -->\r\n    </div>\r\n    <!-- ./col -->\r\n  </div>\r\n</section>"

/***/ }),

/***/ "./src/app/settings/user-managment/user-managment.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/settings/user-managment/user-managment.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".area-settings-nav {\n  list-style-type: none;\n  margin: 0;\n  padding: 0.8rem 0.4rem; }\n\n@media screen and (min-width: 1025px) {\n  .area-settings-nav {\n    padding: 1.6rem;\n    -webkit-column-count: 2;\n    -moz-column-count: 2;\n         column-count: 2;\n    -webkit-column-gap: 1.6rem;\n    -moz-column-gap: 1.6rem;\n         column-gap: 1.6rem; }\n  @supports (display: grid) {\n    .area-settings-nav {\n      display: grid;\n      grid-column-gap: 1.6rem;\n      grid-template-rows: repeat(6, auto); } } }\n\n@media screen and (min-width: 1201px) {\n  .area-settings-nav {\n    -webkit-column-count: 3;\n    -moz-column-count: 3;\n         column-count: 3; }\n  @supports (display: grid) {\n    .area-settings-nav {\n      grid-template-rows: repeat(4, auto); } } }\n\n.area-settings-nav__item {\n  display: flex;\n  -webkit-column-break-inside: avoid;\n  -moz-column-break-inside: avoid;\n       break-inside: avoid; }\n\n.area-settings-nav__action {\n  font-size: 1.5rem;\n  font-weight: 400;\n  line-height: 2rem;\n  text-transform: initial;\n  letter-spacing: initial;\n  box-sizing: border-box;\n  display: flex;\n  width: 100%;\n  padding: 1.2rem;\n  border: 0;\n  color: #212b36;\n  cursor: pointer;\n  border-radius: 3px; }\n\n@media screen and (min-width: 640px) {\n  .area-settings-nav__action {\n    font-size: 1.4rem; } }\n\n.area-settings-nav__action:focus,\n.area-settings-nav__action:hover {\n  background-color: #f9fafb;\n  text-decoration: none;\n  outline: none; }\n\n.area-settings-nav__action:focus .area-settings-nav__title,\n.area-settings-nav__action:hover .area-settings-nav__title {\n  color: #084e8a; }\n\n.area-settings-nav__action:focus .area-settings-nav__media,\n.area-settings-nav__action:hover .area-settings-nav__media {\n  background-color: #dfe3e8; }\n\n.area-settings-nav__action:active {\n  background-color: #f4f6f8; }\n\n.area-settings-nav__action:disabled,\n.area-settings-nav__action[aria-disabled=\"true\"] {\n  color: #919eab;\n  cursor: default;\n  pointer-events: none; }\n\n.area-settings-nav__action:disabled .area-settings-nav__title,\n.area-settings-nav__action:disabled .area-settings-nav__description,\n.area-settings-nav__action[aria-disabled=\"true\"] .area-settings-nav__title,\n.area-settings-nav__action[aria-disabled=\"true\"] .area-settings-nav__description {\n  color: inherit;\n  font-weight: normal; }\n\n.area-settings-nav__action:disabled .area-settings-nav__media,\n.area-settings-nav__action[aria-disabled=\"true\"] .area-settings-nav__media {\n  background: none;\n  opacity: 0.5; }\n\n.area-settings-nav__media {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  flex-shrink: 0;\n  width: 4rem;\n  height: 4rem;\n  margin-right: 1.6rem;\n  border-radius: 3px;\n  background-color: #f4f6f8; }\n\n.area-settings-nav__media .next-icon,\n.area-settings-nav__media .next-icon__text {\n  fill: #919eab;\n  color: transparent; }\n\n.area-settings-nav__title {\n  font-weight: 600;\n  margin: 0;\n  color: #007ace; }\n\n.area-settings-nav__description {\n  color: #637381;\n  margin: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXR0aW5ncy91c2VyLW1hbmFnbWVudC9FOlxcVUktUHJvamVjdFxcRVRMXFxzcmNcXHNyYy9hcHBcXHNldHRpbmdzXFx1c2VyLW1hbmFnbWVudFxcdXNlci1tYW5hZ21lbnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxxQkFBcUI7RUFDckIsU0FBUztFQUNULHNCQUNKLEVBQUE7O0FBRUE7RUFDSTtJQUNJLGVBQWU7SUFDZix1QkFBdUI7SUFDdkIsb0JBQWU7U0FBZixlQUFlO0lBQ2YsMEJBQTBCO0lBQzFCLHVCQUNKO1NBREksa0JBQ0osRUFBQTtFQUN5QjtJQUNyQjtNQUNJLGFBQWE7TUFDYix1QkFBdUI7TUFDdkIsbUNBQW1DLEVBQUEsRUFDdEMsRUFBQTs7QUFJVDtFQUNJO0lBQ0ksdUJBQXVCO0lBQ3ZCLG9CQUNKO1NBREksZUFDSixFQUFBO0VBQ3lCO0lBQ3JCO01BQ0ksbUNBQW1DLEVBQUEsRUFDdEMsRUFBQTs7QUFJVDtFQUlJLGFBQWE7RUFDYixrQ0FBa0M7RUFDbEMsK0JBQ0o7T0FESSxtQkFDSixFQUFBOztBQUVBO0VBQ0ksaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsdUJBQXVCO0VBQ3ZCLHVCQUF1QjtFQUV2QixzQkFBc0I7RUFJdEIsYUFBYTtFQUNiLFdBQVc7RUFDWCxlQUFlO0VBQ2YsU0FBUztFQUNULGNBQWM7RUFDZCxlQUFlO0VBQ2Ysa0JBQ0osRUFBQTs7QUFFQTtFQUNJO0lBQ0ksaUJBQ0osRUFBQSxFQUFDOztBQUdMOztFQUVJLHlCQUF5QjtFQUN6QixxQkFBcUI7RUFDckIsYUFDSixFQUFBOztBQUVBOztFQUVJLGNBQ0osRUFBQTs7QUFFQTs7RUFFSSx5QkFDSixFQUFBOztBQUVBO0VBQ0kseUJBQ0osRUFBQTs7QUFFQTs7RUFFSSxjQUFjO0VBQ2QsZUFBZTtFQUNmLG9CQUNKLEVBQUE7O0FBRUE7Ozs7RUFJSSxjQUFjO0VBQ2QsbUJBQ0osRUFBQTs7QUFFQTs7RUFFSSxnQkFBZ0I7RUFDaEIsWUFDSixFQUFBOztBQUVBO0VBSUksYUFBYTtFQUliLG1CQUFtQjtFQUluQix1QkFBdUI7RUFHdkIsY0FBYztFQUNkLFdBQVc7RUFDWCxZQUFZO0VBQ1osb0JBQW9CO0VBQ3BCLGtCQUFrQjtFQUNsQix5QkFDSixFQUFBOztBQUVBOztFQUVJLGFBQWE7RUFDYixrQkFDSixFQUFBOztBQUVBO0VBQ0ksZ0JBQWdCO0VBQ2hCLFNBQVM7RUFDVCxjQUNKLEVBQUE7O0FBRUE7RUFDSSxjQUFjO0VBQ2QsU0FDSixFQUFBIiwiZmlsZSI6ImFwcC9zZXR0aW5ncy91c2VyLW1hbmFnbWVudC91c2VyLW1hbmFnbWVudC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5hcmVhLXNldHRpbmdzLW5hdiB7XHJcbiAgICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBwYWRkaW5nOiAwLjhyZW0gMC40cmVtXHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDEwMjVweCkge1xyXG4gICAgLmFyZWEtc2V0dGluZ3MtbmF2IHtcclxuICAgICAgICBwYWRkaW5nOiAxLjZyZW07XHJcbiAgICAgICAgLXdlYmtpdC1jb2x1bW4tY291bnQ6IDI7XHJcbiAgICAgICAgY29sdW1uLWNvdW50OiAyO1xyXG4gICAgICAgIC13ZWJraXQtY29sdW1uLWdhcDogMS42cmVtO1xyXG4gICAgICAgIGNvbHVtbi1nYXA6IDEuNnJlbVxyXG4gICAgfVxyXG4gICAgQHN1cHBvcnRzIChkaXNwbGF5OiBncmlkKSB7XHJcbiAgICAgICAgLmFyZWEtc2V0dGluZ3MtbmF2IHtcclxuICAgICAgICAgICAgZGlzcGxheTogZ3JpZDtcclxuICAgICAgICAgICAgZ3JpZC1jb2x1bW4tZ2FwOiAxLjZyZW07XHJcbiAgICAgICAgICAgIGdyaWQtdGVtcGxhdGUtcm93czogcmVwZWF0KDYsIGF1dG8pXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxMjAxcHgpIHtcclxuICAgIC5hcmVhLXNldHRpbmdzLW5hdiB7XHJcbiAgICAgICAgLXdlYmtpdC1jb2x1bW4tY291bnQ6IDM7XHJcbiAgICAgICAgY29sdW1uLWNvdW50OiAzXHJcbiAgICB9XHJcbiAgICBAc3VwcG9ydHMgKGRpc3BsYXk6IGdyaWQpIHtcclxuICAgICAgICAuYXJlYS1zZXR0aW5ncy1uYXYge1xyXG4gICAgICAgICAgICBncmlkLXRlbXBsYXRlLXJvd3M6IHJlcGVhdCg0LCBhdXRvKVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLmFyZWEtc2V0dGluZ3MtbmF2X19pdGVtIHtcclxuICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xyXG4gICAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLXdlYmtpdC1jb2x1bW4tYnJlYWstaW5zaWRlOiBhdm9pZDtcclxuICAgIGJyZWFrLWluc2lkZTogYXZvaWRcclxufVxyXG5cclxuLmFyZWEtc2V0dGluZ3MtbmF2X19hY3Rpb24ge1xyXG4gICAgZm9udC1zaXplOiAxLjVyZW07XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgbGluZS1oZWlnaHQ6IDJyZW07XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogaW5pdGlhbDtcclxuICAgIGxldHRlci1zcGFjaW5nOiBpbml0aWFsO1xyXG4gICAgLXdlYmtpdC1ib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xyXG4gICAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBwYWRkaW5nOiAxLjJyZW07XHJcbiAgICBib3JkZXI6IDA7XHJcbiAgICBjb2xvcjogIzIxMmIzNjtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDNweFxyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA2NDBweCkge1xyXG4gICAgLmFyZWEtc2V0dGluZ3MtbmF2X19hY3Rpb24ge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMS40cmVtXHJcbiAgICB9XHJcbn1cclxuXHJcbi5hcmVhLXNldHRpbmdzLW5hdl9fYWN0aW9uOmZvY3VzLFxyXG4uYXJlYS1zZXR0aW5ncy1uYXZfX2FjdGlvbjpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmYWZiO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgb3V0bGluZTogbm9uZVxyXG59XHJcblxyXG4uYXJlYS1zZXR0aW5ncy1uYXZfX2FjdGlvbjpmb2N1cyAuYXJlYS1zZXR0aW5ncy1uYXZfX3RpdGxlLFxyXG4uYXJlYS1zZXR0aW5ncy1uYXZfX2FjdGlvbjpob3ZlciAuYXJlYS1zZXR0aW5ncy1uYXZfX3RpdGxlIHtcclxuICAgIGNvbG9yOiAjMDg0ZThhXHJcbn1cclxuXHJcbi5hcmVhLXNldHRpbmdzLW5hdl9fYWN0aW9uOmZvY3VzIC5hcmVhLXNldHRpbmdzLW5hdl9fbWVkaWEsXHJcbi5hcmVhLXNldHRpbmdzLW5hdl9fYWN0aW9uOmhvdmVyIC5hcmVhLXNldHRpbmdzLW5hdl9fbWVkaWEge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2RmZTNlOFxyXG59XHJcblxyXG4uYXJlYS1zZXR0aW5ncy1uYXZfX2FjdGlvbjphY3RpdmUge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y0ZjZmOFxyXG59XHJcblxyXG4uYXJlYS1zZXR0aW5ncy1uYXZfX2FjdGlvbjpkaXNhYmxlZCxcclxuLmFyZWEtc2V0dGluZ3MtbmF2X19hY3Rpb25bYXJpYS1kaXNhYmxlZD1cInRydWVcIl0ge1xyXG4gICAgY29sb3I6ICM5MTllYWI7XHJcbiAgICBjdXJzb3I6IGRlZmF1bHQ7XHJcbiAgICBwb2ludGVyLWV2ZW50czogbm9uZVxyXG59XHJcblxyXG4uYXJlYS1zZXR0aW5ncy1uYXZfX2FjdGlvbjpkaXNhYmxlZCAuYXJlYS1zZXR0aW5ncy1uYXZfX3RpdGxlLFxyXG4uYXJlYS1zZXR0aW5ncy1uYXZfX2FjdGlvbjpkaXNhYmxlZCAuYXJlYS1zZXR0aW5ncy1uYXZfX2Rlc2NyaXB0aW9uLFxyXG4uYXJlYS1zZXR0aW5ncy1uYXZfX2FjdGlvblthcmlhLWRpc2FibGVkPVwidHJ1ZVwiXSAuYXJlYS1zZXR0aW5ncy1uYXZfX3RpdGxlLFxyXG4uYXJlYS1zZXR0aW5ncy1uYXZfX2FjdGlvblthcmlhLWRpc2FibGVkPVwidHJ1ZVwiXSAuYXJlYS1zZXR0aW5ncy1uYXZfX2Rlc2NyaXB0aW9uIHtcclxuICAgIGNvbG9yOiBpbmhlcml0O1xyXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbFxyXG59XHJcblxyXG4uYXJlYS1zZXR0aW5ncy1uYXZfX2FjdGlvbjpkaXNhYmxlZCAuYXJlYS1zZXR0aW5ncy1uYXZfX21lZGlhLFxyXG4uYXJlYS1zZXR0aW5ncy1uYXZfX2FjdGlvblthcmlhLWRpc2FibGVkPVwidHJ1ZVwiXSAuYXJlYS1zZXR0aW5ncy1uYXZfX21lZGlhIHtcclxuICAgIGJhY2tncm91bmQ6IG5vbmU7XHJcbiAgICBvcGFjaXR5OiAwLjVcclxufVxyXG5cclxuLmFyZWEtc2V0dGluZ3MtbmF2X19tZWRpYSB7XHJcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuICAgIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcclxuICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XHJcbiAgICAtd2Via2l0LWFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIC13ZWJraXQtYm94LXBhY2s6IGNlbnRlcjtcclxuICAgIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIC13ZWJraXQtZmxleC1zaHJpbms6IDA7XHJcbiAgICAtbXMtZmxleC1uZWdhdGl2ZTogMDtcclxuICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgd2lkdGg6IDRyZW07XHJcbiAgICBoZWlnaHQ6IDRyZW07XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEuNnJlbTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDNweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmNGY2ZjhcclxufVxyXG5cclxuLmFyZWEtc2V0dGluZ3MtbmF2X19tZWRpYSAubmV4dC1pY29uLFxyXG4uYXJlYS1zZXR0aW5ncy1uYXZfX21lZGlhIC5uZXh0LWljb25fX3RleHQge1xyXG4gICAgZmlsbDogIzkxOWVhYjtcclxuICAgIGNvbG9yOiB0cmFuc3BhcmVudFxyXG59XHJcblxyXG4uYXJlYS1zZXR0aW5ncy1uYXZfX3RpdGxlIHtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBjb2xvcjogIzAwN2FjZVxyXG59XHJcblxyXG4uYXJlYS1zZXR0aW5ncy1uYXZfX2Rlc2NyaXB0aW9uIHtcclxuICAgIGNvbG9yOiAjNjM3MzgxO1xyXG4gICAgbWFyZ2luOiAwXHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/settings/user-managment/user-managment.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/settings/user-managment/user-managment.component.ts ***!
  \*********************************************************************/
/*! exports provided: UserManagmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserManagmentComponent", function() { return UserManagmentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var UserManagmentComponent = /** @class */ (function () {
    function UserManagmentComponent() {
    }
    UserManagmentComponent.prototype.ngOnInit = function () {
    };
    UserManagmentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-user-managment',
            template: __webpack_require__(/*! ./user-managment.component.html */ "./src/app/settings/user-managment/user-managment.component.html"),
            styles: [__webpack_require__(/*! ./user-managment.component.scss */ "./src/app/settings/user-managment/user-managment.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], UserManagmentComponent);
    return UserManagmentComponent;
}());



/***/ }),

/***/ "./src/app/settings/users/edit/edit.component.html":
/*!*********************************************************!*\
  !*** ./src/app/settings/users/edit/edit.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"content-header\">\r\n  <a class=\"back-navigation\" [routerLink]=\"['/settings/users']\">\r\n    <svg class=\"icon_user_management\" shape-rendering=\"geometricPrecision\" width=\"10\" height=\"10\">\r\n      <use xlink:href=\"./assets/dist/img/icons.svg#icon_back\"></use>\r\n    </svg> All Users</a>\r\n  <h1 style=\"margin-top:10px\">\r\n    Edit Users\r\n  </h1>\r\n</section>\r\n<section class=\"content\">\r\n  <div class=\"row\">\r\n    <!-- right column -->\r\n    <div class=\"col-md-6\">\r\n      <!-- Horizontal Form -->\r\n      <form #Loginform=\"ngForm\" class=\"form-horizontal\" novalidate (ngSubmit)=\"EditUser(Loginform.value)\">\r\n        <div class=\"box\">\r\n          <!-- form start -->\r\n          <div class=\"box-body bg-shade\">\r\n            <div class=\"col-md-6\" [ngClass]=\"{'has-error': firstName.errors?.required && firstName.touched}\">\r\n              <label>First Name</label>\r\n              <input type=\"text\" class=\"form-control\" [(ngModel)]=\"userModel.firstName\" name=\"firstName\"\r\n                #firstName=\"ngModel\" required [attr.disabled]=\"loading\" placeholder=\"FirstName\" />\r\n              <span class=\"help-block small\" *ngIf=\"firstName.errors?.required && firstName.touched\">\r\n                Required </span>\r\n            </div>\r\n            <div class=\"col-md-6\" [ngClass]=\"{'has-error': lastName.errors?.required && lastName.touched}\"\r\n              style=\"margin-left: 0px;padding-right: 0px;\">\r\n              <label>Last Name</label>\r\n              <input type=\"text\" class=\"form-control\" [(ngModel)]=\"userModel.lastName\" name=\"lastName\"\r\n                #lastName=\"ngModel\" required [attr.disabled]=\"loading\" placeholder=\"LastName\" />\r\n              <span class=\"help-block small\" *ngIf=\"lastName.errors?.required && lastName.touched\">\r\n                Required </span>\r\n            </div>\r\n            <div class=\"col-md-12\" [ngClass]=\"{'has-error': userId.errors?.required && userId.touched}\">\r\n              <label for=\"inputEmail3\">User Name</label>\r\n              <input type=\"text\" class=\"form-control\" [(ngModel)]=\"userModel.userId\" name=\"userId\" #userId=\"ngModel\"\r\n                required [attr.disabled]=\"loading\" placeholder=\"User Name\" />\r\n              <span class=\"help-block small\" *ngIf=\"userId.errors?.required && userId.touched\">\r\n                Required </span>\r\n            </div>\r\n            <div class=\"col-md-12\"\r\n              [ngClass]=\"{'has-error': (emailId.errors?.required && emailId.touched) || (emailId.errors?.pattern && emailId.touched)}\">\r\n              <label>Email</label>\r\n              <input type=\"text\" class=\"form-control\" [(ngModel)]=\"userModel.emailId\" name=\"emailId\" #emailId=\"ngModel\"\r\n                required [attr.disabled]=\"loading\" placeholder=\"Email Id\" />\r\n              <span class=\"help-block small\" *ngIf=\"emailId.errors?.required && emailId.touched\">\r\n                Required </span>\r\n              <span class=\"help-block small\" *ngIf=\"emailId.errors?.pattern && emailId.touched\">\r\n                Email not valid. </span>\r\n            </div>\r\n            <div class=\"col-md-6\" [ngClass]=\"{'has-error': isInternal.errors?.required && isInternal.touched}\">\r\n              <label>User Type<span style=\"color:red\">*</span></label>\r\n              <input type=\"text\" class=\"form-control\" [(ngModel)]=\"userModel.isInternal\" name=\"isInternal\"\r\n                #isInternal=\"ngModel\" required [attr.disabled]=\"loading\" placeholder=\"Is Internal\" />\r\n              <span class=\"help-block small\" *ngIf=\"isInternal.errors?.required && isInternal.touched\">\r\n                Required </span>\r\n            </div>\r\n            <div class=\"col-md-6\" [ngClass]=\"{'has-error': crmCompanyId.errors?.required && crmCompanyId.touched}\">\r\n              <label>CRM Company Id<span style=\"color:red\">*</span></label>\r\n              <input type=\"text\" class=\"form-control\" [(ngModel)]=\"userModel.crmCompanyId\" name=\"crmCompanyId\"\r\n                #crmCompanyId=\"ngModel\" required [attr.disabled]=\"loading\" placeholder=\"CRM Company Id\" />\r\n              <span class=\"help-block small\" *ngIf=\"crmCompanyId.errors?.required && crmCompanyId.touched\">\r\n                Required </span>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </form>\r\n      <!-- /.box -->\r\n    </div>\r\n    <div class=\"col-md-6\">\r\n      <!-- Horizontal Form -->\r\n      <div class=\"box\">\r\n        <div class=\"box-body bg-shade\">\r\n          <div class=\"form-group col-md-5 margin-0 padding-0\">\r\n            <span>\r\n              All Roles <a href=\"javascript:void(0);\" (click)=\"SelectedAll()\"\r\n                style=\"font-weight: bold;display: none\">Select All</a>\r\n            </span>\r\n            <select multiple=\"multiple\" [(ngModel)]=\"selectedRole\" style=\"width:100%;height:180px\">\r\n              <option value=\"{{o.roleId}}\" *ngFor=\"let o of roles\">{{o.role}}</option>\r\n            </select>\r\n          </div>\r\n          <div class=\"form-group col-md-2 margin-0 text-center\" style=\"height:200px\">\r\n            <div class=\"col-md-12 padding-0\" style=\"vertical-align:middle;height:150px\">\r\n              <span>\r\n                <svg class=\"icon_double_right_arrows_angles\" shape-rendering=\"geometricPrecision\" width=\"35\" height=\"35\"\r\n                  (click)=\"AddRole()\"\r\n                  style=\"border: solid 1px #ddd;padding: 5px;border-radius: 50%;margin-top:55px;background: #f7f7f7\">\r\n                  <use xlink:href=\"./assets/dist/img/icons.svg#icon_double_right_arrows_angles\"></use>\r\n                </svg>\r\n              </span>\r\n            </div>\r\n            <div class=\"col-md-12 padding-0\">\r\n              <span>\r\n                <svg class=\"icon_double_left_arrows_angles\" shape-rendering=\"geometricPrecision\" width=\"35\" height=\"35\"\r\n                  (click)=\"RemoveRole()\"\r\n                  style=\"border: solid 1px #ddd;padding: 5px;border-radius: 50%;background: #f7f7f7\">\r\n                  <use xlink:href=\"./assets/dist/img/icons.svg#icon_double_left_arrows_angles\"></use>\r\n                </svg>\r\n              </span>\r\n            </div>\r\n          </div>\r\n          <div class=\"form-group col-md-5 margin-0 padding-0\">\r\n            <span>\r\n              Assigned Roles\r\n            </span>\r\n            <select multiple=\"multiple\" [(ngModel)]=\"removeSelectedRoles\" style=\"width:100%;height:180px\">\r\n              <option value=\"{{o.roleId}}\" *ngFor=\"let o of selectedRoles\">{{o.role}}</option>\r\n            </select>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!--/.col (right) -->\r\n  </div>\r\n  <!-- /.row -->\r\n</section>"

/***/ }),

/***/ "./src/app/settings/users/edit/edit.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/settings/users/edit/edit.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvc2V0dGluZ3MvdXNlcnMvZWRpdC9lZGl0LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/settings/users/edit/edit.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/settings/users/edit/edit.component.ts ***!
  \*******************************************************/
/*! exports provided: EditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditComponent", function() { return EditComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _users_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../users.service */ "./src/app/settings/users/users.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _roles_roles_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../roles/roles.service */ "./src/app/settings/roles/roles.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _model_user__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../model/user */ "./src/app/model/user.ts");
/* harmony import */ var _model_role__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../model/role */ "./src/app/model/role.ts");









var EditComponent = /** @class */ (function () {
    function EditComponent(activatedRoute, userService, roleService, router) {
        this.activatedRoute = activatedRoute;
        this.userService = userService;
        this.roleService = roleService;
        this.router = router;
        this.userModel = new _model_user__WEBPACK_IMPORTED_MODULE_7__["User"]();
        this.submitBtnText = 'Submit';
        this.roles = new Array();
        this.selectedRoles = new Array();
        this.userId = this.activatedRoute.snapshot.paramMap.get('id');
        this.unsubscribeAll = new rxjs__WEBPACK_IMPORTED_MODULE_6__["Subject"]();
    }
    EditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.GetUserRoles(this.userId);
        this.userService.onUserResponseChanged
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this.unsubscribeAll))
            .subscribe(function (response) {
            _this.userModel = JSON.parse(JSON.stringify(response)).results[0];
        });
        //  this.GetUserRoles(this.userId);
        // this.userService.GetUser(this.userId)
        //   .subscribe(response => {
        //     this.userModel = JSON.parse(JSON.stringify(response)).results[0];
        //   },
        //     error => {
        //     });
    };
    EditComponent.prototype.GetUserRoles = function (userid) {
        var _this = this;
        this.userService.GetUserRoles(userid).subscribe(function (data) {
            _this.loading = false;
            _this.GetAllRoles();
            var response = JSON.parse(JSON.stringify(data));
            if (response.success === true && response.results[0].roleDetailsResponse.length > 0) {
                _this.selectedRoles = response.results[0].roleDetailsResponse;
            }
        }, function (err) {
            _this.loading = false;
        });
    };
    EditComponent.prototype.GetAllRoles = function () {
        var _this = this;
        this.roleService.GetAllRoles().subscribe(function (data) {
            _this.roles = JSON.parse(JSON.stringify(data)).results;
            _this.selectedRoles.forEach(function (elemen) {
                var obj = _this.roles.filter(function (n) { return n.roleId === elemen.roleId; })[0];
                var index = _this.roles.indexOf(obj);
                if (index > -1) {
                    _this.roles.splice(index, 1);
                }
            });
        }, function (err) {
            console.error(err);
        });
    };
    EditComponent.prototype.EditUser = function (userModel) {
        var _this = this;
        this.loading = true;
        this.submitBtnText = 'Proccessing...';
        this.userService.UpdateUser(userModel).subscribe(function (data) {
            _this.router.navigate(['settings/users']);
            _this.loading = false;
        }, function (err) {
            _this.submitBtnText = 'Submit';
            _this.loading = false;
        });
    };
    EditComponent.prototype.AddRole = function () {
        var _this = this;
        this.selectedRole.forEach(function (roleId) {
            if (_this.selectedRoles.filter(function (n) { return n.roleId === roleId; }).length === 0) {
                var roles = _this.roles.filter(function (n) { return n.roleId === Number(roleId); });
                roles.forEach(function (elemen) {
                    _this.selectedRoles.push(elemen);
                });
            }
        });
        var roleIds = new Array();
        this.selectedRoles.forEach(function (eleme) {
            roleIds.push(eleme.roleId);
        });
        var req = new _model_role__WEBPACK_IMPORTED_MODULE_8__["RoleId"]();
        req.roleId = roleIds;
        this.userService.AddUserRole(this.userId, req)
            .subscribe(function (response) {
            _this.GetUserRoles(_this.userId);
            console.log(response);
        }, function (error) { return console.log(error); });
    };
    EditComponent.prototype.RemoveRole = function () {
        var _this = this;
        this.removeSelectedRoles.forEach(function (role) {
            var index = _this.selectedRoles.indexOf(_this.selectedRoles.filter(function (n) { return n.roleId === role; })[0]);
            _this.selectedRoles.splice(index, 1);
        });
        var featureIds = new Array();
        this.removeSelectedRoles.forEach(function (elemen) {
            _this.userService.RevokeUserRole(_this.userId, elemen)
                .subscribe(function (response) {
                _this.GetUserRoles(_this.userId);
            }, function (error) { return console.log(error); });
        });
    };
    EditComponent.prototype.SelectedAll = function () {
    };
    EditComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-edit',
            template: __webpack_require__(/*! ./edit.component.html */ "./src/app/settings/users/edit/edit.component.html"),
            styles: [__webpack_require__(/*! ./edit.component.scss */ "./src/app/settings/users/edit/edit.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _users_service__WEBPACK_IMPORTED_MODULE_3__["UserService"],
            _roles_roles_service__WEBPACK_IMPORTED_MODULE_5__["RolesService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], EditComponent);
    return EditComponent;
}());



/***/ }),

/***/ "./src/app/settings/users/users.component.html":
/*!*****************************************************!*\
  !*** ./src/app/settings/users/users.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"content-header\">\r\n  <a [routerLink]=\"['/settings']\">\r\n    <svg class=\"icon_user_management\" shape-rendering=\"geometricPrecision\" width=\"10\" height=\"10\">\r\n      <use xlink:href=\"./assets/dist/img/icons.svg#icon_back\"></use>\r\n    </svg> Settings</a>\r\n  <h1 style=\"margin-top:10px\">\r\n    All Users\r\n  </h1>\r\n  <ol class=\"breadcrumb\">\r\n    <li>\r\n      <a (click)=\"userModal.open()\" class=\"btn btn-net-primary\">Add User</a>\r\n      <a class=\"btn icon-btn btn-default\" href=\"javascript:;\" (click)=\"ExportData()\" *ngIf=\"userList.length > 0 && loading == false\">\r\n        <span class=\"fa btn-net-glyphicon fa-file img-circle text-danger\"></span>Export\r\n        Data</a>\r\n    </li>\r\n  </ol>\r\n</section>\r\n<section class=\"content\" *ngIf=\"userList.length > 0 && loading == false\">\r\n  <div class=\"box\">\r\n    <div class=\"box-body\" style=\"padding: 0px 10px;border: solid 1px #ccc;border-radius: 3px;\">\r\n      <div id=\"example1_wrapper\" class=\"dataTables_wrapper form-inline dt-bootstrap\">\r\n        <div class=\"row\">\r\n          <div class=\"col-lg-12\" style=\"padding: 0px 5px\">\r\n            <table class=\"table table-bordered table-striped dataTable table-hover\" style=\"margin-bottom: 0px\">\r\n              <thead>\r\n                <tr role=\"row\">\r\n                  <th width=\"100\">\r\n                  </th>\r\n                  <th>First Name </th>\r\n                  <th>Last Name </th>\r\n                  <th>User Id </th>\r\n                  <th>Email Id </th>\r\n                  <th>Roles </th>\r\n                  <th>User Type </th>\r\n                </tr>\r\n              <tbody>\r\n                <tr *ngFor=\"let user of userList\">\r\n                  <td>\r\n                    <a [routerLink]=\"['/settings/users/edit/' + user.userId]\">\r\n                      <svg class=\"icon_edit btn-xs\" shape-rendering=\"geometricPrecision\" width=\"25\" height=\"25\">\r\n                        <use xlink:href=\"./assets/dist/img/icons.svg#icon_edit\"></use>\r\n                      </svg>\r\n                    </a>\r\n                    <a href=\"javascipt:void()\" (click)=\"ShowDeleteUserModal(alertModal, user.userId)\">\r\n                      <svg class=\"icon_delete btn-xs\" shape-rendering=\"geometricPrecision\" width=\"25\" height=\"25\">\r\n                        <use xlink:href=\"./assets/dist/img/icons.svg#icon_delete\"></use>\r\n                      </svg>\r\n                    </a>\r\n                    <a (click)=\"LoginAs(user)\" title=\"{{'Logged in As ' + user.firstName + ' ' + user.lastName }}\">\r\n                      <svg class=\"icon_login_as btn-xs\" shape-rendering=\"geometricPrecision\" width=\"25\" height=\"25\"\r\n                        fill=\"#FBBC05\">\r\n                        <use xlink:href=\"./assets/dist/img/icons.svg#icon_login_as\"></use>\r\n                      </svg>\r\n                    </a>\r\n                  </td>\r\n                  <td>\r\n                    {{user.firstName}}\r\n                  </td>\r\n                  <td>\r\n                    {{user.lastName}}\r\n                  </td>\r\n                  <td>{{user.userId}}</td>\r\n                  <td>\r\n                    <a href=\"mailto:{{user.emailId}}\">{{user.emailId}}</a>\r\n                  </td>\r\n                  <td>\r\n                    {{user.roles}}\r\n                  </td>\r\n                  <td>{{user.isInternal}}</td>\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n<section class=\"content\" *ngIf=\"userList.length == 0 && loading == true\">\r\n  <div class=\"box bg-shade\">\r\n    <div class=\"box-body\" style=\"padding: 0px 10px;border: solid 1px #ccc;border-radius: 3px;\">\r\n      <div id=\"example1_wrapper\" class=\"dataTables_wrapper form-inline dt-bootstrap\">\r\n        <div class=\"row\">\r\n          <div class=\"col-lg-12\" style=\"padding: 0px 5px\">\r\n            <table>\r\n              <thead>\r\n                <tr>\r\n                  <td class=\"td-5\">\r\n                    <span></span>\r\n                  </td>\r\n                </tr>\r\n              </thead>\r\n              <tbody>\r\n                <tr>\r\n                  <td class=\"td-3\">\r\n                    <span></span>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td class=\"td-3\">\r\n                    <span></span>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td class=\"td-3\">\r\n                    <span></span>\r\n                  </td>\r\n                </tr>\r\n                <tr>\r\n                  <td class=\"td-3\">\r\n                    <span></span>\r\n                  </td>\r\n                </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- /.box-body -->\r\n  </div>\r\n</section>\r\n<!-- first modal: modal with custom header, content and footer -->\r\n<modal #userRoleModal>\r\n  <modal-header>\r\n    <h3 style=\"margin:0px\">Assign Role</h3>\r\n  </modal-header>\r\n  <modal-content>\r\n    <div class=\"row\">\r\n      <div class=\"col-md-12\">\r\n        <table class=\"table table-hover\">\r\n          <tr>\r\n            <th></th>\r\n            <th width=\"300\">Role </th>\r\n          </tr>\r\n          <tbody>\r\n            <tr *ngFor=\"let o of roles\">\r\n              <td width=\"20\" class=\"text-center\" style=\"height:unset\">\r\n                <span title=\"Add Role\" *ngIf=\"!IsAssignedRole(o)\" (click)=\"AssignRole(o)\">\r\n                  <svg class=\"icon_add\" shape-rendering=\"geometricPrecision\" width=\"25\" height=\"25\">\r\n                    <use xlink:href=\"./assets/dist/img/icons.svg#icon_add\"></use>\r\n                  </svg>\r\n                </span>\r\n                <span title=\"Remove Role\" *ngIf=\"IsAssignedRole(o)\" (click)=\"RemoveRole(o)\">\r\n                  <svg class=\"icon_minus\" shape-rendering=\"geometricPrecision\" width=\"25\" height=\"25\">\r\n                    <use xlink:href=\"./assets/dist/img/icons.svg#icon_minus\"></use>\r\n                  </svg>\r\n                </span>\r\n              </td>\r\n              <td style=\"height:unset\">\r\n                {{o.role}}\r\n              </td>\r\n            </tr>\r\n            <tr *ngIf=\"roles.length == 0 && loading == true\">\r\n              <td colspan=\"5\" class=\"text-center\">\r\n                <svg id=\"icon_loading\" width=\"60\" height=\"60\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 100 100\"\r\n                  preserveAspectRatio=\"xMidYMid\" class=\"lds-eclipse\" style=\"background:0 0\">\r\n                  <path d=\"M9.998 50.08a40 40 0 0 0 80-.16 40 42-.115 0 1-80 .16\" fill=\"#0067C5\">\r\n                    <animateTransform attributeName=\"transform\" type=\"rotate\" calcMode=\"linear\" values=\"0 50 51;360 50 51\"\r\n                      keyTimes=\"0;1\" dur=\"1s\" begin=\"0s\" repeatCount=\"indefinite\" />\r\n                  </path>\r\n                </svg>\r\n              </td>\r\n            </tr>\r\n          </tbody>\r\n        </table>\r\n      </div>\r\n    </div>\r\n  </modal-content>\r\n</modal>\r\n<modal #userFeatureModal>\r\n  <modal-header>\r\n    <strong>Assign Feature</strong>\r\n  </modal-header>\r\n  <modal-content>\r\n    <div class=\"row\">\r\n      <div class=\"col-md-12\">\r\n        <div class=\"ui-select__wrapper next-input--has-content\">\r\n          <select class=\"ui-select\" [(ngModel)]=\"selectedFeature\">\r\n            <option *ngFor=\"let f of features\" [ngValue]=\"f\">{{f.featureName}}</option>\r\n          </select>\r\n          <svg class=\"icon_up_down_select\" shape-rendering=\"geometricPrecision\" class=\"next-icon next-icon--size-16\"\r\n            width=\"13\" height=\"13\">\r\n            <use xlink:href=\"./assets/dist/img/icons.svg#icon_up_down_select\"></use>\r\n          </svg>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-12\">\r\n        <label>\r\n          Features\r\n        </label>\r\n        <!-- <ngx-treeview [config]=\"config\" [items]=\"items\">\r\n        </ngx-treeview> -->\r\n      </div>\r\n    </div>\r\n  </modal-content>\r\n  <modal-footer>\r\n    <button class=\"btn btn-default\" (click)=\"userFeatureModal.close()\">Close</button>\r\n  </modal-footer>\r\n</modal>\r\n\r\n<modal #alertModal>\r\n  <modal-header>\r\n    <span>Alert!</span>\r\n  </modal-header>\r\n  <modal-content>\r\n    Are you sure, you want to delete?\r\n  </modal-content>\r\n  <modal-footer>\r\n    <button class=\"btn btn-default\" (click)=\"Cancel(alertModal)\">\r\n      No\r\n    </button>\r\n    <button type=\"submit\" class=\"btn btn-danger\" (click)=\"DeleteUser(alertModal)\">Yes</button>\r\n  </modal-footer>\r\n</modal>\r\n<modal #userModal>\r\n  <modal-header>\r\n    <span>New SSO User</span>\r\n  </modal-header>\r\n  <modal-content>\r\n    <div class=\"row\" style=\"margin-left: 0px\">\r\n      <form #Loginform=\"ngForm\" class=\"form-horizontal\" novalidate (ngSubmit)=\"OnSubmit(userModal)\">\r\n        <!-- form start -->\r\n        <div class=\"form-group col-md-12\" [ngClass]=\"{'has-error' : userId.error?.required && userId.touched}\">\r\n          <label>SSO User Name</label>\r\n          <input type=\"text\" class=\"form-control\" [(ngModel)]=\"userModel.userId\" name=\"userId\" placeholder=\"SSO User Name\"\r\n            #userId=\"ngModel\" required />\r\n          <span class=\"help-block small\" *ngIf=\"userId.errors?.required && userId.touched\">\r\n            Required </span>\r\n        </div>\r\n        <kendo-switch name=\"switch\"[(ngModel)]=\"userModal.isExternal\" (valueChange)=\"toggleChange(userModal.isExternal)\"></kendo-switch>\r\n     \r\n      </form>\r\n    </div>\r\n    <!-- end form -->\r\n  </modal-content>\r\n  <modal-footer>\r\n    <div class=\"col-md-2 pull-right\">\r\n      <button type=\"submit\" class=\"btn btn-net-primary\" (click)=\"OnSubmit(userModal)\" [disabled]=\"!Loginform.valid\">\r\n        Submit\r\n      </button>\r\n    </div>\r\n    <div class=\"col-md-2 pull-right\">\r\n      <button class=\"btn btn-default\" (click)=\"userModal.close()\">\r\n        Cancel\r\n      </button>\r\n    </div>\r\n  </modal-footer>\r\n</modal>"

/***/ }),

/***/ "./src/app/settings/users/users.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/settings/users/users.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "tr td .user-quick-actions {\n  display: inline-block; }\n\ntr:hover td .user-quick-actions {\n  display: block; }\n\ntr td .user-quick-actions a {\n  font-size: 12px; }\n\n@-webkit-keyframes moving-gradient {\n  0% {\n    background-position: -250px 0; }\n  100% {\n    background-position: 250px 0; } }\n\n.dataTable > thead > tr > th {\n  color: #333333; }\n\n.dataTable > tbody > tr > td {\n  font-size: 12px; }\n\ntable {\n  width: 100%; }\n\ntable tr {\n    border-bottom: 1px solid rgba(0, 0, 0, 0.1); }\n\ntable tr td {\n      vertical-align: middle;\n      padding: 8px; }\n\ntable tr td span {\n        display: block; }\n\ntable tr td.td-1 {\n        width: 20px; }\n\ntable tr td.td-1 span {\n          width: 20px;\n          height: 20px; }\n\ntable tr td.td-2 {\n        width: 50px; }\n\ntable tr td.td-2 span {\n          background-color: rgba(0, 0, 0, 0.15);\n          width: 50px;\n          height: 50px; }\n\ntable tr td.td-3 {\n        width: 400px; }\n\ntable tr td.td-3 span {\n          height: 12px;\n          background: linear-gradient(to right, #eee 20%, #ddd 50%, #eee 80%);\n          background-size: 500px 100px;\n          -webkit-animation-name: moving-gradient;\n                  animation-name: moving-gradient;\n          -webkit-animation-duration: 1s;\n                  animation-duration: 1s;\n          -webkit-animation-iteration-count: infinite;\n                  animation-iteration-count: infinite;\n          -webkit-animation-timing-function: linear;\n                  animation-timing-function: linear;\n          -webkit-animation-fill-mode: forwards;\n                  animation-fill-mode: forwards; }\n\ntable tr td.td-5 {\n        width: 100px; }\n\ntable tr td.td-5 span {\n          background-color: rgba(0, 0, 0, 0.15);\n          width: 100%;\n          height: 30px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zZXR0aW5ncy91c2Vycy9FOlxcVUktUHJvamVjdFxcRVRMXFxzcmNcXHNyYy9hcHBcXHNldHRpbmdzXFx1c2Vyc1xcdXNlcnMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFBNEIscUJBQXNCLEVBQUE7O0FBQ2xEO0VBQWtDLGNBQWMsRUFBQTs7QUFDaEQ7RUFBOEIsZUFBZSxFQUFBOztBQUU3QztFQUNJO0lBQUssNkJBQTZCLEVBQUE7RUFDbEM7SUFBTyw0QkFBNEIsRUFBQSxFQUFBOztBQUt2QztFQUNFLGNBQWMsRUFBQTs7QUFFaEI7RUFDRSxlQUFjLEVBQUE7O0FBRWhCO0VBQ0UsV0FBVyxFQUFBOztBQURiO0lBR0ksMkNBQXVDLEVBQUE7O0FBSDNDO01BTU0sc0JBQXNCO01BQ3RCLFlBQVksRUFBQTs7QUFQbEI7UUFTUSxjQUFjLEVBQUE7O0FBVHRCO1FBWVEsV0FBVyxFQUFBOztBQVpuQjtVQWNVLFdBQVc7VUFDWCxZQUFZLEVBQUE7O0FBZnRCO1FBbUJRLFdBQVcsRUFBQTs7QUFuQm5CO1VBcUJVLHFDQUFpQztVQUNqQyxXQUFXO1VBQ1gsWUFBWSxFQUFBOztBQXZCdEI7UUEyQlEsWUFBWSxFQUFBOztBQTNCcEI7VUE4QlUsWUFBWTtVQUNaLG1FQUFtRTtVQUNuRSw0QkFBNEI7VUFDNUIsdUNBQStCO2tCQUEvQiwrQkFBK0I7VUFDL0IsOEJBQXNCO2tCQUF0QixzQkFBc0I7VUFDdEIsMkNBQW1DO2tCQUFuQyxtQ0FBbUM7VUFDbkMseUNBQWlDO2tCQUFqQyxpQ0FBaUM7VUFDakMscUNBQTZCO2tCQUE3Qiw2QkFBNkIsRUFBQTs7QUFyQ3ZDO1FBMkNRLFlBQVksRUFBQTs7QUEzQ3BCO1VBNkNVLHFDQUFpQztVQUNqQyxXQUFXO1VBQ1gsWUFDRixFQUFBIiwiZmlsZSI6ImFwcC9zZXR0aW5ncy91c2Vycy91c2Vycy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInRyIHRkIC51c2VyLXF1aWNrLWFjdGlvbnMgeyBkaXNwbGF5OiBpbmxpbmUtYmxvY2sgfVxyXG50cjpob3ZlciB0ZCAudXNlci1xdWljay1hY3Rpb25zIHsgZGlzcGxheTogYmxvY2s7IH1cclxudHIgdGQgLnVzZXItcXVpY2stYWN0aW9ucyBhIHsgZm9udC1zaXplOiAxMnB4fVxyXG5cclxuQC13ZWJraXQta2V5ZnJhbWVzIG1vdmluZy1ncmFkaWVudCB7XHJcbiAgICAwJSB7IGJhY2tncm91bmQtcG9zaXRpb246IC0yNTBweCAwOyB9XHJcbiAgICAxMDAlIHsgYmFja2dyb3VuZC1wb3NpdGlvbjogMjUwcHggMDsgfVxyXG59XHJcblxyXG5cclxuXHJcbi5kYXRhVGFibGU+dGhlYWQ+dHI+dGh7XHJcbiAgY29sb3I6ICMzMzMzMzM7ICAgXHJcbn1cclxuLmRhdGFUYWJsZT50Ym9keT50cj50ZHtcclxuICBmb250LXNpemU6MTJweDtcclxufVxyXG50YWJsZSB7XHJcbiAgd2lkdGg6IDEwMCU7ICBcclxuICB0ciB7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgwLDAsMCwuMSk7XHJcbiAgICAgdGQge1xyXG4gICAgIFxyXG4gICAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG4gICAgICBwYWRkaW5nOiA4cHg7XHJcbiAgICAgIHNwYW4ge1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICB9XHJcbiAgICAgICYudGQtMSB7XHJcbiAgICAgICAgd2lkdGg6IDIwcHg7XHJcbiAgICAgICAgc3BhbiB7XHJcbiAgICAgICAgICB3aWR0aDogMjBweDtcclxuICAgICAgICAgIGhlaWdodDogMjBweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgJi50ZC0yIHtcclxuICAgICAgICB3aWR0aDogNTBweDtcclxuICAgICAgICBzcGFuIHtcclxuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsLjE1KTtcclxuICAgICAgICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgICAgICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAmLnRkLTMge1xyXG4gICAgICAgIHdpZHRoOiA0MDBweDtcclxuICAgICAgICAvLyBwYWRkaW5nLXJpZ2h0OiAxMDBweDtcclxuICAgICAgICBzcGFuIHtcclxuICAgICAgICAgIGhlaWdodDogMTJweDtcclxuICAgICAgICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgI2VlZSAyMCUsICNkZGQgNTAlLCAjZWVlIDgwJSk7XHJcbiAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IDUwMHB4IDEwMHB4O1xyXG4gICAgICAgICAgYW5pbWF0aW9uLW5hbWU6IG1vdmluZy1ncmFkaWVudDtcclxuICAgICAgICAgIGFuaW1hdGlvbi1kdXJhdGlvbjogMXM7XHJcbiAgICAgICAgICBhbmltYXRpb24taXRlcmF0aW9uLWNvdW50OiBpbmZpbml0ZTtcclxuICAgICAgICAgIGFuaW1hdGlvbi10aW1pbmctZnVuY3Rpb246IGxpbmVhcjtcclxuICAgICAgICAgIGFuaW1hdGlvbi1maWxsLW1vZGU6IGZvcndhcmRzO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAmLnRkLTQge1xyXG4gICAgICB9XHJcbiAgICAgICYudGQtNSB7XHJcbiAgICAgICAgd2lkdGg6IDEwMHB4O1xyXG4gICAgICAgIHNwYW4ge1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwuMTUpO1xyXG4gICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICBoZWlnaHQ6IDMwcHhcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn0gICJdfQ== */"

/***/ }),

/***/ "./src/app/settings/users/users.component.ts":
/*!***************************************************!*\
  !*** ./src/app/settings/users/users.component.ts ***!
  \***************************************************/
/*! exports provided: UsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersComponent", function() { return UsersComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _users_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./users.service */ "./src/app/settings/users/users.service.ts");
/* harmony import */ var ngx_treeview__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-treeview */ "./node_modules/ngx-treeview/src/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var _roles_roles_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../roles/roles.service */ "./src/app/settings/roles/roles.service.ts");
/* harmony import */ var _model_user__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../model/user */ "./src/app/model/user.ts");
/* harmony import */ var _shared_activity_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../shared/activity.service */ "./src/app/shared/activity.service.ts");
/* harmony import */ var _app_session_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../app.session-service */ "./src/app/app.session-service.ts");
/* harmony import */ var _shared_export_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../shared/export.service */ "./src/app/shared/export.service.ts");



// import { FeatureService } from '../features/features.service';








var UsersComponent = /** @class */ (function () {
    function UsersComponent(userService, activityService, roleService, 
    // private featureService: FeatureService,
    sessionService, router, exportService) {
        this.userService = userService;
        this.activityService = activityService;
        this.roleService = roleService;
        this.sessionService = sessionService;
        this.router = router;
        this.exportService = exportService;
        this.userModel = new _model_user__WEBPACK_IMPORTED_MODULE_7__["User"]();
        this.dropdownSettings = {};
        this.selectedRoles = [];
        this.dropdownList = new Array();
        this.userList = new Array();
        this.roles = new Array();
        this.features = new Array();
        this.userRoleList = new Array();
        this.userFeatureList = new Array();
        this.dropdownEnabled = true;
        this.config = ngx_treeview__WEBPACK_IMPORTED_MODULE_3__["TreeviewConfig"].create({
            hasAllCheckBox: true,
            hasFilter: true,
            hasCollapseExpand: true,
            decoupleChildFromParent: false,
            maxHeight: 400
        });
        this.buttonClasses = [
            'btn-outline-primary',
            'btn-outline-secondary',
            'btn-outline-success',
            'btn-outline-danger',
            'btn-outline-warning',
            'btn-outline-info',
            'btn-outline-light',
            'btn-outline-dark'
        ];
        this.buttonClass = this.buttonClasses[0];
        this.dataExport = null;
        this.startFrm = 90;
        this.progress = 0;
        this.isInternal = true;
        this.checked = true;
        this.cancelExport = false;
    }
    UsersComponent.prototype.ngOnInit = function () {
        this.loading = true;
        this.GetAllUser();
        // this.items = this.service.getBooks();
    };
    UsersComponent.prototype.OnSubmit = function (userModal) {
        var _this = this;
        userModal.close();
        this.userService.AddUser(this.userModel).subscribe(function (data) {
            _this.GetAllUser();
        }, function (err) {
            _this.GetAllUser();
        });
        // this.activityJson = [];
        // this.allActivity = this.activityService.getActivityList();
        // this.activityId = this.allActivity.find(x => x.activityName === EnumType.Summary_Select_Partner).id;
        var activitylog = {
            activityId: 1,
            activityData: JSON.stringify('test')
        };
        this.activityService.setActivityLog(activitylog).subscribe(function (data) {
        }, function (error) {
            console.error(error);
        });
    };
    UsersComponent.prototype.toggleChange = function (isExternal) {
        this.userModel.isInternal = isExternal ? 'External' : 'Internal';
    };
    UsersComponent.prototype.ShowDeleteUserModal = function (alertModal, userId) {
        alertModal.open();
        this.selectedUserId = userId;
    };
    UsersComponent.prototype.DeleteUser = function (alertModal) {
        var _this = this;
        alertModal.close();
        this.userService.DeleteUser(this.selectedUserId).subscribe(function (data) {
            _this.GetAllUser();
        }, function (err) {
            console.error(err);
        });
    };
    UsersComponent.prototype.GetAllUser = function () {
        var _this = this;
        this.userService.GetAllUser().subscribe(function (data) {
            _this.userList = JSON.parse(JSON.stringify(data)).results;
            _this.loading = false;
        }, function (err) {
            console.error(err);
        });
    };
    UsersComponent.prototype.GetAllRoles = function () {
        var _this = this;
        this.roleService.GetAllRoles().subscribe(function (data) {
            _this.roles = JSON.parse(JSON.stringify(data)).results;
        }, function (err) {
            console.error(err);
        });
    };
    // GetAllFeatureNode() {
    //   this.featureService.GetAllFeatureNode().subscribe(data => {
    //     this.features = JSON.parse(JSON.stringify(data)).results;
    //     this.selectedFeature = this.features[0];
    //     this.SelectApplication(this.selectedFeature);
    //   }, err => {
    //     console.error(err);
    //   });
    // }
    // GetUser(userid) {
    //   this.userService.GetUser(userid).subscribe(data => {
    //     console.log(data);
    //   },
    //     err => {
    //       console.error(err);
    //     }
    //   );
    // }
    UsersComponent.prototype.GetUserRoles = function (userid, userRoleModal) {
        var _this = this;
        this.GetAllRoles();
        this.loading = true;
        this.selectedUserId = userid;
        this.userRoleList = new Array();
        userRoleModal.open();
        this.userRoleList = [];
        this.userService.GetUserRoles(userid).subscribe(function (data) {
            _this.loading = false;
            if (JSON.parse(JSON.stringify(data)).results[0].roleDetailsResponse.length > 0) {
                _this.userRoleList = JSON.parse(JSON.stringify(data)).results[0].roleDetailsResponse;
            }
        }, function (err) {
        });
    };
    UsersComponent.prototype.GetUserFeatures = function (userid, userFeatureModal) {
        var _this = this;
        // this.GetAllFeatureNode();
        this.selectedUserId = userid;
        userFeatureModal.open();
        this.userService.GetUserFeatures(userid).subscribe(function (data) {
            if (JSON.parse(JSON.stringify(data)).results != null &&
                JSON.parse(JSON.stringify(data)).results[0].featureDetailsResponse.length > 0) {
                _this.userFeatureList = JSON.parse(JSON.stringify(data)).results[0].featureDetailsResponse;
            }
            else {
                _this.userFeatureList = null;
            }
        }, function (err) {
            _this.userRoleList = [];
        });
    };
    UsersComponent.prototype.IsAssignedRole = function (role) {
        if (this.userRoleList.length > 0) {
            return this.userRoleList.filter(function (n) { return n.roleId === role.roleId; }).length === 0 ? false : true;
        }
        else {
            return null;
        }
    };
    UsersComponent.prototype.AssignRole = function (o) {
    };
    UsersComponent.prototype.RemoveRole = function () {
    };
    // SelectApplication(selectedFeature) {
    //   this.featureService.GetAllMenu(selectedFeature.linkID)
    //     .subscribe(data => {
    //       if (JSON.parse(JSON.stringify(data)).results != null &&
    //         JSON.parse(JSON.stringify(data)).results.length > 0) {
    //         const response = JSON.parse(JSON.stringify(data)).results;
    //         response[1].forEach(element => {
    //           const children = [];
    //           const submenu = response[2].filter(n => n.parentID = element.id);
    //           submenu.forEach(elemen => {
    //             children.push({ text: elemen.name, value: elemen.featureID, checked: false });
    //           });
    //           this.items = [];
    //           this.items.push(new TreeviewItem({
    //             text: element.name, value: element.featureID, checked: false, children: children
    //           }));
    //         });
    //       } else {
    //         this.userFeatureList = null;
    //       }
    //     },
    //       err => {
    //         this.userRoleList = [];
    //       }
    //     );
    // }
    UsersComponent.prototype.Cancel = function (alertModal) {
        alertModal.close();
    };
    UsersComponent.prototype.LoginAs = function (user) {
        this.sessionService.setCookie('LA_USER_DETAIL', JSON.stringify(user));
        this.sessionService.setCookie('ROLES', '');
        var domain = window.location.origin;
        window.location.href = domain;
    };
    UsersComponent.prototype.GetRoles = function (userid) {
        var _this = this;
        this.userService.GetUserRoles(userid).subscribe(function (data) {
            if (JSON.parse(JSON.stringify(data)).success === true && JSON.parse(JSON.stringify(data)).results[0].roleDetailsResponse.length > 0) {
                _this.roles = JSON.parse(JSON.stringify(data)).results[0].roleDetailsResponse;
                return _this.roles;
            }
        }, function (err) {
        });
    };
    UsersComponent.prototype.ExportData = function () {
        this.exportService.exportAsExcelFile(this.userList, 'user-data');
    };
    UsersComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-users',
            template: __webpack_require__(/*! ./users.component.html */ "./src/app/settings/users/users.component.html"),
            styles: [__webpack_require__(/*! ./users.component.scss */ "./src/app/settings/users/users.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_users_service__WEBPACK_IMPORTED_MODULE_2__["UserService"],
            _shared_activity_service__WEBPACK_IMPORTED_MODULE_8__["ActivityService"],
            _roles_roles_service__WEBPACK_IMPORTED_MODULE_6__["RolesService"],
            _app_session_service__WEBPACK_IMPORTED_MODULE_9__["SessionService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _shared_export_service__WEBPACK_IMPORTED_MODULE_10__["ExportService"]])
    ], UsersComponent);
    return UsersComponent;
}());



/***/ }),

/***/ "./src/app/settings/users/users.service.ts":
/*!*************************************************!*\
  !*** ./src/app/settings/users/users.service.ts ***!
  \*************************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _app_session_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../app.session-service */ "./src/app/app.session-service.ts");
/* harmony import */ var _environments_config__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../environments/config */ "./src/environments/config.ts");







var UserService = /** @class */ (function () {
    function UserService(http, sessionService, httpClient, common) {
        this.http = http;
        this.sessionService = sessionService;
        this.httpClient = httpClient;
        this.common = common;
        this.LA_SessionData = "";
        this.baseUrl = _environments_config__WEBPACK_IMPORTED_MODULE_6__["Config"].getEnvironmentVariable('endPoint');
        this.AddUserEndpoint = this.baseUrl + 'users';
        this.AddUserRoleEndpoint = this.baseUrl + 'users/';
        this.UpdateUserEndpoint = this.baseUrl + 'users/';
        this.DeleteUserEndpoint = this.baseUrl + 'users/';
        this.GetUserEndpoint = this.baseUrl + 'users/';
        this.SignInUserEndpoint = this.baseUrl + 'user/';
        this.UserAuditEndpoint = this.baseUrl + 'user/';
        this.GetUserFeaturesEndpoint = this.baseUrl + 'users' + '/userid' + '/features';
        this.GetAllUserEndpoint = this.baseUrl + 'users';
        this.onUserResponseChanged = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"]({});
        this.onRoleResponseChanged = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"]({});
    }
    UserService.prototype.AddUser = function (data) {
        data.createdBy = this.sessionService.getSessionValue('uid');
        return this.http.post(this.AddUserEndpoint, data);
    };
    UserService.prototype.AddUserRole = function (userId, data) {
        data.createdBy = this.sessionService.getSessionValue('uid');
        return this.http.post(this.AddUserRoleEndpoint + (userId + "/roles"), data);
    };
    UserService.prototype.AddUserFeature = function (userId, data) {
        data.createdBy = this.sessionService.getSessionValue('uid');
        return this.http.post(_environments_config__WEBPACK_IMPORTED_MODULE_6__["Config"].getEnvironmentVariable('endPoint') + ("users/" + userId + "/features"), data);
    };
    UserService.prototype.UpdateUser = function (data) {
        data.updatedBy = this.sessionService.getSessionValue('uid');
        return this.http.put(this.UpdateUserEndpoint + data.userId, data);
    };
    UserService.prototype.DeleteUser = function (id) {
        return this.http.delete(this.DeleteUserEndpoint + id);
    };
    UserService.prototype.GetUser = function () {
        var _this = this;
        // return this.http.get<User>(this.GetUserEndpoint + userid);
        return new Promise(function (resolve, reject) {
            if (_this.routeParams.id === '') {
                _this.onUserResponseChanged.next(false);
                resolve(false);
            }
            else {
                // this.GetUserRoles(this.routeParams.id);
                return _this.http.get(_this.GetUserEndpoint + _this.routeParams.id).subscribe(function (res) {
                    var response = res;
                    _this.onUserResponseChanged.next(response);
                    resolve(response);
                }, reject);
            }
        });
    };
    UserService.prototype.GetSignInUser = function (userid) {
        // this.sessionData = JSON.parse(this.sessionService.getCookie("LD_USER_DETAIL"));
        // let uid = this.sessionData.uid;
        // if (this.sessionService.getCookie("LA_USER_DETAIL") != "") {
        //     this.LA_SessionData = JSON.parse(this.sessionService.getCookie("LA_USER_DETAIL"));
        //     uid = this.LA_SessionData.userId;
        // }
        // return this.http.get<User>(this.SignInUserEndpoint + userid);
        return this.http.get(this.SignInUserEndpoint + userid);
    };
    UserService.prototype.UserAudit = function (userid, statu) {
        var data = {
            status: statu,
            loginBy: this.sessionService.getSessionValue('uid')
        };
        return this.http.post(this.UserAuditEndpoint + userid, data);
    };
    UserService.prototype.GetUserRoles = function (userId) {
        return this.http.get(_environments_config__WEBPACK_IMPORTED_MODULE_6__["Config"].getEnvironmentVariable('endPoint') + ("users/" + userId + "/roles"));
        //   return new Promise((resolve, reject) => {
        //     if (this.routeParams.id === '') {
        //         this.onRoleResponseChanged.next(false);
        //         resolve(false);
        //     } else {
        //         // this.GetUserRoles(this.routeParams.id);
        //         return this.http.get(Config.getEnvironmentVariable('endPoint') + `users/${this.routeParams.id}/roles`).subscribe((res: any) => {
        //             const response = res;
        //             this.onUserResponseChanged.next(response);
        //             resolve(response);
        //         }, reject);
        //     }
        // });
    };
    UserService.prototype.RevokeUserRole = function (userId, roleId) {
        return this.http.delete(_environments_config__WEBPACK_IMPORTED_MODULE_6__["Config"].getEnvironmentVariable('endPoint') + ("users/" + userId + "/roles/" + roleId));
    };
    UserService.prototype.GetUserFeatures = function (userid) {
        return this.http.get(this.GetUserFeaturesEndpoint);
    };
    UserService.prototype.GetAllUser = function () {
        return this.http.get(this.GetAllUserEndpoint);
    };
    UserService.prototype.resolve = function (route, state) {
        var _this = this;
        this.routeParams = route.params;
        return new Promise(function (resolve, reject) {
            Promise.all([
                _this.GetUser()
            ]).then(function () {
                resolve();
            }, reject);
            // Promise.all([
            //     this.GetUserRoles()
            // ]).then(
            //     () => {
            //         resolve();
            //     },
            //     reject
            // );
        });
    };
    UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _app_session_service__WEBPACK_IMPORTED_MODULE_5__["SessionService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/app/shared/activity.service.ts":
/*!********************************************!*\
  !*** ./src/app/shared/activity.service.ts ***!
  \********************************************/
/*! exports provided: ActivityService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActivityService", function() { return ActivityService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/config */ "./src/environments/config.ts");
/* harmony import */ var _app_session_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../app.session-service */ "./src/app/app.session-service.ts");





var ActivityService = /** @class */ (function () {
    function ActivityService(http, _SessionService) {
        this.http = http;
        this._SessionService = _SessionService;
        this.baseUrl = _environments_config__WEBPACK_IMPORTED_MODULE_3__["Config"].getEnvironmentVariable('endPoint');
        this.activityEndPoint = this.baseUrl + 'api/v1/' + 'users/';
        this.LA_SessionData = "";
        this.myGlobalVar = "true";
        // alert("My intial global variable value is: " + this.myGlobalVar);
    }
    ActivityService.prototype.setActivityData = function () {
        this.sessionData = JSON.parse(this._SessionService.getCookie("LD_USER_DETAIL"));
        var uid = this.sessionData.uid;
        if (this._SessionService.getCookie("LA_USER_DETAIL") !== "") {
            this.LA_SessionData = JSON.parse(this._SessionService.getCookie("LA_USER_DETAIL"));
            uid = this.LA_SessionData.userId;
        }
        return this.http.get(this.activityEndPoint + (uid + "/activity"));
    };
    ActivityService.prototype.setValue = function (value) {
        this.allActivity = value;
        localStorage.setItem("allActivity", JSON.stringify(value));
    };
    ActivityService.prototype.getActivityList = function () {
        if (this.allActivity.length > 0) {
            return this.allActivity;
        }
        else {
            var allActivityList = [];
            return allActivityList;
        }
    };
    ActivityService.prototype.setActivityLog = function (activityData) {
        this.sessionData = JSON.parse(this._SessionService.getCookie("LD_USER_DETAIL"));
        var uid = this.sessionData.uid;
        if (this._SessionService.getCookie("LA_USER_DETAIL") !== "") {
            this.LA_SessionData = JSON.parse(this._SessionService.getCookie("LA_USER_DETAIL"));
            uid = this.LA_SessionData.userId;
        }
        return this.http.post(this.activityEndPoint + (uid + "/activity"), activityData);
    };
    ActivityService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _app_session_service__WEBPACK_IMPORTED_MODULE_4__["SessionService"]])
    ], ActivityService);
    return ActivityService;
}());



/***/ }),

/***/ "./src/app/shared/data.service.ts":
/*!****************************************!*\
  !*** ./src/app/shared/data.service.ts ***!
  \****************************************/
/*! exports provided: DataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataService", function() { return DataService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _model_connection__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../model/connection */ "./src/app/model/connection.ts");




var DataService = /** @class */ (function () {
    function DataService() {
        this.selectedConnection = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]('');
        this.getConnector = this.selectedConnection.asObservable();
        this.messageSource = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]('');
        this.currentMessage = this.messageSource.asObservable();
        this.barChartData = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](new Object());
        this.connectionDetail = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](new _model_connection__WEBPACK_IMPORTED_MODULE_3__["Connection"]());
        this.jobDetail = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](new _model_connection__WEBPACK_IMPORTED_MODULE_3__["Connection"]());
        this.invokeEvent = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
    }
    DataService.prototype.setConnector = function (connector) {
        this.selectedConnection.next(connector);
    };
    DataService.prototype.setCurrentUrl = function (message) {
        this.messageSource.next(message);
    };
    DataService.prototype.getCurrentUrl = function () {
        return this.messageSource;
    };
    DataService.prototype.setData = function (data) {
        this.barChartData.next(data);
    };
    DataService.prototype.getData = function () {
        return this.barChartData;
    };
    DataService.prototype.setConnectionDetail = function (data) {
        this.connectionDetail.next(data);
    };
    DataService.prototype.getConnectionDetail = function () {
        return this.connectionDetail;
    };
    DataService.prototype.setAction = function (key) {
        this.invokeEvent.next(key);
    };
    DataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DataService);
    return DataService;
}());



/***/ }),

/***/ "./src/app/shared/export.service.ts":
/*!******************************************!*\
  !*** ./src/app/shared/export.service.ts ***!
  \******************************************/
/*! exports provided: ExportService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExportService", function() { return ExportService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! file-saver */ "./node_modules/file-saver/dist/FileSaver.min.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var xlsx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! xlsx */ "./node_modules/xlsx/xlsx.js");
/* harmony import */ var xlsx__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(xlsx__WEBPACK_IMPORTED_MODULE_3__);




var EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
var EXCEL_EXTENSION = '.xlsx';
var ExportService = /** @class */ (function () {
    function ExportService() {
    }
    ExportService.prototype.exportAsExcelFile = function (json, excelFileName) {
        var worksheet = xlsx__WEBPACK_IMPORTED_MODULE_3__["utils"].json_to_sheet(json);
        var workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        var excelBuffer = xlsx__WEBPACK_IMPORTED_MODULE_3__["write"](workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    };
    ExportService.prototype.exportAccountOwnerExcelFile = function (json, excelFileName) {
        var worksheet = xlsx__WEBPACK_IMPORTED_MODULE_3__["utils"].json_to_sheet(json);
        var workbook = { Sheets: { 'AccountOwner': worksheet }, SheetNames: ['AccountOwner'] };
        var excelBuffer = xlsx__WEBPACK_IMPORTED_MODULE_3__["write"](workbook, { bookType: 'xlsx', type: 'array' });
        var today = new Date();
        var month = new Array();
        month[0] = "Jan";
        month[1] = "Feb";
        month[2] = "Mar";
        month[3] = "Apr";
        month[4] = "May";
        month[5] = "Jun";
        month[6] = "Jul";
        month[7] = "Aug";
        month[8] = "Sep";
        month[9] = "Oct";
        month[10] = "Nov";
        month[11] = "Dec";
        var MON = month[today.getMonth()];
        var DD = today.getDate();
        var YYYY = today.getFullYear();
        this.saveAsExcelFile(excelBuffer, (excelFileName + ("_AccountOwnership_<" + MON + "/" + DD + "/" + YYYY + ">")));
    };
    ExportService.prototype.saveAsExcelFile = function (buffer, fileName) {
        var data = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        file_saver__WEBPACK_IMPORTED_MODULE_2__["saveAs"](data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    };
    ExportService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ExportService);
    return ExportService;
}());



/***/ }),

/***/ "./src/app/shared/shared.service.ts":
/*!******************************************!*\
  !*** ./src/app/shared/shared.service.ts ***!
  \******************************************/
/*! exports provided: SharedService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedService", function() { return SharedService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _model_EnumType__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../model/EnumType */ "./src/app/model/EnumType.ts");
/* harmony import */ var _model_user__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/user */ "./src/app/model/user.ts");
/* harmony import */ var _settings_users_users_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../settings/users/users.service */ "./src/app/settings/users/users.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_session_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../app.session-service */ "./src/app/app.session-service.ts");







var SharedService = /** @class */ (function () {
    function SharedService(userService, sessionService) {
        this.userService = userService;
        this.sessionService = sessionService;
        this.setDataSubject = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subject"]();
        this.UIPermissionEnabled = false;
        this.settingPermission = false;
        this.isUserAuthorized = true;
        this.roles = new Array();
        this.laSession = '';
        this.isFullScreen = true;
        // this.setData$ = this.setDataSubject.asObservable();
        // this.setDatas$ = this.setDataSubject.asObservable();
    }
    SharedService.prototype.setData = function (data) {
        this.setDataSubject.next(data);
    };
    SharedService.prototype.GetUserRoles = function () {
        var _this = this;
        var userid = this.sessionService.getCookie('LD_USER_DETAIL').uid;
        if (userid !== undefined) {
            userid = JSON.parse(JSON.stringify(this.sessionService.getCookie('LOGIN').results[0].userId));
        }
        if (this.sessionService.getCookie('LA_USER_DETAIL') !== '') {
            this.laSession = this.sessionService.getCookie('LA_USER_DETAIL');
            userid = this.laSession.userId;
        }
        this.userService.GetUserRoles(userid).subscribe(function (data) {
            if (JSON.parse(JSON.stringify(data)).success === true && JSON.parse(JSON.stringify(data)).results[0].roleDetailsResponse.length > 0) {
                _this.UIPermissionEnabled = true;
                _this.isUserAuthorized = true;
                _this.roles = JSON.parse(JSON.stringify(data)).results[0].roleDetailsResponse;
                _this.settingPermission = _this.roles.filter(function (n) { return n.role === _model_EnumType__WEBPACK_IMPORTED_MODULE_1__["EnumType"].SUPER_ADMIN; }).length > 0 ? true : false;
                _this.sessionService.setCookie('ROLES', JSON.stringify(_this.roles));
            }
            else {
                _this.UIPermissionEnabled = false;
                _this.isUserAuthorized = false;
            }
        }, function (err) {
        });
    };
    SharedService.prototype.updateUserProfile = function () {
        var sessionData = this.sessionService.getCookie('LD_USER_DETAIL');
        var user = new _model_user__WEBPACK_IMPORTED_MODULE_2__["User"]();
        user.firstName = sessionData.firstname;
        user.lastName = sessionData.lastname;
        user.emailId = sessionData.mail;
        user.userId = sessionData.uid;
        user.createdBy = 'System';
        this.userService.AddUser(user).subscribe(function (data) {
        }, function (err) { });
    };
    SharedService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_settings_users_users_service__WEBPACK_IMPORTED_MODULE_3__["UserService"],
            _app_session_service__WEBPACK_IMPORTED_MODULE_6__["SessionService"]])
    ], SharedService);
    return SharedService;
}());



/***/ }),

/***/ "./src/app/shared/tour-guide/tour-guide.component.html":
/*!*************************************************************!*\
  !*** ./src/app/shared/tour-guide/tour-guide.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"help_icon\" (click)=\"isShow = true\">\r\n    Help <i class=\"fa fa-arrow-up\"></i>\r\n</div>\r\n<kendo-dialog title=\"How can we help you?\" *ngIf=\"isShow\" (close)=\"close('cancel')\" [width]=\"400\" [height]=\"350\">\r\n    <div class=\"input-group\">\r\n        <input type=\"text\" class=\"form-control\" [(ngModel)]=\"query\">\r\n        <span class=\"input-group-addon\"><i class=\"fa fa-search\"></i></span>\r\n    </div>\r\n    <ul class=\"list-group\">\r\n        <li [routerLink]=\"[item.route]\" [queryParams]=\"{ action: 'intro'}\" (click)=\"isShow = false\" \r\n            *ngFor=\"let item of introList | search:'title':query\" class=\"list-group-item\" style=\"cursor: pointer\" (click)=\"setCookie(item.view)\">\r\n            <div style=\"cursor: pointer;display: inline-flex;\">\r\n                <svg [attr.class]=\"item.icon\" shape-rendering=\"geometricPrecision\" width=\"20\" height=\"20\">\r\n                    <use [attr.xlink:href]=\"'./assets/dist/img/icons.svg#' + item.icon\"></use>\r\n                </svg> &nbsp;{{item.title}}\r\n            </div>\r\n        </li>\r\n    </ul>\r\n</kendo-dialog>"

/***/ }),

/***/ "./src/app/shared/tour-guide/tour-guide.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/shared/tour-guide/tour-guide.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#help_icon {\n  position: fixed;\n  bottom: 0px;\n  margin-left: 30%;\n  border: solid 1px;\n  padding: 5px 30px;\n  border-radius: 10px 10px 0px 0px;\n  background: white;\n  border-bottom: solid 3px #38414A !important;\n  cursor: pointer; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvdG91ci1ndWlkZS9FOlxcVUktUHJvamVjdFxcRVRMXFxzcmNcXHNyYy9hcHBcXHNoYXJlZFxcdG91ci1ndWlkZVxcdG91ci1ndWlkZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGVBQWU7RUFDZixXQUFXO0VBQ1gsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixpQkFBaUI7RUFDakIsZ0NBQWdDO0VBQ2hDLGlCQUFpQjtFQUNqQiwyQ0FBMkM7RUFDM0MsZUFBZSxFQUFBIiwiZmlsZSI6ImFwcC9zaGFyZWQvdG91ci1ndWlkZS90b3VyLWd1aWRlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2hlbHBfaWNvbiB7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIGJvdHRvbTogMHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiAzMCU7XHJcbiAgYm9yZGVyOiBzb2xpZCAxcHg7XHJcbiAgcGFkZGluZzogNXB4IDMwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweCAxMHB4IDBweCAwcHg7XHJcbiAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgYm9yZGVyLWJvdHRvbTogc29saWQgM3B4ICMzODQxNEEgIWltcG9ydGFudDtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/shared/tour-guide/tour-guide.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/shared/tour-guide/tour-guide.component.ts ***!
  \***********************************************************/
/*! exports provided: TourGuideComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TourGuideComponent", function() { return TourGuideComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");



var TourGuideComponent = /** @class */ (function () {
    function TourGuideComponent(cookieService) {
        this.cookieService = cookieService;
        this.introList = [
            {
                route: 'connections', title: 'New Odata Connection', icon: 'icon_plug', view: 'connections'
            },
            {
                route: 'jobs', title: 'Run Job', icon: 'icon_job', view: 'runJobs'
            },
            {
                route: 'jobs', title: 'Edit Job', icon: 'icon_job', view: 'editJobs'
            },
            {
                route: 'jobs', title: 'Remove Job', icon: 'icon_job', view: 'removeJobs'
            },
            {
                route: 'jobs/new', title: 'New Job', icon: 'icon_job', view: 'createNewJob'
            },
            {
                route: 'bulk-load/new', title: 'New Bulk Job Without Schedular', icon: 'icon_job', view: 'newbulkjob'
            },
            {
                route: 'bulk-load/new', title: 'New Bulk Job With Schedular', icon: 'icon_job', view: 'newbulkjobWithSchedular'
            },
            {
                route: 'jobs/new', title: 'New Job with Existing Table', icon: 'icon_job', view: 'createNewJobWithExistingTable'
            },
            {
                route: 'jobs/new', title: 'Create New Job with Filter', icon: 'icon_job', view: 'createNewJobWithFilter'
            },
            {
                route: 'jobs/new', title: 'Create New Job with Filter With Extsting Table', icon: 'icon_job', view: 'createNewJobWithFilterWithExistingTable'
            },
            {
                route: 'jobs/new', title: 'Create New Job with Filter With Increment Value', icon: 'icon_job', view: 'createNewJobWithIncrementValue'
            },
            {
                route: 'jobs/new', title: 'Create New Job with Filter With Increment Value With Existing Table', icon: 'icon_job', view: 'createNewJobWithIncrementValueExistingtable'
            }
        ];
        this.isShow = false;
    }
    TourGuideComponent.prototype.setCookie = function (value) {
        this.cookieService.set('intro', value);
    };
    TourGuideComponent.prototype.ngOnInit = function () {
    };
    TourGuideComponent.prototype.close = function () {
        this.isShow = false;
    };
    TourGuideComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tour-guide',
            template: __webpack_require__(/*! ./tour-guide.component.html */ "./src/app/shared/tour-guide/tour-guide.component.html"),
            styles: [__webpack_require__(/*! ./tour-guide.component.scss */ "./src/app/shared/tour-guide/tour-guide.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [ngx_cookie_service__WEBPACK_IMPORTED_MODULE_2__["CookieService"]])
    ], TourGuideComponent);
    return TourGuideComponent;
}());



/***/ }),

/***/ "./src/environments/config.ts":
/*!************************************!*\
  !*** ./src/environments/config.ts ***!
  \************************************/
/*! exports provided: Config */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Config", function() { return Config; });
var Config = /** @class */ (function () {
    function Config() {
    }
    Config.getEnvironmentVariable = function (value) {
        var environment;
        var data = {};
        environment = window.location.hostname;
        switch (environment) {
            //         case 'localhost':
            //             data = {
            //                 // apiUrl: 'http://10.103.129.17:8099/api/v1/',
            //                 apiUrl: 'http://192.168.0.107:8090/api/v1/',
            //                 endPoint: 'http://192.168.0.69:8090/api/v1/'
            //             };
            //             break;
            //         case '10.103.129.17':
            //             data = {
            //                 apiUrl: 'http://10.103.129.17:8088/api/v1/',
            //                 // apiUrl: 'http://192.168.0.104:8099/api/v1/',
            //                 // endPoint: 'http://192.168.0.69:8090/api/v1/'
            //                 endPoint: 'https://core-api-rxz-core-stg.npc-us-west-dc61.ncloud.netapp.com/api/v1/',
            //             };
            //             break;
            //         case '10.209.14.97':
            //             data = {
            //                 apiUrl: 'http://10.209.14.97:8090/api/v1/',
            //                 // apiUrl: 'http://192.168.0.104:8099/api/v1/',
            //                 endPoint: 'http://10.209.14.97:8090/api/v1'
            //             };
            //             break;
            //         default:
            //             data = {
            //                 endPoint: 'https://dev.xxxx.com/'
            //             };
            //     }
            //     return data[value];
            case 'localhost':
                data = {
                    // apiUrl: 'http://10.103.129.17:8099/api/v1/',
                    apiUrl: 'http://192.168.0.107:8090/api/v1/',
                    endPoint: 'http://cst-etl-vm.eastus.cloudapp.azure.com:8090/api/v1'
                };
                break;
            case '10.103.129.17':
                data = {
                    apiUrl: 'http://10.103.129.17:8088/api/v1/',
                    // apiUrl: 'http://192.168.0.104:8099/api/v1/',
                    endPoint: 'http://192.168.0.69:8090/api/v1'
                };
                break;
            case '10.209.14.97':
                data = {
                    apiUrl: 'http://10.209.14.97:8090/api/v1/',
                    // apiUrl: 'http://192.168.0.104:8099/api/v1/',
                    endPoint: 'http://10.209.14.97:8090/api/v1'
                };
                break;
            case 'cst-etl-vm.eastus.cloudapp.azure.com':
                data = {
                    apiUrl: 'http://10.209.14.97:8090/api/v1/',
                    // apiUrl: 'http://192.168.0.104:8099/api/v1/',
                    endPoint: 'http://cst-etl-vm.eastus.cloudapp.azure.com:8090/api/v1'
                };
                break;
            default:
                data = {
                    endPoint: 'https://dev.xxxx.com/'
                };
        }
        return data[value];
    };
    return Config;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    //apiUrl: "http://192.168.0.24:8081/secured/",
    // ------------------ TEST REMOTE SERVER ------------------ //
    apiUrl: "http://10.103.129.17:8080/api/secured/",
    authApiUrl: "api/v1/",
    oAuthApiUrl: "http://10.103.129.17:8080/api/v1/oauth/",
    // ------------------ TPASS SERVER ------------------ //
    // apiUrl: "http://api-netapp.tpaas.netapp.com/api/secured/",
    // authApiUrl: "http://api-netapp.tpaas.netapp.com/api/v1/",
    // oAuthApiUrl: "http://api-netapp.tpaas.netapp.com/api/v1/oauth/",
    // ------------------ SPASS SERVER ------------------ //
    //apiUrl: "http://api-netapp.spaas.netapp.com/api/secured/",
    //authApiUrl: "http://api-netapp.spaas.netapp.com/api/v1/",
    //oAuthApiUrl: "http://api-netapp.spaas.netapp.com/api/v1/oauth/",
    //oAuth_SSO_Url: "https://login-stage.netapp.com/ms_oauth/oauth2/endpoints/oauthservice/",
    // ------------------ PPASS SERVER ------------------ //
    // apiUrl: "http://api-netapp.paas.netapp.com/api/secured/",
    // authApiUrl: "api/v1/",
    // oAuthApiUrl: "http://api-netapp.paas.netapp.com/api/v1/oauth/",
    // ------------------ PRODUCTION SSO ------------------ //
    // oAuth_SSO_Url: "https://login.netapp.com/ms_oauth/oauth2/endpoints/oauthservice/",
    // oAuth_SSO_Auth_Url: "authorize?response_type=code&client_id=leadapp&redirect_uri=",
    // oAuth_SSO_Auth_Profile_Url: "https://login.netapp.com/ms_oauth/resources/leadapp/me",
    // oAuth_SSO_Logout: " https://login.netapp.com/oam/server/logout?end_url=",
    // oAuth_SSO_Token: "https://login.netapp.com/ms_oauth/oauth2/endpoints/oauthservice/tokens",
    dataMigrationUrl: "http://10.103.129.17:10095/api/v1/",
    // ------------------ STAGE SSO ------------------ //
    oAuth_SSO_Url: "https://login-stage.netapp.com/ms_oauth/oauth2/endpoints/oauthservice/",
    oAuth_SSO_Auth_Url: "authorize?response_type=code&client_id=leadapp&redirect_uri=",
    oAuth_SSO_Auth_Profile_Url: "https://login-stage.netapp.com/ms_oauth/resources/leadapp/me",
    oAuth_SSO_Logout: " https://login-stage.netapp.com/oam/server/logout?end_url=",
    oAuth_SSO_Token: "https://login-stage.netapp.com/ms_oauth/oauth2/endpoints/oauthservice/tokens",
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\UI-Project\ETL\src\src\main.ts */"./src/main.ts");


/***/ }),

/***/ 1:
/*!********************!*\
  !*** fs (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 2:
/*!************************!*\
  !*** crypto (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 3:
/*!************************!*\
  !*** stream (ignored) ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map