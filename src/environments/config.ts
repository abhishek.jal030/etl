export class Config {
    public static getEnvironmentVariable(value) {
        var environment: string;
        var data = {};
        environment = window.location.hostname;
        switch (environment) {
            //         case 'localhost':
            //             data = {
            //                 // apiUrl: 'http://10.103.129.17:8099/api/v1/',
            //                 apiUrl: 'http://192.168.0.107:8090/api/v1/',
            //                 endPoint: 'http://192.168.0.69:8090/api/v1/'
            //             };
            //             break;
            //         case '10.103.129.17':
            //             data = {
            //                 apiUrl: 'http://10.103.129.17:8088/api/v1/',
            //                 // apiUrl: 'http://192.168.0.104:8099/api/v1/',
            //                 // endPoint: 'http://192.168.0.69:8090/api/v1/'
            //                 endPoint: 'https://core-api-rxz-core-stg.npc-us-west-dc61.ncloud.netapp.com/api/v1/',

            //             };
            //             break;
            //         case '10.209.14.97':
            //             data = {
            //                 apiUrl: 'http://10.209.14.97:8090/api/v1/',
            //                 // apiUrl: 'http://192.168.0.104:8099/api/v1/',
            //                 endPoint: 'http://10.209.14.97:8090/api/v1'
            //             };
            //             break;
            //         default:
            //             data = {
            //                 endPoint: 'https://dev.xxxx.com/'
            //             };
            //     }
            //     return data[value];
            case 'localhost':
                data = {
                    // apiUrl: 'http://10.103.129.17:8099/api/v1/',
                    apiUrl: 'http://10.209.14.17:8098/api/v1/',
                    endPoint: 'http://cst-etl-vm.eastus.cloudapp.azure.com:8090/api/v1'
                };
                break;
            case '10.103.129.17':
                data = {
                    apiUrl: 'http://10.103.129.17:8088/api/v1/',
                    // apiUrl: 'http://192.168.0.104:8099/api/v1/',
                    endPoint: 'http://192.168.0.69:8090/api/v1'
                };
                break;
            case '10.209.14.97':
                data = {
                    apiUrl: 'http://10.209.14.97:8090/api/v1/',
                    // apiUrl: 'http://192.168.0.104:8099/api/v1/',
                    endPoint: 'http://10.209.14.97:8090/api/v1'
                };
                break;
            case 'cst-etl-vm.eastus.cloudapp.azure.com':
                data = {
                    apiUrl: 'http://10.209.14.97:8090/api/v1/',
                    // apiUrl: 'http://192.168.0.104:8099/api/v1/',
                    endPoint: 'http://cst-etl-vm.eastus.cloudapp.azure.com:8090/api/v1'
                };
                break;
            default:
                data = {
                    endPoint: 'https://dev.xxxx.com/'
                };
        }
        return data[value];


    }

}
