export const environment = {
  production: true,
  //apiUrl: "http://192.168.0.24:8081/secured/",
  //apiUrl: "http://api-netapp.paas.netapp.com/api/secured/",
 apiUrl: "http://10.103.129.17:8080/api/secured/",
  //authApiUrl: "http://api-netapp.paas.netapp.com/api/v1/",
  authApiUrl: "http://10.103.129.17:8080/api/v1/",
  oAuthApiUrl: "http://10.103.129.17:8080/api/v1/oauth/",
  oAuth_SSO_Url: "https://login.netapp.com/ms_oauth/oauth2/endpoints/oauthservice/",
  oAuth_SSO_Auth_Url: "authorize?response_type=code&client_id=leadapp&redirect_uri=",
  oAuth_SSO_Auth_Profile_Url: "https://login.netapp.com/ms_oauth/resources/leadapp/me",
  oAuth_SSO_Logout: " https://login.netapp.com/oam/server/logout?end_url=",
  oAuth_SSO_Token: "https://login.netapp.com/ms_oauth/oauth2/endpoints/oauthservice/tokens"
};
