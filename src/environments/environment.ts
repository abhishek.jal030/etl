// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  //apiUrl: "http://192.168.0.24:8081/secured/",

  // ------------------ TEST REMOTE SERVER ------------------ //
  apiUrl: "http://10.103.129.17:8080/api/secured/",
  authApiUrl: "api/v1/",
  oAuthApiUrl: "http://10.103.129.17:8080/api/v1/oauth/",

  // ------------------ TPASS SERVER ------------------ //
  // apiUrl: "http://api-netapp.tpaas.netapp.com/api/secured/",
  // authApiUrl: "http://api-netapp.tpaas.netapp.com/api/v1/",
  // oAuthApiUrl: "http://api-netapp.tpaas.netapp.com/api/v1/oauth/",

  // ------------------ SPASS SERVER ------------------ //
  //apiUrl: "http://api-netapp.spaas.netapp.com/api/secured/",
  //authApiUrl: "http://api-netapp.spaas.netapp.com/api/v1/",
  //oAuthApiUrl: "http://api-netapp.spaas.netapp.com/api/v1/oauth/",
  //oAuth_SSO_Url: "https://login-stage.netapp.com/ms_oauth/oauth2/endpoints/oauthservice/",

  // ------------------ PPASS SERVER ------------------ //
  // apiUrl: "http://api-netapp.paas.netapp.com/api/secured/",
  // authApiUrl: "api/v1/",
  // oAuthApiUrl: "http://api-netapp.paas.netapp.com/api/v1/oauth/",

  // ------------------ PRODUCTION SSO ------------------ //
  // oAuth_SSO_Url: "https://login.netapp.com/ms_oauth/oauth2/endpoints/oauthservice/",
  // oAuth_SSO_Auth_Url: "authorize?response_type=code&client_id=leadapp&redirect_uri=",
  // oAuth_SSO_Auth_Profile_Url: "https://login.netapp.com/ms_oauth/resources/leadapp/me",
  // oAuth_SSO_Logout: " https://login.netapp.com/oam/server/logout?end_url=",
  // oAuth_SSO_Token: "https://login.netapp.com/ms_oauth/oauth2/endpoints/oauthservice/tokens",
  dataMigrationUrl: "http://10.103.129.17:10095/api/v1/",

  // ------------------ STAGE SSO ------------------ //
  oAuth_SSO_Url: "https://login-stage.netapp.com/ms_oauth/oauth2/endpoints/oauthservice/",
  oAuth_SSO_Auth_Url: "authorize?response_type=code&client_id=leadapp&redirect_uri=",
  oAuth_SSO_Auth_Profile_Url: "https://login-stage.netapp.com/ms_oauth/resources/leadapp/me",
  oAuth_SSO_Logout: " https://login-stage.netapp.com/oam/server/logout?end_url=",
  oAuth_SSO_Token: "https://login-stage.netapp.com/ms_oauth/oauth2/endpoints/oauthservice/tokens",
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
