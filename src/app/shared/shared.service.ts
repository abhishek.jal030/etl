
import { Role } from '../model/role';
import { EnumType } from '../model/EnumType';
import { User } from '../model/user';
import { UserService } from '../settings/users/users.service';
import { Subject, Observable, BehaviorSubject, Subscription } from 'rxjs';
import { Injectable, EventEmitter } from '@angular/core';
import { SessionService } from '../app.session-service';
@Injectable()
export class SharedService {
  setData$: Observable<any>;
  private setDataSubject = new Subject<any>();
  UIPermissionEnabled = false;
  settingPermission = false;
  isUserAuthorized = true;
  isFullScreen: boolean;
  roles: Array<Role> = new Array<Role>();
  laSession: any;
  constructor(
    private userService: UserService,
    private sessionService: SessionService,
    
  ) {
    this.laSession = '';
     this.isFullScreen = true;
    // this.setData$ = this.setDataSubject.asObservable();
    // this.setDatas$ = this.setDataSubject.asObservable();
  }

  setData(data) {
    this.setDataSubject.next(data);
  }
  GetUserRoles() {
    let userid = this.sessionService.getCookie('LD_USER_DETAIL').uid;
if(userid!==undefined) {
  userid =  JSON.parse(JSON.stringify(this.sessionService.getCookie('LOGIN').results[0].userId));

}
    if (this.sessionService.getCookie('LA_USER_DETAIL') !== '') {
      this.laSession = this.sessionService.getCookie('LA_USER_DETAIL');
      userid = this.laSession.userId;
    }
    this.userService.GetUserRoles(userid).subscribe(data => {
      if (JSON.parse(JSON.stringify(data)).success === true && JSON.parse(JSON.stringify(data)).results[0].roleDetailsResponse.length > 0) {
        this.UIPermissionEnabled = true;
        this.isUserAuthorized = true;
        this.roles = JSON.parse(JSON.stringify(data)).results[0].roleDetailsResponse;
        this.settingPermission = this.roles.filter(n => n.role === EnumType.SUPER_ADMIN).length > 0 ? true : false;
        this.sessionService.setCookie('ROLES', JSON.stringify(this.roles));
      } else {
        this.UIPermissionEnabled = false;
        this.isUserAuthorized = false;
      }
    }, err => {
    });
  }
  updateUserProfile() {
    const sessionData = this.sessionService.getCookie('LD_USER_DETAIL');
    const user = new User();
    user.firstName = sessionData.firstname;
    user.lastName = sessionData.lastname;
    user.emailId = sessionData.mail;
    user.userId = sessionData.uid;
    user.createdBy = 'System';
    this.userService.AddUser(user).subscribe(data => {
    }, err => { });
  }


}
