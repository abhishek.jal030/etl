import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-tour-guide',
  templateUrl: './tour-guide.component.html',
  styleUrls: ['./tour-guide.component.scss']
})
export class TourGuideComponent implements OnInit {
  isShow: boolean;
  query: string;
  introList = [
   
    {
      route: 'connections', title: 'New Odata Connection', icon: 'icon_plug', view: 'connections'
    },
    {
      route: 'jobs', title: 'Run Job', icon: 'icon_job', view: 'runJobs'
    },
    {
      route: 'jobs', title: 'Edit Job', icon: 'icon_job', view: 'editJobs'
    },
    {
      route: 'jobs', title: 'Remove Job', icon: 'icon_job', view: 'removeJobs'
    },
    {
      route: 'jobs/new', title: 'New Job', icon: 'icon_job', view: 'createNewJob'
    },
    {
      route: 'bulk-load/new', title: 'New Bulk Job Without Schedular', icon: 'icon_job', view: 'newbulkjob'
    },
    {
      route: 'bulk-load/new', title: 'New Bulk Job With Schedular', icon: 'icon_job', view: 'newbulkjobWithSchedular'
    },
    {
      route: 'jobs/new', title: 'New Job with Existing Table', icon: 'icon_job', view: 'createNewJobWithExistingTable'
    },
    {
      route: 'jobs/new', title: 'Create New Job with Filter', icon: 'icon_job', view: 'createNewJobWithFilter'
    },
    {
      route: 'jobs/new', title: 'Create New Job with Filter With Extsting Table', icon: 'icon_job', view: 'createNewJobWithFilterWithExistingTable'
    },
    {
      route: 'jobs/new', title: 'Create New Job with Filter With Increment Value', icon: 'icon_job', view: 'createNewJobWithIncrementValue'
    },
    {
      route: 'jobs/new', title: 'Create New Job with Filter With Increment Value With Existing Table', icon: 'icon_job', view: 'createNewJobWithIncrementValueExistingtable'
    }


  ];
  constructor(private cookieService: CookieService) {
    this.isShow = false;
  }
  setCookie(value: any) {
    this.cookieService.set('intro', value);
  }

  ngOnInit() {
  }

  close() {
    this.isShow = false;
  }

}
