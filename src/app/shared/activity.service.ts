import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Config } from "../../environments/config";
import { SessionService } from "../app.session-service";
@Injectable()
export class ActivityService {
    baseUrl = Config.getEnvironmentVariable('endPoint');
    activityEndPoint = this.baseUrl + 'api/v1/' + 'users/';
    myGlobalVar: string;
    sessionData: any;
    LA_SessionData: any = "";

    allActivity: [];
    constructor(private http: HttpClient, private _SessionService: SessionService) {
        this.myGlobalVar = "true";
        // alert("My intial global variable value is: " + this.myGlobalVar);
    }

    setActivityData() {
        this.sessionData = JSON.parse(this._SessionService.getCookie("LD_USER_DETAIL"));
        let uid = this.sessionData.uid;
        if (this._SessionService.getCookie("LA_USER_DETAIL") !== "") {
            this.LA_SessionData = JSON.parse(this._SessionService.getCookie("LA_USER_DETAIL"));
            uid = this.LA_SessionData.userId;
        }
        return this.http.get(this.activityEndPoint + `${uid}/activity`);
    }

    setValue(value: []) {
        this.allActivity = value;
        localStorage.setItem("allActivity", JSON.stringify(value));
    }
    getActivityList() {
        if (this.allActivity.length > 0) {
            return this.allActivity;
        } else {
            const allActivityList = [];
            return allActivityList;
        }

    }

    setActivityLog(activityData: any) {
        this.sessionData = JSON.parse(this._SessionService.getCookie("LD_USER_DETAIL"));
        let uid = this.sessionData.uid;
        if (this._SessionService.getCookie("LA_USER_DETAIL") !== "") {
            this.LA_SessionData = JSON.parse(this._SessionService.getCookie("LA_USER_DETAIL"));
            uid = this.LA_SessionData.userId;
        }
        return this.http.post(this.activityEndPoint + `${uid}/activity`, activityData);

    }

}
