import { Component, Input, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { FilterService, SinglePopupService, PopupCloseEvent } from '@progress/kendo-angular-grid';
import { addDays } from '@progress/kendo-date-math';

const closest = (node: any, predicate: any): any => {
    while (node && !predicate(node)) {
        node = node.parentNode;
    }

    return node;
};

@Component({
    selector: 'search-filter',
    template: `
        <div class="k-form">
            <label class="k-form-field">
                <input kendoTextBox [(ngModel)]="value" placeholder="Search..."/>
            </label>
        </div>
   `,
   styles: [`
        .k-form {
            padding: 5px;
        }
   `]
})
export class SearchFilterComponent implements OnInit, OnDestroy {

    @Input() public filter: CompositeFilterDescriptor;
    @Input() public filterService: FilterService;
    @Input() public field: string;

    public value: any;

    public popupSettings: any = {
        popupClass: 'search-filter'
    };

    private popupSubscription: any;

    constructor(private element: ElementRef, private popupService: SinglePopupService) {

        // Handle the service onClose event and prevent the menu from closing when the datepickers are still active.
        this.popupSubscription = popupService.onClose.subscribe((e: PopupCloseEvent) => {
            if (document.activeElement && closest(document.activeElement,
                node => node === this.element.nativeElement || (String(node.className).indexOf('search-filter') >= 0))) {
                e.preventDefault();
            }
        });
    }

    public ngOnInit(): void {
        
    }

    public ngOnDestroy(): void {
        this.popupSubscription.unsubscribe();
    }

    private findValue(operator) {
      return null;
    }

    
}
