import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { Connection } from '../model/connection';
import { NewJobComponent } from '../jobs/new-job/new-job.component';

@Injectable()
export class DataService {
  private selectedConnection = new BehaviorSubject('');
  getConnector = this.selectedConnection.asObservable();
  private messageSource = new BehaviorSubject('');
  currentMessage = this.messageSource.asObservable();
  private barChartData = new BehaviorSubject(new Object());
  private connectionDetail = new BehaviorSubject(new Connection());
  private jobDetail = new BehaviorSubject(new Connection());
  invokeEvent: Subject<any> = new Subject();

  constructor() { }


  setConnector(connector: string) {
    this.selectedConnection.next(connector);
  }

  setCurrentUrl(message: string) {
    this.messageSource.next(message);
  }

  getCurrentUrl() {
    return this.messageSource;
  }

  setData(data: any) {
    this.barChartData.next(data);
  }

  getData() {
    return this.barChartData;
  }

  setConnectionDetail(data: any) {
    this.connectionDetail.next(data);
  }

  getConnectionDetail() {
    return this.connectionDetail;
  }

  setAction(key) {
    this.invokeEvent.next(key);
  }

}
