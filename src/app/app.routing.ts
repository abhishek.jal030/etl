import { Routes } from '@angular/router';
import { AdminAuthGuardService } from './app.admin-auth-guard.service';
import { UserAuthGuardService } from './app.user-auth-guard.service';
import { DashboardComponent } from './dashboard/dashboard.component';
// import { UserManagmentComponent } from './user-managment/user-managment.component';
// import { UsersComponent } from './users/users.component';
// import { RolesComponent } from './roles/roles.component';
// import { EditComponent } from './users/edit/edit.component';
import { CommentComponent } from './core-component/comment/comment.component';
import { JobsComponent } from './jobs/jobs.component';
import { ConnectionsComponent } from './connections/connections.component';
import { NewJobComponent } from './jobs/new-job/new-job.component';
import { BulkLoadComponent } from './bulk-load/bulk-load.component';
import { AuthGuardService } from './auth-guard.service';
import { LoginComponent } from './login/login.component';


export const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService] },
    { path: 'connections', component: ConnectionsComponent, canActivate: [AuthGuardService] },
    { path: 'jobs', component: JobsComponent, canActivate: [AuthGuardService] },
    { path: 'jobs/:jobId', component: NewJobComponent, canActivate: [AuthGuardService] },
    { path: 'jobs/new', component: NewJobComponent },
    { path: 'bulk-load/new', component: BulkLoadComponent },
    { path: 'comment', component: CommentComponent },
    { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    { path: '**', redirectTo: 'not-found' }
];
