import { Injectable } from "@angular/core";
import { CookieService } from 'ngx-cookie-service';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Config } from "../environments/config";

@Injectable()
export class SessionService {

    // oAuth_SSO_Url: string = environment.oAuth_SSO_Url;

    // getTokenEndpoint = environment.oAuthApiUrl + 'token/';

    // getProfileEndpoint = environment.oAuthApiUrl + 'userDetail/';
    baseUrl = Config.getEnvironmentVariable('endPoint');

    getTokenEndpoint = this.baseUrl + 'oauth/token/';

    getProfileEndpoint = this.baseUrl + 'oauth/userDetail/';

    constructor(private http: HttpClient, private cookieService: CookieService) {

    }

    getToken(code: string, url: string) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
        const urls = this.getTokenEndpoint + `${code}?redirectionURL=${url}`;
        return this.http.get(urls, httpOptions);
    }

    getProfile(accessToken: string) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
        const url = this.getProfileEndpoint + `${accessToken}`;
        return this.http.get(url, httpOptions);
    }

    /**
     * Method to get session data
     */
    getData() {
        return this.cookieService.get('LD_USER_DETAIL');
    }

    /**
     * Setting local session for development
     */
    setData() {
        const data = {
            uid: 'karnesh', mail: 'Karnesh.Kumar@netapp.com', lastname: 'Kumar',
            commonname: 'Karnesh Kumar', firstname: 'Karnesh', uri: '/ms_oauth/resources/leadapp/me/karnesh'
        };
        this.cookieService.set('LD_USER_DETAIL', JSON.stringify(data));
    }

    /**
     * Method to set cookie
     * @param key Set Cookie Name
     * @param value Set Cookie Value
     */
    setCookie(key: string, value: any) {
        this.cookieService.set(key, value);
    }

    getCookie(key: string) {
        return this.cookieService.get(key) === '' ? '' : JSON.parse(this.cookieService.get(key));
    }

    deleteCookie(key: string) {
        this.cookieService.delete(key);
    }

    /**
     * Session data method to get value from session data
     * @param Key Pass key to get session parameter value
     */
    getSessionValue(Key: string) {
        return this.cookieService.get('LD_USER_DETAIL') === '' ? '' : JSON.parse(this.cookieService.get('LD_USER_DETAIL'))[Key];
    }

    deleteSession() {
        this.cookieService.delete('LD_USER_DETAIL');
    }

}
