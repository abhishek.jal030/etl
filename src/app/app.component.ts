// import { Component } from '@angular/core';
// import { DataService } from './shared/data.service';
// import { Router } from '@angular/router';
// import { SessionService } from './app.session-service';

// import { User } from './model/user';
// import { Role } from './model/role';
// import { EnumType } from './model/EnumType';
// import { OrderPipe } from 'ngx-order-pipe';
// import { UIPermission } from './model/menu';
// import { ActivityService } from './shared/activity.service';
// import { FeatureService } from './features/features.service';
// import { environment } from '../environments/environment';
// import { UserService } from './settings/users/users.service';
// import { SharedService } from './shared/shared.service';

// @Component({
//   selector: 'app-root',
//   templateUrl: './app.component.html',
//   styleUrls: ['./app.component.scss'],
//   providers: [
//     DataService
//   ]
// })
// export class AppComponent {
//   title = 'PRM';
//   url = '';
//   absPath = '';
//   code = '';
//   sessionData: any;
//   selectedItem: string;
//   LA_SessionData = '';
//   roles: Array<Role> = new Array<Role>();
//   settingPermission = false;
//   newUser: User = new User();
//   UIPermissionEnabled = false;
//   access_token = '';
//   response: any;
//   appMenu: Array<UIPermission> = new Array<UIPermission>();
//   fullscreen = false;
//   getSOS_URL(redirectUrl) {
//     return environment.oAuth_SSO_Url + environment.oAuth_SSO_Auth_Url + `${redirectUrl}&scope=leadapp.me&state=foobar`;
//   }

//   getSOS_Logout_URL() {
//     return environment.oAuth_SSO_Logout + this.url;
//   }

//   constructor(
//     private route: Router,
//     private activityService: ActivityService,
//     private sessionService: SessionService,
//     private userService: UserService,
//     private featureService: FeatureService,
//     private orderPipe: OrderPipe,
//     private sharedService: SharedService,
//     private dataService: DataService) {
//     this.url = window.location.origin + '/';
//     this.absPath = window.location.href;
//     this.fullscreen = this.IsAuthAllowed(window.location.pathname);
//     this.code = window.location.search == '' ? '' : window.location.search.match('code=([^&]*)') == null ? '' : window.location.search.match('code=([^&]*)')[1];
//   }

//   ngOnInit() {

//     this.getAllAuthMenu();

//   }

//   getAllAuthMenu() {
//     const data = '{"requestId":"7cf77166-bfd1-4b5b-b280-0463262792c3","success":true,"message":"","results":[[{"id":100,"name":"RuleEngine","url":" ","sequenceOrder":1,"createdBy":"KARNESH","updatedBy":"KARNESH"}],[{"id":101,"featureId":101,"name":"Dashboard","url":"dashboard","sequenceOrder":1,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_dashboard"},{"id":102,"featureId":102,"name":"Connections","url":"connections","sequenceOrder":2,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_plug"},{"id":103,"featureId":103,"name":"Jobs","url":"jobs","sequenceOrder":3,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_job"}, {"id":103,"featureId":103,"name":"Reports","url":"reports","sequenceOrder":3,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_report"}]]}';
//     if (JSON.parse(data).results != null &&
//       JSON.parse(data).results.length > 0) {
//       this.response = JSON.parse(data).results;
//       localStorage.setItem('UILIST', JSON.stringify(this.response));
//       const levelZero = this.orderPipe.transform(this.response[1], 'sequenceOrder');
//       if (levelZero != null) {
//         levelZero.forEach(appElement => { //
//           const appMenuList = [];
//           const levelOne = this.orderPipe.transform(this.response[2], 'sequenceOrder');
//           if (levelOne != null) {
//             levelOne.filter(n => n.parentId === appElement.id).forEach(appMenuElement => {
//               const appSubMenuList = [];
//               appMenuList.push({
//                 id: appMenuElement.id, name: appMenuElement.name, url: appMenuElement.url,
//                 featureId: appMenuElement.featureId, createdBY: appMenuElement.createdBY, icon: appMenuElement.icon,
//                 sequenceOrder: appMenuElement.sequenceOrder, updatedBY: appMenuElement.updatedBY, parentId: appMenuElement.parentId,
//                 submenu: appSubMenuList
//               });
//             });
//           }
//           this.appMenu.push({
//             id: appElement.id, name: appElement.name, url: appElement.url,
//             featureId: appElement.featureId, createdBY: appElement.createdBY, icon: appElement.icon,
//             sequenceOrder: appElement.sequenceOrder, updatedBY: appElement.updatedBY, parentId: appElement.parentId,
//             submenu: appMenuList
//           });
//         });
//       }
//     }
//     // this.featureService.GetAllMenu('RuleEngine')
//     //   .subscribe(data => {
//     //     data = '{"requestId":"7cf77166-bfd1-4b5b-b280-0463262792c3","success":true,"message":"","results":[[{"id":100,"name":"RuleEngine","url":" ","sequenceOrder":1,"createdBy":"KARNESH","updatedBy":"KARNESH"}],[{"id":101,"featureId":101,"name":"Dashboard","url":"dashboard","sequenceOrder":1,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_home"},{"id":102,"featureId":102,"name":"Connection","url":"connection","sequenceOrder":2,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_leads"},{"id":103,"featureId":103,"name":"Jobs","url":"jobs","sequenceOrder":3,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_leads"}, {"id":103,"featureId":103,"name":"Report","url":"jobs","sequenceOrder":3,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_report"}]]}';
//     //     if (JSON.parse(JSON.stringify(data)).results != null &&
//     //       JSON.parse(JSON.stringify(data)).results.length > 0) {
//     //       this.response = JSON.parse(JSON.stringify(data)).results;
//     //       localStorage.setItem('UILIST', JSON.stringify(this.response));
//     //       const levelZero = this.orderPipe.transform(this.response[1], 'sequenceOrder');
//     //       if (levelZero != null) {
//     //         levelZero.forEach(appElement => { //
//     //           const appMenuList = [];
//     //           const levelOne = this.orderPipe.transform(this.response[2], 'sequenceOrder');
//     //           if (levelOne != null) {
//     //             levelOne.filter(n => n.parentId === appElement.id).forEach(appMenuElement => {
//     //               const appSubMenuList = [];
//     //               appMenuList.push({
//     //                 id: appMenuElement.id, name: appMenuElement.name, url: appMenuElement.url,
//     //                 featureId: appMenuElement.featureId, createdBY: appMenuElement.createdBY, icon: appMenuElement.icon,
//     //                 sequenceOrder: appMenuElement.sequenceOrder, updatedBY: appMenuElement.updatedBY, parentId: appMenuElement.parentId,
//     //                 submenu: appSubMenuList
//     //               });
//     //             });
//     //           }
//     //           this.appMenu.push({
//     //             id: appElement.id, name: appElement.name, url: appElement.url,
//     //             featureId: appElement.featureId, createdBY: appElement.createdBY, icon: appElement.icon,
//     //             sequenceOrder: appElement.sequenceOrder, updatedBY: appElement.updatedBY, parentId: appElement.parentId,
//     //             submenu: appMenuList
//     //           });
//     //         });

//     //       }
//     //     }
//     //   }
//     // , err => {
//     // });
//   }
//   listClick(event, newValue) {
//     console.log(newValue);
//     this.selectedItem = newValue;  // don't forget to update the model here
//     // ... do other stuff here ...
//   }

//   getToken() {
//     this.sessionService.getToken(this.code, this.url.substr(0, this.url.length - 1))
//       .subscribe(data => {
//         if (data !== null && data !== '') {
//           this.sessionService.setCookie('LD_ACCESS_TOKEN', JSON.stringify(data));
//           const response = JSON.parse(this.sessionService.getCookie('LD_ACCESS_TOKEN'));
//           this.sessionService.getProfile(response.access_token)
//             .subscribe(profile => {
//               this.sessionService.setCookie('LD_USER_DETAIL', JSON.stringify(profile));
//               this.sessionData = JSON.parse(this.sessionService.getCookie('LD_USER_DETAIL'));
//               this.GetSignInUser(this.sessionData.uid);
//             }, error => {
//               console.log(error);
//             });
//         }
//         console.log(data);
//       }, error => {
//         console.log(error);
//       });
//   }

//   isActive() {
//     this.dataService.getCurrentUrl().subscribe(response => {
//       return response;
//     }, error => {
//       return error;
//     });
//   }

//   OpenSettings() {
//     this.url = '';
//     this.route.navigate(['settings']);
//   }

//   Logout() {
//     this.sessionService.deleteCookie('LD_ACCESS_TOKEN');
//     this.sessionService.deleteCookie('LD_USER_DETAIL');
//     window.location.href = this.getSOS_Logout_URL();
//   }

//   LALogout() {
//     this.sessionService.deleteCookie('LA_USER_DETAIL');
//     const domain = window.location.origin;
//     window.location.href = domain;
//   }

//   private GetSignInUser(userid: string) {
//     this.ProtralRedirection();
//     if (this.sessionService.getCookie('LA_USER_DETAIL') !== '') {
//       this.LA_SessionData = JSON.parse(this.sessionService.getCookie('LA_USER_DETAIL'));
//       userid = this.LA_SessionData['userId'];
//     }
//     this.userService.GetSignInUser(userid).subscribe(data => {
//       const response = JSON.parse(JSON.stringify(data));
//       if (response.success) {
//         this.GetUserRoles(userid);
//         if (this.sessionService.getCookie('LA_USER_DETAIL') !== '') {
//           this.userService.UserAudit(userid, 'Success').subscribe(userAudit => console.log(userAudit), error => console.log(error));
//         }
//       } else {
//         if (this.sessionService.getCookie('LA_USER_DETAIL') !== '') {
//           this.userService.UserAudit(userid, 'Failed').subscribe(userAudit => console.log(userAudit), error => console.log(error));
//         }
//         this.UIPermissionEnabled = false;
//         this.dataService.setData(this.UIPermissionEnabled);
//       }
//       this.UpdateProfile();
//     }, error => {

//     });
//   }

//   private UpdateProfile() {
//     this.newUser.firstName = this.sessionData.firstname;
//     this.newUser.lastName = this.sessionData.lastname;
//     this.newUser.emailId = this.sessionData.mail;
//     this.newUser.userId = this.sessionData.uid;
//     this.newUser.createdBy = 'System';
//     this.userService.AddUser(this.newUser).subscribe(data => {
//     }, err => { });
//   }

//   private GetUserRoles(userid) {
//     if (this.sessionService.getCookie('LA_USER_DETAIL') !== '') {
//       this.LA_SessionData = JSON.parse(this.sessionService.getCookie('LA_USER_DETAIL'));
//       userid = this.LA_SessionData['userId'];
//     }
//     this.userService.GetUserRoles(userid).subscribe(data => {
//       if (JSON.parse(JSON.stringify(data)).success === true && JSON.parse(JSON.stringify(data)).results[0].roleDetailsResponse.length > 0) {
//         this.UIPermissionEnabled = true;
//         this.getAllAuthMenu();
//         this.roles = JSON.parse(JSON.stringify(data)).results[0].roleDetailsResponse;
//         this.settingPermission = this.roles.filter(n => n.role === EnumType.SUPER_ADMIN).length > 0 ? true : false;
//         this.sessionService.setCookie('ROLES', JSON.stringify(this.roles));
//       }
//     }, err => {
//     });
//   }

//   private IsAuthAllowed(url): boolean {
//     let ret = false;
//     switch (true) {
//       case (url.indexOf('login') !== -1):
//         ret = true;
//         break;
//       case (url.indexOf('promotion/oppty/add/') !== -1):
//         ret = true;
//         break;
//       case (url.indexOf('solution') !== -1):
//         ret = true;
//         break;
//       default:
//         ret = false;
//         break;
//     }
//     return ret;
//   }

//   private ProtralRedirection(): void {
//     const redirectURI = this.sessionService.getCookie('redirect_uri');
//     this.sessionService.deleteCookie('redirect_uri');
//     if (redirectURI !== '' && !this.IsAuthAllowed(redirectURI)) {
//       this.route.navigate([redirectURI]);
//     } else if (redirectURI !== '') {
//       window.location.href = this.url.substr(0, this.url.length - 1) + '/' + redirectURI;
//     }
//   }
// }








import { Component, OnInit } from '@angular/core';
import { DataService } from './shared/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SessionService } from './app.session-service';
import { User } from './model/user';
import { Role } from './model/role';
import { EnumType } from './model/EnumType';
import { OrderPipe } from 'ngx-order-pipe';
import { UIPermission } from './model/menu';
import { ActivityService } from './shared/activity.service';
import { SharedService } from './shared/shared.service';
import { AvatarModule } from 'ngx-avatar';
import { UserService } from './settings/users/users.service';
import { Config } from '../environments/config';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [
    DataService,
    DataService
  ]
})
export class AppComponent implements OnInit {
  title = 'app';
  url: string;
  headertext = 'Welcome to Sales OnBoard Checklist';
  ret = false;
  absPath: string;
  code: string;
  sessionData: any;
  // LA_SessionData: any;
  laSessionData: any;
  loginData: any;
  roles: Array<Role> = new Array<Role>();
  settingPermission: boolean;
  newUser: User = new User();
  UIPermissionEnabled = true;
  // access_token: string = '';
  response: any;
  appMenu: Array<UIPermission> = new Array<UIPermission>();
  fullscreen: boolean;
  // byPassHost = ['localhost', 'core-ui-rxz-core-stg.npc-us-west-dc61.ncloud.netapp.com', '10.103.129.17','http://cst-etl-vm.eastus.cloudapp.azure.com'];

  byPassHost = ['localhost', 'core-ui-rxz-core-stg.npc-us-west-dc61.ncloud.netapp.com','cst-etl-vm.eastus.cloudapp.azure.com'];

  constructor(
    private route: Router,
    private activityService: ActivityService,
    private activatedRoute: ActivatedRoute,
    private sessionService: SessionService,
    private userService: UserService,
    private orderPipe: OrderPipe,
    private dataService: DataService,
    public sharedService: SharedService
  ) {
    localStorage.setItem('filtersMV', null);
    localStorage.setItem('defaultFilter', null);
    this.laSessionData = '';
    this.settingPermission = false;
    this.fullscreen = false;
    this.url = window.location.origin + '/';
    this.absPath = window.location.href;
    this.fullscreen = this.IsAuthAllowed(window.location.pathname);
    this.code = window.location.search === '' ? '' : window.location.search.match('code=([^&]*)') === null ? '' : window.location.search.
      match('code=([^&]*)')[1];

  }

  getSOS_URL() {
    return `${Config.getEnvironmentVariable('oAuth_SSO_Url')}${Config.getEnvironmentVariable('oAuth_SSO_Auth_Url')}
    ${Config.getEnvironmentVariable('redirectURI')}${Config.getEnvironmentVariable('oAuth_SSO_Scope_Url')}`;
  }

  getSOS_Logout_URL() {
    return `${Config.getEnvironmentVariable('oAuth_SSO_Logout')}${this.url}`;
  }

  ngOnInit() {
    this.loginData =  JSON.parse(JSON.stringify(this.sessionService.getCookie('LOGIN').results[0]));

    this.getAllAuthMenu();
    if (this.byPassHost.filter(n => this.url.indexOf(n) > -1).length > 0) {
      this.sessionService.setData();
     this.sharedService.GetUserRoles();
    }
    this.sessionData = this.sessionService.getCookie('LD_USER_DETAIL') === '' ? null : this.sessionService.getCookie('LD_USER_DETAIL');
    if (this.sessionData === null && this.code === '') { //  && !this.fullscreen
      window.location.href = this.getSOS_URL(); // REDIRECT TO SSO LOGIN PAGE
     
    } else {
      if (this.sessionData === null) {
        this.getToken();
      } else {
         this.sharedService.GetUserRoles();
      }

    }

  }
  loginAs() {
    this.loginData =  JSON.parse(JSON.stringify(this.sessionService.getCookie('LOGIN').results[0]));

   // this.sessionService.setCookie('LD_USER_DETAIL', JSON.stringify(this.loginData));
    this.appMenu = [];
    this.getAllAuthMenu();
    this.sharedService.GetUserRoles();
    // this.sharedService.GetUserRoles();
  }




  // getAllAuthMenu() {
  //   this.featureService.GetAllMenu('ALL').subscribe(data => {
  //     if (JSON.parse(JSON.stringify(data)).results != null &&
  //       JSON.parse(JSON.stringify(data)).results.length > 0) {
  //       this.response = JSON.parse(JSON.stringify(data)).results;
  //       const levelZero = this.orderPipe.transform(this.response[0], 'sequenceOrder');
  //       levelZero.forEach(appElement => { //
  //         const appMenuList = [];
  //         const levelOne = this.orderPipe.transform(this.response[1], 'sequenceOrder');
  //         levelOne.filter(n => n.parentId === appElement.id).forEach(appMereportsnuElement => {
  //           const appSubMenuList = [];
  //           const levelTwo = this.orderPipe.transform(this.response[2], 'sequenceOrder');
  //           levelTwo.filter(n => n.parentId === appMenuElement.id).forEach(appSubMenuElement => {
  //             appSubMenuList.push({
  //               id: appSubMenuElement.id, name: appSubMenuElement.name, url: appSubMenuElement.url,
  //               featureId: appSubMenuElement.featureId, createdBY: appSubMenuElement.createdBY, icon: appSubMenuElement.icon,
  //               sequenceOrder: appSubMenuElement.sequenceOrder, updatedBY:
  // appSubMenuElement.updatedBY, parentId: appSubMenuElement.parentId
  //             });
  //           });
  //           appMenuList.push({
  //             id: appMenuElement.id, name: appMenuElement.name, url: appMenuElement.url,
  //             featureId: appMenuElement.featureId, createdBY: appMenuElement.createdBY, icon: appMenuElement.icon,
  //             sequenceOrder: appMenuElement.sequenceOrder, updatedBY: appMenuElement.updatedBY,
  //             parentId: appMenuElement.parentId, submenu: appSubMenuList
  //           });
  //         });
  //         this.appMenu.push({
  //           id: appElement.id, name: appElement.name, url: appElement.url,
  //           featureId: appElement.featureId, createdBY: appElement.createdBY, icon: appElement.icon,
  //           sequenceOrder: appElement.sequenceOrder, updatedBY: appElement.updatedBY, parentId: appElement.parentId, submenu: appMenuList
  //         });
  //       });
  //     }

  //   }, err => {
  //   });
  // }
    getAllAuthMenu() {
    const data = '{"requestId":"7cf77166-bfd1-4b5b-b280-0463262792c3","success":true,"message":"","results":[[{"id":100,"name":"RuleEngine","url":" ","sequenceOrder":1,"createdBy":"KARNESH","updatedBy":"KARNESH"}],[{"id":101,"featureId":101,"name":"Dashboard","url":"dashboard","sequenceOrder":1,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_dashboard"},{"id":102,"featureId":102,"name":"Connections","url":"connections","sequenceOrder":2,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_plug"},{"id":103,"featureId":103,"name":"Jobs","url":"jobs","sequenceOrder":3,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_job"}, {"id":103,"featureId":103,"name":"Reports","url":"reports","sequenceOrder":3,"parentId":100,"createdBy":"KARNESH","updatedBy":"KARNESH","icon":"icon_report"}]]}';
    if (JSON.parse(data).results != null &&
      JSON.parse(data).results.length > 0) {
      this.response = JSON.parse(data).results;
      localStorage.setItem('UILIST', JSON.stringify(this.response));
      const levelZero = this.orderPipe.transform(this.response[1], 'sequenceOrder');
      if (levelZero != null) {
        levelZero.forEach(appElement => { //
          const appMenuList = [];
          const levelOne = this.orderPipe.transform(this.response[2], 'sequenceOrder');
          if (levelOne != null) {
            levelOne.filter(n => n.parentId === appElement.id).forEach(appMenuElement => {
              const appSubMenuList = [];
              appMenuList.push({
                id: appMenuElement.id, name: appMenuElement.name, url: appMenuElement.url,
                featureId: appMenuElement.featureId, createdBY: appMenuElement.createdBY, icon: appMenuElement.icon,
                sequenceOrder: appMenuElement.sequenceOrder, updatedBY: appMenuElement.updatedBY, parentId: appMenuElement.parentId,
                submenu: appSubMenuList
              });
            });
          }
          this.appMenu.push({
            id: appElement.id, name: appElement.name, url: appElement.url,
            featureId: appElement.featureId, createdBY: appElement.createdBY, icon: appElement.icon,
            sequenceOrder: appElement.sequenceOrder, updatedBY: appElement.updatedBY, parentId: appElement.parentId,
            submenu: appMenuList
          });
        });
      }
    }
  }

  getToken() {
    const codeString = this.code;
    console.log('code = ' + codeString);
    this.sessionService.getToken(codeString, Config.getEnvironmentVariable('redirectURI'))
      .subscribe(data => {
        if (data != null && data !== '') {
          console.log('data = ' + data);
          this.sessionService.setCookie('LD_ACCESS_TOKEN', JSON.stringify(data));
          this.sessionService.getProfile(this.sessionService.getCookie('LD_ACCESS_TOKEN').access_token).subscribe(res => {
            this.sessionService.setCookie('LD_USER_DETAIL', JSON.stringify(res));
            // this.sharedService.updateUserProfile();
            this.sessionData = this.sessionService.getCookie('LD_USER_DETAIL');
            console.log('getToken() called');
            this.sharedService.GetUserRoles();
          }, error => {
            console.log(error);
          });
        }
      }, error => {
        console.log(error);
      });
  }

  // isActive() {
  //   this.dataService.getCurrentUrl().subscribe(response => function () {
  //     return response;
  //   }, error => function () {
  //     return error;
  //   });
  // }
  OpenSettings() {
    this.url = '';
    this.route.navigate(['settings']);
  }

  Logout() {
    this.sessionService.deleteCookie('LD_ACCESS_TOKEN');
    this.sessionService.deleteCookie('LD_USER_DETAIL');
    this.sessionService.deleteCookie('LOGIN');
    this.sharedService.isFullScreen = true;
    this.route.navigate(['login']);

   //  window.location.href = this.getSOS_Logout_URL();
  }

  laLogout() {
    this.sessionService.deleteCookie('LA_USER_DETAIL');
    const domain = window.location.origin;
    window.location.href = domain;
  }

  private UpdateProfile() {
    this.newUser.firstName = this.sessionData.firstname;
    this.newUser.lastName = this.sessionData.lastname;
    this.newUser.emailId = this.sessionData.mail;
    this.newUser.userId = this.sessionData.uid;
    this.newUser.createdBy = 'System';
    this.userService.AddUser(this.newUser).subscribe(data => {
    }, err => { });
  }

private IsAuthAllowed(url): boolean {
  let ret = false;
  switch (true) {
    case (url.indexOf('login') !== -1):
      ret = true;
      break;
    case (url.indexOf('promotion/oppty/add/') !== -1):
      ret = true;
      break;
    case (url.indexOf('solution') !== -1):
      ret = true;
      break;
    default:
      ret = false;
      break;
  }
  return ret;
}

  private ProtralRedirection(): void {
    const redirectUri = this.sessionService.getCookie('redirect_uri');
    this.sessionService.deleteCookie('redirect_uri');
    if (redirectUri !== '' && !this.IsAuthAllowed(redirectUri)) {
      this.route.navigate([redirectUri]);
    } else if (redirectUri !== '') {
      window.location.href = this.url.substr(0, this.url.length - 1) + '/' + redirectUri;
    }
  }
}
