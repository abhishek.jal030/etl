import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Connector } from '../model/connector';
import { UserService } from '../settings/users/users.service';
import { SharedService } from '../shared/shared.service';
import { SessionService } from '../app.session-service';
import { AppComponent } from '../app.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  connectorField: Connector;

  constructor(
    private route: Router,
    private userService: UserService,
    private sharedService: SharedService,
    private sessionSevice: SessionService,
    private appcomponent: AppComponent,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.connectorField = new Connector();

    this.username = '';
    this.password = '';
  }

  doSignIn() {
    this.userService.GetSignInUser(this.username).subscribe(data => {
      const response = JSON.parse(JSON.stringify(data));
      if (this.password !== 'welcome1') {
        this.toastr.info('Password is incorrect');
      }
      if (response.success === false) {
        this.toastr.info(response.message);
      }
      if (response.success && this.password === 'welcome1') {

        this.sessionSevice.setCookie('LOGIN', JSON.stringify(response));
        this.sharedService.isFullScreen = false;
        this.sessionSevice.setData();
        this.appcomponent.loginAs();

        this.route.navigate(['dashboard']);
      }
    }, error => {

    });
  }


}
