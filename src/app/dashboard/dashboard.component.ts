import { Component, OnInit, OnDestroy } from '@angular/core';
import * as introJs from 'intro.js/intro.js';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    if (this.route.snapshot.queryParams.action === 'intro') {
      introJs().start();
    }
  }

  ngOnDestroy() {
    introJs().exit();
  }

}
