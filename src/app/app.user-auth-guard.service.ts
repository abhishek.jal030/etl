import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import { SessionService } from "./app.session-service";
import { EnumType } from "./model/EnumType";
import { SharedService } from "./shared/shared.service";

@Injectable()
export class UserAuthGuardService implements CanActivate {

    constructor(private router: Router, private _SessionService: SessionService, private sharedService: SharedService) {
    }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        this.sharedService.isFullScreen = false;
        let url: string = state.url;
        var roles = (this._SessionService.getCookie("ROLES") == ""
            || this._SessionService.getCookie("ROLES") == null) ? [] : JSON.parse(JSON.stringify(this._SessionService.getCookie("ROLES")));
               return true;
      //  if (roles.filter(n => n.role !== EnumType.SUPER_ADMIN).length > 0) {
       //     return true;
       // }
        this.router.navigate(['']);
        return false;
    }
}