import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Log } from '../../model/log';
import { JobService } from '../job.service';
import { Job } from '../../model/job';
import { JobsComponent } from '../jobs.component';
import { process, State } from '@progress/kendo-data-query';
import { SortDescriptor } from '@progress/kendo-data-query';
import {
  GridComponent,
  GridDataResult,
  DataStateChangeEvent,
  PageChangeEvent
} from '@progress/kendo-angular-grid';
@Component({
  // providers: [JobsComponent],
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.css']
})
export class LogComponent implements OnInit {
  @ViewChild(GridComponent) grid: GridComponent;

  public log: Array<Log> = new Array<Log>();
  id: string;
  jobs: Array<Job>;

  @Input()
  job: Job;
  constructor(private jobService: JobService) { }
  public state: State = {
    skip: 0,
    take: 5,
  };
  public gridData: GridDataResult = process(this.log, this.state);
  ngOnInit() {
    this.getJobLog();
     this.jobStatusId();
  }
  public ngAfterViewInit(): void {
    // Expand the first row initially
    // this.grid.expandRow(0);
  }
  getJobLog() {
    this.jobService.getJobLog(this.job.jobId).subscribe(response => {
      console.log(response);
      const res = JSON.parse(JSON.stringify(response)).results;
      console.log(res);
      this.gridData = res;
      this.id = res[0].jobStatusId;
      console.log(this.id);
    });
  }

    jobStatusId()
    {
      this.jobService.getJobLog(this.job.jobId).subscribe(response => {
        console.log(response);
        const res = JSON.parse(JSON.stringify(response)).results;
        console.log(res);
        this.gridData = res;
        this.id = res[0].jobStatusId;
        console.log(this.id);
      this.jobService.getJobLogStatus(this.job.jobId,this.id).subscribe(response=> {
        console.log(response);
      })
    });
  }
}
