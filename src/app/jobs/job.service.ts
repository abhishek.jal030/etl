import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Config } from '../../environments/config';
import { JobRequest } from '../model/job';
import { Schdule } from '../model/schdule';
import { RequestOptions } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class JobService {
  headers: any;
  options: any;
  baseUrl = Config.getEnvironmentVariable('apiUrl');
  allJobsEndPoint = this.baseUrl + 'job';
  deleteJobsEndPoint = this.baseUrl + 'jobs';
  jobsFilterEndPoint = this.baseUrl + 'jobs';
  collectionsEndPoint = this.baseUrl + 'collections';
  jobsEndPoint = this.baseUrl + 'jobs';
  queryEndPoint = this.baseUrl + 'collections/odata';
  viewLogEndPoint = this.baseUrl + 'jobs';
  constructor(private http: HttpClient) { }
  getAllJobs(filter: any, jobName: string, startRecord: number, recordCount: number, sortableColumn: string, sortingType: string) {
    return this.http.post(this.allJobsEndPoint + `?jobName=${jobName ? jobName : ''}&startRecord=${startRecord}&recordCount=${recordCount}&sortableColumn=${sortableColumn}&sortingType=${sortingType}`, filter);
  }

  deleteJob(mapIds: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      }),
      body: mapIds
    };
    return this.http.delete(this.deleteJobsEndPoint, httpOptions);
  }


  getAllEntities(connectionId: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.get(this.collectionsEndPoint + `/${connectionId}`, httpOptions);
  }

  getCollectionMetadata(connectionId: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.get(this.collectionsEndPoint + `/${connectionId}/tableMetadata`, httpOptions);
  }

  refreshOdataCollectionMetadata(connectionId: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.get(this.collectionsEndPoint + `/odata/${connectionId}/refreshMetadata`, httpOptions);
  }

  getOdataCollection(connectionId: string, collectionName: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.get(this.collectionsEndPoint + `/odata/${connectionId}/tableMetadata?collectionName=${collectionName}`, httpOptions);
  }

  getTableMetadata(connectionId: string, collectionName: string, type: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.get(this.collectionsEndPoint + `/${connectionId}/${collectionName}/metadata/?type=${type}`, httpOptions);
  }

  createTable(connectionId: string, table: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.post(this.collectionsEndPoint + `/odata/${connectionId}/tableMetadata`, table, httpOptions);
  }

  createjob(jobRequest: JobRequest) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.post(this.jobsEndPoint, jobRequest, httpOptions);
  }

  updateJob(jobRequest: JobRequest) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.put(this.jobsEndPoint + `/${jobRequest.jobId}`, jobRequest, httpOptions);
  }

  runJob(jobId: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.get(this.jobsEndPoint + `/${jobId}/run`, httpOptions);
  }

  stopJob(jobId: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.get(this.jobsEndPoint + `/${jobId}/stop`, httpOptions);
  }

  syncMetadata(jobRequest: JobRequest) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.post(this.jobsEndPoint + '/difference', jobRequest, httpOptions);
  }

  getUpdatedTableMetadata(jobId: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.get(this.queryEndPoint + `/${jobId}/updatedTableMetadata`, httpOptions);
  }

  saveJobsMetadataDifference(connectionId, jobRequest: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.post(this.queryEndPoint + `/${connectionId}/alterTableMetadata`, jobRequest, httpOptions);
  }

  updateTableMetadata(jobId, jobRequest: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.post(this.jobsEndPoint + `/${jobId}/difference`, jobRequest, httpOptions);
  }

  logView(jobId: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.get(this.jobsEndPoint + `/${jobId}/logs`, httpOptions);
  }

  scheduleJob(jobId: string, schedule: Schdule) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.post(this.jobsEndPoint + `/${jobId}/schedule`, schedule, httpOptions);
  }

  testFilterQuery(connectionId: string, collectionName: string, testFilterRequest: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.post(this.queryEndPoint + `/${connectionId}/testFilterQuery?collectionName=${collectionName}`, testFilterRequest, httpOptions);
  }

  gettJobStatus(jobId: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.get(this.jobsEndPoint + `/${jobId}/progress`, httpOptions);
  }

  gettJob(jobId: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.get(this.jobsEndPoint + `/${jobId}`, httpOptions);
  }

  gettJobsInProgress() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.get(this.jobsEndPoint + `/InProgress`, httpOptions);
  }
  getJobLog(jobId: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.get(this.viewLogEndPoint + `/${jobId}` + '/logs');
  }
  getJobLogStatus(jobId: string, jobStatusId: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.get(this.viewLogEndPoint + `/${jobId}` + '/logs' + `/${jobStatusId}`);
  }
  getDataForPreview(connectionId: string, collectionName: string, collectionType: string, startRecord: number, recordCount: number, filter: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic YWRtaW46YWRtaW4'
      })
    };
    return this.http.post(this.collectionsEndPoint + `/${connectionId}` + `/${collectionName}` + `/data?collectionType=${collectionType}&startRecord=${startRecord}&recordCount=${recordCount}`, filter);
  }
  getExcelExport(jobId: string) {
    return this.http.get(this.baseUrl + `jobs/${jobId}/run/export`, {responseType: 'blob'});
  }
  upLoad(jobId: string, file: File) {
    this.headers = new Headers({'enctype': 'multipart/form-data'});
    // headers.append('Accept', 'application/json');
    this.options = new RequestOptions({headers: this.headers});
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
    console.log('before hist the service' + formData);
    return this.http.post(this.baseUrl + `jobs/${jobId}/run/upload`, formData, this.options);
  }

}
// upload(fileToUpload: File) {
//   const headers = new Headers({'enctype': 'multipart/form-data'});
//   // headers.append('Accept', 'application/json');
//   const options = new RequestOptions({headers: headers});
//   const formData: FormData = new FormData();
//   formData.append('file', fileToUpload, fileToUpload.name);
//   console.log('before hist the service' + formData);
//   return this.http
//     .post(`${this.appSettings.baseUrl}/Containers/avatar/upload/`, formData , options).map(
//       res => {
//         const data = res.json();
//         return data;
//       }
//     ).catch(this.handleError);
// }