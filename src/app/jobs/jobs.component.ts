import { Component, OnInit, OnDestroy, HostListener, ElementRef, Input } from '@angular/core';
import { Job, JobRequest, JobDifferenceDetails } from '../model/job';
import { process, State } from '@progress/kendo-data-query';
import { Collection } from 'src/app/model/collection';
import { CookieService } from 'ngx-cookie-service';
import { ConnectionService } from 'src/app/connections/connection-service';
import { Connection } from 'src/app/connections/connections.component';
import { JobService } from './job.service';
import { JobFilter } from '../model/job';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { filterBy, CompositeFilterDescriptor, FilterDescriptor, SortDescriptor } from '@progress/kendo-data-query';
import { SelectableSettings, FilterService, SelectAllCheckboxState } from '@progress/kendo-angular-grid';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import {
  GridComponent,
  GridDataResult,
  DataStateChangeEvent,
  PageChangeEvent
} from '@progress/kendo-angular-grid';
import * as introJs from 'intro.js/intro.js';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss']
})
export class JobsComponent implements OnInit, OnDestroy {
  isSourceCollectionSelected: boolean;
  isSourceConnectionConnected: boolean;
  isOpenedLog: boolean;
  isDestinationConnectionConnected: boolean;
  isDestinationCollectionSelected: boolean;
  isConnectionVisibleOption: boolean;
  job: Job;
  isCollectionVisibleOption: boolean;
  destinationConnectionValue: Connection;
  public jobStartIndex: number;
  deleteConnection: any;
  deleteMessage: any;
  downloadJsonHref: any;
  jobPageSize: number;
  jobSkip: number;
  public jobFilter: JobFilter;
  public jobPageSizes = true;
  public previousNext = true;
  public info = true;
  public buttonCount = 5;
  public type: 'numeric' | 'input' = 'input';
  sourceConnectionValue: Connection;
  requestFilterObj: Connection = new Connection();
  sourceCollectionValue: { text: string, value: string };
  color: 'red';
  connections: Array<Connection>;
  public sourceCollection: Array<{ text: string, value: string }>;
  public selectedSourceCollection: Array<Collection>;
  deleteJob: Array<string> = Array<string>();
  public jobGridDataResult: GridDataResult;
  jobGridLoading: boolean;
  isAlertModelVisible: boolean;
  jobs: Array<Job>;
  public jobFilterModel: Job = new Job();
  public state: State = {
    skip: 0,
    take: 10
  };
  intervalObj: any;
  statusDropDownFilter: Array<{ description: string, code: string }> = Array<{ description: string, code: string }>();
  introJS = introJs();
  constructor(
    private jobService: JobService,
    private toastr: ToastrService,
    private cookieService: CookieService,
    private eRef: ElementRef,
    private router: Router,
    private sanitizer: DomSanitizer,
    private connectionService: ConnectionService,
    private route: ActivatedRoute) {
    this.jobs = new Array<Job>();
    this.jobFilter = new JobFilter();
    this.jobFilter.recordCount = 10;
    this.jobStartIndex = 0;
    this.jobSkip = 0;
    this.jobPageSize = 10;
    this.jobGridLoading = false;
    this.isAlertModelVisible = false;
    this.isOpenedLog = false;
    this.sourceCollection = new Array<{ text: string, value: string }>();
    this.getAllConnection();
    this.statusDropDownFilter.push({ description: 'In Process', code: 'In Process' }, { description: 'Failed', code: 'Failed' }, { description: 'Completed', code: 'Completed' });
    if (this.cookieService.get('intro') === 'runJobs') {
      this.introJS.setOptions({
        steps: [
          {
            element: '#dotBtn',
            intro: 'Press three vertical dot '
          },
          {
            element: '#runJob',
            intro: 'Press Run Job Text'
          },
        ]
      });

    }
    else if (this.cookieService.get('intro') === 'editJobs') {
      this.introJS.setOptions({
        steps: [
          {
            element: '#dotBtn',
            intro: 'Press three vertical dot '
          },
          {
            element: '#editJob',
            intro: 'Press Edit Job Text'
          },
        ]
      });
    }
    else if (this.cookieService.get('intro') === 'removeJobs') {
      this.introJS.setOptions({
        steps: [
          {
            element: '#dotBtn',
            intro: 'Press three vertical dot '
          },
          {
            element: '#removeJob',
            intro: 'Press Remove Job Text'
          },
        ]
      });
    }
  }
  ngOnInit() {
    this.getAllJobs();
     this.checkProgress();
    if (this.route.snapshot.queryParams.action === 'intro') {
      this.introJS.start();
      this.introJS.onchange((targetElement) => {
      });
      this.introJS.oncomplete(() => {
        this.router.navigate(['/jobs'], { queryParams: {} });
      });
      this.introJS.onexit(() => {
        this.router.navigate(['/jobs'], { queryParams: {} });
      });
    }
  }
  public cellClickHandler({ sender, rowIndex, columnIndex, dataItem, isEdited }) {
    if (columnIndex > 1) {
      this.router.navigate(['jobs/', dataItem.jobId]);
    }
  }
  public onInputFilterValue(e, filterService, fieldName) {
    filterService.filter({
      filters: [{
        field: fieldName,
        operator: 'contains',
        value: e.srcElement.value
      }],
      logic: 'and'
    });
  }
  getAllConnection() {
    this.connectionService.getAllConnection(new Connection(), 0, 10, 'createdDate', 'desc').subscribe(data => {
      this.connections = Array<Connection>();
      if (JSON.parse(JSON.stringify(data)).results[0].connectionResponseList !== null) {
        JSON.parse(JSON.stringify(data)).results[0].connectionResponseList.forEach(element => {
          this.connections.push(new Connection(element));
        });
        const obj = new Connection();
        obj.name = 'Select Connection';
        this.connections.splice(0, 0, obj);
        this.sourceConnectionValue = obj;
        this.destinationConnectionValue = obj;
      }
    });
  }
  public jobStateChange(state: DataStateChangeEvent): void {

  }
  public pageJobChange(event: PageChangeEvent): void {
    this.jobStartIndex = event.skip;
    this.jobPageSize = event.take;
    this.getAllJobs();
  }
  getAllJobs() {
    this.jobGridLoading = true;
    this.jobs = [];
    this.jobService.getAllJobs(this.jobFilter, this.jobFilter.jobName, this.jobFilter.startRecord, this.jobFilter.recordCount, this.jobFilter.sortableColumn, this.jobFilter.sortingType).subscribe(data => {
      this.jobGridLoading = false;
      this.jobs = new Array<Job>();
      const jobList = JSON.parse(JSON.stringify(data)).results[0].jobResponseList;
      this.jobs = JSON.parse(JSON.stringify(data)).results[0].jobResponseList;
      this.jobGridDataResult = {
        data: this.jobs,
        total: JSON.parse(JSON.stringify(data)).results[0].totalRecordCount
      };
    });
  }
  public DropDownFilterChange(event, filterService: FilterService, filterOn): void {
    this.jobFilter.name = event;
    filterService.filter({
      filters: [{
        field: filterOn,
        operator: 'contains',
        value: event
      }],
      logic: 'and'
    });
  }

  public close(status) {
    console.log(`Dialog result: ${status}`);
    this.isOpenedLog = false;
  }

  public openLog(job) {
    this.job = job;
    this.isOpenedLog = true;
  }
  allClear() {
    this.state.filter = {
      logic: 'and',
      filters: []
    };
    this.jobFilter = new JobFilter();
    this.getAllJobs();
  }
  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
  }
  public pageChangeJob(event: PageChangeEvent): void {
    this.jobFilter.startRecord = event.skip;
    this.getAllJobs();
  }
  public onToggle(job): void {
    this.jobGridDataResult.data.map(n => n.toggle = false);
    job.toggle = !job.toggle;
    if (this.route.snapshot.queryParams.action === 'intro') {
      this.introJS.goToStepNumber(1).start();
    }
  }
  showInfo(job): void {
    this.jobGridDataResult.data.map(n => n.toggleInfo = false);
    job.toggleInfo = !job.toggleInfo === null ? true : !job.toggleInfo;
  }
  public filterChange(filter: CompositeFilterDescriptor): void {
    this.jobFilter.startRecord = 0;
    this.jobFilter.recordCount = 10;
    this.state.skip = 0;
    this.state.take = 10;
    if (filter.filters.length === 0) {
      this.jobFilter = new JobFilter();
      this.jobFilter.jobName = '';

    }
    this.getAllJobs();
  }
  public sortChangeConnection(sort: SortDescriptor[]): void {
    this.jobFilter.sortingType = sort[0]['dir'] === undefined ? 'asc' : 'desc';
    this.jobFilter.sortableColumn = sort[0]['field'];
    this.jobFilter.startRecord = 0;
    this.getAllJobs();
  }

  public filterChangeConnection(e, filterService, filterOn) {
    this.jobFilter[filterOn] = e.srcElement.value;
    filterService.filter({
      filters: [{
        field: filterOn,
        operator: 'contains',
        value: e.srcElement.value
      }], logic: 'and'
    });
  }

  runJob(dataItem) {
    this.jobService.runJob(dataItem.jobId)
      .subscribe(response => {
        const res = JSON.parse(JSON.stringify(response));
        this.toastr.warning(res.message);
      }, error => {
        console.log(error);
      });
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'runJobs') {
      this.introJS.exit();
    }
  }

  removeJob(jobId: string) {
    this.deleteJob = [];
    this.deleteJob.push(jobId);
    this.jobService.deleteJob(this.deleteJob).subscribe(data => {
      this.deleteMessage = JSON.parse(JSON.stringify(data)).message;
      this.getAllJobs();
      this.toastr.success(this.deleteMessage);
      console.log(data);
      if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'removeJobs') {
        this.introJS.exit();
      }
    });
  }

  exportJob(jobId: string) {
    const v1 = jobId;
    var theJSON = JSON.stringify(v1);
    var uri = this.sanitizer.bypassSecurityTrustUrl('data:text/json;charset=UTF-8,' + encodeURIComponent(theJSON));
    this.downloadJsonHref = uri;
  }

  checkProgress() {
    this.intervalObj = setInterval(() => {
      this.jobService.gettJobsInProgress()
        .subscribe(response => {
          const stats = JSON.parse(JSON.stringify(response)).results;
          stats.forEach(job => {
            this.jobGridDataResult.data.filter(n => n.jobId === job.jobId).map(n => { n.state = `Copied ${job.processedRecord} of ${job.totalRecordCount}`; n.status = job.status; });
          });
        }, error => {
        });
    }, 3000);
  }

  syncMetadata() {
    this.jobGridLoading = true;
    const jobRequest = new JobRequest();
    this.jobService.syncMetadata(jobRequest)
      .subscribe(response => {
        this.jobGridLoading = false;
        const metadataDifference = JSON.parse(JSON.stringify(response)).results;
        this.jobGridDataResult.data.forEach(element => {
          metadataDifference.filter(n => n.jobId === element.jobId).forEach(e => {
            element.viewJobDifferenceDetailsResponseList = new Array<JobDifferenceDetails>();
            element.viewJobDifferenceDetailsResponseList = e.viewJobDifferenceDetailsResponseList;
          });
        });
      }, error => {
        this.jobGridLoading = false;
        console.log(error);
      });
  }

  @HostListener('document:click', ['$event'])
  public documentClick(event: any): void {
    if (event.srcElement.alt !== undefined && event.srcElement.alt === 'vertical-dot') {
    } else if (this.eRef.nativeElement.contains(event.target)) {
      this.jobGridDataResult.data.map(n => n.toggle = false);
    } else {

      this.jobGridDataResult.data.map(n => n.toggle = false);
    }
    this.jobGridDataResult.data.map(n => n.toggleInfo = false);

  }
  ngOnDestroy() {
    clearInterval(this.intervalObj);
    introJs().exit();
  }



}
