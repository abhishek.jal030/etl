import { Component, OnInit, OnDestroy, ElementRef, HostListener, ɵConsole } from '@angular/core';
import { Connection } from 'src/app/connections/connections.component';
import { ConnectionService } from 'src/app/connections/connection-service';
import { CookieService } from 'ngx-cookie-service';
import { JobService } from '../job.service';
import { Collection, Metadata } from 'src/app/model/collection';
import { JobRequest } from 'src/app/model/job';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../../shared/data.service';
import { ToastrService } from 'ngx-toastr';
import { FileState } from '@progress/kendo-angular-upload';
import { saveAs } from '@progress/kendo-file-saver'
import { Schdule } from 'src/app/model/schdule';
import { DataFilter } from 'src/app/model/data-filter';
import { JobFilter } from '../../model/job';
import { TourGuideComponent } from '../../shared/tour-guide/tour-guide.component';
import { CronOptions } from 'cron-editor/cron-editor';
import { GroupResult, groupBy } from '@progress/kendo-data-query';

import { FileInfo } from '@progress/kendo-angular-upload';
import {
  DataStateChangeEvent, SelectableSettings, PageChangeEvent,
  FilterService, GridDataResult, SelectAllCheckboxState
} from '@progress/kendo-angular-grid';
import { State } from '@progress/kendo-data-query';
import { Subscriber } from 'rxjs';
import * as introJs from 'intro.js/intro.js';
import { lastDayOfMonth } from '@progress/kendo-date-math';
import { DropEvent } from 'angular-draggable-droppable';
import { DropdownGroupItem } from '../../model/dropdown';
import { SessionService } from '../../app.session-service';
import { UserService } from '../../settings/users/users.service';
import { RolesService } from '../../settings/roles/roles.service';
import { Role } from '../../model/role';
// import { saveAs } from '@progress/kendo-drawing/pdf';
interface ColummnSetting {
  field: string;
  title: string;
  //format?: string;
  type: 'text' | 'numeric' | 'boolean' | 'date';
  tooltip: boolean;
  locked: boolean;


  filterable: boolean;
  width: string;
  sortable: boolean;
  sortBy: string;
  style: string;
  hidden: boolean;
}


@Component({
  selector: 'app-new-job',
  templateUrl: './new-job.component.html',
  styleUrls: ['./new-job.component.scss']
})
export class NewJobComponent implements OnInit, OnDestroy {

  source: Array<{ text: string, value: number }> = [
    { text: 'Select Connection', value: 1 }];
  types: Array<{ text: string, value: number }> = [
    { text: 'Select Type', value: 1 },
    { text: 'VARCHAR2', value: 2 },
    { text: 'NCHAR', value: 3 },
    { text: 'NVARCHAR2', value: 3 },
    { text: 'CLOB', value: 3 },
    { text: 'NCLOB', value: 3 },
    { text: 'LONG', value: 3 },
    { text: 'NUMBER', value: 3 },
    { text: 'DATE', value: 3 },
    { text: 'BLOB', value: 3 },
    { text: 'BFILE', value: 3 },
    { text: 'RAW', value: 3 },
    { text: 'LONG ROW', value: 3 },
  ];
  logicalOperators: Array<{ text: string, value: string }> = [
    // { text: 'Select Operator', value: 'null' },
    { text: 'and', value: 'and' },
    { text: 'or', value: 'or' },
  ];
  comparisonOperators: Array<{ text: string, value: string }> = [
    { text: 'Select Type', value: '' },
    { text: 'equal', value: 'equal' },
    { text: 'not equal', value: 'not equal' },
    { text: 'less than', value: 'less than' },
    { text: 'less than or equal', value: 'less than or equal' },
    { text: 'greater than', value: 'greater than' },
    { text: 'Greater than or equal', value: 'Greater than or equal' }
  ];
  dataFilters: Array<DataFilter> = new Array<DataFilter>();
  dataPreviewerFilters: Array<DataFilter> = new Array<DataFilter>();
  selectedRoles: Array<Role> = new Array<Role>();
  roles: Array<Role> = new Array<Role>();

  public jobFilter: JobFilter;
  isSourceCollectionSelected: boolean;
  isSourceConnectionConnected: boolean;
  isDestinationConnectionConnected: boolean;
  isDestinationCollectionSelected: boolean;
  isConnectionVisibleOption: boolean;
  isCollectionVisibleOption: boolean;
  procedureColumns: any;
  inResponseName: any = [];
  inResponseType: any = [];
  destinationConnectionType: any;
  isDestinationConnectionOption: boolean;
  isSourceConnectionOption: boolean;
  isCreatedTableRequest: boolean;
  isCreateTableRequested: boolean;
  apiUrl: any;
  j: number;
  connectionId: string;
  procedureField: any;
  procedureType: any;
  isSyncAble: boolean;
  isNewSyncMetadataUpdated: boolean;
  isSchedulerHide: boolean;
  isProcessing: boolean;
  showSpinner: boolean;
  isIncrementParameter: boolean;
  incrementColumnDateTimeValue: boolean;
  public myFiles: Array<FileInfo> = [];
  files: File;
  uploadSaveUrl = 'saveUrl'; // should represent an actual API endpoint
  uploadRemoveUrl = 'removeUrl';
  // sourceCollection: Array<{ text: string, value: string }>;
  // destinationCollection: Array<{ text: string, value: string }>;
  typeCollection: Array<{ text: string, value: number }>;
  logicalOperatorCollections: Array<{ text: string, value: string }>;
  comparisonOperatorCollections: Array<{ text: string, value: string }>;
  // sourceCollectionValue: any;
  sourceCollectionValue: any;

  destinationCollectionValue: any;
  sourceCollectionList: Array<any>;
  sourceCollectionUrlList: Array<any>;
  destinationCollectionList: Array<any>;
  sourceDataPreviewGridHeader: string[] = [];
  destinationDataPreviewGridHeader: string[] = [];
  dataPreviewGridHeader: ColummnSetting[] = [];
  testFilterRequest = {};
  destinationValueType: any;
  sourceValueType: any;
  public metadataName: any;
  public tableOrProcedure: string;
  collectionName: string;
  sourceGridDataResult: GridDataResult;
  destinationGridDataResult: GridDataResult;
  tourGuideComponent: TourGuideComponent;
  logViewGridDataResult: GridDataResult;
  connections: Array<Connection>;
  sourceConnections: Array<Connection>;
  destinationConnections: Array<Connection>;
  allConnectiona: Array<Connection>;

  connectionsList: Array<Connection>;
  sourceConnectionValue: Connection;
  destinationConnectionValue: Connection;
  selectedSourceCollection: Array<Collection>;
  filterResponseProcedure: Array<any>;
  inresponse: any;
  selectedDestinationCollection: Array<Collection>;
  jobRequest: JobRequest;
  stats: any;
  i: number;
  connectionType: string;
  dataTimeOffsettype: any;
  height: number;
  incrementalColumns: string[];
  sourceFilterableColumns: Metadata[];
  incrementalColumnSelectedValue: string;
  selectedModelTabName: string;
  statsMessage: string;
  incrementValue: string;
  jobDetail: any;
  loginData: any;
  dublicateColumnName: string;
  opened = false;
  incrementColumnValue: Date;
  intervalObj: any;
  table: any;
  editIconVisibility: boolean;
  sourceCollectionValues: any;
  cronExpression = '4 3 2 12 1/1 ? *';
  isCronDisabled = false;
  isfilterValue = 0;
  isRunEnabled: boolean;
  metadataDifference: any;
  editColumn: string;
  cronOptions: CronOptions = {
    formInputClass: 'form-control cron-editor-input',
    formSelectClass: 'form-control cron-editor-select',
    formRadioClass: 'cron-editor-radio',
    formCheckboxClass: 'cron-editor-checkbox',

    defaultTime: '10:00:00',
    use24HourTime: true,

    hideMinutesTab: false,
    hideHourlyTab: false,
    hideDailyTab: false,
    hideWeeklyTab: false,
    hideMonthlyTab: false,
    hideYearlyTab: false,
    hideAdvancedTab: true,

    hideSeconds: false,
    removeSeconds: false,
    removeYears: false
  };
  sourceItemsSkip = 0;
  destinationItemsSkip = 0;
  accountRequestPageSizes = true;
  info = true;
  previousNext = true;
  buttonCount = 5;
  public state: State = {
    skip: 0,
    take: 1
  };
  buttonCountDP = 1;
  infoDP = true;
  typeDP: 'numeric' | 'input' = 'numeric';
  pageSizesDP = true;
  previousNextDP = true;
  sourceGridLoading: boolean;
  destinationGridLoading: boolean;
  introJS = introJs();
  sourceCollectionItems: Array<any>;
  sourceCollectionItemsUrl: Array<any>;
  destinationCollectionItems: Array<any>;
  public data: Array<any> = [
    { name: 'Pork', category: 'Food', subcategory: 'Tables' },
    { name: 'Pepper', category: 'Food', subcategory: 'Procedure' },
  ];
  public sourceCollection: GroupResult[];
  public destinationCollection: GroupResult[];
  // = groupBy(this.groupedData, [{ field: 'subcategory' }])

  constructor(
    private connectionService: ConnectionService,
    private cookieService: CookieService,
    private tourGuideComponents: TourGuideComponent,
    private router: Router,
    private jobService: JobService,
    private route: ActivatedRoute,
    private eRef: ElementRef,
    private toastr: ToastrService,
    private dataService: DataService,
    private sessionService: SessionService,
    private userService: UserService,
    private roleService: RolesService
  ) {

    if (this.sessionService.getCookie('LA_USER_DETAIL') !== '') {
      this.loginData = JSON.parse(JSON.stringify(this.sessionService.getCookie('LA_USER_DETAIL')));
    }
    else {
      this.loginData = JSON.parse(JSON.stringify(this.sessionService.getCookie('LOGIN').results[0]));
    }
    this.GetUserRoles(this.loginData.userId);
    this.isSourceConnectionConnected = false;
    this.isSourceCollectionSelected = false;
    this.isDestinationConnectionConnected = false;
    this.isDestinationCollectionSelected = false;
    this.isConnectionVisibleOption = false;
    this.editIconVisibility = false;
    this.isCollectionVisibleOption = false;
    this.isDestinationConnectionOption = false;
    this.isSourceConnectionOption = false;
    this.isCreatedTableRequest = false;
    this.isCreateTableRequested = false;
    this.isNewSyncMetadataUpdated = false;
    this.isSyncAble = false;
    this.showSpinner = false;
    this.isRunEnabled = false;
    this.incrementColumnDateTimeValue = false;
    this.isSchedulerHide = false;
    this.sourceConnections = Array<Connection>();
    this.destinationConnections = Array<Connection>();
    this.selectedSourceCollection = new Array<Collection>();
    this.filterResponseProcedure = new Array<any>();
    this.selectedDestinationCollection = new Array<Collection>();
    this.sourceCollectionItems = new Array<DropdownGroupItem>();
    this.sourceCollectionItemsUrl = new Array<DropdownGroupItem>();
    this.destinationCollectionItems = new Array<DropdownGroupItem>();
    this.sourceFilterableColumns = new Array<Metadata>();
    this.dataFilters = new Array<DataFilter>();
    this.dataPreviewerFilters = new Array<DataFilter>();
    // this.dataFilters.push(new DataFilter());
    this.dataPreviewerFilters.push(new DataFilter());
    this.jobRequest = new JobRequest();
    this.jobFilter = new JobFilter();
    this.jobFilter.recordCount = 10;
    this.height = 700;
    this.i = 1;
    this.j = 0;
    this.stats = { status: '', processedRecord: '', totalRecordCount: '' };
    // this.sourceCollection = new Array<{ text: string, value: string }>();
    // this.destinationCollection = new Array<{ text: string, value: string }>();
    this.incrementColumnValue = new Date();
    this.incrementValue = 'singleProcess';
    this.intervalObj = setInterval(() => {
      this.route.paramMap.subscribe(params => {
        if (params.get('jobId') !== 'new') {
          this.checkProgress(params.get('jobId'));
        }
      });
    }, 1000);
    if (this.cookieService.get('intro') === 'createNewJob') {
      this.introJS.setOptions({
        steps: [
          {
            element: '#job_name',      // 0
            intro: 'Write the job name '
          },
          {
            element: '#sourceConnections',   //1
            intro: 'Select Source Connection',
            position: 'right'
          },
          {
            element: '#sourceConnectionsBtn',   //2
            intro: 'Press button'
          },
          {
            element: '#sourceCollection',      //3
            intro: 'Select Source Collection.',
            position: 'right'
          },
          {
            element: '#destinationConnections',     //4
            intro: 'Select Destination Connection.',
            position: 'left'
          },
          {
            element: '#destinationConnectionsBtn',     //5
            intro: 'Press button'
          },
          {
            element: '#createTableBtn',               //6
            intro: 'Press vertical three dot'
          },
          {
            element: '#createTableOption',           //7
            intro: 'Select Create Table',
            position: 'top'
          },
          {
            element: '#OkBtn',                    //8
            intro: 'Press the Ok button'
          },
          {
            element: '#saveBtn',                //9
            intro: 'Press Save Icon'
          },
        ]
      });
    } else if (this.cookieService.get('intro') === 'createNewJobWithExistingTable') {
      this.introJS.setOptions({
        steps: [
          {
            element: '#job_name',           //0
            intro: 'Write the job name '
          },
          {
            element: '#sourceConnections',      //1
            intro: 'Select Source Connection',
            position: 'right'
          },
          {
            element: '#sourceConnectionsBtn',       //2
            intro: 'Press button'
          },
          {
            element: '#sourceCollection',     //3
            intro: 'Select Source Collection.',
            position: 'right'
          },
          {
            element: '#destinationConnections',        //4
            intro: 'Select Destination Connection.',
            position: 'left'
          },
          {
            element: '#destinationConnectionsBtn',       //5
            intro: 'Press button'
          },
          {
            element: '#selectTableCollection',       //6
            intro: 'Select the table ',
            position: 'top'
          },
          {
            element: '#saveBtn',          //7
            intro: 'Press Save Icon'
          },
        ]
      });
    } else if (this.cookieService.get('intro') === 'createNewJobWithFilter') {
      this.introJS.setOptions({
        steps: [
          {
            element: '#job_name',           //0
            intro: 'Write the job name '
          },
          {
            element: '#sourceConnections',      //1
            intro: 'Select Source Connection',
            position: 'left'
          },
          {
            element: '#sourceConnectionsBtn',       //2
            intro: 'Press button'
          },
          {
            element: '#sourceCollection',       //3
            intro: 'Select Source Collection.',
            position: 'left'
          },
          {
            element: '#openFilterBtn',      //4
            intro: 'Press Vertical Three Dot',
            position: 'top'
          },
          {
            element: '#filterBtn',       //5
            intro: 'Press Filter Text.',
            position: 'right'
          },
          {
            element: '#filterField',        //6
            intro: 'Select Filterable field.'
          },
          {
            element: '#selectOperator',      //7
            intro: 'Select Operator.'
          },
          {
            element: '#writeValue',         //8
            intro: 'Write value.'
          },
          {
            element: '#validate',          //9
            intro: 'Press Validate Filter.'
          },
          {
            element: '#OkBtn',          //10
            intro: 'Press Ok.'
          },
          {
            element: '#destinationConnections',      //11
            intro: 'Select Destination Connection.'
          },
          {
            element: '#destinationConnectionsBtn',       //12
            intro: 'Press button'
          },
          {
            element: '#createTableBtn',              //13
            intro: 'Press vertical three dot'
          },
          {
            element: '#createTableOption',         //14
            intro: 'Select Create Table'
          },
          {
            element: '#OkBtn',                   //15
            intro: 'Press the Ok button'
          },
          {
            element: '#saveBtn',               //16
            intro: 'Press Save Icon'
          },
        ]
      });
    } else if (this.cookieService.get('intro') === 'createNewJobWithFilterWithExistingTable') {
      this.introJS.setOptions({
        steps: [
          {
            element: '#job_name',           //0
            intro: 'Write the job name '
          },
          {
            element: '#sourceConnections',      //1
            intro: 'Select Source Connection'
          },
          {
            element: '#sourceConnectionsBtn',       //2
            intro: 'Press button'
          },
          {
            element: '#sourceCollection',       //3
            intro: 'Select Source Collection.'
          },
          {
            element: '#openFilterBtn',      //4
            intro: 'Press Vertical Three Dot'
          },
          {
            element: '#filterBtn',       //5
            intro: 'Press Filter Text.'
          },
          {
            element: '#filterField',        //6
            intro: 'Select Filterable field.'
          },
          {
            element: '#selectOperator',      //7
            intro: 'Select Operator.'
          },
          {
            element: '#writeValue',         //8
            intro: 'Write value.'
          },
          {
            element: '#validate',          //9
            intro: 'Press Validate Filter.'
          },
          {
            element: '#OkBtn',          //10
            intro: 'Press Ok.'
          },
          {
            element: '#destinationConnections',      //11
            intro: 'Select Destination Connection.'
          },
          {
            element: '#destinationConnectionsBtn',       //12
            intro: 'Press button'
          },

          {
            element: '#selectTableCollection',       //13
            intro: 'Select the table '
          },

          {
            element: '#saveBtn',               //14
            intro: 'Press Save Icon'
          },
        ]
      });
    } else if (this.cookieService.get('intro') === 'createNewJobWithIncrementValue') {
      this.introJS.setOptions({
        steps: [
          {
            element: '#job_name',           //0
            intro: 'Write the job name '
          },
          {
            element: '#sourceConnections',      //1
            intro: 'Select Source Connection'
          },
          {
            element: '#sourceConnectionsBtn',       //2
            intro: 'Press button'
          },
          {
            element: '#sourceCollection',       //3
            intro: 'Select Source Collection.'
          },
          {
            element: '#openFilterBtn',      //4
            intro: 'Press Vertical Three Dot'
          },
          {
            element: '#filterBtn',       // 5
            intro: 'Press Net Change Text.'
          },
          {
            element: '#radioBtn',       // 6
            intro: 'Select Second Radio Butoon.'
          },
          {
            element: '#selectNetChaneValue',       // 7
            intro: 'Select Net Change Field.'
          },
          {
            element: '#selectIncrementDate',       // 8
            intro: 'Select Date.'
          },
          {
            element: '#selectIncrementTime',       // 9
            intro: 'Select Time.'
          },
          {
            element: '#OkBtn',          // 10
            intro: 'Press Ok.'
          },
          {
            element: '#destinationConnections',      // 11
            intro: 'Select Destination Connection.'
          },

          {
            element: '#destinationConnectionsBtn',       // 12
            intro: 'Press button'
          },

          {
            element: '#createTableBtn',              // 13
            intro: 'Press vertical three dot'
          },
          {
            element: '#createTableOption',         //14
            intro: 'Select Create Table'
          },
          {
            element: '#OkBtn',                   //15
            intro: 'Press the Ok button'
          },
          {
            element: '#saveBtn',               //16
            intro: 'Press Save Icon'
          },
        ]
      })

    } else if (this.cookieService.get('intro') === 'createNewJobWithIncrementValueExistingtable') {
      this.introJS.setOptions({
        steps: [
          {
            element: '#job_name',           //0
            intro: 'Write the job name '
          },
          {
            element: '#sourceConnections',      //1
            intro: 'Select Source Connection'
          },
          {
            element: '#sourceConnectionsBtn',       //2
            intro: 'Press button'
          },
          {
            element: '#sourceCollection',       //3
            intro: 'Select Source Collection.'
          },
          {
            element: '#openFilterBtn',      //4
            intro: 'Press Vertical Three Dot'
          },
          {
            element: '#filterBtn',       //5
            intro: 'Press Net Change Text.'
          },
          {
            element: '#radioBtn',       //6
            intro: 'Select Second Radio Butoon.'
          },
          {
            element: '#selectNetChaneValue',       //7
            intro: 'Select Net Change Field.'
          },
          {
            element: '#selectIncrementDate',       //8
            intro: 'Select Date.'
          },
          {
            element: '#selectIncrementTime',       //9
            intro: 'Select Time.'
          },
          {
            element: '#OkBtn',          //10
            intro: 'Press Ok.'
          },
          {
            element: '#destinationConnections',      //11
            intro: 'Select Destination Connection.'
          },
          {
            element: '#destinationConnectionsBtn',       //12
            intro: 'Press button'
          },

          {
            element: '#selectTableCollection',       //13
            intro: 'Select the table '
          },

          {
            element: '#saveBtn',               //14
            intro: 'Press Save Icon'
          },
        ]
      })

    }
    this.introJS.oncomplete(() => {
      this.router.navigate(['jobs/new'], { queryParams: {} });
    });
    this.introJS.onexit(() => {
      this.router.navigate(['jobs/new'], { queryParams: {} });

    });
  }


  ngOnInit() {
    this.typeCollection = this.types.slice();
    this.logicalOperatorCollections = this.logicalOperators.slice();
    this.comparisonOperatorCollections = this.comparisonOperators.slice();
    if (this.route.snapshot.queryParams.action === 'intro') {
      this.introJS.start();
      this.introJS.onchange((targetElement) => {
      });
      this.introJS.oncomplete(() => {
        this.router.navigate(['/jobs/new'], { queryParams: {} });
      });
      this.introJS.onexit(() => {
        // this.router.navigate(['/jobs/new'], { queryParams: {} });
      });
    }
  }
  GetUserRoles(userid) {
    this.userService.GetUserRoles(userid).subscribe(data => {
      this.GetAllRoles();
      const response = JSON.parse(JSON.stringify(data));
      if (response.success === true && response.results[0].roleDetailsResponse.length > 0) {
        this.selectedRoles = response.results[0].roleDetailsResponse;
      }
    }, err => {
    });
  }

  GetAllRoles() {
    this.roleService.GetAllRoles().subscribe(data => {
      this.roles = JSON.parse(JSON.stringify(data)).results;
      this.getAllConnection();

      this.selectedRoles.forEach(elemen => {
        const obj = this.roles.filter(n => n.roleId === elemen.roleId)[0];
        const index = this.roles.indexOf(obj);
        if (index > -1) {
          this.roles.splice(index, 1);
        }
      });
      console.log(this.roles);
    }, err => {
      console.error(err);
    });
  }
  onSelectFile(event) {
    this.files = event.target.files[0];
  }

  handleChange(evt) {
    this.isIncrementParameter = !this.isIncrementParameter;
    this.incrementColumnDateTimeValue = false;
    this.incrementalColumnSelectedValue = '';
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithIncrementValue') {
      this.introJS.goToStepNumber(7).start();
    }
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithIncrementValueExistingtable') {
      this.introJS.goToStepNumber(7).start();
    }
  }


  selectionChangeSourceConnection() {

    this.populateSourceCollection(this.sourceConnectionValue.connectionId);
    if (this.route.snapshot.queryParams.action === 'intro') {
      this.introJS.goToStepNumber(2).start();
    }


  }

  // public itemDisabled(itemArgs: { dataItem: string, index: number }) {
  //     return itemArgs.dataItem.type !== '' && this.sourceConnectionValue != undefined && itemArgs.dataItem.type !== this.sourceConnectionValue.type;
  // }
  changeSourceConnection() {
    this.isSourceConnectionConnected = false;

  }

  public itemDisabled(itemArgs: { dataItem: any, index: number }) {
    return !itemArgs.dataItem.isDisabled;
  }

  selectionChangeSourceCollection(event) {
    this.sourceCollectionValue.text = event.text;
    this.sourceCollectionValue.value = event.value;
    this.sourceCollectionValue.subcategory = event.subcategory;
    this.populateSourceMetadata(this.sourceConnectionValue.connectionId, event.value);
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJob') {
      this.introJS.goToStepNumber(4).start();
    }
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithExistingTable') {
      this.introJS.goToStepNumber(4).start();
    }
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilter') {
      this.introJS.goToStepNumber(4).start();
    }
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilterWithExistingTable') {
      this.introJS.goToStepNumber(4).start();
    }
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithIncrementValue' && this.isSourceConnectionOption === false) {
      this.introJS.goToStepNumber(4).start();
    }
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithIncrementValueExistingtable' && this.isSourceConnectionOption === false) {
      this.introJS.goToStepNumber(4).start();
    }

  }

  selectionChangeDestinationConnection() {

    this.populateDestinationCollection(this.destinationConnectionValue.connectionId);
    this.populateDestinationMetadata(this.destinationConnectionValue.connectionId, '');
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJob') {
      this.introJS.goToStepNumber(5).start();
    } if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithExistingTable') {
      this.introJS.goToStepNumber(5).start();
    }

    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilter') {
      this.introJS.goToStepNumber(12).start();
    }
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilterWithExistingTable' && this.i === 1) {

      this.introJS.goToStepNumber(13).start();
    }
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilterWithExistingTable' && this.i === 0) {

      this.introJS.goToStepNumber(12).start();
    }
    this.i = this.i + 1;
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithIncrementValue') {

      this.introJS.goToStepNumber(12).start();
    }
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithIncrementValueExistingtable') {

      this.introJS.goToStepNumber(12).start();
    }
  }

  changeDestinationConnection() {
    this.isDestinationConnectionConnected = false;
    this.destinationCollection = [];

  }

  selectionChangeDataType(event, filter) {

    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJob') {
      this.introJS.goToStepNumber(6).start();
    }
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilter') {
      this.introJS.goToStepNumber(7).start();
    }
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilterWithExistingTable') {
      this.introJS.goToStepNumber(7).start();
    }
    if (this.sourceCollectionValue.subcategory !== 'procedure') {
      filter.dataType = event.type;
    }
    if (this.sourceCollectionValue.subcategory === 'procedure') {
      this.dataFilters = [];
      event.forEach(element => {

        this.procedureType = element.type;
        this.procedureField = element.name;
        this.dataFilters.push({ comparisonOperator: '', field: this.procedureField, logicalOperator: '', value: '', dataType: this.procedureType });
      });

    }

  }

  populateSourceCollection(connectionId) {
    if (this.sourceConnectionValue.type === 'EXCEL') {
      this.sourceValueType = 'EXCEL';
      this.isSourceConnectionConnected = true;
      // return false;
    }
    console.log(this.sourceConnectionValue.type);
    this.sourceValueType = this.sourceConnectionValue.name
    this.isProcessing = true;
    this.connectionId = connectionId;
    this.jobService.getAllEntities(connectionId)
      .subscribe(response => {
        this.isProcessing = false;
        if (this.sourceConnectionValue.type !== 'EXCEL') {
          const metadataTableResponse = JSON.parse(JSON.stringify(response)).results[0].metadataTableResponse === undefined ? [] : JSON.parse(JSON.stringify(response)).results[0].metadataTableResponse;
          const metadataProcedureResponse = JSON.parse(JSON.stringify(response)).results[0].metadataProcedureResponse === undefined ? [] : JSON.parse(JSON.stringify(response)).results[0].metadataProcedureResponse;
          //  this.data
          this.sourceCollectionItems = new Array<DropdownGroupItem>();
          if (this.sourceConnectionValue.type === 'ODATA') {
            metadataTableResponse.forEach(element => {
              this.tableOrProcedure = 'table';
              this.sourceCollectionItems.push({ text: `${element.collectionName} (${element.collectionCount})`, value: element.collectionName, subcategory: 'table' });
            });
          } else {
            metadataTableResponse.forEach(element => {
              this.tableOrProcedure = 'table';
              this.sourceCollectionItems.push({ text: `${element.collectionName}`, value: element.collectionName, subcategory: 'table' });
            });
          }

          if (this.sourceConnectionValue.type !== 'ODATA') {
            metadataProcedureResponse.forEach(element => {
              this.tableOrProcedure = 'procedure';
              this.sourceCollectionItems.push({ text: element.collectionName, value: element.collectionName, subcategory: 'procedure' });
            });
          } else {
            metadataTableResponse.forEach(element => {
              this.sourceCollectionItems.push({ text: element.collectionName, value: element.collectionName, subcategory: 'procedure' });
            });

          }
        }
        this.sourceCollectionList = this.sourceCollectionItems;
        this.sourceCollection = groupBy(this.sourceCollectionItems, [{ field: 'subcategory' }]);
        if (this.sourceConnectionValue.type === 'EXCEL') {
          this.sourceCollectionItems.push({ text: '', value: '', subcategory: '' });
        }
        const defaultvalue = { text: 'Select Collection', value: '0' };
        // this.sourceCollection.splice(0, 0, defaultvalue);
        // = groupBy(this.groupedData, [{ field: 'subcategory' }])
        this.sourceCollectionValue = defaultvalue;
        this.isSourceConnectionConnected = true;
        if (this.isSourceConnectionConnected && this.route.snapshot.queryParams.action === 'intro') {
          this.introJS.goToStepNumber(3).start();
        }
        this.isSourceCollectionSelected = false;
        this.route.paramMap.subscribe(params => {
          console.log(params);
          if (params.get('jobId') !== 'new') {
            this.sourceCollectionValue = this.sourceCollectionItems.filter(n => n.value.toLowerCase() === this.jobDetail.sourceCollection.toLowerCase())[0];
            this.populateSourceMetadata(this.sourceConnectionValue.connectionId, this.jobDetail.sourceCollection);
          }
        });
      }, error => {
        this.isProcessing = false;
      });
  }


  populateDestinationCollection(connectionId) {
    console.log(this.destinationConnectionValue.type);
    if (this.destinationConnectionValue.type === 'EXCEL') {
      this.isDestinationConnectionConnected = true;
      // return false;
    }
    this.isProcessing = true;
    this.jobService.getAllEntities(connectionId)
      .subscribe(response => {
        this.isProcessing = false;
        if (this.destinationConnectionValue.type !== 'EXCEL') {
          const metadataTableResponse = JSON.parse(JSON.stringify(response)).results[0].metadataTableResponse === undefined ? [] : JSON.parse(JSON.stringify(response)).results[0].metadataTableResponse;
          const metadataProcedureResponse = JSON.parse(JSON.stringify(response)).results[0].metadataProcedureResponse === undefined ? [] : JSON.parse(JSON.stringify(response)).results[0].metadataProcedureResponse;
          const list = JSON.parse(JSON.stringify(response)).results;

          // this.destinationCollection = new Array<{ text: string, value: string }>();
          this.destinationCollectionItems = new Array<DropdownGroupItem>();
          if (this.sourceConnectionValue.type === 'OData') {
            metadataTableResponse.forEach(element => {
              this.destinationCollectionItems.push({ text: `${element.collectionName} (${element.collectionCount})`, value: element.collectionName, subcategory: 'table' });
            });
          } else {
            metadataTableResponse.forEach(element => {
              this.destinationCollectionItems.push({ text: `${element.collectionName}`, value: element.collectionName, subcategory: 'table', url: element.apiUrl });
              // this.destinationCollectionItems.push({ text: `${element.apiUrl}`, value: element.apiUrl, subcategory: 'table' });
              this.sourceCollectionItemsUrl.push({ text: `${element.collectionName}`, value: element.collectionName, subcategory: 'table', apiUrl: element.apiUrl });
            });
          }
        }
        // metadataProcedureResponse.forEach(element => {

        //   this.destinationCollectionItems.push({ text: element.collectionName, value: element.collectionName, subcategory: 'procedure' });
        // });
        //  this.destinationCollectionList = this.destinationCollection;
        this.destinationCollectionList = this.destinationCollectionItems;
        // this.sourceCollectionUrlList = this.sourceCollectionItemsUrl;
        this.destinationCollection = groupBy(this.destinationCollectionItems, [{ field: 'subcategory' }]);
        const defaultvalue = { text: 'Select Collection', value: '0' };
        //  this.destinationCollection.splice(0, 0, defaultvalue);

        this.destinationCollectionValue = defaultvalue;
        this.isDestinationConnectionConnected = true;
        if (this.isDestinationConnectionConnected && this.route.snapshot.queryParams.action === 'intro') {
          this.introJS.goToStepNumber(6).start();
        }


        this.introJS.onexit(() => {
          this.router.navigate(['jobs/new'], { queryParams: {} });
        });

        this.isDestinationCollectionSelected = false;
        console.log(response);
        this.route.paramMap.subscribe(params => {
          console.log(params);
          if (params.get('jobId') !== 'new' && (this.sourceConnectionValue.type !== 'ODATA') && params.get('jobId') !== 'new' && (this.sourceConnectionValue.type !== 'ORACLE')) {
            this.destinationCollectionValue = this.destinationCollectionItems.
              filter(n => n.url.toLowerCase() === this.jobDetail.destinationCollection.toLowerCase())[0];
            console.log(this.sourceCollectionItemsUrl);
            const destinationColl = this.sourceCollectionItemsUrl.filter(x => x.apiUrl === this.jobDetail.destinationCollection).map(n => n.text);

            this.populateDestinationMetadata(this.destinationConnectionValue.connectionId, destinationColl[0]);
          }
          else {
            console.log(this.destinationCollection);
            this.destinationCollectionValue = this.destinationCollectionItems.filter(n => n.value.toLowerCase() === this.jobDetail.destinationCollection.toLowerCase())[0];
            this.populateDestinationMetadata(this.destinationConnectionValue.connectionId, this.jobDetail.destinationCollection);

          }
          //   this.destinationCollectionValue = this.destinationCollectionItems.
          //     filter(n => n.value.toLowerCase() === this.jobDetail.destinationCollection.toLowerCase())[0];
          //   console.log(this.sourceCollectionItemsUrl);
          //   const destinationColl = this.sourceCollectionItemsUrl.filter(x => x.apiUrl === this.jobDetail.destinationCollection).map(n => n.text);

          //   this.populateDestinationMetadata(this.destinationConnectionValue.connectionId, destinationColl[0]);
          // }
          // this.getJob(params.get('jobId'));
        });
      }, error => {
        this.isProcessing = false;
        console.log(error);
      });
  }

  populateSourceMetadata(connectionId, collectionName) {

    // this.connectionType = this.sourceCollectionValue.subcategory;
    // collectionName = collectionName;
    this.filterResponseProcedure = [];

    this.connectionType = this.sourceCollectionValue.subcategory;
    this.isProcessing = true;
    this.jobService.getTableMetadata(connectionId, collectionName, this.sourceCollectionValue.subcategory)
      .subscribe(response => {
        this.isProcessing = false;
        const list = JSON.parse(JSON.stringify(response)).results;
        const inResponse = JSON.parse(JSON.stringify(response)).results[0].inResponseList;
        this.selectedSourceCollection = new Array<Collection>();
        list.forEach(element => {
          this.selectedSourceCollection.push({
            collectionName: element.collectionName,
            collectionNameToggle: element.collectionNameToggle,
            isNameError: false,
            collectionMetadataResponseList: element.collectionMetadataResponseList,
            toggleName: false,
            toggleChildObject: false
          });
        });
        if (inResponse !== undefined) {
          inResponse.forEach(element => {
            this.filterResponseProcedure.push({
              name: element.name,
              type: element.type
            });
          });
        }

        if (this.sourceConnectionValue.type === 'ODATA') {
          this.incrementalColumns = this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.type === 'DateTimeOffset' && n.filterable === 'true').map(n => n.name);
          this.incrementalColumns.splice(0, 0, '');
        } else if (this.sourceConnectionValue.type !== 'ODATA' && this.sourceCollectionValue.subcategory === 'table') {
          this.incrementalColumns = this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.type === 'datetime').map(n => n.name);
          this.incrementalColumns.splice(0, 0, '');
        } else if (this.sourceConnectionValue.type !== 'ODATA' && this.sourceCollectionValue.subcategory === 'procedure') {
          this.incrementalColumns = this.filterResponseProcedure.filter(n => n.type === 'datetime').map(n => n.name);
        }

        if (this.sourceConnectionValue.type === 'ODATA') {
          this.sourceFilterableColumns = this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.filterable === 'true').map(n => n);
        } else if (this.sourceCollectionValue.subcategory === 'procedure' && this.sourceConnectionValue.type !== 'ODATA') {
          this.sourceFilterableColumns = this.filterResponseProcedure.filter(n => n.name).map(n => n);
        } else if (this.sourceCollectionValue.subcategory === 'table' && this.sourceConnectionValue.type !== 'ODATA') {
          this.sourceFilterableColumns = this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name);
        }

        this.isSourceCollectionSelected = true;
      }, error => {
        this.isProcessing = false;
        console.log(error);
      });

  }

  populateDestinationMetadata(connectionId, collectionName) {
    this.destinationConnectionType = localStorage.setItem('destinationConnectionValue', this.destinationConnectionValue.type);
    this.destinationValueType = this.destinationConnectionValue.name;
    this.apiUrl = this.sourceCollectionItemsUrl.filter(x => x.text === collectionName).map(n => n.apiUrl);
    this.isProcessing = true;
    this.jobService.getTableMetadata(connectionId, collectionName, this.destinationCollectionValue.subcategory)
      .subscribe(response => {
        this.isProcessing = false;
        const list = JSON.parse(JSON.stringify(response)).results;
        this.selectedDestinationCollection = new Array<Collection>();
        list.forEach(element => {
          if (this.jobDetail === undefined || this.jobDetail === null) {
            this.selectedDestinationCollection.push({
              collectionName: element.collectionName,
              collectionNameToggle: element.collectionNameToggle || false,
              isNameError: element.isNameError || false,
              collectionMetadataResponseList: element.collectionMetadataResponseList.map(n => new Metadata(n)),
              toggleName: false,
              toggleChildObject: false
            });
          } else {
            let mapping = JSON.parse(this.jobDetail.mapping);
            this.selectedDestinationCollection.push({
              collectionName: element.collectionName,
              collectionNameToggle: element.collectionNameToggle || false,
              isNameError: element.isNameError || false,
              collectionMetadataResponseList: element.collectionMetadataResponseList.map(n => new Metadata(n, mapping)),
              toggleName: false,
              toggleChildObject: false
            });
          }
        });
        console.log(this.selectedDestinationCollection);
        this.isDestinationCollectionSelected = true;
        this.collectionName = list[0].collectionName;
        this.metadataName = list[0].collectionMetadataResponseList.map(n => n.name);
        console.log(this.collectionName);
        console.log(this.metadataName);

        console.log(response);
      }, error => {
        this.isProcessing = false;
        console.log(error);
      });
  }
  dragEnd(event) {
    console.log('Element was dragged', event);
  }
  onDrop({ dropData }: DropEvent<any>, index): void {
    if(this.destinationConnectionValue.type === 'ODATA') {
    if ( this.selectedDestinationCollection[0].collectionMetadataResponseList[index].updatable === 'false') {

    } else {
      this.selectedDestinationCollection[0].collectionMetadataResponseList[index].mapping = `${this.selectedSourceCollection[0].collectionName}.${dropData}`;
    }
  } else {
    this.selectedDestinationCollection[0].collectionMetadataResponseList[index].mapping = `${this.selectedSourceCollection[0].collectionName}.${dropData}`;

  }
  }
  selectionChangeDestinationCollection(event) {
    this.populateDestinationMetadata(this.destinationConnectionValue.connectionId, event.value);
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJob') {
      this.introJS.goToStepNumber(7).start();
    }
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithExistingTable') {
      this.introJS.goToStepNumber(7).start();
    }
    // if (this.route.snapshot.queryParams.action === 'intro' &&  this.cookieService.get('intro') === 'createNewJobw') {
    //   this.introJS.goToStepNumber(7).start();
    // }

  }

  getAllConnection() {
    const roles = this.selectedRoles;
    this.connectionService.getAllConnection(new Connection(), 0, 36, 'createdDate', 'desc').subscribe(data => {
      this.sourceConnections = Array<Connection>();
      this.destinationConnections = Array<Connection>();
      this.allConnectiona = Array<Connection>();
      if (JSON.parse(JSON.stringify(data)).results[0].connectionResponseList !== null) {
        JSON.parse(JSON.stringify(data)).results[0].connectionResponseList.forEach(element => {
          this.allConnectiona.push(new Connection(element));
          if (roles.filter(n => n.role === 'Datahug') && (roles.filter(n => n.role === 'Admin').length === 0)) {
            if (element.type === 'EXCEL' || element.type === 'DATAHUG') {
              this.sourceConnections.push(new Connection(element));
              this.destinationConnections.push(new Connection(element));
            }
          }
          // else if(roles.filter(n => n.role ==='Admin')) {
          else {

            this.sourceConnections.push(new Connection(element));
            this.destinationConnections.push(new Connection(element));
          }
        });

        const obj = new Connection();
        obj.name = 'Select Connection';
        this.sourceConnections.splice(0, 0, obj);
        this.connectionsList = this.sourceConnections;
        this.sourceConnectionValue = obj;
        this.destinationConnectionValue = obj;
        this.route.paramMap.subscribe(params => {
          if (params.get('jobId') !== 'new') {
            this.isProcessing = false;
            this.getJob(params.get('jobId'));
          }
        });
      }
    });
  }

  onToggleSourceConnection(isSourceConnectionOption) {
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') !== 'createNewJobWithFilter') {
      this.introJS.goToStepNumber(5).start();
    }

    this.isSourceConnectionOption = !isSourceConnectionOption;
    // if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilter' && this.isSourceConnectionOption ===true) {
    //   this.introJS.goToStepNumber(5).start();
    // }
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilter' && this.isSourceConnectionOption === false) {
      this.introJS.goToStepNumber(6).start();
    }
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilterWithExistingTable' && this.isSourceConnectionOption === false) {
      this.introJS.goToStepNumber(6).start();
    }
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithIncrementValue' && this.isSourceConnectionOption === false) {
      this.introJS.goToStepNumber(6).start();
    }
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithIncrementValueExistingtable' && this.isSourceConnectionOption === false) {
      this.introJS.goToStepNumber(6).start();
    }
  }

  onToggleDestinationConnection(isDestinationConnectionOption) {
    this.isDestinationConnectionOption = !isDestinationConnectionOption;
    if (this.route.snapshot.queryParams.action === 'intro' && this.isDestinationConnectionOption === true && this.cookieService.get('intro') === 'createNewJob') {
      this.introJS.goToStepNumber(7).start();
    }
    // this.createTable();
  }

  copyMetadata() {
    this.jobService.getOdataCollection(this.sourceConnectionValue.connectionId, this.sourceCollectionValue.value)
      .subscribe(response => {
        this.isCreateTableRequested = true;
        const list = JSON.parse(JSON.stringify(response)).results;
        this.selectedDestinationCollection = new Array<Collection>();
        list.forEach(element => {
          this.selectedDestinationCollection.push(
            {
              collectionName: element.collectionName,
              collectionNameToggle: element.collectionNameToggle,

              isNameError: element.collectionName.length > 30 ? true : false,
              collectionMetadataResponseList: element.collectionMetadataResponseList.map(n => new Metadata(n)),
              toggleName: false,
              toggleChildObject: false
            });
          this.metadataName = element.collectionMetadataResponseList.map(n => new Metadata(n));
        });
        if (this.sourceCollectionValue.value.length > 30 && this.destinationConnectionValue.type === 'ORACLE') {
          this.toastr.warning('Table name length is more than 30 character');
        }
        if (this.route.snapshot.queryParams.action === 'intro' && this.isDestinationConnectionOption === false && this.cookieService.get('intro') === 'createNewJob') {
          this.introJS.goToStepNumber(8).start();
        }
        this.selectedDestinationCollection.filter(n => n.collectionName.length > 30).map(n => n.isNameError = true);
        this.selectedDestinationCollection.map(n => n.collectionMetadataResponseList.filter(n => n.name.length > 30).map(n => n.isNameError = true));
        console.log(this.selectedDestinationCollection.map(n => n.collectionMetadataResponseList.filter(n => n.name.length > 30).map(n => n.isNameError = true)));
        console.log((this.selectedDestinationCollection.filter(n => n.collectionMetadataResponseList.filter(j => j.isNameError === true))[0].collectionMetadataResponseList.length > 30));
        if (this.selectedDestinationCollection.filter(n => n.collectionMetadataResponseList.filter(j => j.isNameError === true))[0].collectionMetadataResponseList.length > 30 && this.destinationConnectionValue.type === 'ORACLE') {
          this.toastr.warning('Some column length is greater than 30, please scroll and change the column name.');
        }
        this.isDestinationCollectionSelected = true;

      }, error => {
        console.log(error);
      });

  }

  clearCopyTable() {
    this.isCreateTableRequested = false;
    this.isDestinationCollectionSelected = false;
    this.selectedDestinationCollection = new Array<Collection>();
  }

  createTable() {
    if (this.selectedDestinationCollection[0].collectionMetadataResponseList.filter(j => j.isNameError === true).length > 30) {
      this.toastr.warning('Some column length is greater than 30, please scroll and change the column name.');
      return false;
    }

    const table = {};
    table['tableName'] = this.selectedDestinationCollection[0].collectionName;
    table['tableMetadataList'] = [];
    this.selectedDestinationCollection[0].collectionMetadataResponseList.forEach(element => {
      const obj = new Object();
      obj['name'] = element.name;
      obj['type'] = element.type;
      table['tableMetadataList'].push(obj);
    });
    this.jobService.createTable(this.destinationConnectionValue.connectionId, table)
      .subscribe(response => {
        this.isCreateTableRequested = false;


        const res = JSON.parse(JSON.stringify(response));
        this.toastr.info(res.message);
        this.isCreateTableRequested = !res.success;
        if (res.success) {
          if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJob') {
            this.introJS.goToStepNumber(9).start();
          }

        }
        console.log(response);
      }, error => {
        console.log(error);
      });
  }

  runJob() {

    // this.isDestinationCollectionSelected = false;
    if (this.destinationValueType === 'EXCEL') {
      this.jobService.getExcelExport(this.jobRequest.jobId).subscribe(res => {
        saveAs(res, 'export.xlsx');
      });
    } else if (this.sourceValueType === 'EXCEL') {

      this.jobService.upLoad(this.jobRequest.jobId, this.files).subscribe(res => {
        console.log(res);
      });
    }
    else {
      this.jobService.runJob(this.jobRequest.jobId)
        .subscribe(response => {
          const res = JSON.parse(JSON.stringify(response));
          this.toastr.warning(res.message);
          if (res.message === 'Job Run Successfully') {
            this.isRunEnabled = true;
          } else {
            this.isRunEnabled = false;
          }
          this.isDestinationCollectionSelected = true;
          this.intervalObj = setInterval(() => {
            this.route.paramMap.subscribe(params => {
              if (params.get('jobId') !== 'new') {
                this.checkProgress(params.get('jobId'));
              }
            });
          }, 1000);
        }, error => {
          console.log(error);
        });
    }
  }

  stopJob() {
    this.jobService.stopJob(this.jobRequest.jobId)
      .subscribe(response => {
        const res = JSON.parse(JSON.stringify(response));
        this.toastr.warning(res.message);
        this.isRunEnabled = false;
      }, error => {
        console.log(error);
      });
  }

  schduleJob() {
    const data = new Date();
    const schduleJob = new Schdule();
    // schduleJob.cronExpression = `0 ${data.getMinutes() + 5} ${data.getHours()} * * *`;
    schduleJob.cronExpression = this.cronExpression.substring(0, this.cronExpression.length - 2);
    this.jobService.scheduleJob(this.jobRequest.jobId, schduleJob)
      .subscribe(response => {
        const res = JSON.parse(JSON.stringify(response));
        this.toastr.warning(res.message);
      }, error => {
        console.log(error);
      });
  }

  saveJob() {
    console.log(this.myFiles);
    if (this.jobRequest.jobName === '') {
      this.toastr.warning('Please enter the job name.');
      return false;
    }
    if (this.sourceConnectionValue.type === 'ODATA') {
      this.jobRequest.destinationCollection = this.selectedDestinationCollection.length > 0 ? this.selectedDestinationCollection[0].collectionName : '';
    }
    else if (this.sourceConnectionValue.type === 'ORACLE') {
      this.jobRequest.destinationCollection = this.selectedDestinationCollection.length > 0 ? this.selectedDestinationCollection[0].collectionName : '';


    }
    else {
      this.jobRequest.destinationCollection = this.apiUrl[0];
    }
    this.jobRequest.destinationConnection = this.destinationConnectionValue.connectionId;

    this.jobRequest.destinationConnectionName = this.destinationConnectionValue.name;
    if (this.jobRequest.sourceCollectionType !== '') {
      this.jobRequest.sourceCollectionType = this.connectionType.toUpperCase();
    }
    this.jobRequest.sourceCollection = this.selectedSourceCollection.length > 0 ? this.selectedSourceCollection[0].collectionName : '';
    this.jobRequest.sourceConnection = this.sourceConnectionValue.connectionId;
    this.jobRequest.sourceConnectionName = this.sourceConnectionValue.name;
    this.jobRequest.incrementalParameter = this.incrementalColumnSelectedValue;
    this.jobRequest.jobFilter = this.testFilterRequest['filterJson']
    this.jobRequest.incrementalParameterValue = `${this.incrementColumnValue.getMonth() + 1}/${this.incrementColumnValue.getDate()}/${this.incrementColumnValue.getFullYear()} ${this.incrementColumnValue.getHours()}:${this.incrementColumnValue.getMinutes()}:${this.incrementColumnValue.getSeconds()}`;

    console.log(this.incrementColumnValue);
    if (this.incrementalColumnSelectedValue === '') {
      this.jobRequest.incrementalParameterValue = '';
    }

    const object = {};
    if (this.selectedDestinationCollection.length > 0) {
      this.selectedDestinationCollection[0].collectionMetadataResponseList.forEach(element => {
        object[element.name] = element.mapping;
      });
    }
    this.jobRequest.mapping = JSON.stringify(object);
    if (this.jobRequest.jobId === '') {
      this.jobService.createjob(this.jobRequest)
        .subscribe(response => {
          this.toastr.success('Job created successfully');

          const jobId = JSON.parse(JSON.stringify(response)).results[0].jobId;
          this.jobRequest.jobId = jobId;
        }, error => {
          console.log(error);
        });
      if (this.route.snapshot.queryParams.action === 'intro') {
        this.introJS.goToStepNumber(9).start();
        this.introJS.exit();
      }
    } else {
      this.alterTableMetadata();
      //  this.jobRequest.mapping = this.jobDetail.mapping;
      this.jobService.updateJob(this.jobRequest)
        .subscribe(response => {
          this.toastr.success('Job updated successfully');

          const jobId = JSON.parse(JSON.stringify(response)).results[0].jobId;
          this.jobRequest.jobId = jobId;
        }, error => {
          console.log(error);
        });
    }
  }

  //clear() {
  // this.incrementColumnValue = new Date(this.jobRequest.incrementalParameterValue);
  // console.log(this.incrementColumnValue);
  //this.incrementColumnValue = new Date();
  //}

  saveTabOptions() {

    switch (this.selectedModelTabName) {
      case 'netChange':
        this.jobRequest.incrementalParameter = this.incrementalColumnSelectedValue;
        this.jobRequest.incrementalParameterValue = `${this.incrementColumnValue.getMonth() + 1}/${this.incrementColumnValue.getDate()}/${this.incrementColumnValue.getFullYear()} ${this.incrementColumnValue.getHours()}:${this.incrementColumnValue.getMinutes()}:${this.incrementColumnValue.getSeconds()}`;
        break;
      case 'filter':
        if (this.dataFilters.length === 1 && this.dataFilters.filter(n => n.field === '' || n.comparisonOperator === '' || n.value === '').length > 0 && this.connectionType !== 'procedure') {
          this.opened = !this.opened;
          return false;
        }
        if (this.dataFilters.filter(n => n.field === '' || n.comparisonOperator === '' || n.value === '').length > 0 && this.connectionType === 'procedure') {
          this.opened = !this.opened;


        }
        // const testFilterRequest = {};
        this.dataFilters.forEach(element => {
          const parameters = Object.keys(element);
          parameters.forEach(parameter => {
            if (element[parameter] !== '') {
              element[parameter] = element[parameter] === null ? '' : element[parameter];
            }
          });
        });
        this.testFilterRequest['filterJson'] = JSON.stringify(this.dataFilters);
        this.jobService.testFilterQuery(this.sourceConnectionValue.connectionId,
          this.selectedSourceCollection[0].collectionName, this.testFilterRequest)
          .subscribe(response => {
            const res = JSON.parse(JSON.stringify(response));
            if (res.success === 'true') {
              this.jobRequest.jobFilter = this.dataFilters.length > 0 ? JSON.stringify(this.dataFilters) : '';
              this.dataPreviewerFilters = this.dataFilters;
            } else {
              this.toastr.warning(res.message);
            }
          });
        break;
      case 'dataPreviewer':

        break;
      default:
        break;
    }

    // this.opened = !this.opened;
    this.opened = false;
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilter') {
      this.introJS.goToStepNumber(11).start();
    }
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilterWithExistingTable') {
      this.introJS.goToStepNumber(11).start();
    }
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithIncrementValue') {
      this.introJS.goToStepNumber(11).start();
    }
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithIncrementValueExistingtable') {
      this.introJS.goToStepNumber(11).start();
    }


  }


  validateFilter() {
    if (this.dataFilters.length === 1 && this.dataFilters.filter(n => n.field === '' || n.comparisonOperator === '' || n.value === '').length > 0) {
      return false;
    }
    const testFilterRequest = {};
    testFilterRequest['collectionName'] = this.sourceCollectionValue.value;
    this.dataFilters.forEach(element => {
      const parameters = Object.keys(element);
      parameters.forEach(parameter => {
        if (element[parameter] !== '') {
          // this.isfilterValue = 1;
          element[parameter] = element[parameter] === null ? '' : element[parameter];
        }
      });
    });
    testFilterRequest['filterJson'] = JSON.stringify(this.dataFilters);
    this.jobService.testFilterQuery(this.sourceConnectionValue.connectionId,
      this.selectedSourceCollection[0].collectionName, testFilterRequest)
      .subscribe(response => {
        console.log(response);
        const res = JSON.parse(JSON.stringify(response));
        if (res.success === 'true') {
          this.toastr.success(res.message);
        } else {
          this.toastr.warning(res.message);
        }
      });
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithExistingTable') {
      this.introJS.goToStepNumber(10).start();
    }
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'createNewJobWithFilterWithExistingTable') {
      this.introJS.goToStepNumber(10).start();
    }
  }

  checkProgress(jobId) {
    let progress = '0';
    this.showSpinner = true;

    this.jobService.gettJobStatus(jobId)
      .subscribe(response => {
        this.stats = JSON.parse(JSON.stringify(response)).results[0];
        if (this.stats.status === 'Completed') {
          clearInterval(this.intervalObj);
          this.isRunEnabled = false;
          this.statsMessage = '';
          this.showSpinner = false;
          progress = this.getPercentageChange(this.stats.totalRecordCount, this.stats.processedRecord).toFixed(2);

          this.statsMessage = `Copied ${this.stats.processedRecord} of ${this.stats.totalRecordCount}`;
        }
        if (this.stats.status === 'In Process') {
          this.isRunEnabled = true;
          progress = this.getPercentageChange(this.stats.totalRecordCount, this.stats.processedRecord).toFixed(2);

          this.statsMessage = `Copied ${this.stats.processedRecord} of ${this.stats.totalRecordCount}`;
        }
        if (this.stats.status === 'Failed') {
          this.isRunEnabled = false;
        }

      }, error => {
      });
  }

  getJob(jobId: string) {
    // if (jobId !== '' && this.destinationConnectionValue.type === 'ORACLE') {
    //   this.editIconVisibility = true;
    // }
    this.jobService.gettJob(jobId)
      .subscribe(response => {
        this.jobDetail = JSON.parse(JSON.stringify(response)).results[0];
        this.jobRequest.jobName = this.jobDetail.jobName;
        this.sourceConnectionValue = this.sourceConnections.filter(n => n.name === this.jobDetail.sourceConnectionName)[0];
        this.destinationConnectionValue = this.sourceConnections.filter(n => n.name === this.jobDetail.destinationConnectionName)[0];
        this.sourceConnections.forEach(element => {
          if (element.connectionId !== '' && element.type !== this.sourceConnectionValue.type) {
            element.isDisabled = false;
          }
        });

        this.populateSourceCollection(this.sourceConnectionValue.connectionId);
        this.populateDestinationCollection(this.destinationConnectionValue.connectionId);
        this.jobRequest.jobId = this.jobDetail.jobId;
        this.jobRequest.incrementalParameter = this.jobDetail.incrementalParameter;
        this.jobRequest.incrementalParameterValue = this.jobDetail.incrementalParameterValue;
        this.incrementColumnValue = new Date(this.jobRequest.incrementalParameterValue);

        this.incrementalColumnSelectedValue = this.jobRequest.incrementalParameter;
        if (this.jobRequest.incrementalParameterValue !== '') {
          this.isIncrementParameter = true;
          this.incrementValue = 'allProcess';
        }

        if (this.jobDetail.jobFilter !== '') {
          this.dataFilters = JSON.parse(this.jobDetail.jobFilter);
          this.dataPreviewerFilters = JSON.parse(this.jobDetail.jobFilter);
        }
      }, error => {
        console.log(error);
      });
  }

  close(status) {
    console.log(`Dialog result: ${status}`);
    this.opened = false;
  }

  openTab(tabName) {
    // this.dataFilters = [];
    const columnLength = this.sourceFilterableColumns.length;

    this.inResponseName = [];
    this.inResponseType = [];
    this.selectedModelTabName = tabName;
    if (tabName === 'dataPreviewer') {
      this.dataPreviewerFilters = this.dataFilters;
      this.getDataForPreview();
    } else if (tabName === 'logView') {
      this.height = 500;
      this.getLogView();
    } else if (tabName === 'filter' && this.sourceCollectionValue.subcategory === 'table') {
      if (this.dataFilters.length === 0) {
        this.dataFilters.push(new DataFilter());
      }
    } else if (tabName === 'filter' && this.sourceCollectionValue.subcategory === 'procedure') {

      // this.filterResponseProcedure = [];
      // this.filterResponseProcedure.forEach(element => {

      //   this.inResponseName.push({ comparisonOperator: '', field: '', logicalOperator: '', value: '', dataType: '' });
      //   this.inResponseType.push({ name: element.name, type: element.type });

      // });
      // this.selectionChangeDataType(this.inResponseType, this.inResponseName);
      if (columnLength > this.dataFilters.length) {
        this.sourceFilterableColumns.map(column => {
          this.dataFilters.push({ comparisonOperator: '', field: column.name, logicalOperator: '', value: '', dataType: column.type });
        });

      }
    }
    this.opened = true;

  }

  refreshSourceMetadata() {
    this.showSpinner = true;
    if (this.sourceConnectionValue.type === 'Odata' || this.sourceConnectionValue.type === 'OData_Working') {
      this.jobService.refreshOdataCollectionMetadata(this.sourceConnectionValue.connectionId).subscribe(data => {
        console.log(data);
        const refreshMetadata = JSON.parse(JSON.stringify(data)).message;
        this.toastr.warning(refreshMetadata);
        this.showSpinner = false;
      });
    }
  }
  destinationRefreshMetadata() {
    this.showSpinner = true;
    if (this.destinationConnectionValue.type === 'Odata' || this.sourceConnectionValue.type === 'OData_Working') {
      this.jobService.refreshOdataCollectionMetadata(this.destinationConnectionValue.connectionId).subscribe(data => {
        console.log(data);
        const refreshMetadata = JSON.parse(JSON.stringify(data)).message;
        this.toastr.warning(refreshMetadata);
        this.showSpinner = false;
      });
    }
  }

  addFilter() {
    this.dataFilters.push(new DataFilter());
  }

  removeFilter(index) {
    this.dataFilters.splice(index, 1);
  }

  addDataPreviewerFilters() {
    this.dataPreviewerFilters.push(new DataFilter());

  }
  removeDataPreviewerFilters(index) {
    this.dataPreviewerFilters.splice(index, 1);
  }

  getPercentageChange(oldNumber, newNumber) {
    const decreaseValue = oldNumber - newNumber;
    return (decreaseValue / oldNumber) * .1;
  }

  getDataForPreview() {
    this.sourceGridLoading = true;
    const testFilterRequest = {};
    // this.dataPreviewerFilters = this.dataFilters;
    this.dataPreviewerFilters.forEach(element => {
      const parameters = Object.keys(element);
      parameters.forEach(parameter => {
        if (element[parameter] !== '') {
          element[parameter] = element[parameter] === null ? '' : element[parameter];
        }
      });
    });
    testFilterRequest['previewFilter'] = JSON.stringify(this.dataPreviewerFilters);
    if (this.dataFilters.length === 1 && this.dataFilters.filter(n => n.field === '' || n.comparisonOperator === '' || n.value === '').length > 0) {
      testFilterRequest['previewFilter'] = '';
    }
    this.jobService.getDataForPreview(this.sourceConnectionValue.connectionId,
      this.selectedSourceCollection[0].collectionName, this.sourceCollectionValue.subcategory.toUpperCase(), this.jobFilter.startRecord, this.jobFilter.recordCount, testFilterRequest)
      .subscribe(response => {
        const res = JSON.parse(JSON.stringify(response));
        console.log(res);
        console.log(res.success);
        if (res.success === true) {
          const list = JSON.parse(JSON.stringify(response)).results[0];
          console.log(list.reviewList);
          this.sourceGridDataResult = list.reviewList;
          this.sourceGridLoading = true;
          this.destinationGridLoading = true;
          this.previewSourceData();
          this.previewDestinationData();
        } else {
          this.toastr.warning(res.message);
        }
      });
  }

  getLogView() {
    this.jobService.logView(this.jobRequest.jobId)
      .subscribe(response => {
        const res = JSON.parse(JSON.stringify(response));
        //this.toastr.warning(res.message);
        const list = JSON.parse(JSON.stringify(response)).results;

        const dataPreviewGridHeader = Object.keys(list[0]);
        this.logViewGridDataResult = {
          data: list,
          total: list.length
        };
      }, error => {
        console.log(error);
      });
  }

  previewSourceData() {
    this.sourceGridLoading = true;
    this.dataPreviewerFilters.forEach(element => {
      const parameters = Object.keys(element);
      parameters.forEach(parameter => {
        element[parameter] = element[parameter] === null ? '' : element[parameter];
      });
    });
    // testFilterRequest['filterJson'] = JSON.stringify(this.dataPreviewerFilters);
    const filter = new Object();

    filter['previewFilter'] = this.dataPreviewerFilters.filter(n => n.field !== '').length > 0 ? JSON.stringify(this.dataPreviewerFilters) : '';
    this.jobService.getDataForPreview(this.sourceConnectionValue.connectionId, this.sourceCollectionValue.value, this.sourceCollectionValue.subcategory.toUpperCase(), this.sourceItemsSkip, 1, filter)
      .subscribe(response => {
        const list = JSON.parse(JSON.stringify(response)).results[0];
        const parameters = Object.keys(list.reviewList[0]);
        this.sourceDataPreviewGridHeader = ['Name', 'Value'];
        const dataCollection = [];
        list.reviewList.forEach(data => {
          parameters.forEach(parameter => {
            dataCollection.push({ Name: parameter, Value: data[parameter] });
          });
        });
        this.sourceGridDataResult = {
          data: dataCollection,
          total: list.totalRecordCount
        };
        this.sourceGridLoading = false;

      });
  }

  previewDestinationData() {
    this.destinationGridLoading = true;
    const filter = new Object();
    filter['previewFilter'] = this.dataPreviewerFilters.filter(n => n.field !== '').length > 0 ? JSON.stringify(this.dataPreviewerFilters) : '';
    this.jobService.getDataForPreview(this.destinationConnectionValue.connectionId, this.destinationCollectionValue.value, this.sourceCollectionValue.subcategory.toUpperCase(), this.destinationItemsSkip, 10, filter)
      .subscribe(response => {
        const list = JSON.parse(JSON.stringify(response)).results[0];
        const parameters = Object.keys(list.reviewList[0]);
        this.destinationDataPreviewGridHeader = ['Name', 'Value'];
        const dataCollection = [];

        list.reviewList.forEach(data => {
          parameters.forEach(parameter => {
            dataCollection.push({ Name: parameter, Value: data[parameter] });
          });
        });
        this.destinationGridDataResult = {
          data: dataCollection,
          total: list.totalRecordCount
        };
        this.destinationGridLoading = false;
      });
  }

  public sourceItemsPageChange(event: PageChangeEvent): void {
    this.sourceItemsSkip = event.skip;
    this.previewSourceData();
  }

  public destinationItemsPageChange(event: PageChangeEvent): void {
    this.destinationItemsSkip = event.skip;
    this.previewDestinationData();
  }

  autoLinking() {
    if(this.sourceConnectionValue.type === 'ORACLE') {
    this.selectedDestinationCollection[0].collectionMetadataResponseList.forEach(element => {
      if (element.updatable === 'false') {
        // this.selectedDestinationCollection[0].collectionMetadataResponseList = this.selectedDestinationCollection[0].collectionMetadataResponseList.filter(n => n.creatable = element.updatable);



      } else {
        this.selectedDestinationCollection[0].collectionMetadataResponseList = this.selectedDestinationCollection[0].collectionMetadataResponseList.filter(n => n.creatable = element.updatable);
        element.mapping = `${this.selectedSourceCollection[0].collectionName}.${element.name.toUpperCase()}`;
      }
    });
  } else {
    this.selectedDestinationCollection[0].collectionMetadataResponseList.map(m => m.mapping = this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === m.name.toLocaleLowerCase()).length > 0 ? `${this.selectedSourceCollection[0].collectionName}.${this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === m.name.toLocaleLowerCase())[0].name}` : '');


  }
    // const mapping = map = this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === name.toLocaleLowerCase()).length > 0 ? `${this.selectedSourceCollection[0].collectionName}.${this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === name.toLocaleLowerCase())[0].name}` : `${this.selectedSourceCollection[0].collectionName}.${name}`;
    // metadata.isNameError = name.length > 30 || this.selectedDestinationCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === name.toLocaleLowerCase()).length > 1 ? true : false;

    // const mapping = metadata.mapping = this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === metadata.name.toLocaleLowerCase()).length > 0 ? `${this.selectedSourceCollection[0].collectionName}.${this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === metadata.name.toLocaleLowerCase())[0].name}` : `${this.selectedSourceCollection[0].collectionName}.${metadata.name}`;
    // metadata.isNameError =  metadata.name.length > 30 || this.selectedDestinationCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() ===  metadata.name.toLocaleLowerCase()).length > 1 ? true : false;
    //  const mapping =  metadata.map(m => m.mapping = this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === m.name.toLocaleLowerCase()).length > 0 ? `${this.selectedSourceCollection[0].collectionName}.${this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() ===  m.name.toLocaleLowerCase())[0].name}` : `${this.selectedSourceCollection[0].collectionName}.${ m.name}`);
    // console.log(metadata.mapping);
    //   if(this.selectedDestinationCollection[0].collectionMetadataResponseList.filter(n => n.creatable === 'false' ) && this.selectedDestinationCollection[0].collectionMetadataResponseList.filter(n => n.updatable === 'false' )) {
    //   } else {
    //   this.selectedDestinationCollection[0].collectionMetadataResponseList.map(m => m.mapping = this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === m.name.toLocaleLowerCase()).length > 0 ? `${this.selectedSourceCollection[0].collectionName}.${this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === m.name.toLocaleLowerCase())[0].name}` : '');
    // }
  }

  changeMetaMapping(e, index) {
    this.selectedDestinationCollection[0].collectionMetadataResponseList[index].mapping = ''
  }

  // autoLinking(metadata: Metadata) {
  //   console.log(this.selectedDestinationCollection[0].collectionMetadataResponseList);
  // }

  checkColumnName(metadata: Metadata) {
    if (this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === metadata.name.toLocaleLowerCase()).length > 1) {
      this.toastr.warning(`${this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === metadata.name.toLocaleLowerCase())[0].name} column exist multiple times`);
    }
    if (metadata.name.length <= 30 && this.destinationConnectionValue.type === 'ORACLE') {
      //const checkCondition = this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === metadata.name).map(n => n.isNameError);
      console.log(metadata.name)
      metadata.isNameError = false;
      this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === metadata.name);
    } else if (metadata.name.length > 30 && this.destinationConnectionValue.type === 'ORACLE') {
      metadata.isNameError = true;
    }
  }
  tableName(tableName) {
    if (this.destinationConnectionValue.type === 'ORACLE' && tableName.length <= 30) {
      tableName.toggleName = true
    } else if (this.destinationConnectionValue.type === 'ORACLE' && tableName.length > 30) {
      tableName.toggleName = false

    }
  }

  filterSourceCollection(value) {
    this.sourceCollection = this.sourceCollectionList.filter((s) => s.value.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }
  filterDestinationCollection(value) {
    this.destinationCollection = this.destinationCollectionList.filter((s) => s.value.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }
  filterDestinationConnection(value) {
    this.destinationConnections = this.connectionsList.filter(n => n.name.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }
  filterSourceConnection(value) {
    this.sourceConnections = this.connectionsList.filter(n => n.name.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  validateCollection(collection: Collection): void {
    collection.collectionNameToggle = collection.collectionName.length > 30 ? true : false;
  }

  validateCollectionParameter(metadata: Metadata): boolean {
    return metadata.isNameError = metadata.name.length > 30 ? true : false;
  }

  syncMetadata() {
    const jobRequest = new JobRequest();
    jobRequest.jobId = this.jobRequest.jobId;
    this.jobService.syncMetadata(jobRequest)
      .subscribe(response => {
        this.metadataDifference = JSON.parse(JSON.stringify(response)).results[0];
        this.selectedSourceCollection.forEach(element => {
          element.collectionMetadataResponseList.forEach(metadata => {
            metadata.isNewColumn = this.metadataDifference.viewJobDifferenceDetailsResponseList.filter(n => n.fieldName === metadata.name && n.status === 'New').length > 0;
            metadata.isDeletedColumn = this.metadataDifference.viewJobDifferenceDetailsResponseList.filter(n => n.fieldName === metadata.name && n.status === 'Delete').length > 0;
          });
        });
        if (this.metadataDifference.viewJobDifferenceDetailsResponseList.filter(n => n.status === 'New').length > 0) {
          this.isSyncAble = true;
        } else {
          this.toastr.success('No change found in metadata');
        }
      }, error => {
        console.log(error);
      });
  }

  getUpdatedTableMetadata() {
    this.isProcessing = true;
    this.jobService.getUpdatedTableMetadata(this.jobRequest.jobId)
      .subscribe(response => {
        const list = JSON.parse(JSON.stringify(response)).results;
        this.selectedDestinationCollection = new Array<Collection>();
        list.forEach(element => {
          this.selectedDestinationCollection.push({
            collectionName: element.collectionName,
            collectionNameToggle: element.collectionNameToggle || false,
            isNameError: element.isNameError || false,
            collectionMetadataResponseList: element.collectionMetadataResponseList.map(n => new Metadata(n)),
            toggleName: false,
            toggleChildObject: false
          });
        });

        this.selectedDestinationCollection.forEach(n => {
          n.collectionMetadataResponseList.forEach(j => {
            j.isNewColumn = this.selectedSourceCollection.filter(s => s.collectionName.toLocaleLowerCase() === n.collectionName.toLocaleLowerCase())
            [0].collectionMetadataResponseList.filter(k => k.name.toLocaleLowerCase() === j.name.toLocaleLowerCase() && k.isNewColumn === true).length > 0;
          });
        });
        this.toastr.success('Target metadata synced');
        this.isSyncAble = false;
        this.isProcessing = false;
        this.isNewSyncMetadataUpdated = true;
      }, error => {
        console.log(error);
      });
  }

  alterTableMetadata() {
    if (this.selectedDestinationCollection[0].collectionMetadataResponseList.filter(n => n.isNewColumn === true).length === 0) {
      return false;
    }
    this.isProcessing = true;
    const table = {};
    table['tableName'] = this.selectedDestinationCollection[0].collectionName;
    table['tableMetadataList'] = [];
    this.selectedDestinationCollection[0].collectionMetadataResponseList.filter(n => n.isNewColumn === true).forEach(element => {
      const obj = new Object();
      obj['name'] = element.name;
      obj['type'] = element.type;
      table['tableMetadataList'].push(obj);
    });
    this.jobService.saveJobsMetadataDifference(this.destinationConnectionValue.connectionId, table)
      .subscribe(response => {
        this.isProcessing = false;
        this.isNewSyncMetadataUpdated = true;
        this.updateTableMetadata();
      }, error => {
        this.isProcessing = false;
      });
  }

  updateTableMetadata() {
    if (this.selectedDestinationCollection[0].collectionMetadataResponseList.filter(n => n.isNewColumn === true).length === 0) {
      return false;
    }
    this.isProcessing = true;
    const job = {};
    job['jobId'] = this.jobRequest.jobId;
    const object = {};
    this.selectedDestinationCollection[0].collectionMetadataResponseList.filter(n => n.isNewColumn === true).forEach(element => {
      object[element.name] = `${this.selectedSourceCollection[0].collectionName}.${this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === element.name.toLocaleLowerCase())[0].name}`;
    });
    job['newMapping'] = JSON.stringify(object);
    this.jobService.updateTableMetadata(this.jobRequest.jobId, job)
      .subscribe(response => {
        this.isProcessing = false;
      }, error => {
        this.isProcessing = false;
      });
  }
  editColumnName(metadata: Metadata) {
    const mapping = this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === metadata.name.toLocaleLowerCase()).length > 0 ? `${this.selectedSourceCollection[0].collectionName}.${this.selectedSourceCollection[0].collectionMetadataResponseList.filter(n => n.name.toLocaleLowerCase() === metadata.name.toLocaleLowerCase())[0].name}` : `${this.selectedSourceCollection[0].collectionName}.${metadata.name}`;
    console.log(metadata.name.length);
    // this.editColumn = metadata.name.length;
  }
  onTabSelect(e) {
    switch (e.title) {
      case 'Data Previewer':
        // this.getDataForPreview();
        break;
      case 'View Log':
        this.getLogView();
        break;
      default:
        break;
    }
  }

  @HostListener('document:click', ['$event'])
  documentClick(event: any): void {
    if (event.srcElement.nodeName !== 'svg' && event.srcElement.className.baseVal !== 'icon_connection') {
      this.isDestinationConnectionOption = false;
      this.isSourceConnectionOption = false;
      // this.isSchedulerHide = false;
    } else {

    }
  }

  ngOnDestroy() {
    clearInterval(this.intervalObj);
    this.introJS.exit();
  }

}