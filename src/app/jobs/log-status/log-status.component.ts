import { Component, OnInit, Input } from '@angular/core';
import { LogStatus } from '../../model/logstatus';
import { JobService } from '../job.service';
import { Job } from '../../model/job';

import {
  GridComponent,
  GridDataResult,
  DataStateChangeEvent,
  PageChangeEvent
} from '@progress/kendo-angular-grid';
import { Log } from 'src/app/model/log';
@Component({
  selector: 'app-log-status',
  templateUrl: './log-status.component.html',
  styleUrls: ['./log-status.component.css']
})
export class LogStatusComponent implements OnInit {

  public logStatus: Array<LogStatus> = new Array<LogStatus>();
  id: string;
  @Input()
  jobStatus: Log;
  constructor(private jobService: JobService) { }
  public gridData: GridDataResult;

  ngOnInit() {
    this.jobStatusId();
  }

  jobStatusId() {

    this.jobService.getJobLogStatus(this.jobStatus.jobId, this.jobStatus.jobStatusId).subscribe(response => {
      const res = JSON.parse(JSON.stringify(response)).results;
      res.forEach(element => {
        this.logStatus.push({ skipRecordCount: element.skipRecordCount, message: element.message });

      });

    });


  }

}
