import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Config } from '../../environments/config';

@Injectable()
export class BulkService {
    baseUrl = Config.getEnvironmentVariable('apiUrl');
    createBulkJobsEndPoint = this.baseUrl + 'jobs/bulkJobs';
    constructor(private http: HttpClient) { }
    createBulkJobs(bulkJob: any) {
        return this.http.post(this.createBulkJobsEndPoint, bulkJob);
    }
}


