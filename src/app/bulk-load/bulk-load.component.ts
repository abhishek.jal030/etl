import { Component, OnInit } from '@angular/core';
import { CronOptions } from 'cron-editor';
import { BulkService } from './bulk.service';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { ConnectionService } from '../connections/connection-service';
import { JobService } from '../jobs/job.service';
import { CookieService } from 'ngx-cookie-service';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { Router, ActivatedRoute } from '@angular/router';

import { LocaleConfig } from 'ngx-daterangepicker-material';
import * as introJs from 'intro.js/intro.js';

export class Connection {
  connectionId: string;
  type: string;
  name: string;
  aliasName: string;
  createdDate: string;
  updatedDate: string;
  createdBy: string;
  updatedBy: string;
  toggle: boolean;

  constructor(connection?) {
    connection = connection || {};
    this.connectionId = connection.connectionId || '';
    this.type = connection.type || '';
    this.name = connection.name || '';
    this.aliasName = connection.aliasName || '';
    this.createdDate = connection.createdDate || '';
    this.updatedDate = connection.updatedDate || '';
    this.createdBy = connection.createdBy || '';
    this.updatedBy = connection.updatedBy || '';
    this.toggle = connection.toggle || false;
  }
}

export class BulkJob {
  collectionNames: string;
  createdBy: string;
  destinationConnection: string;
  destinationConnectionName: string;
  id: string;
  name: string;
  scheduleDateTime: string;
  prefix: string;
  sourceConnection: string;
  sourceConnectionName: string;
  updatedBy: string;
  constructor(bulkJob?) {
    bulkJob = BulkJob || {};
    this.collectionNames = bulkJob.collectionNames || '';
    this.createdBy = bulkJob.createdBy || '';
    this.destinationConnection = bulkJob.destinationConnection || '';
    this.destinationConnectionName = bulkJob.destinationConnectionName || '';
    this.id = bulkJob.id || '';
    this.name = bulkJob.name || '';
    this.scheduleDateTime = bulkJob.scheduleDateTime || '';
    this.prefix = bulkJob.prefix || '';
    this.sourceConnection = bulkJob.sourceConnection || '';
    this.sourceConnectionName = bulkJob.sourceConnectionName || '';
    this.updatedBy = bulkJob.updatedBy || '';
  }
}
@Component({
  selector: 'app-bulk-load',
  templateUrl: './bulk-load.component.html',
  styleUrls: ['./bulk-load.component.scss']
})
export class BulkLoadComponent implements OnInit {
  cronExpression = '4 3 2 12 1/1 ? *';
  cronOptions: CronOptions = {
    formInputClass: 'form-control cron-editor-input',
    formSelectClass: 'form-control cron-editor-select',
    formRadioClass: 'cron-editor-radio',
    formCheckboxClass: 'cron-editor-checkbox',

    defaultTime: '10:00:00',
    use24HourTime: true,

    hideMinutesTab: false,
    hideHourlyTab: false,
    hideDailyTab: false,
    hideWeeklyTab: false,
    hideMonthlyTab: false,
    hideYearlyTab: false,
    hideAdvancedTab: true,

    hideSeconds: false,
    removeSeconds: false,
    removeYears: false
  };
  sourceConnections: Array<Connection>;
  bulkJob: BulkJob;
  destinationConnections: Array<Connection>;
  connectionsList: Array<Connection>;
  sourceConnectionValue: Connection;
  destinationConnectionValue: Connection;
  sourceCollection: Array<{ text: string, value: string }>;
  sourceCollectionList: Array<{ text: string, value: string }>;
  sourceCollectionValue: { text: string, value: string };
  isSourceConnectionConnected: boolean;
  isSourceCollectionSelected: boolean;
  alwaysShowCalendars: boolean;
  selected: any;
  locale: LocaleConfig = {
    applyLabel: 'Appliquer',
    customRangeLabel: ' - ',
    daysOfWeek: moment.weekdaysMin(),
    monthNames: moment.monthsShort(),
    firstDay: moment.localeData().firstDayOfWeek(),
  };
  todo = [
    'Get to work',
    'Pick up groceries',
    'Go home',
    'Fall asleep'
  ];

  done = [
  ];
  selectedEffectiveDate: { start: moment.Moment, end: moment.Moment };
  selectedSubmittedDate: { start: moment.Moment, end: moment.Moment };
  startDate: string;
  endDate: string;
  datetime: string;
  introJS = introJs();
  constructor(
    private connectionService: ConnectionService,
    private jobService: JobService,
    private router: Router,
    private route: ActivatedRoute,
    private cookieService: CookieService,
    private bulkService: BulkService,
    private toastr: ToastrService
  ) {
    this.alwaysShowCalendars = true;
    this.sourceConnections = Array<Connection>();
    this.destinationConnections = Array<Connection>();
    this.bulkJob = new BulkJob();
    this.sourceCollection = new Array<{ text: string, value: string }>();
    if (this.cookieService.get('intro') === 'newbulkjob') {
      this.introJS.setOptions({
        steps: [
       
          {
            element: '#bulkJobName',
            intro: 'Write Bulk Job Name'
          },
          {
            element: '#sourceConnection',
            intro: ' Select Source Connection',
            position: 'left'
          },
          {
            element: '#dragDrop',
            intro: ' Drag and drop the Collection Here'
          },
          {
            element: '#destinationCollection',
            intro: '  Select Destination Connection',
            position: 'left'
          },
          {
            element: '#prefix',
            intro: '  Write the Prefix'
          },
          {
            element: '#saveBtn',
            intro: '  Press save job icon '
          },
         
        ]
      });

    }
    if (this.cookieService.get('intro') === 'newbulkjobWithSchedular') {
      this.introJS.setOptions({
        steps: [
       
          {
            element: '#bulkJobName',
            intro: 'Write Bulk Job Name'
          },
          {
            element: '#sourceConnection',
            intro: ' Select Source Connection',
            position: 'left'
          },
          {
            element: '#dragDrop',
            intro: ' Drag and drop the Collection Here'
          },
          {
            element: '#destinationCollection',
            intro: '  Select Destination Connection',
            position: 'left'
          },
          {
            element: '#prefix',
            intro: '  Write the Prefix'
          },
          {
            element: '#schedular',
            intro: '  Select Schedular',
            position: 'top'
          },
          {
            element: '#saveBtn',
            intro: '  Press save job icon '
          },
         
        ]
      });

    }
  }

  ngOnInit() {
    this.getAllConnection();
    if (this.route.snapshot.queryParams.action === 'intro') {
      this.introJS.start();
      this.introJS.onchange((targetElement) => {
      });
      this.introJS.oncomplete(() => {
        this.router.navigate(['bulk-load/new'], { queryParams: {} });
      });
      this.introJS.onexit(() => {
        this.router.navigate(['bulk-load/new'], { queryParams: {} });
      });
    }
  }

  getAllConnection() {
    this.connectionService.getAllConnection(new Connection(), 0, 10, 'createdDate', 'desc').subscribe(data => {
      this.sourceConnections = Array<Connection>();
      this.destinationConnections = Array<Connection>();
      if (JSON.parse(JSON.stringify(data)).results[0].connectionResponseList !== null) {
        JSON.parse(JSON.stringify(data)).results[0].connectionResponseList.forEach(element => {
          this.sourceConnections.push(new Connection(element));
          this.destinationConnections.push(new Connection(element));
        });
        const sourceObj = new Connection();
        sourceObj.name = 'Select Connection';
        this.sourceConnections.splice(0, 0, sourceObj);
        this.connectionsList = this.sourceConnections;
        this.sourceConnectionValue = sourceObj;
        const targetObj = new Connection();
        targetObj.name = 'Select Connection';
        targetObj.name = 'Select Target Connection';
        this.destinationConnectionValue = targetObj;
        // this.route.paramMap.subscribe(params => {
        //   if (params.get('jobId') !== 'new') {
        //     this.isProcessing = false;
        //     this.getJob(params.get('jobId'));
        //   }
        // });
      }
    });
  }

  filterSourceConnection(value) {
    this.sourceConnections = this.connectionsList.filter(n => n.name.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  selectionChangeSourceConnection(event) {
    this.populateSourceCollection(event.connectionId);
    this.sourceConnectionValue = event;
    if (this.route.snapshot.queryParams.action === 'intro' &&this.cookieService.get('intro') === 'newbulkjob')  {
      this.introJS.goToStepNumber(2).start();
    }
    if (this.route.snapshot.queryParams.action === 'intro' &&this.cookieService.get('intro') === 'newbulkjobWithSchedular')  {
      this.introJS.goToStepNumber(2).start();
    }
  }
  selectionChangeDestinationConnection(event) {
   
    if (this.route.snapshot.queryParams.action === 'intro' &&this.cookieService.get('intro') === 'newbulkjob')  {
      this.introJS.goToStepNumber(4).start();
    }
    if (this.route.snapshot.queryParams.action === 'intro' &&this.cookieService.get('intro') === 'newbulkjobWithSchedular')  {
      this.introJS.goToStepNumber(4).start();
    }
  }

  populateSourceCollection(connectionId) {
    // this.isProcessing = true;
    this.jobService.getAllEntities(connectionId)
      .subscribe(response => {
        // this.isProcessing = false;
        const list = JSON.parse(JSON.stringify(response)).results;
        list.forEach(element => {
          this.sourceCollection.push({ text: `${element.collectionName} (${element.collectionCount})`, value: element.collectionName });
        });
        this.sourceCollectionList = this.sourceCollection;
        const defaultvalue = { text: 'Select Collection', value: '0' };
        // this.sourceCollection.splice(0, 0, defaultvalue);

        this.sourceCollectionValue = defaultvalue;
        // this.isSourceConnectionConnected = true;
        // this.isSourceCollectionSelected = false;
        // this.route.paramMap.subscribe(params => {
        //   console.log(params);
        //   if (params.get('jobId') !== 'new') {
        //     this.sourceCollectionValue = this.sourceCollection.filter(n => n.value === this.jobDetail.sourceCollection)[0];
        //     this.populateSourceMetadata(this.sourceConnectionValue.connectionId, this.jobDetail.sourceCollection);
        //   }
        // });
      }, error => {
        // this.isProcessing = false;
      });
  }
  getSubmittedDateTimeRange(event: any) {
    this.datetime = moment(event['startDate']._d).format('MM/DD/YYYY HH:mm:ss');
  }
  saveBulkJob() {
    this.bulkJob.collectionNames = this.done.map(n => n.value).join(',');
    this.bulkJob.sourceConnection = this.sourceConnectionValue.connectionId;
    this.bulkJob.sourceConnectionName = this.sourceConnectionValue.name;
    this.bulkJob.destinationConnection = this.destinationConnectionValue.connectionId;
    this.bulkJob.destinationConnectionName = this.destinationConnectionValue.name;
    this.bulkJob.scheduleDateTime = this.datetime;
    console.log(this.datetime);
    this.bulkService.createBulkJobs(this.bulkJob).subscribe(data => {
      const list = JSON.parse(JSON.stringify(data)).results;
      const message = JSON.parse(JSON.stringify(data)).message;
      this.toastr.info(message);
      console.log(message);
      console.log(list);

    });
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'newbulkjob')  {
      this.introJS.exit();
    }
    if (this.route.snapshot.queryParams.action === 'intro' && this.cookieService.get('intro') === 'newbulkjobWithSchedular')  {
      this.introJS.exit();
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  isInvalidDate(date) {
    return date.weekday() === 0;
  }
  isCustomDate(date) {
    return (
      date.weekday() === 0 ||
      date.weekday() === 6
    ) ? 'mycustomdate' : false;
  }
}
