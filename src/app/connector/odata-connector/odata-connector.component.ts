import { Component, OnInit, Input, Output } from '@angular/core';
import { Connector } from 'src/app/model/connector';
import { EventEmitter } from '@angular/core';
import { ConnectionService } from 'src/app/connections/connection-service';
import { Connection } from 'src/app/model/connection';
import { DataService } from 'src/app/shared/data.service';
import { ToastrService } from 'ngx-toastr';
import { JobService } from 'src/app/jobs/job.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import * as introJs from 'intro.js/intro.js';

@Component({
  selector: 'app-odata-connector',
  templateUrl: './odata-connector.component.html',
  styleUrls: ['./odata-connector.component.scss']
})
export class ODataConnectorComponent implements OnInit {
  @Input()
  connectionFields: Connection;
  @Input()
  connectorField: Connector;
  @Output() public saveConnection = new EventEmitter();
  connection: any;
  isShow: boolean;
  isProcessing: boolean;
  processingText: string;
  introJS = introJs();
  constructor(
    private connectionService: ConnectionService,
    private jobService: JobService,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private dataService: DataService
  ) {
    this.isProcessing = false;
    this.processingText = '';
    this.connectionFields = new Connection();
    this.connectorField = new Connector();
    this.dataService.getConnectionDetail().subscribe(connection => {
      if (connection.connectionId !== '') {
        this.connectionFields = connection;
        this.dataService.setConnector(this.connectionFields.type);
        const parameters = Object.keys(this.connectionFields.connectionConfigRequestList);
        parameters.forEach(element => {
          this.connectorField[element] = this.connectionFields.connectionConfigRequestList[element];
        });
      }
    });
    this.isShow = false;
    this.introJS.setOptions({
      steps: [
        {
          element: '#connectionName',
          intro: 'Write Connection Name '
        },
        {
          element: '#aliasName',
          intro: 'Write alias name (optional)',
          position: 'left'
        },
        {
          element: '#connectionUrl',
          intro: 'Write Connection Url',
          position: 'left'
        },
        {
          element: '#userName',
          intro: 'Write User Name',
          position: 'left'
        },
        {
          element: '#password',
          intro: 'Write Password',
          position: 'left'
        },
        {
          element: '#testConnection',
          intro: 'Press Test Connection'
        },
        {
          element: '#saveBtn',
          intro: 'Press Save Connection'
        },
        {
          element: '#closeBtn',
          intro: ' Press Close button'
        },
      ]
    })


  }

  ngOnInit() {
    if (this.route.snapshot.queryParams.action === 'intro') {
      this.introJS.start();
      this.introJS.onchange((targetElement) => {
      });
      this.introJS.oncomplete(() => {
        this.router.navigate(['/connections'], { queryParams: {} });
      });
      this.introJS.onexit(() => {
       // this.router.navigate(['/connections'], { queryParams: {} });
      });
    }
  }

  testConnection() {
    this.isProcessing = true;
    this.processingText = 'Validating...';
    this.dataService.getConnector.subscribe(connector => this.connectorField['connectionType'] = connector);
    this.connectionService.testConnection(this.connectorField).subscribe(response => {
      const res = JSON.parse(JSON.stringify(response));
      if (res.results[0].status) {
        this.toastr.info(res.results[0].message);
      } else {
        this.toastr.error(res.results[0].message);

      }
      if (this.route.snapshot.queryParams.action === 'intro') {
        this.introJS.goToStepNumber(6).start();
      }
      this.isProcessing = false;
      this.processingText = '';
    }, error => {
      this.isProcessing = false;
      this.processingText = '';
    });
  }

  onSaveConnection() {
    this.isProcessing = true;
    this.processingText = 'Validating...';
    this.dataService.getConnector.subscribe(connector => this.connectorField['connectionType'] = connector);
    this.connectionService.testConnection(this.connectorField).subscribe(response => {
      const res = JSON.parse(JSON.stringify(response));
      if (res.results[0].status=== false) {
        this.connectionFields.name = this.connectorField.connectionName;
        this.connectionFields.aliasName = this.connectorField.aliasName;
        this.dataService.getConnector.subscribe(connector => this.connectionFields.type = connector);
        const parameters = Object.keys(this.connectorField);
        this.connectionFields.connectionConfigRequestList = [];
        parameters.forEach(element => {
          this.connectionFields.connectionConfigRequestList.push({ key: element, value: this.connectorField[element] });
        });
        this.isProcessing = true;
        this.processingText = 'Processing...';
        if (this.connectionFields.connectionId === undefined) {
          this.connectionService.add(this.connectionFields).subscribe(response => {
            const res = JSON.parse(JSON.stringify(response));
            if (res.sucess === false) {
              this.saveConnection.emit('2');
            }
            this.saveConnection.emit('2');
            this.connectionFields = new Connection();
            this.connectorField = new Connector();
            this.dataService.setConnectionDetail(new Connection());
            this.dataService.setConnector('0');
            this.isProcessing = false;
            this.processingText = '';
            this.jobService.refreshOdataCollectionMetadata(res.results[0].connectionId)
              .subscribe(res1 => {
                console.log(res1);
              }, error => {
                console.log(error);
              });
          }, error => {
            this.isProcessing = false;
            this.processingText = '';
          });
        } else {
          this.connectionService.update(this.connectionFields.connectionId, this.connectionFields).subscribe(response => {
            const res = JSON.parse(JSON.stringify(response));
            if (res.sucess === false) {
              this.saveConnection.emit('2');
            }
            this.saveConnection.emit('2');
            this.connectionFields = new Connection();
            this.connectorField = new Connector();
            this.dataService.setConnectionDetail(new Connection());
            this.dataService.setConnector('0');
            this.isProcessing = false;
            this.processingText = '';
            this.jobService.refreshOdataCollectionMetadata(res.results[0].connectionId)
              .subscribe(res1 => {
                console.log(res1);
              }, error => {
                console.log(error);
              });
          }, error => {
            this.isProcessing = false;
            this.processingText = '';
          });
        }
      } else {
        this.toastr.error(res.results[0].message);
      }
      this.isProcessing = false;
      this.processingText = '';
    }, error => {
      this.isProcessing = false;
      this.processingText = '';
    });
    if (this.route.snapshot.queryParams.action === 'intro') {
      this.introJS.goToStepNumber(7).start();
    }
  }

  closeIt() {
    this.dataService.setConnectionDetail(new Connection());
    this.dataService.setConnector('0');
    this.saveConnection.emit('2');
    if (this.route.snapshot.queryParams.action === 'intro') {
      this.introJS.exit();
    }
  }
  ngOnDestroy() {

    this.introJS.exit();
  }

}
