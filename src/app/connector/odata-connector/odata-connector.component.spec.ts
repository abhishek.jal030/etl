import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ODataConnectorComponent } from './odata-connector.component';

describe('ODataConnectorComponent', () => {
  let component: ODataConnectorComponent;
  let fixture: ComponentFixture<ODataConnectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ODataConnectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ODataConnectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
