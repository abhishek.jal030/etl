import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ConnectionService } from '../../connections/connection-service';
import { ToastrService } from 'ngx-toastr';
import { DataService } from '../../shared/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Connection } from '../../model/connection';
import { Connector } from '../../model/connector';

@Component({
  selector: 'app-api-connector',
  templateUrl: './api-connector.component.html',
  styleUrls: ['./api-connector.component.css']
})
export class ApiConnectorComponent implements OnInit {
  @Input()
  connectionFields: Connection;
  @Input()
  connectorField: Connector;
  @Output() public saveConnection = new EventEmitter();

  connectorType: string;
  connection: any;
  isProcessing: boolean;
  isShow: boolean;
  processingText: string;
  constructor(
    private connectionService: ConnectionService,
    private toastr: ToastrService,
    private dataService: DataService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.isProcessing = false;
    this.processingText = '';
    this.connectionFields = new Connection();
    this.connectorField = new Connector();
    this.dataService.getConnector.subscribe(connector => this.connectorField['connectionType'] = connector);
    this.dataService.getConnectionDetail().subscribe(connection => {
      if (connection.connectionId !== '') {
        this.connectionFields = connection;
        this.dataService.setConnector(this.connectionFields.type);
        const parameters = Object.keys(this.connectionFields.connectionConfigRequestList);
        parameters.forEach(element => {
          this.connectorField[element] = this.connectionFields.connectionConfigRequestList[element];
        });
      }
    });
    this.isShow = false;
    this.dataService.invokeEvent.subscribe(value => {
      if (value === 'connectorVal') {
        this.dataService.getConnector.subscribe(connector => this.connectorField['connectionType'] = connector);
      }
    });
  }

  ngOnInit() {
  }

  testConnection() {
    this.isProcessing = true;
    this.processingText = 'Validating...';
    this.dataService.getConnector.subscribe(connector =>
      this.connectorField['connectionType'] = connector);

    this.connectionService.testConnection(this.connectorField).subscribe(response => {
      const res = JSON.parse(JSON.stringify(response));
      if (res.results[0].status) {
        this.toastr.info(res.results[0].message);
      } else {
        this.toastr.error(res.results[0].message);

      }
      this.isProcessing = false;
      this.processingText = '';
    }, error => {
      this.isProcessing = false;
      this.processingText = '';
    });
  }

  onSaveConnection() {

    this.isProcessing = true;
    this.processingText = 'Validating...';
    // this.dataService.getConnector.subscribe(connector => this.connectorField['connectionType'] = connector);
    this.connectionService.testConnection(this.connectorField).subscribe(response => {
      const res = JSON.parse(JSON.stringify(response));
      if (res.results[0].status) {
        this.connectionFields.name = this.connectorField.connectionName;
        this.connectionFields.aliasName = this.connectorField.aliasName;
        this.dataService.getConnector.subscribe(connector => this.connectionFields.type = connector);
        const parameters = Object.keys(this.connectorField);
        this.connectionFields.connectionConfigRequestList = [];
        parameters.forEach(element => {
          this.connectionFields.connectionConfigRequestList.push({ key: element, value: this.connectorField[element] });
        });
        this.isProcessing = true;
        this.processingText = 'Processing...';
        if (this.connectionFields.connectionId === undefined) {
          this.connectionService.add(this.connectionFields).subscribe(response => {
            const res = JSON.parse(JSON.stringify(response));
            if (res.sucess === false) {
              this.saveConnection.emit('2');
            }
            this.saveConnection.emit('2');
            this.connectionFields = new Connection();
            this.connectorField = new Connector();
            this.dataService.setConnectionDetail(new Connection());
            this.dataService.setConnector('0');
            this.isProcessing = false;
            this.processingText = '';
          }, error => {
            this.isProcessing = false;
            this.processingText = '';
          });
        } else {
          this.connectionService.update(this.connectionFields.connectionId, this.connectionFields).subscribe(response => {
            const res = JSON.parse(JSON.stringify(response));
            if (res.sucess === false) {
              this.saveConnection.emit('2');
            }
            this.saveConnection.emit('2');
            this.connectionFields = new Connection();
            this.connectorField = new Connector();
            this.dataService.setConnectionDetail(new Connection());
            this.dataService.setConnector('0');
            this.isProcessing = false;
            this.processingText = '';
          }, error => {
            this.isProcessing = false;
            this.processingText = '';
          });
        }
      } else {
        this.toastr.error(res.results[0].message);
      }
      this.isProcessing = false;
      this.processingText = '';
    }, error => {
      this.isProcessing = false;
      this.processingText = '';
    });
  }

  closeIt() {
    this.dataService.setConnectionDetail(new Connection());
    this.dataService.setConnector('0');
    this.saveConnection.emit('2');
  }
  ngOnDestroy() {

  }

}
