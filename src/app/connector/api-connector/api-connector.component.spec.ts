import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApiConnectorComponent } from './api-connector.component';

describe('ApiConnectorComponent', () => {
  let component: ApiConnectorComponent;
  let fixture: ComponentFixture<ApiConnectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApiConnectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApiConnectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
