import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RdbmsConnectorComponent } from './rdbms-connector.component';

describe('RdbmsConnectorComponent', () => {
  let component: RdbmsConnectorComponent;
  let fixture: ComponentFixture<RdbmsConnectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RdbmsConnectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RdbmsConnectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
