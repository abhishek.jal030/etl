import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { Config } from "../../environments/config";
import { map } from 'rxjs/operators';
import { User } from "../model/user";
import { Feature } from '../model/feature';
import { Role } from '../model/role';
import { Menu } from '../model/menu';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SessionService } from "../app.session-service";
//import { Observable} from 'rxjs';


@Injectable()
export class FeatureService {
    constructor(private _http: HttpClient, private _SessionService: SessionService) { }
    baseUrl = Config.getEnvironmentVariable('endPoint');
    AddFeatureEndpoint = this.baseUrl + environment.authApiUrl + "features";
    UpdateFeatureEndpoint = this.baseUrl + environment.authApiUrl + "features" + "/featureid";
    DeleteFeatureEndpoint = this.baseUrl + environment.authApiUrl + "features/"
    GetFeatureEndpoint = this.baseUrl + environment.authApiUrl + "features/";
    GetUserEndpoint = this.baseUrl + environment.authApiUrl + "features" + "/userid";
    GetAllFeatureEndpoint = this.baseUrl + environment.authApiUrl + "features";
    GetAllFeatureNodeEndpoint = this.baseUrl + environment.authApiUrl + "features/linkId";
    GetAllFeatureEndPoint = this.baseUrl + environment.authApiUrl + "features/";
    GetAllMenuEndPoint = this.baseUrl + environment.authApiUrl + "menu/";


    AddFeature(data): Observable<Feature> {
        return this._http.post<Feature>(this.AddFeatureEndpoint, data);
    }

    UpdateFeature(data): Observable<Feature> {
        return this._http.put<Feature>(this.UpdateFeatureEndpoint, data);
    }

    DeleteFeature(id: string): Observable<User> {
        return this._http.delete<User>(this.DeleteFeatureEndpoint + id)
    }

    GetFeature(featureID: string): Observable<Feature> {
        return this._http.get<Feature>(this.GetFeatureEndpoint + featureID);
    }

    GetUser(menuid: number): Observable<Menu> {
        return this._http.get<Menu>(this.GetUserEndpoint);
    }

    GetAllFeature(): Observable<Feature> {
        return this._http.get<Feature>(this.GetAllFeatureEndpoint);
    }

    GetAllFeatureNode(): Observable<Feature> {
        return this._http.get<Feature>(this.GetAllFeatureNodeEndpoint);
    }

    GetAllFeatureList(linkID: string) {
        return this._http.get(this.GetAllFeatureEndPoint + linkID);
    }

    GetAllMenu(linkID: string) {
        var uid = this._SessionService.getSessionValue("uid");
        if (this._SessionService.getCookie("LA_USER_DETAIL") != "") {
            var LA_SessionData = JSON.parse(this._SessionService.getCookie("LA_USER_DETAIL"));
            uid = LA_SessionData.userId;
        }
        return this._http.get(this.GetAllMenuEndPoint + linkID + '/' + uid);
    }

    GetAllMenuByLinkId(linkID: string) {
        return this._http.get(this.GetAllMenuEndPoint + linkID);
    }

}