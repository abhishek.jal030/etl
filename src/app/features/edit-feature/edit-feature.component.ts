import { Component, OnInit } from '@angular/core';
import {Feature} from '../../model/feature';
import {FeatureService} from '../features.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-feature',
  templateUrl: './edit-feature.component.html',
  styleUrls: ['./edit-feature.component.scss']
})
export class EditFeatureComponent implements OnInit {
  
  featureID: string="";
  featureModel: Feature = new Feature();
  constructor(private activatedRoute: ActivatedRoute, private FeatureService: FeatureService, private router: Router ) {
    this.featureID= this.activatedRoute.snapshot.paramMap.get("id");
   }

  ngOnInit() {
    this.FeatureService.GetFeature(this.featureID)
      .subscribe(response => {
        this.featureModel = JSON.parse(JSON.stringify(response)).results[0];
      }, error => function (error) {

      });
  }
  EditFeature(featureModel: Feature)
  {
  this.FeatureService.UpdateFeature(featureModel).subscribe(data => {
    this.router.navigate(['/features']);
    console.log(data);
  }, err => {
      console.error(err);
    }
  )
}
}
