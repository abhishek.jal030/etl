import { Component, OnInit } from '@angular/core';
import { Feature } from '../model/feature';
import { User } from '../model/user';
import { Role } from '../model/role';
import { Menu, UIPermission } from '../model/menu';
import { Http, HttpModule } from '@angular/http';
import { Injectable } from "@angular/core";
import { FeatureService } from './features.service';
import { CompileShallowModuleMetadata } from '@angular/compiler';
import { TreeviewItem, TreeviewConfig } from 'ngx-treeview';
import { Router } from '@angular/router';

@Component({
  selector: 'app-features',
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.scss']
})
export class FeaturesComponent implements OnInit {
  featureModel: Feature
  menuModel: Menu = new Menu();
  userFeature: Array<User>;
  selectedFeature: any;
  features: Array<Feature> = new Array<Feature>();
  featureList: Array<Feature> = new Array<Feature>();
  loading: boolean = false;
  items: TreeviewItem[];
  config = TreeviewConfig.create({
    hasAllCheckBox: false,
    hasFilter: true,
    hasCollapseExpand: false,
    decoupleChildFromParent: false,
    maxHeight: 400
  });
  
  constructor(private FeatureService: FeatureService, private router: Router) { }

  ngOnInit() {
    this.GetAllFeatureNode();
  }
  
  AddFeature() {
    this.FeatureService.AddFeature(this.featureModel).subscribe(data => {
      console.log(data);
    }, err => {
      console.error(err);
    })

  }
  UpdateFeature() {
    this.FeatureService.UpdateFeature(this.featureModel).subscribe(data => {
      console.log(data);
    }, err => {
      console.error(err);
    })

  }

  DeleteFeature(featureID: string) {
    this.FeatureService.DeleteFeature(featureID).subscribe(data => {
      this.selectedFeature();
    }, err => {
      console.error(err);
    })
  }

  GetFeature(featureID) {
    this.FeatureService.GetFeature(featureID).subscribe(data => {
      console.log(data);
    }, err => {
      console.error(err);
    })
  }

  GetUser(menuId) {
    this.FeatureService.GetUser(menuId).subscribe(data => {
      console.log(data);
    }, err => {
      console.error(err);
    })
  }

  GetAllFeatureNode() {
    this.loading = true;
    this.FeatureService.GetAllFeatureNode().subscribe(data => {
      this.features = JSON.parse(JSON.stringify(data)).results;
      this.selectedFeature = this.features[0];
      this.SelectApplication();
    }, err => {
      console.error(err);
    })
  }

  GetAllMenu(menuId) {
    this.FeatureService.GetUser(menuId).subscribe(data => {
      console.log(data);
    }, err => {
      console.error(err);
    })
  }

  SelectApplication() {
    this.loading = true;
    this.featureList = new Array<Feature>();
    this.FeatureService.GetAllFeatureList(this.selectedFeature.linkID)
      .subscribe(data => {
        this.loading = false;
        if (JSON.parse(JSON.stringify(data)).results != null &&
          JSON.parse(JSON.stringify(data)).results.length > 0) {
          this.featureList = JSON.parse(JSON.stringify(data)).results;
        }
        else {
        }
      }, err => { console.log(err) });
  }

  /**
   * Show menu tree based on selected feature
   */
  ViewMenu(menuMappingModal) {
    debugger;
    this.FeatureService.GetAllMenu(this.selectedFeature.linkID)
      .subscribe(data => {
        if (JSON.parse(JSON.stringify(data)).results != null &&
          JSON.parse(JSON.stringify(data)).results.length > 0) {
          var response = JSON.parse(JSON.stringify(data)).results;
          response[1].forEach(element => {
            var children = [];
            var submenu = response[2].filter(n => n.parentID = element.id);
            submenu.forEach(element => {
              children.push({ text: element.name, value: element.featureID, checked: false, disabled: false })
            });
            this.items = [];
            this.items.push(new TreeviewItem({
              text: element.name, value: element.featureID, checked: false, children: children, disabled: false
            }))
          });
          menuMappingModal.open();
        }
        else {
          this.items = null;
        }
      },
        err => {
          this.items = [];
        }
      )
  }
}





