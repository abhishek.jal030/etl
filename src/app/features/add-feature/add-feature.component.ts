import { Component, OnInit } from '@angular/core';
import {FeatureService} from '../features.service';
import {Feature} from '../../model/feature';
import { Http, HttpModule } from '@angular/http';
import { Injectable } from "@angular/core";
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-feature',
  templateUrl: './add-feature.component.html',
  styleUrls: ['./add-feature.component.scss']
})
export class AddFeatureComponent implements OnInit {
featureModel: Feature = new Feature();

  constructor(private FeatureService: FeatureService, private router: Router) { }

  ngOnInit() {
    
  }
  AddFeature(feature) {
    this.FeatureService.AddFeature(feature).subscribe(data => {
      this.router.navigate(['features']);
      console.log(data);
    },
      err => {
        console.error(err);
      }
    )

  }
}