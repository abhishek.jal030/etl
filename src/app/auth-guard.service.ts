import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { SessionService } from './app.session-service';
import { SharedService } from './shared/shared.service';

@Injectable()
export class AuthGuardService implements CanActivate {

  // you would usually put this in it's own service and not access it directly!
  // this is just for the sake of the demo.
  sessionData: any;
  constructor(private router: Router,
    private sessionService: SessionService,
    private sharedService: SharedService,
  ) { }


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    this.sessionData = this.sessionService.getCookie('LOGIN');
    if (this.sessionData === undefined || this.sessionData === '') {
      this.router.navigate(['dashboard']);
    }
   
  
    else if (window.location.pathname.indexOf('login') !== -1 || window.location.pathname.indexOf('/') !== -1) {
      this.sharedService.isFullScreen = false;
      return true;
    } else {
      this.router.navigate(['dashboard']);
      return false;
    }
    return true;

  }

}
