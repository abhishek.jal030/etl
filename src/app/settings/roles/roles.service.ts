import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';


import { Config } from '../../../environments/config';
import { Role } from '../../model/role';
import { SessionService } from '../../app.session-service';
import { FeatureId } from '../../model/feature';


@Injectable()
export class RolesService {
    baseUrl = Config.getEnvironmentVariable('endPoint');
    constructor(private http: HttpClient, private sessionService: SessionService) { }

    AddRolesEndpoint = this.baseUrl + 'roles';
    UpdateRoleEndpoint = this.baseUrl + 'roles/';
    DeleteRoleEndpoint = this.baseUrl + 'roles/';
    GetRoleEndpoint = this.baseUrl + 'roles/';
    GetAllRoleEndpoint = this.baseUrl + 'roles/';
    GetRoleFeaturesEndpoint = this.baseUrl + 'roles' + '/roleid' + '/features';


    AddRole(data: Role): Observable<Role> {
        data.createdBy = this.sessionService.getSessionValue('uid');
        data.updatedBy = this.sessionService.getSessionValue('uid');
        return this.http.post<Role>(this.AddRolesEndpoint, data);
    }

    UpdateRole(data: Role): Observable<Role> {
        data.updatedBy = this.sessionService.getSessionValue('uid');
        return this.http.put<Role>(this.UpdateRoleEndpoint, data);
    }

    DeleteRole(id: string): Observable<Role> {
        return this.http.delete<Role>(this.DeleteRoleEndpoint + id);
    }

    GetRole(userid: string): Observable<Role> {
        return this.http.get<Role>(this.GetRoleEndpoint + userid);
    }

    GetRoleFeatures(roleid: string, linkId: string): Observable<Role> {
        return this.http.get<Role>(this.baseUrl + `roles/${roleid}/features?linkId=` + linkId);
    }

    AssignRoleFeatured(roleId: string, data: FeatureId) {
        data.createdBy = this.sessionService.getSessionValue('uid');
        return this.http.post(this.baseUrl + `roles/${roleId}/features`, data);
    }

    RevokeRoleFeatured(roleId: string, featureId: string) {
        return this.http.delete(this.baseUrl + `roles/${roleId}/features/${featureId}`);
    }

    GetAllRoles(): Observable<Role[]> {
        return this.http.get<Role[]>(this.GetAllRoleEndpoint);
    }

}
