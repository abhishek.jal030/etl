import { Component, OnInit } from '@angular/core';

import { RolesService } from './roles.service';
// import { FeatureService } from '../features/features.service';
import { TreeviewItem, TreeviewConfig } from 'ngx-treeview';
import { Role } from '../../model/role';
import { Feature, FeatureId } from '../../model/feature';
import { Menu } from '../../model/menu';



@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {

  roleModel: Role = new Role();
  roles: Array<Role> = new Array<Role>();
  features: Array<Feature> = new Array<Feature>();
  featuresList: Array<Feature> = new Array<Feature>();
  selectedFeatures: Array<Feature> = new Array<Feature>();
  menu: Array<Menu> = new Array<Menu>();
  removeSelectedFeatures: any;
  selectedFeature: any;
  addNewFeature: any;
  items: TreeviewItem[];
  config = TreeviewConfig.create({
    hasAllCheckBox: true,
    hasFilter: true,
    hasCollapseExpand: true,
    decoupleChildFromParent: false,
    maxHeight: 400
  });
  selectRoleId: string;
  loading: boolean;

  constructor(private RoleService: RolesService, private roleService: RolesService) {
    this.loading = false;
  }

  ngOnInit() {
    this.loading = true;
    this.GetAllRoles();
  }

  /**
   * Add new role
   */
  OnSubmit(roleModal) {
    roleModal.close();
    if (this.roleModel.roleId == null) {
      this.RoleService.AddRole(this.roleModel).subscribe(data => {
        this.GetAllRoles();
      }, err => {
        console.error(err);
      });
    } else {
      this.RoleService.UpdateRole(this.roleModel).subscribe(data => {
        this.GetAllRoles();
      }, err => {
        console.error(err);
      });
    }
  }

  /**
   * Update existing role
   */
  UpdateRole() {

  }

  /**
   * Delete Role
   */
  DeleteRole(alertModal) {
    alertModal.close();
    if (this.selectRoleId !== '') {
      this.RoleService.DeleteRole(this.selectRoleId).subscribe(data => {
        this.GetAllRoles();
      }, err => {
        console.error(err);
      });
    }
  }

  /**
   * Get all role assigned featueres
   * @param roleID RoleId Parameter
   */
  GetRoleFeatures(roleID: string, linkId: string) {
    this.RoleService.GetRoleFeatures(roleID, linkId).subscribe(data => {
      if (JSON.parse(JSON.stringify(data)).results != null &&
        JSON.parse(JSON.stringify(data)).results.length > 0) {
        this.selectedFeatures = JSON.parse(JSON.stringify(data)).results;
      } else {
      }
      this.featuresList = [];
      // this.featureService.GetAllFeatureList(linkId)
      //   .subscribe(res => {
      //     this.loading = false;
      //     if (JSON.parse(JSON.stringify(res)).results != null &&
      //       JSON.parse(JSON.stringify(res)).results.length > 0) {
      //       this.featuresList = JSON.parse(JSON.stringify(res)).results;
      //       this.selectedFeatures.forEach(element => {
      //         const obj = this.featuresList.filter(n => n.featureId === element.featureId)[0];
      //         const index = this.featuresList.indexOf(obj);
      //         if (index > -1) {
      //           this.featuresList.splice(index, 1);
      //         }
      //       });
      //     } else {
      //     }
      //   }, err => {
      //     console.log(err);
      //   });
    },
      err => {
        console.error(err);
      }
    );
  }

  /**
   * Get all roles
   */
  GetAllRoles() {
    this.RoleService.GetAllRoles().subscribe(data => {
      this.loading = false;
      this.roles = JSON.parse(JSON.stringify(data)).results;
    }, err => {
      this.roles = new Array<Role>();
      this.loading = false;
    });
  }

  /**
   *
   * @param roleModal Modal role object
   * @param roleId RoleId Parameter
   */
  ShowRoleForm(roleModal, roleId?: string) {
    roleModal.open();
    this.roleModel = new Role();
    if (roleId != null) {
      this.GetRole(roleId);
    }
  }

  /**
   * Prompt Yes/No model to ask user to delete role.
   * @param alertModal Modal alert object
   * @param roleId RoleId Parameter
   */
  ShowDeleteModel(alertModal, roleId?: string) {
    alertModal.open();
    this.selectRoleId = roleId;
  }

  /**
   * Cancel to perform delete action
   * @param alertModal Model alert object
   */
  Cancel(alertModal) {
    alertModal.close();
    this.selectRoleId = '';
  }

  /**
   * Get role detail
   * @param roleId Role Id
   */
  GetRole(roleId: string) {
    this.RoleService.GetRole(roleId)
      .subscribe(response => {
        this.roleModel = JSON.parse(JSON.stringify(response)).results[0];
      }, err => {
        console.error(err);
      });
  }

  ShowDeleteModal(alertModal, roleId) {
    alertModal.open();
    this.selectRoleId = roleId;
  }

  // GetAllFeatureNode() {
  //   this.features = [];
  //   this.featureService.GetAllFeatureNode().subscribe(data => {
  //     this.features = JSON.parse(JSON.stringify(data)).results;
  //     this.selectedFeature = this.features[0];


  //     if (this.features.length > 0) {
  //       this.SelectApplication(this.selectedFeature);
  //     }
  //   }, err => {
  //     console.error(err);
  //   });
  // }

  SelectApplication(selectedFeature) {
    this.loading = true;
    this.featuresList = new Array<Feature>();
    this.selectedFeatures = [];
    this.GetRoleFeatures(this.selectRoleId, selectedFeature.linkId);
  }

  AddRole() {
    this.addNewFeature.forEach(e => {
      if (this.selectedFeatures.filter(n => n = e).length === 0) {
        const features = this.featuresList.filter(n => n.featureId === parseInt(e, 10));
        features.forEach(element => {
          this.selectedFeatures.push(element);
        });
      }
    });
    const featureIds: Array<number> = new Array<number>();
    this.selectedFeatures.forEach(element => {
      featureIds.push(element.featureId);
    });
    const req = new FeatureId();
    req.featureId = featureIds;
    this.roleService.AssignRoleFeatured(this.selectRoleId, req)
      .subscribe(response => {
        // this.GetAllFeatureNode();
        console.log(response);
      }, error => console.log(error));
  }

  RemoveRole() {
    this.removeSelectedFeatures.forEach(role => {
      const indexs = this.selectedFeatures.indexOf(this.selectedFeatures.filter(n => n.featureId === role)[0]);
      this.selectedFeatures.splice(indexs, 1);
    });
    const featureIds: Array<number> = new Array<number>();
    let index = 0;
    this.removeSelectedFeatures.forEach(element => {
      this.roleService.RevokeRoleFeatured(this.selectRoleId, element)
        .subscribe(response => {
          index++;
          // if (index === this.removeSelectedFeatures.length) {
          //   this.GetAllFeatureNode();
          // }
        }, error => console.log(error));
    });
  }

  ShowFeatureModel(featureModal, roleId) {
    this.selectRoleId = roleId;
    featureModal.open();
    // this.GetAllFeatureNode();
  }

  // GetAllUIPermissionByLinkId(LinkId: string, features: FeatureId) {
  //   const allFeature = features;
  //   this.featureService.GetAllMenuByLinkId(LinkId)
  //     .subscribe(data => { //
  //       const allLeveles = JSON.parse(JSON.stringify(data)).results;

  //       const lastLevel = allLeveles[allLeveles.length - 1];
  //       features.featureId.forEach(fid => {
  //         if (lastLevel.filter(n => n.id === fid).length > 0) { //
  //           const obj = lastLevel.filter(n => n.id === fid)[0];
  //           for (let index = allLeveles.length; index > 0; index--) {
  //             const nextLevel = allLeveles[index - 1];
  //             nextLevel.forEach(element => {
  //               if (element.id === obj.parentId) {
  //                 allFeature.featureId.push(obj.id);
  //               }
  //             });
  //           }
  //         }
  //       });
  //     }, error => { });
  //   return allFeature;
  // }

}
