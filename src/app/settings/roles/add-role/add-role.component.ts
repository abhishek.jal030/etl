import { Component, OnInit } from '@angular/core';
import { Http, HttpModule } from '@angular/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { RolesService } from '../roles.service';
import { Role } from '../../../model/role';
@Component({
  selector: 'app-add-role',
  templateUrl: './add-role.component.html',
  styleUrls: ['./add-role.component.scss']
})
export class AddRoleComponent implements OnInit {

  userRole: Role = new Role();
  constructor(private RoleService: RolesService, private router: Router) { }

  ngOnInit() {

  }

  AddRole(Roles) {
    this.RoleService.AddRole(Roles).subscribe(data => {
      this.router.navigate(['roles']);
      console.log(data);
    },
      err => {
        console.error(err);
      }
    );

  }
}
