import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RolesService } from '../roles.service';
import { AddRoleComponent } from './add-role.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { SessionService } from 'client/app/app.session-service';
describe('AddRoleComponent', () => {
  let component: AddRoleComponent;
  let fixture: ComponentFixture<AddRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [],
      declarations: [AddRoleComponent],
      imports: [HttpClientModule, RouterTestingModule, FormsModule],
      providers: [RolesService, SessionService, CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
