import { Component, OnInit } from '@angular/core';
import { RolesService } from '../roles.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Role } from '../../../model/role';

@Component({
  selector: 'app-edit-role',
  templateUrl: './edit-role.component.html',
  styleUrls: ['./edit-role.component.scss']
})
export class EditRoleComponent implements OnInit {

  roleID: string;
  userRole: Role = new Role();
  constructor(private activatedRoute: ActivatedRoute, private RoleService: RolesService, private router: Router) {
    this.roleID = this.activatedRoute.snapshot.paramMap.get('id');

  }

  ngOnInit() {
    this.RoleService.GetRole(this.roleID)
      .subscribe(response => {
        this.userRole = JSON.parse(JSON.stringify(response)).results[0];
      }, error => {

      });
  }

  EditRole(userRole: Role) {
    this.RoleService.UpdateRole(userRole).subscribe(data => {
      this.router.navigate(['/roles']);
      console.log(data);
    }, err => {
      console.error(err);
    }
    );
  }
}
