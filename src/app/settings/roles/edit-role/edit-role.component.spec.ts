import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RolesService } from '../roles.service';
import { EditRoleComponent } from './edit-role.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { SessionService } from 'client/app/app.session-service';
describe('EditRoleComponent', () => {
  let component: EditRoleComponent;
  let fixture: ComponentFixture<EditRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [],
      declarations: [EditRoleComponent],
      imports: [HttpClientModule, RouterTestingModule, FormsModule],
      providers: [RolesService, SessionService, CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
