import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { SessionService } from '../app.session-service';
import { CookieService } from 'ngx-cookie-service';
import { RolesComponent } from './roles.component';
import { RolesService } from './roles.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
describe('RolesComponent', () => {
  let component: RolesComponent;
  let fixture: ComponentFixture<RolesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [RolesComponent],
      imports: [HttpClientModule, FormsModule, RouterTestingModule],
      providers: [RolesService, CookieService, SessionService]

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
