import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { UserService } from '../users.service';
import { EditComponent } from './edit.component';
import { RolesService } from '../../roles/roles.service';
import { SessionService } from '../../app.session-service';
import { CookieService } from 'ngx-cookie-service';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
describe('EditComponent', () => {
  let component: EditComponent;
  let fixture: ComponentFixture<EditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [],
      declarations: [EditComponent],
      imports: [HttpClientModule, FormsModule, RouterTestingModule],
      providers: [UserService, RolesService, SessionService, CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
