import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../users.service';
import { element } from 'protractor';
import { takeUntil } from 'rxjs/operators';

import { RolesService } from '../../roles/roles.service';
import { Subject } from 'rxjs';
import { User } from '../../../model/user';
import { Role, RoleId } from '../../../model/role';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  userId: string;
  loading: boolean;
  userModel: User = new User();
  submitBtnText = 'Submit';
  roles: Array<Role> = new Array<Role>();
  selectedRoles: Array<Role> = new Array<Role>();
  removeSelectedRoles: any;
  selectedRole: any;
  private unsubscribeAll: Subject<any>;
  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private roleService: RolesService,
    private router: Router
  ) {
    this.userId = this.activatedRoute.snapshot.paramMap.get('id');
    this.unsubscribeAll = new Subject();
  }

  ngOnInit() {
    this.GetUserRoles(this.userId)
    this.userService.onUserResponseChanged
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(response => {
        this.userModel = JSON.parse(JSON.stringify(response)).results[0];
      });

    //  this.GetUserRoles(this.userId);
    // this.userService.GetUser(this.userId)
    //   .subscribe(response => {
    //     this.userModel = JSON.parse(JSON.stringify(response)).results[0];
    //   },
    //     error => {

    //     });
  }

  GetUserRoles(userid) {
    this.userService.GetUserRoles(userid).subscribe(data => {
      this.loading = false;
      this.GetAllRoles();
      const response = JSON.parse(JSON.stringify(data));
      if (response.success === true && response.results[0].roleDetailsResponse.length > 0) {
        this.selectedRoles = response.results[0].roleDetailsResponse;
      }
    }, err => {
      this.loading = false;
    });
  }

  GetAllRoles() {
    this.roleService.GetAllRoles().subscribe(data => {
      this.roles = JSON.parse(JSON.stringify(data)).results;
      this.selectedRoles.forEach(elemen => {
        const obj = this.roles.filter(n => n.roleId === elemen.roleId)[0];
        const index = this.roles.indexOf(obj);
        if (index > -1) {
          this.roles.splice(index, 1);
        }
      });
    }, err => {
      console.error(err);
    });
  }

  EditUser(userModel: User) {
    this.loading = true;
    this.submitBtnText = 'Proccessing...';
    this.userService.UpdateUser(userModel).subscribe(data => {
      this.router.navigate(['settings/users']);
      this.loading = false;
    }, err => {
      this.submitBtnText = 'Submit';
      this.loading = false;
    }
    );
  }

  AddRole() {
    this.selectedRole.forEach(roleId => {
      if (this.selectedRoles.filter(n => n.roleId === roleId).length === 0) {
        const roles = this.roles.filter(n => n.roleId === Number(roleId));
        roles.forEach(elemen => {
          this.selectedRoles.push(elemen);
        });
      }
    });
    const roleIds: Array<number> = new Array<number>();
    this.selectedRoles.forEach(eleme => {
      roleIds.push(eleme.roleId);
    });
    const req = new RoleId();
    req.roleId = roleIds;
    this.userService.AddUserRole(this.userId, req)
      .subscribe(response => {
        this.GetUserRoles(this.userId);
        console.log(response);
      }, error => console.log(error));
  }

  RemoveRole() {
    this.removeSelectedRoles.forEach(role => {
      const index = this.selectedRoles.indexOf(this.selectedRoles.filter(n => n.roleId === role)[0]);
      this.selectedRoles.splice(index, 1);
    });
    const featureIds: Array<number> = new Array<number>();
    this.removeSelectedRoles.forEach(elemen => {
      this.userService.RevokeUserRole(this.userId, elemen)
        .subscribe(response => {
          this.GetUserRoles(this.userId);
        }, error => console.log(error));
    });
  }

  SelectedAll() {

  }

}
