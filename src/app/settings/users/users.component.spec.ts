import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { UserService } from './users.service';
import { UsersComponent } from './users.component';
import { RolesService } from '../roles/roles.service';
import { ExportService } from '../shared/export.service';
import { ActivityService } from '../shared/activity.service';
import { SessionService } from '../app.session-service';
import { CookieService } from 'ngx-cookie-service';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { InputsModule } from '@progress/kendo-angular-inputs';
describe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [UsersComponent],
      imports: [HttpClientModule, InputsModule, FormsModule, RouterTestingModule, HttpClientModule],
      providers: [UserService, RolesService, ExportService, ActivityService, SessionService, CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
