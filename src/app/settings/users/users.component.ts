import { Component, OnInit } from '@angular/core';

import { UserService } from './users.service';


// import { FeatureService } from '../features/features.service';
import { TreeviewConfig, TreeviewItem } from 'ngx-treeview';

import { Router } from '@angular/router';
import 'rxjs/add/operator/map';

import { RolesService } from '../roles/roles.service';
import { User } from '../../model/user';
import { Item } from '../../model/item';
import { Role } from '../../model/role';
import { Feature } from '../../model/feature';
import { ActivityService } from '../../shared/activity.service';
import { SessionService } from '../../app.session-service';
import { ExportService } from '../../shared/export.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  userModel: User = new User();
  dropdownSettings = {};
  selectedRoles = [];
  selectedUserId: string;
  loading: boolean;
  isInternal: boolean;

  dropdownList: Array<Item> = new Array<Item>();
  userList: Array<User> = new Array<User>();
  roles: Array<Role> = new Array<Role>();
  features: Array<Feature> = new Array<Feature>();
  userRoleList: Array<Role> = new Array<Role>();
  userFeatureList: Array<Feature> = new Array<Feature>();
  dropdownEnabled = true;
  selectedFeature: any;
  items: TreeviewItem[];
  values: number[];
  public checked: boolean;

  config = TreeviewConfig.create({
    hasAllCheckBox: true,
    hasFilter: true,
    hasCollapseExpand: true,
    decoupleChildFromParent: false,
    maxHeight: 400
  });

  buttonClasses = [
    'btn-outline-primary',
    'btn-outline-secondary',
    'btn-outline-success',
    'btn-outline-danger',
    'btn-outline-warning',
    'btn-outline-info',
    'btn-outline-light',
    'btn-outline-dark'
  ];
  buttonClass = this.buttonClasses[0];
  dataExport: any = null;
  startFrm = 90;
  progress = 0;
  cancelExport: boolean;
  constructor(
    private userService: UserService,
    private activityService: ActivityService,
    private roleService: RolesService,
    // private featureService: FeatureService,

    private sessionService: SessionService,
    private router: Router,
    private exportService: ExportService
  ) {
    this.isInternal = true;
    this.checked = true;
    this.cancelExport = false;
  }


  ngOnInit() {
    this.loading = true;

    this.GetAllUser();
    // this.items = this.service.getBooks();
  }

  OnSubmit(userModal: any) {
    userModal.close();
    this.userService.AddUser(this.userModel).subscribe(data => {

      this.GetAllUser();
    }, err => {
      this.GetAllUser();
    });
    // this.activityJson = [];
    // this.allActivity = this.activityService.getActivityList();
    // this.activityId = this.allActivity.find(x => x.activityName === EnumType.Summary_Select_Partner).id;
    const activitylog = {
      activityId: 1,
      activityData: JSON.stringify('test')
    };

    this.activityService.setActivityLog(activitylog).subscribe(data => {
    }, error => {
      console.error(error);
    });
  }
  toggleChange(isExternal) {
    this.userModel.isInternal = isExternal ? 'External' : 'Internal';
  }

  ShowDeleteUserModal(alertModal, userId: string) {
    alertModal.open();
    this.selectedUserId = userId;
  }


  DeleteUser(alertModal) {
    alertModal.close();
    this.userService.DeleteUser(this.selectedUserId).subscribe(data => {
      this.GetAllUser();
    }, err => {
      console.error(err);
    });
  }

  GetAllUser() {
    this.userService.GetAllUser().subscribe(data => {
      this.userList = JSON.parse(JSON.stringify(data)).results;
      this.loading = false;
    },
      err => {
        console.error(err);
      });
  }

  GetAllRoles() {
    this.roleService.GetAllRoles().subscribe(data => {
      this.roles = JSON.parse(JSON.stringify(data)).results;
    }, err => {
      console.error(err);
    });
  }

  // GetAllFeatureNode() {
  //   this.featureService.GetAllFeatureNode().subscribe(data => {
  //     this.features = JSON.parse(JSON.stringify(data)).results;
  //     this.selectedFeature = this.features[0];
  //     this.SelectApplication(this.selectedFeature);
  //   }, err => {
  //     console.error(err);
  //   });
  // }

  // GetUser(userid) {
  //   this.userService.GetUser(userid).subscribe(data => {
  //     console.log(data);
  //   },
  //     err => {
  //       console.error(err);
  //     }
  //   );
  // }

  GetUserRoles(userid, userRoleModal) {
    this.GetAllRoles();
    this.loading = true;
    this.selectedUserId = userid;
    this.userRoleList = new Array<Role>();
    userRoleModal.open();
    this.userRoleList = [];
    this.userService.GetUserRoles(userid).subscribe(data => {
      this.loading = false;
      if (JSON.parse(JSON.stringify(data)).results[0].roleDetailsResponse.length > 0) {
        this.userRoleList = JSON.parse(JSON.stringify(data)).results[0].roleDetailsResponse;
      }
    }, err => {

    });
  }

  GetUserFeatures(userid, userFeatureModal) {
   // this.GetAllFeatureNode();
    this.selectedUserId = userid;
    userFeatureModal.open();
    this.userService.GetUserFeatures(userid).subscribe(data => {
      if (JSON.parse(JSON.stringify(data)).results != null &&
        JSON.parse(JSON.stringify(data)).results[0].featureDetailsResponse.length > 0) {
        this.userFeatureList = JSON.parse(JSON.stringify(data)).results[0].featureDetailsResponse;
      } else {
        this.userFeatureList = null;
      }
    },
      err => {
        this.userRoleList = [];
      }
    );
  }

  IsAssignedRole(role: Role) {
    if (this.userRoleList.length > 0) {
      return this.userRoleList.filter(n => n.roleId === role.roleId).length === 0 ? false : true;
    } else {
      return null;
    }
  }

  AssignRole(o: Role) {

  }

  RemoveRole() {

  }

  // SelectApplication(selectedFeature) {
  //   this.featureService.GetAllMenu(selectedFeature.linkID)
  //     .subscribe(data => {
  //       if (JSON.parse(JSON.stringify(data)).results != null &&
  //         JSON.parse(JSON.stringify(data)).results.length > 0) {
  //         const response = JSON.parse(JSON.stringify(data)).results;
  //         response[1].forEach(element => {
  //           const children = [];
  //           const submenu = response[2].filter(n => n.parentID = element.id);
  //           submenu.forEach(elemen => {
  //             children.push({ text: elemen.name, value: elemen.featureID, checked: false });
  //           });
  //           this.items = [];
  //           this.items.push(new TreeviewItem({
  //             text: element.name, value: element.featureID, checked: false, children: children
  //           }));
  //         });
  //       } else {
  //         this.userFeatureList = null;
  //       }
  //     },
  //       err => {
  //         this.userRoleList = [];
  //       }
  //     );
  // }

  Cancel(alertModal) {
    alertModal.close();
  }

  LoginAs(user: User) {
    this.sessionService.setCookie('LA_USER_DETAIL', JSON.stringify(user));
    this.sessionService.setCookie('ROLES', '');
    const domain = window.location.origin;
    window.location.href = domain;
  }

  GetRoles(userid) {
    this.userService.GetUserRoles(userid).subscribe(data => {
      if (JSON.parse(JSON.stringify(data)).success === true && JSON.parse(JSON.stringify(data)).results[0].roleDetailsResponse.length > 0) {
        this.roles = JSON.parse(JSON.stringify(data)).results[0].roleDetailsResponse;
        return this.roles;
      }
    }, err => {
    });
  }
  ExportData(): void {
    this.exportService.exportAsExcelFile(this.userList, 'user-data');
  }
}


