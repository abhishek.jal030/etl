import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { headersToString } from 'selenium-webdriver/http';
import { RequestOptions } from '@angular/http';
import { PlatformLocation, CommonModule } from '@angular/common';

import { BehaviorSubject } from 'rxjs';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SessionService } from '../../app.session-service';
import { Config } from '../../../environments/config';
import { User } from '../../model/user';
import { RoleId, Role } from '../../model/role';
import { FeatureId } from '../../model/feature';
@Injectable()
export class UserService {
    onUserResponseChanged: BehaviorSubject<any>;
    onRoleResponseChanged: BehaviorSubject<any>;

    routeParams: any;
    sessionData: any;
    LA_SessionData: any = "";


    constructor(
        private http: HttpClient,
        private sessionService: SessionService,
        private httpClient: HttpClient,
        private common: CommonModule,
    ) {
        this.onUserResponseChanged = new BehaviorSubject({});
        this.onRoleResponseChanged = new BehaviorSubject({});

    }
    baseUrl = Config.getEnvironmentVariable('endPoint');
    AddUserEndpoint = this.baseUrl + 'users';
    AddUserRoleEndpoint = this.baseUrl + 'users/';
    UpdateUserEndpoint = this.baseUrl + 'users/';
    DeleteUserEndpoint = this.baseUrl + 'users/';
    GetUserEndpoint = this.baseUrl + 'users/';
    SignInUserEndpoint = this.baseUrl + 'user/';
    UserAuditEndpoint = this.baseUrl + 'user/';
    GetUserFeaturesEndpoint = this.baseUrl + 'users' + '/userid' + '/features';
    GetAllUserEndpoint = this.baseUrl + 'users';

    AddUser(data: User) {
        data.createdBy = this.sessionService.getSessionValue('uid');
        return this.http.post<User>(this.AddUserEndpoint, data);
    }

    AddUserRole(userId: string, data: RoleId): Observable<Role> {
        data.createdBy = this.sessionService.getSessionValue('uid');
        return this.http.post<Role>(this.AddUserRoleEndpoint + `${userId}/roles`, data);
    }

    AddUserFeature(userId: string, data: FeatureId): Observable<Role> {
        data.createdBy = this.sessionService.getSessionValue('uid');
        return this.http.post<Role>(Config.getEnvironmentVariable('endPoint') + `users/${userId}/features`, data);
    }

    UpdateUser(data: User): Observable<User> {
        data.updatedBy = this.sessionService.getSessionValue('uid');
        return this.http.put<User>(this.UpdateUserEndpoint + data.userId, data);
    }

    DeleteUser(id: string): Observable<User> {
        return this.http.delete<User>(this.DeleteUserEndpoint + id);
    }

    GetUser(): Promise<any> {
        // return this.http.get<User>(this.GetUserEndpoint + userid);
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === '') {
                this.onUserResponseChanged.next(false);
                resolve(false);
            } else {
                // this.GetUserRoles(this.routeParams.id);
                return this.http.get(this.GetUserEndpoint + this.routeParams.id).subscribe((res: any) => {
                    const response = res;
                    this.onUserResponseChanged.next(response);
                    resolve(response);
                }, reject);
            }
        });
    }


    GetSignInUser(userid: string): Observable<User> {
        // this.sessionData = JSON.parse(this.sessionService.getCookie("LD_USER_DETAIL"));
        // let uid = this.sessionData.uid;
        // if (this.sessionService.getCookie("LA_USER_DETAIL") != "") {
        //     this.LA_SessionData = JSON.parse(this.sessionService.getCookie("LA_USER_DETAIL"));
        //     uid = this.LA_SessionData.userId;
        // }
        // return this.http.get<User>(this.SignInUserEndpoint + userid);
        return this.http.get<User>(this.SignInUserEndpoint + userid);

        
    }

    UserAudit(userid: string, statu: string) {
        const data = {
            status: statu,
            loginBy: this.sessionService.getSessionValue('uid')
        };
        return this.http.post(this.UserAuditEndpoint + userid, data);
    }

    GetUserRoles(userId): Observable<User> {

        return this.http.get<User>(Config.getEnvironmentVariable('endPoint') + `users/${userId}/roles`);
    //   return new Promise((resolve, reject) => {
    //     if (this.routeParams.id === '') {
    //         this.onRoleResponseChanged.next(false);
    //         resolve(false);
    //     } else {

    //         // this.GetUserRoles(this.routeParams.id);
    //         return this.http.get(Config.getEnvironmentVariable('endPoint') + `users/${this.routeParams.id}/roles`).subscribe((res: any) => {
    //             const response = res;
    //             this.onUserResponseChanged.next(response);
    //             resolve(response);
    //         }, reject);
    //     }
    // });
    }

    RevokeUserRole(userId: string, roleId: string) {
        return this.http.delete(Config.getEnvironmentVariable('endPoint') + `users/${userId}/roles/${roleId}`);
    }

    GetUserFeatures(userid: number): Observable<User> {
        return this.http.get<User>(this.GetUserFeaturesEndpoint);
    }

    GetAllUser(): Observable<User[]> {
        return this.http.get<User[]>(this.GetAllUserEndpoint);
    }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.GetUser()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
            // Promise.all([
            //     this.GetUserRoles()
            // ]).then(
            //     () => {
            //         resolve();
            //     },
            //     reject

            // );
        });
    }
    // getUserDetail(): Promise<any> {
    //     return new Promise((resolve, reject) => {
    //         if (this.routeParams.id === '') {
    //             this.onUserResponseChanged.next(false);
    //             resolve(false);
    //         } else {
    //             this.GetUser(this.routeParams.id).subscribe((res: any) => {
    //                 const response = res;
    //                 this.onUserResponseChanged.next(User);
    //                 resolve(response);
    //             }, reject);
    //         }
    //     });
    // }

}
