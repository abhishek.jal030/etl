import { Component, OnInit } from '@angular/core';
import { UsersComponent } from '../users.component';
import { Http, HttpModule } from '@angular/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../../../model/user';
import { UserService } from '../../../settings/users/users.service';
// import { RolesService } from '../../roles/roles.service';


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  userModel: User = new User();
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
  loading: boolean;
  submitBtnText = 'Submit';

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
  }

  AddUser(userModel: User) {
    this.loading = true;
    this.submitBtnText = 'Processing...';
    this.userService.AddUser(this.userModel).subscribe(data => {
      this.router.navigate(['settings/users']);
      this.loading = false;
    }, err => {
      this.submitBtnText = 'Submit';
      this.loading = false;
    });
  }

  Cancel() {
    this.router.navigate(['settings/users']);
  }

}


