import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { UserService } from '../users.service';
import { AddComponent } from './add.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { SessionService } from '../../app.session-service';
import { CookieService } from 'ngx-cookie-service';
describe('AddComponent', () => {
  let component: AddComponent;
  let fixture: ComponentFixture<AddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [],
      declarations: [AddComponent],
      imports: [HttpClientModule, RouterTestingModule, FormsModule],
      providers: [UserService, SessionService, CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
