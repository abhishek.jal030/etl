import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { UsersRoutingModule } from './users-routing.module';

import { ModalModule } from 'ngx-modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { RouterModule, Routes } from '@angular/router';
import { UserManagmentComponent } from './user-managment/user-managment.component';
import { UsersComponent } from './users/users.component';
import { EditComponent } from './users/edit/edit.component';
import { RolesComponent } from './roles/roles.component';
import { UserService } from './users/users.service';
import { AdminAuthGuardService } from '../app.admin-auth-guard.service';
import { UserAuthGuardService } from '../app.user-auth-guard.service';
// import { EditRoutingModule } from './edit-routing.module';

// import { UserManagementRoutingModule } from './user-management-routing.module';

// import { RolesRoutingModule } from './roles-routing.module';

const routes: Routes = [

    {
        path: 'settings',
        component: UserManagmentComponent,
        canActivate: [AdminAuthGuardService]
    },
    {
        path: 'settings/users',
        component: UsersComponent,
        canActivate: [AdminAuthGuardService]

    },
    {
        path: 'settings/users/edit/:id',
        component: EditComponent,
        resolve: {
            data: UserService
        }
    },
    {
        path: 'roles',
        component: RolesComponent,
        canActivate: [UserAuthGuardService]
    },


];

@NgModule({
    exports: [RouterModule],
    imports: [
        CommonModule,
        // UsersRoutingModule,
        ModalModule,
        FormsModule,
        InputsModule,
        RouterModule.forChild(routes)],
    declarations: [
        UsersComponent,
        UsersComponent,
        EditComponent,
        UserManagmentComponent,
        RolesComponent
    ]
})
export class SettingModule { }
