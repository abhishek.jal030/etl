import { Component, OnInit, Input } from '@angular/core';
declare const load: any;
import { CommentService } from './comment-service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css'],
  providers: [
    CommentService
  ]
})
export class CommentComponent implements OnInit {
  @Input()
  moduleId: string;
  @Input()
  entityId: string;
  @Input()
  entityValue: string;
  @Input()
  userId: string;
  comment: Comment = new Comment();
  allComments: Array<Comment> = new Array<Comment>();

  constructor(private commentService: CommentService) {

  }

  ngOnInit() {
    load();
    this.getAllComment();
  }

  getAllComment() {
    this.commentService.getAllComment(this.moduleId, this.entityId, this.entityValue)
      .subscribe(response => {
        this.allComments = JSON.parse(JSON.stringify(response)).results;
      }, error => {
        console.log(error);
      });
  }

  addComment() {
    this.comment.entityValue = this.entityValue;
    this.comment.moduleId = parseInt(this.moduleId, 0);
    this.comment.entityId = parseInt(this.entityId, 0);
    this.comment.userId = this.userId;
    this.commentService.addComment(this.comment).subscribe(data => {
      this.comment.notes = '';
      this.getAllComment();
    }, error => {
      console.log(error);
    });
  }
}

export class Comment {
  id: number;
  comment: string;
  createdBy: string;
  createdDate: string;
  toggle: boolean;
  entityId: number;
  entityValue: string;
  moduleId: number;
  notes: string;
  userId: string;
}
