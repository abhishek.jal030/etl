import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Config } from 'src/environments/config';
@Injectable({
  providedIn: 'root'
})
export class CommentService {
  baseUrl = Config.getEnvironmentVariable('apiUrl');
  getCommnetEndPoint = this.baseUrl + 'comments/';
  addCommentEndPoint = this.baseUrl + 'comments/';
  constructor(private http: HttpClient) { }
  getAllComment(moduleId: string, entityId: string, entityValue: string) {

    return this.http.get(this.getCommnetEndPoint + moduleId + '/' + entityId + '/' + entityValue);
  }

  addComment(req: any) {
    return this.http.post(this.addCommentEndPoint, req);
  }
}
