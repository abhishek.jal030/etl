import { Injectable } from '@angular/core';
import { Config } from 'src/environments/config';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Connection } from '../model/connection';


@Injectable({
    providedIn: 'root'
})
export class ConnectionService {
    baseUrl = Config.getEnvironmentVariable('apiUrl');
    connectionEndPoint = this.baseUrl + 'connection';
    connectionsEndPoint = this.baseUrl + 'connections/';

    testConnectionEndPoint = this.baseUrl + 'test';
    constructor(private httpClient: HttpClient) { }

    getAllConnection(connectionFilter: any, startRecord: number, recordCount: number, sortableColumn: string, sortingType: string) {
        // const httpOptions = {
        //     headers: new HttpHeaders({
        //         'Content-Type': 'application/json',
        //         'Authorization': 'Basic YWRtaW46YWRtaW4'
        //     })
        // };
        return this.httpClient.post(this.connectionEndPoint + `?startRecord=${startRecord}&recordCount=${recordCount}&sortableColumn=${sortableColumn}&sortingType=${sortingType}`,
             connectionFilter); //
    }

    getConnectionDetail(connectionId: string) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this.httpClient.get(this.connectionsEndPoint + `/${connectionId}`, httpOptions);
    }

    delete(connectionIds: any) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            }),
            body: [connectionIds]
        };
        return this.httpClient.delete(this.connectionsEndPoint, httpOptions);
    }

    add(data: Connection) {
        return this.httpClient.post(this.connectionsEndPoint, data);
    }

    update(connectionId: string, data: Connection) {
        return this.httpClient.put(this.connectionsEndPoint + `${connectionId}`, data);
    }

    edit(connectionId: string, data: Connection) {
        return this.httpClient.put(this.connectionEndPoint + connectionId, data);
    }

    testConnection(connection: any) {
        return this.httpClient.post(this.testConnectionEndPoint, connection);
    }
}
