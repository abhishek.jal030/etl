import { Component, OnInit, HostListener, ElementRef } from '@angular/core';
import { ConnectionService } from './connection-service';
import { ConnectionFilter } from '../model/connection';
import { DataStateChangeEvent, GridDataResult, PageChangeEvent, FilterService } from '@progress/kendo-angular-grid';
import { State, SortDescriptor, CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { DataService } from '../shared/data.service';
import { Router,ActivatedRoute } from '@angular/router';
import * as introJs from 'intro.js/intro.js';

export class Connection {
  connectionId: string;
  type: string;
  name: string;
  aliasName: string;
  createdDate: string;
  updatedDate: string;
  createdBy: string;
  updatedBy: string;
  toggle: boolean;
  isDisabled: boolean;
  constructor(connection?) {
    connection = connection || {};
    this.connectionId = connection.connectionId || '';
    this.type = connection.type || '';
    this.name = connection.name || '';
    this.aliasName = connection.aliasName || '';
    this.createdDate = connection.createdDate || '';
    this.updatedDate = connection.updatedDate || '';
    this.createdBy = connection.createdBy || '';
    this.updatedBy = connection.updatedBy || '';
    this.toggle = connection.toggle || false;
    this.isDisabled = connection.isDisabled || true;
  }
}

@Component({
  selector: 'app-connections',
  templateUrl: './connections.component.html',
  styleUrls: ['./connections.component.scss']
})
export class ConnectionsComponent implements OnInit {

  public state: State = {
    skip: 0,
    take: 10
  };
  public connectionId: Array<string> = new Array<string>();
  deleteConnection: Array<string> = Array<string>();
  public data: Array<{ text: string, value: number }>;
  public selectedValue = 0;
  connectionPageSize : number;
  public selectedConnector = '';
  public connectionName = '';
  successMessage: any;
  public aliasName = '';
  public connectionPageSizes = true;
  public previousNext = true;
  public info = true;
  public buttonCount = 5;
  public type: 'numeric' | 'input' = 'input';
  public connectionsGridDataResult: GridDataResult;
  public connectionFilter: ConnectionFilter;
  isConnectionVisible: boolean;
  isAlertModelVisible: boolean;
  connections: Array<Connection>;
  connectionGridLoading: boolean;
  selectedConnection: Connection;
  introJS = introJs();
  constructor(
    private connectionService: ConnectionService,
    private router: Router,
    private route: ActivatedRoute,
    private dataService: DataService,
    private eRef: ElementRef) {
    this.isConnectionVisible = false;
    this.isAlertModelVisible = false;
    this.connectionFilter = new ConnectionFilter();
    this.connectionFilter.type = '';
    this.connectionFilter.name = '';
    this.connectionFilter.aliasName = '';
    this.connectionFilter.sortingType = 'desc';
    this.connectionFilter.sortableColumn = 'createdDate';
    this.connectionFilter.startRecord = 0;
    this.connectionFilter.recordCount = 10;
    this.connectionPageSize = 10;
    this.connections = Array<Connection>();
    this.connectionGridLoading = false;
    this.introJS.setOptions({
      steps: [
        {
          element: '#connectionBtn',
          intro: 'Press New Connection Button'
        },
        
      ]
    });
  }

  ngOnInit() {
    this.getAllConnection();
    if (this.route.snapshot.queryParams.action === 'intro') {
      this.introJS.start();
      this.introJS.onchange((targetElement) => {
      });
      this.introJS.oncomplete(() => {
        this.router.navigate(['/connections'], { queryParams: {} });
      });
      this.introJS.onexit(() => {
        this.router.navigate(['/connections'], { queryParams: {} });
      });
    }
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
  }
  public onInputOppIdQF(e, filterService) {
    filterService.filter({
      filters: [{
        field: 'Connection Name',
        operator: 'contains',
        value: e.srcElement.value
      }],
      logic: 'and'
    });
  }

  getAllConnection() {
    this.connectionGridLoading = true;
    this.connectionService.getAllConnection(this.connectionFilter, this.connectionFilter.startRecord,
      this.connectionFilter.recordCount, this.connectionFilter.sortableColumn,
      this.connectionFilter.sortingType).subscribe(data => {
        this.connectionGridLoading = false;

        this.connections = Array<Connection>();
        this.connectionsGridDataResult = {
          data: this.connections,
          total: 0
        };
        if (JSON.parse(JSON.stringify(data)).results[0].connectionResponseList !== null) {
          JSON.parse(JSON.stringify(data)).results[0].connectionResponseList.forEach(element => {
            this.connections.push(new Connection(element));
          });
          this.connectionsGridDataResult = {
            data: this.connections,
            total: JSON.parse(JSON.stringify(data)).results[0].totalRecordCount
          };
        }

      });
  }
  allClear() {
    this.connectionFilter = new ConnectionFilter();
    this.getAllConnection();
  }
  public pageChangeConnection(event: PageChangeEvent): void {
    this.connectionFilter.startRecord = event.skip;
    this.getAllConnection();
  }

  public sortChangeConnection(sort: SortDescriptor[]): void {
    this.connectionFilter.sortingType = sort[0]['dir'] === undefined ? 'asc' : this.connectionFilter.sortingType;
    this.connectionFilter.sortableColumn = sort[0]['field'];
    this.connectionFilter.startRecord = 0;
    this.getAllConnection();
  }

  public filterChange(filter: CompositeFilterDescriptor): void {
    this.connectionFilter.startRecord = 0;
    this.connectionFilter.recordCount = 10;
    this.state.skip = 0;
    this.state.take = 10;
    if (filter.filters.length === 0) {
      // this.connectionFilter = new ConnectionFilter();
      this.connectionFilter.type = "";
      this.connectionFilter = new ConnectionFilter();
    }
    this.getAllConnection();
  }

  public filterChangeConnection(e, filterService, filterOn) {
    this.connectionFilter[filterOn] = e.srcElement.value;
    filterService.filter({
      filters: [{
        field: filterOn,
        operator: 'contains',
        value: e.srcElement.value
      }], logic: 'and'
    });
  }

  public onToggle(account): void {
    this.connectionsGridDataResult.data.map(n => n.toggle = false);
    account.toggle = !account.toggle;
  }

  newConnection(): void {
    this.dataService.setConnectionDetail(new Connection());
    this.dataService.setConnector('0');
    this.isConnectionVisible = !this.isConnectionVisible;
    if (this.route.snapshot.queryParams.action === 'intro') {
      this.introJS.goToStepNumber(1).start();
    }
  }

  editConnection(connection: Connection) {
    this.connectionService.getConnectionDetail(connection.connectionId).subscribe(data => {
      console.log(data);
      this.dataService.setConnectionDetail(JSON.parse(JSON.stringify(data)).results[0]);
      this.isConnectionVisible = !this.isConnectionVisible;
      // this.selectedConnection;
    }, error => {

    });
  }

  removeConnection(connection: Connection) {
    this.connectionService.delete(connection.connectionId).subscribe(data => {
      this.getAllConnection();
    }, error => {

    });
  }

  public cellClickHandler({ sender, rowIndex, columnIndex, dataItem, isEdited }) {
    if (columnIndex > 0) {
      this.editConnection(dataItem);
    }
  }

  @HostListener('document:click', ['$event'])
  public documentClick(event: any): void {
    if (event.srcElement.alt !== undefined && event.srcElement.alt === 'vertical-dot') {
    } else if (this.eRef.nativeElement.contains(event.target)) {
      this.connectionsGridDataResult.data.map(n => n.toggle = false);
    } else {

      this.connectionsGridDataResult.data.map(n => n.toggle = false);
    }
  }
  ngOnDestroy() {
    
    this.introJS.exit();
  }

}

