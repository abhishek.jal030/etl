import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Connector } from 'src/app/model/connector';
import { DataService } from 'src/app/shared/data.service';
import { Connection } from '../connections.component';
import { Router, ActivatedRoute } from '@angular/router';
import * as introJs from 'intro.js/intro.js';

@Component({
  selector: 'app-add-connection',
  templateUrl: './add-connection.component.html',
  styleUrls: ['./add-connection.component.scss']
})
export class AddConnectionComponent implements OnInit, OnDestroy {
  connectionFields: Connector;
  introJS = introJs();
  selectedConnector = 0;
  connectorType: string;

  public listItems: Array<{ text: string, value: number, img: string }> = [

    { text: 'Select Connector', value: 0, img: '' },
    { text: 'ODATA', value: 1, img: 'https://www.accely.com/wp-content/uploads/2018/09/1-88.png' },
    { text: 'ORACLE', value: 2, img: 'https://www.datocms-assets.com/2885/1522106007-oracle.png' },
    { text: 'MYSQL', value: 3, img: 'https://mc.qcloudimg.com/static/img/2742c21902443c72d3b0e198b7c49efb/MySQL.png' },
    { text: 'API', value: 4, img: 'https://mc.qcloudimg.com/static/img/2742c21902443c72d3b0e198b7c49efb/MySQL.png' },
    { text: 'DATAHUG', value: 5, img: 'https://mc.qcloudimg.com/static/img/2742c21902443c72d3b0e198b7c49efb/MySQL.png' },

  ];
  public connectors: Array<{ text: string, value: number }>;
  @Output() public closeModel = new EventEmitter();
  connection: any;
  constructor(
    private dataService: DataService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.connectionFields = new Connector();
    this.connectors = this.listItems.slice();
    this.introJS.setOptions({
      steps: [
        {
          element: '#selectConnector',
          intro: 'Select Connector '
        },
      ]
    });
  }




  ngOnInit() {
    this.dataService.getConnectionDetail().subscribe(connection => {
      if (connection.connectionId !== '') {
        this.connection = connection;
        this.selectedConnector = this.listItems.filter(n => n.text.toLowerCase() === this.connection.type.toLowerCase())[0].value;
      }
    });
    if (this.route.snapshot.queryParams.action === 'intro') {
      this.introJS.start();
      this.introJS.onchange((targetElement) => {
      });
      this.introJS.oncomplete(() => {
        this.router.navigate(['/connections'], { queryParams: {} });
      });
      this.introJS.onexit(() => {
        this.router.navigate(['/connections'], { queryParams: {} });
      });
    }
  }

  filterConnector(value) {

    this.connectors = this.listItems.filter((s) => s.text.toLowerCase().indexOf(value.toLowerCase()) !== -1);
  }

  saveConnection() {
    console.log(this.connectionFields);
  }

  onSaveConnection(event) {
    console.log(event);
  }

  public closeIt(id: string) {
    this.closeModel.emit();
    this.selectedConnector = 0;
    this.connectionFields = new Connector();
  }

  selectionChangeConnector(event): void {
    const connectorType = event.text;
    // localStorage.setItem('connectorType', connectorType);
    this.connectorType = connectorType;
    this.introJS.exit();
    this.dataService.setConnector(event.text);
    this.dataService.setAction('connectorVal');
  }
  ngOnDestroy() {
    this.introJS.exit();
  }
}
