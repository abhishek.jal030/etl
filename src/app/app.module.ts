import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './app.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { OrderModule } from 'ngx-order-pipe';
import { ToastrModule } from 'ngx-toastr';
import { ModalModule } from 'ngx-modal';
import {  UploadModule } from '@progress/kendo-angular-upload';
import { TreeviewModule } from 'ngx-treeview';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ProgressBarModule } from 'angular-progress-bar';
import { TooltipModule } from '@progress/kendo-angular-tooltip';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { GridModule } from '@progress/kendo-angular-grid';
import { ToolBarModule } from '@progress/kendo-angular-toolbar';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { RippleModule } from '@progress/kendo-angular-ripple';
import { DialogsModule } from '@progress/kendo-angular-dialog';
import { SettingModule } from './settings/setting-module';
import { UserService } from './settings/users/users.service';
import { RolesService } from './settings/roles/roles.service';
import { PopupModule } from '@progress/kendo-angular-popup';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { WindowModule } from '@progress/kendo-angular-dialog';
import { AvatarModule } from 'ngx-avatar';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatCardModule } from '@angular/material';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { CronEditorModule } from 'cron-editor';
import { ODataModule, ODataService } from 'odata-v4-ng';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';

import { SessionService } from './app.session-service';
import { FeatureService } from './features/features.service';
import { ExportService } from './shared/export.service';

import { CookieService } from 'ngx-cookie-service';
import { AdminAuthGuardService } from './app.admin-auth-guard.service';
import { UserAuthGuardService } from './app.user-auth-guard.service';
import { CommentService } from './core-component/comment/comment-service';
import { CommonService } from './common-service';
import { ActivityService } from './shared/activity.service';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { CKEditorModule } from 'ckeditor4-angular';
import { CommentComponent } from './core-component/comment/comment.component';
import { JobsComponent } from './jobs/jobs.component';
import { ConnectionsComponent } from './connections/connections.component';
import { AddConnectionComponent } from './connections/add-connection/add-connection.component';
import { ODataConnectorComponent } from './connector/odata-connector/odata-connector.component';
import { RdbmsConnectorComponent } from './connector/rdbms-connector/rdbms-connector.component';
import { NewJobComponent } from './jobs/new-job/new-job.component';
import { LogComponent } from './jobs/log/log.component';
import { LogStatusComponent } from './jobs/log-status/log-status.component';
import { BulkLoadComponent } from './bulk-load/bulk-load.component';
import { BulkService } from './bulk-load/bulk.service';
import { TourGuideComponent } from './shared/tour-guide/tour-guide.component';
import { SearchPipe } from './pipe/search.pipe';
import { ApiConnectorComponent } from './connector/api-connector/api-connector.component';
import { DragAndDropModule } from 'angular-draggable-droppable';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './auth-guard.service';
import { SharedService } from './shared/shared.service';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    CommentComponent,
    JobsComponent,
    ConnectionsComponent,
    AddConnectionComponent,
    RdbmsConnectorComponent,
    ODataConnectorComponent,
    NewJobComponent,
    LogComponent,
    LogStatusComponent,
    BulkLoadComponent,
    TourGuideComponent,
    SearchPipe,
    ApiConnectorComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    SettingModule,
    ModalModule,
    TreeviewModule.forRoot(),
    FormsModule,
    OrderModule,
    BrowserAnimationsModule,
    NgxChartsModule,
    
    ChartsModule,
    ProgressBarModule,
    DeviceDetectorModule.forRoot(),
    RouterModule.forRoot(routes),
    TooltipModule,
    ToastrModule.forRoot(),
    GridModule,
    ToolBarModule,
    ButtonsModule,
    RippleModule,
    BsDatepickerModule.forRoot(),
    LayoutModule,
    DropDownsModule,
    RippleModule,
    DialogsModule,

    DateInputsModule,
    PopupModule, // ToastrModule added
    AvatarModule,
    WindowModule,
    DragDropModule,
    MatCardModule,
    InputsModule,
    CKEditorModule,
    ODataModule,
    CronEditorModule,
    NgxDaterangepickerMd.forRoot(),
    ToastrModule.forRoot(),
    DragAndDropModule,
    UploadModule
  ],
  providers: [
    AdminAuthGuardService,
    UserAuthGuardService,
    SessionService,
    UserService,
    RolesService,
    CookieService,
    ExportService,
    DatePipe,
    SharedService,
    CommonService,
    ActivityService,
    CommentService,
    ODataService,
    FeatureService,
    BulkService,
    
    TourGuideComponent,
    AuthGuardService
  ],
  entryComponents: [AddConnectionComponent, RdbmsConnectorComponent, ODataConnectorComponent],
  exports: [
    AddConnectionComponent, RdbmsConnectorComponent, ODataConnectorComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
