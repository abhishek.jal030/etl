
import { Injectable } from "@angular/core";
import { CookieService } from 'ngx-cookie-service';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Config } from "src/environments/config";
import { SessionService } from "./app.session-service";


@Injectable()
export class CommonService {
    baseUrl = Config.getEnvironmentVariable('endPoint');
    getGridColumnsEndpoint = this.baseUrl + environment.authApiUrl + 'grid';
    filterRequestEndpoint = this.baseUrl + environment.authApiUrl + 'secured/accountChange/request/filter';
    sessionData: any;
    LA_SessionData: any;

    constructor(private _http: HttpClient, private _SessionService: SessionService) { }

    GetGridColumns(gridId) {
        this.sessionData = JSON.parse(this._SessionService.getCookie("LD_USER_DETAIL"));
        let uid = this.sessionData.uid;
        if (this._SessionService.getCookie("LA_USER_DETAIL") != "") {
            this.LA_SessionData = JSON.parse(this._SessionService.getCookie("LA_USER_DETAIL"));
            uid = this.LA_SessionData.userId;
        }
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this._http.get(this.getGridColumnsEndpoint + `/${gridId}/columns/${uid}`, httpOptions);
    }

    SaveGridColumns(gridId, columnsSettings) {
        this.sessionData = JSON.parse(this._SessionService.getCookie("LD_USER_DETAIL"));
        let uid = this.sessionData.uid;
        if (this._SessionService.getCookie("LA_USER_DETAIL") != "") {
            this.LA_SessionData = JSON.parse(this._SessionService.getCookie("LA_USER_DETAIL"));
            uid = this.LA_SessionData.userId;
        }
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        var req = {
            columnProperty: columnsSettings.toString()
        }
        return this._http.post(this.getGridColumnsEndpoint + `/${gridId}/columns/${uid}`, req, httpOptions);
    }

    filterRequest(requestId: string, status: string, somStatus: string, requestType: string, startRecord: number, recordCount: number, requestor?: string) {
        this.sessionData = JSON.parse(this._SessionService.getCookie("LD_USER_DETAIL"));
        let uid = this.sessionData.uid;
        if (this._SessionService.getCookie("LA_USER_DETAIL") != "") {
            this.LA_SessionData = JSON.parse(this._SessionService.getCookie("LA_USER_DETAIL"));
            uid = this.LA_SessionData.userId;
        }
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Basic YWRtaW46YWRtaW4'
            })
        };
        return this._http.get(this.filterRequestEndpoint + `/${uid}?requestId=${requestId == null ? "" : requestId}&requestor=${requestor}&status=${(status == null || status == "0") ? "" : status}&somStatus=${(somStatus == null || somStatus == "0") ? "" : somStatus}&&requestType=${(requestType == null || requestType == "0") ? "" : requestType}&startRecord=${startRecord}&recordCount=${recordCount}`, httpOptions)
    }

}