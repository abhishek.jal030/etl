import { AccountFilterModel } from "./account";

export class OwnerAccount {
    accountId: string = "";
    accountName: string = "";
    currentOwner: string = "";
    currentOwnerName: string = "";
    newOwner: string = "";
    newOwnerName: string = "";
    checked: boolean = false;
    filter: string = "";
    newValue: string = "";
    newValueText: string = "";
}

export class AccountRequest {
    accountOwnerGridRequestArray: Array<OwnerAccount> = new Array<OwnerAccount>();

    accountFilterRquest: AccountFilterModel = new AccountFilterModel();

}


export class AccountOwnershipChangeRequest {
    accountOwnerGridRequestArray: Array<OwnerAccount> = new Array<OwnerAccount>();
    accountFilterRquestArray: Array<AccountFilterModel> = new Array<AccountFilterModel>();
    public newOwner: string;
    public newOwnerName: string;

    public newValue: string;
    public newValueText: string;

}