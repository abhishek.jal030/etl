export class Collection {
    collectionName: string;
    collectionNameToggle: boolean;
    collectionMetadataResponseList: Array<Metadata>;
    // filterable: string;
    toggleChildObject: boolean;
    isNameError: boolean;
    toggleName: boolean;
    constructor(collection?) {
        collection = collection || {};
        this.collectionName = collection.collectionName || '';
        // this.filterable = collection.type || '';
        this.collectionNameToggle = collection.collectionNameToggle || false;
        this.isNameError = collection.isNameError || false;
        this.collectionMetadataResponseList = collection.collectionMetadataResponseList || Array<Metadata>();
        this.toggleChildObject = this.toggleChildObject || false;
        this.toggleName = this.toggleName || false;
    }
}

export class Metadata {
    creatable: string;
    filterable: string;
    maxLength: string;
    name: string;
    nullable: string;
    type: string;
    updatable: string;
    toggleName: boolean;
    toggleType: boolean;
    isNameError: boolean;
    mapping: string;
    isNewColumn: boolean;
    isDeletedColumn: boolean;
    constructor(metadata?, mappingObject?) {
        metadata = metadata || {};
        this.creatable = metadata.creatable || '';
        this.filterable = metadata.filterable || 'false';
        this.maxLength = metadata.maxLength || '';
        this.name = metadata.name || '';
        this.nullable = metadata.nullable || '';
        this.type = metadata.type || '';
        this.updatable = metadata.updatable || '';
        this.toggleName = metadata.toggleName || false;
        this.toggleType = metadata.toggleType || false;
        if (localStorage.getItem('destinationConnectionValue') === 'destinationConnectionValue') {
            this.isNameError = metadata.name.length > 30 ? true : false;
        } else {
            this.isNameError = false;
        }
        this.isNewColumn = metadata.isNewColumn || false;
        this.isDeletedColumn = metadata.isDeletedColumn || false;
        this.mapping = metadata.mapping || '';
        if (mappingObject !== undefined && mappingObject !== null) {
            const keys = Object.keys(mappingObject);
            keys.forEach(key => {
                if (key.toUpperCase() === metadata.name.toUpperCase()) {
                    this.mapping = mappingObject[key];
                }
            });
        }
    }
}

export class Difference {
    fieldName: string;
    status: string;
}
