export class Role {
    id: number;
    public roleId: number;
    public role: string;
    createdBy: string;
    updatedBy: string;
}

export class RoleId {
    roleId: Array<number> = Array<number>();
    createdBy: string;
    updatedBy: string;

}