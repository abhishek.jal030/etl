export class DataFilter {
    comparisonOperator: string;
    field: string;
    logicalOperator: string;
    value: string;
    dataType: string;
    constructor(dataFilter?) {
        dataFilter = dataFilter || {};
        this.comparisonOperator = dataFilter.comparisonOperator || '';
        this.field = dataFilter.field || '';
        this.logicalOperator = dataFilter.logicalOperator || '';
        this.value = dataFilter.value || '';
        this.dataType = dataFilter.dataType || '';
    }
}

