
export class Round {
    sequence: number;
    leadAssignedPartner: string;
    rules: Array<Rule> = new Array<Rule>();
}

export class Rule {
    name: string;
    partners: Array<Partner> = new Array<Partner>();
}

export class Partner {
    name: string;
}

export class RuleFlow {
    sequence: number;
    rules: Array<string> = new Array<string>();
}
