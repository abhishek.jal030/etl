export class Log {
    jobStatusId: string;
    jobId: string;
    totalRecordCount: number;
    totalRecordProcessed: number;
    status: string;
}
