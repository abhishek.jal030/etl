export class Connection {
    connectionId: string;
    type: string;
    name: string;
    aliasName: string;
    createdDate: string;
    updatedDate: string;
    createdBy: string;
    updatedBy: string;
    connectionConfigRequestList = new Array<ConnectionConfigRequestList>();
}
export class ConnectionConfigRequestList {
    key: string;
    value: string;
}

export class ConnectionFilter {
    type: string;
    name: string;
    aliasName: string;
    startRecord: number;
    recordCount: number;
    sortableColumn: string;
    sortingType: string;
    constructor(connectionFilter?) {
        connectionFilter = connectionFilter || {};
        this.type = connectionFilter.connectionName || '';
        this.name = connectionFilter.aliasName || '';
        this.aliasName = connectionFilter.serviceName || '';
        this.startRecord = connectionFilter.host || 0;
        this.recordCount = connectionFilter.port || 10;
        this.sortableColumn = connectionFilter.sortableColumn || 'createdDate';
        this.sortingType = connectionFilter.password || 'desc';
    }
}
