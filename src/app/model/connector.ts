export class Connector {
    connectionName: string;
    aliasName: string;
    serviceName: string;
    host: string;
    port: string;
    userName: string;
    password: string;
    url: string;
    schemaName: string;

    constructor(connector?) {
        connector = connector || {};
        this.connectionName = connector.connectionName || '';
        this.aliasName = connector.aliasName || '';
        this.serviceName = connector.serviceName || '';
        this.host = connector.host || '';
        this.port = connector.port || '';
        this.userName = connector.userName || '';
        this.password = connector.password || '';
        this.url = connector.url || '';
        this.schemaName = connector.schemaName || ''
    }
}
