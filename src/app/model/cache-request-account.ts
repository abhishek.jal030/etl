export class CacheRequestAccount {

    private _objectId: string = "";
    get objectId(): string {
        return this._objectId;
    }
    set objectId(objectId: string) {
        this._objectId = objectId;
    }

    private _newOwner: string = "";
    get newOwner(): string {
        return this._newOwner;
    }
    set newOwner(newOwner: string) {
        this._newOwner = newOwner;
    }

    private _newOwnerName: string = "";
    get newOwnerName(): string {
        return this._newOwnerName;
    }
    set newOwnerName(newOwnerName: string) {
        this._newOwnerName = newOwnerName;
    }

    private _checked: boolean = false;
    get checked(): boolean {
        return this._checked;
    }
    set checked(checked: boolean) {
        this._checked = checked;
    }

}