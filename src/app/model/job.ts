export class Job {
    jobId: string;
    jobName: string;
    sourceConnectionName: string;
    destinationConnectionName: string;
    sourceCollectionType: string;
    status: string;
    state: string;
    lastRun: string;
    created: string;
    updated: string;
    createdBy: string;
    updatedBy: string;
    toggle: boolean;
    toggleInfo = false;
    viewJobDifferenceDetailsResponseList: Array<JobDifferenceDetails>;
}

export class JobDifferenceDetails {
    fieldName: string;
    status: string;
}
export class JobFilter {
    jobName: string;
    startRecord: number;
    recordCount: number;
    sortableColumn: string;
    sortingType: string;
    name: string;
    status: string;
    incrementalParameter: string;
    constructor(jobFilter?) {
        jobFilter = jobFilter || {};
        this.jobName = jobFilter.jobName || '';
        this.startRecord = jobFilter.host || 0;
        this.recordCount = jobFilter.port || 10;
        this.sortableColumn = jobFilter.sortableColumn || 'created';
        this.sortingType = jobFilter.password || 'desc';
    }
}

export class JobRequest {
    createdBy: string;
    destinationCollection: string;
    destinationConnection: string;
    destinationConnectionName: string;
    sourceCollectionType: string;
    
    jobId: string;
    jobFilter: string;
    jobName: string;
    mapping: string;
    sourceCollection: string;
    sourceConnection: string;
    sourceConnectionName: string;
    updatedBy: string;
    incrementalParameter: string;
    incrementalParameterValue: string;
    constructor(jobRequest?) {
        jobRequest = jobRequest || {};
        this.createdBy = jobRequest.createdBy || '';
        this.destinationCollection = jobRequest.destinationCollection || '';
        this.destinationConnection = jobRequest.destinationConnection || '';
        this.destinationConnectionName = jobRequest.destinationConnectionName || '';
        this.jobId = jobRequest.jobId || '';
        this.sourceCollectionType = jobRequest.sourceCollectionType || '';
        this.jobFilter = jobRequest.jobFilter || '';
        this.jobName = jobRequest.jobName || '';
        this.mapping = jobRequest.mapping || '';
        this.sourceCollection = jobRequest.sourceCollection || '';
        this.sourceConnection = jobRequest.sourceConnection || '';
        this.sourceConnectionName = jobRequest.sourceConnectionName || '';
        this.updatedBy = jobRequest.updatedBy || '';
        this.incrementalParameter = jobRequest.incrementalParameter || '';
        this.incrementalParameterValue = jobRequest.incrementalParameterValue || '';
    }
}
