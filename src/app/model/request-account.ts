export class RequestAccount {
    
    
    private _accountId: string = "";
    get accountId(): string {
        return this._accountId;
    }
    set accountId(accountId: string) {
        this._accountId = accountId;
    }
    private _accountName: string = "";
    get accountName(): string {
        return this._accountName;
    }
    set accountName(accountName: string) {
        this._accountName = accountName;
    }

    private _currentAccount: string = "";
    get currentAccount(): string {
        return this._currentAccount;
    }
    set currentAccount(currentAccount: string) {
        this._currentAccount = currentAccount;
    }

    private _currentOwner: string = "";
    get currentOwner(): string {
        return this._currentOwner;
    }
    set currentOwner(currentOwner: string) {
        this._currentOwner = currentOwner;
    }

    private _dpId: string = "";
    get dpId(): string {
        return this._dpId;
    }
    set dpId(dpId: string) {
        this._dpId = dpId;
    }

    private _dpName: string = "";
    get dpName(): string {
        return this._dpName;
    }
    set dpName(dpName: string) {
        this._dpName = dpName;
    }

    private _employee: string = "";
    get employee(): string {
        return this._employee;
    }
    set employee(employee: string) {
        this._employee = employee;
    }

    private _nagpId: string = "";
    get nagpId(): string {
        return this._nagpId;
    }
    set nagpId(nagpId: string) {
        this._nagpId = nagpId;
    }
    
    private _nagpName: string = "";
    get nagpName(): string {
        return this._nagpName;
    }
    set nagpName(nagpName: string) {
        this._nagpName = nagpName;
    }
    
    private _newOwner: string = "";
    get newOwner(): string {
        return this._newOwner;
    }
    set newOwner(newOwner: string) {
        this._newOwner = newOwner;
    } 

    private _newOwnerName: string = "";
    get newOwnerName(): string {
        return this._newOwnerName;
    }
    set newOwnerName(newOwnerName: string) {
        this._newOwnerName = newOwnerName;
    } 

    private _objectId: string = "";
    get objectId(): string {
        return this._objectId;
    }
    set objectId(objectId: string) {
        this._objectId = objectId;
    } 

    private _parentObjectId: string = "";
    get parentObjectId(): string {
        return this._parentObjectId;
    }
    set parentObjectId(parentObjectId: string) {
        this._parentObjectId = parentObjectId;
    }
    
    private _recordNumber: string = "";
    get recordNumber(): string {
        return this._recordNumber;
    }
    set recordNumber(recordNumber: string) {
        this._recordNumber = recordNumber;
    }

    private _checked: boolean = false;
    get checked(): boolean {
        return this._checked;
    }
    set checked(checked: boolean) {
        this._checked = checked;
    }

    
    private _ownerName: string = "";
    get ownerName(): string {
        return this._ownerName;
    }
    set ownerName(ownerName: string) {
        this._ownerName = ownerName;
    }

    private _toggle: boolean = false;
    get toggle(): boolean {
        return this._toggle;
    }
    set toggle(toggle: boolean) {
        this._toggle = toggle;
    }
    
    private _salesCreditException: string = "";
    get salesCreditException(): string {
        return this._salesCreditException;
    }
    set salesCreditException(salesCreditException: string) {
        this._salesCreditException = salesCreditException;
    }

    private _currentValueText: string = "";
    get currentValueText(): string {
        return this._currentValueText;
    }
    set currentValueText(currentValueText: string) {
        this._currentValueText = currentValueText;
    }

    private _newValueText: string = "";
    get newValueText(): string {
        return this._newValueText;
    }
    set newValueText(newValueText: string) {
        this._newValueText = newValueText;
    }
}