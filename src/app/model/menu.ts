export class Menu {
    id:  number;
    name: string;
    url:  string;
    icon: string;
    featureId: number;
    createdBY: string;
    sequenceOrder: string;
    updatedBY: string;
    parentID: number;
}

export class UIPermission {
    id:  number;
    name: string;
    url:  string;
    featureId: number;
    icon: string;
    createdBY: string;
    sequenceOrder: string;
    updatedBY: string;
    parentId: number;
    submenu: Array<UIPermission>;
}