export class Feature
{
    linkID:string;
    id: number;
    featureId : number;
    featureName : string;
    createdBy: string;
    updatedBy: string;
}


export class FeatureId {
    featureId: Array<number> = Array<number>();
    createdBy: string;
}