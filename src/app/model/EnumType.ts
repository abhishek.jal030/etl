export enum EnumType {
    SUPER_ADMIN = "Admin",
    USER = "User",
    SOM = "SOM",
    L2 = "L2",
    DATA_HUG="DATAHUG"
    
}

export enum EnumPromotion {
    DRAFT = 1,
    SUBMITTED = 2,
    APPROVED = 3,
    REJECTED = 4,
    APPEALED = 5,
    CLONE = 12

}