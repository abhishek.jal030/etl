export class AddJob {
  createdBy: string;
  destinationConnection: string;
  jobName: string;
  sourceConnection: string;
  incrementalParameter: string;
  status: string;
  updatedBy: string;
  mapping: string;
}
