export class Employee {
    checked: boolean = false;
    internalID: string = "";
    salesRepId: string = "";
    ownerName: string = "";
    planType: string = "";
    country: string = "";
    geo: string = "";
    jobFunction: string = "";
}