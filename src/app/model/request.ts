import { Dropdown } from "./dropdown";

export class Request {
    objectId: string = null;
    zeffectivedate: string;
    zeffectivedatePicker: Date;
    zreasonforchange: string;
    zreasonforchangeD: Dropdown;
    zcompletedby: string;
    zrequestId: string;
    zsomstatus: string;
    zsomstatusD: Dropdown;
    zstatusD: Dropdown;
    zrequestor: string;
    zstatus: string;
    checked: boolean = false;
    processing_status: string = "";

    accountRequestType: Dropdown;
    requestType: number;

    dropDownDefaultText: string;


}