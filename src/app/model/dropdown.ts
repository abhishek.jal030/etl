export class Dropdown {
    code: string;
    description: string;
}

export class DropdownGroupItem {
    text: string;
    value: string;
    subcategory: string;
}
