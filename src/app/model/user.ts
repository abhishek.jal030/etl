export class User {
    id: number;
    userId: string;
    firstName: string;
    lastName: string;
    emailId: string;
    isInternal: string;
    crmCompanyId: number;
    createdBy: string;
    updatedBy: string;
    isExternal: boolean = false;
}
